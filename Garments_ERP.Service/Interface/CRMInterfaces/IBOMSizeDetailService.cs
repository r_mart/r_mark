﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IBOMSizeDetailService
    {
        List<M_BOMSizeDetailMasterEntity> get();
        decimal Insert(M_BOMSizeDetailMasterEntity entity);
        bool Update(M_BOMSizeDetailMasterEntity entity);
        M_BOMSizeDetailMasterEntity GetById(long id);
        bool Delete(long id);
    }
}
