﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
   public interface IQuotationRawItemDetailService
    {
       long Insert(CRM_QuotationRawItemDetailEntity record);
       List<CRM_QuotationRawItemDetailEntity> getbyquotationid(long id);
       bool deleterawitem(long id);
       bool Update(CRM_QuotationRawItemDetailEntity entity);
    }
}
