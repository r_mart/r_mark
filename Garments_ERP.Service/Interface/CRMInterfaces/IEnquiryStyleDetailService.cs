﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IEnquiryStyleDetailService
    {
      long Insert(CRM_EnquiryStyleDetailEntity entity);
      CRM_EnquiryStyleDetailEntity getbyenquiryid(long id);
      bool Update(CRM_EnquiryStyleDetailEntity entity);
      object EditAttributeValue(string MyJson, int type, XmlDocument doc);
    }
}
