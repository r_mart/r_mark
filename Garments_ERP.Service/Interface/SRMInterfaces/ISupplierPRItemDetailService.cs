﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ISupplierPRItemDetailService
    {
        bool Update(SRM_PurchaseRequestItemDetailEntity entity);
        List<SRM_PurchaseRequestItemDetailEntity> getbyid(long id);
        List<SRM_PurchaseRequestItemDetailEntity> get();
        bool Delete(long id);
        long Insert(SRM_PurchaseRequestItemDetailEntity entity);
    }
}
