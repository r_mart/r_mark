﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface ISupplierQuotationService
    {
        string getnextQuotatioNo();
        List<CRM_QuotationMasterEntity> get();
        long Insert(SRM_QuotationMasterEntity entity);
        SRM_QuotationMasterEntity getbyid(long id);
        bool Update(SRM_QuotationMasterEntity entity);
        bool Delete(long id);
        List<SRM_QuotationMasterEntity> get(DateTime from, DateTime to);
        List<SRM_QuotationMasterEntity> getquotationnobysupplierid(long supplierid);
    }
}
