﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ISupplierBankService
    {
        List<M_SupplierBankDetailsEntity> get();
        M_SupplierBankDetailsEntity GetById(long id);
        long Insert(M_SupplierBankDetailsEntity entity);
        bool Update(M_SupplierBankDetailsEntity entity);
    }
}
