﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IMaterialIssueService
    {
        string getnextmino();
        List<SP_GetDetailsForMaterialIssueEntity> GetWOProcessMaterialDetails(long woId);
        bool Update(MFG_MaterialIssueMasterEntity entity);
        MFG_MaterialIssueMasterEntity getById(long id);
        List<MFG_MaterialIssueMasterEntity> get();
        bool Delete(long id,int Uid);
        long Insert(MFG_MaterialIssueMasterEntity entity);
        bool deleterawitem(long id);
        MFG_MaterialIssueItemDetailsEntity getbyitemid(long itemid, long MIid);
        MFG_WorkOrderDetailsForProductionPlan GetWorkOrderDetails(long workOrderID, long planID, long MIid);
        List<MFG_MaterialIssueMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<MFG_MaterialIssueMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);


    }
}
