﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
   public interface IAttributeValueService
    {
        int Insert(M_Attribute_Value_MasterEntity entity);
        List<M_Attribute_Value_MasterEntity> get();
        M_Attribute_Value_MasterEntity GetById(int id);
        bool Update(M_Attribute_Value_MasterEntity entity);
        bool Delete(int id);
        object getAttributeValue(string MyJson,int type);
    }
}
