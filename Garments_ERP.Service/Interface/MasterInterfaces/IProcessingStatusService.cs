﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
    public interface IProcessingStatusService
    {
        List<M_ProcessingStatusEntity> get();
        long Insert(M_ProcessingStatusEntity entity);
        bool Update(M_ProcessingStatusEntity entity);
        bool Delete(int id);
        bool CheckProcessingStatusExistance(string ProcessingStatus, int? Id);
    }
}
