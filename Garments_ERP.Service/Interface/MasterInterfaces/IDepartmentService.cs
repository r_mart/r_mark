﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface IDepartmentService
    {
       List<M_DepartmentMasterEntity> get();
       int Insert(M_DepartmentMasterEntity entity);
       bool CheckDepartmentNameExistance(string DepartmentName, int? Id);
       bool Update(M_DepartmentMasterEntity entity);
       bool Delete(int id);
    }
}
