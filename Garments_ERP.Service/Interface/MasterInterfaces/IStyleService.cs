﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
   public interface IStyleService
    {
       List<M_StyleMasterEntity> get();
       long Insert(M_StyleMasterEntity entity);
       bool Update(M_StyleMasterEntity entity);
       bool Delete(int id);
       bool CheckStyleNameExistance(string Stylename, int? Id,int companyid,int branchid);
    }
}
