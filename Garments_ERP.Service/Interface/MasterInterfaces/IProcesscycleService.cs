﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
    public interface IProcesscycleService
    {
        List<M_ProcessCycleEntity> get();
        List<ProcessCycleTransactionEntity> Getbycycleid(int id);
        int Insert(M_ProcessCycleEntity entity);
        bool Update(M_ProcessCycleEntity entity);
        bool Delete(int id);
        M_ProcessCycleEntity getbyid(int id);
    }
}
