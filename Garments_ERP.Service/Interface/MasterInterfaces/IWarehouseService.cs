﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IWarehouseService
    {
        List<M_WarehouseEntity> get(int companyid, int branchid);
        M_WarehouseEntity getbyid(long id);
        long Insert(M_WarehouseEntity entity);
        bool Update(M_WarehouseEntity entity);
        bool Delete(long id);
    }
}
