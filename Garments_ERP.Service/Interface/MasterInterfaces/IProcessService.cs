﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IProcessService
    {
        List<M_ProcessMasterEntity> get();
        decimal Insert(M_ProcessMasterEntity entity);
        bool Update(M_ProcessMasterEntity entity);
        M_ProcessMasterEntity GetById(long id);
        bool Delete(long id);
    }
}
