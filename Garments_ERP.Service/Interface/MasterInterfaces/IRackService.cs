﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface IRackService
    {
       List<M_RackMasterEntity> get();
       M_RackMasterEntity GetById(int id);
       int Insert(M_RackMasterEntity entity);
       bool Update(M_RackMasterEntity entity);
       bool Delete(int id);
    }
}
