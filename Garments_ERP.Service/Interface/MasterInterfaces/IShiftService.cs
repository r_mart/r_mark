﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IShiftService
    {
        List<M_ShiftMasterEntity> get();
        M_ShiftMasterEntity getbyid(long id);
        long Insert(M_ShiftMasterEntity entity);
        bool Update(M_ShiftMasterEntity entity);
        bool Delete(long id);
    }
}
