﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ICityService
    {
        List<M_CityMasterEntity> get();
        List<M_CityMasterEntity> getByid(long id);
    }
}
