﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.ProductionServices;
using Garments_ERP.Service.Service.MasterServices;
using Garments_ERP.Service.Service.Accounts;

namespace Garments_ERP.Service.Interface
{
    public interface IServiceFactory
    {
        #region property till 21-07-2019
        DailyProductionPlanEntryServices DailyProductionPlanEntryServices { get; }
        LedgerAccountServices LedgerAccountServices { get; }
        CustomerService CustomerService { get; }
        EnquiryRawItemDetailService EnquiryRawItemDetailService { get; }
        EnquiryService EnquiryService { get; }
        EnquiryStyleDetailService EnquiryStyleDetailService { get; }
        ItemCategoryService ItemCategoryService { get; }
        ItemService ItemService { get; }
        ItemSizeService ItemSizeService { get; }
        ItemSubCategoryService ItemSubCategoryService { get; }
        SegmentTypeService SegmentTypeService { get; }
        StyleService StyleService { get; }
        ItemshadeService ItemshadeService { get; }
        ItemColorService ItemColorService { get; }
        QuotationService QuotationService { get; }
        QuotationRawItemDetailService QuotationRawItemDetailService { get; }
        QuotationStyleDetailService QuotationStyleDetailService { get; }
        StyleSizeDetailService StyleSizeDetailService { get; }
        EmployeeService EmployeeService { get; }
        SRM_GRNInwardService SRMGRNInwardService { get; }
        WarehouseService WarehouseService { get; }
        SupplierService SupplierService { get; }
        DepartmentService DepartmentService { get; }
        UnitService UnitService { get; }
        SupplierContactService SupplierContactService { get; }
        SupplierBankService SupplierBankService { get; }
        CountryService CountryService { get; }
        StateService StateService { get; }
        CityService CityService { get; }
        SupplierTypeService SupplierTypeService { get; }
        CustomerContactService CustomerContactService { get; }
        CustomerBankService CustomerBankService { get; }
        CustomerTypeService CustomerTypeService { get; }
        RackService RackService { get; }
        PackingTypeService PackingTypeService { get; }
        MachineTypeService MachineTypeService { get; }
        ProcessService ProcesService { get; }
        ItemGroupService ItemGroupService { get; }
        BillOfMaterialService BillOfMaterialService { get; }
        BillOfMaterialDetailService BillOfMaterialDetailService { get; }
        BOMSizeDetailService BOMSizeDetailService { get; }
        ProcessCycleService ProcessCycleService { get; }
        MachineService MachineService { get; }
        InspectionTypeService InspectionTypeService { get; }
        WarehouseRoleService WarehouseRoleService { get; }
        WarehouseTypeService WarehouseTypeService { get; }
        ShiftService ShiftService { get; }
        CustomerPOService CustomerPOService { get; }
        SupplierPRService SupplierPRService { get; }
        SupplierPRItemDetailService SupplierPRItemDetailService { get; }
        BrandService BrandService { get; }
        DiscountTypeService DiscountTypeService { get; }
        RateIndicatorService RateIndicatorService { get; }
        WorkorderService WorkorderService { get; }
        SupplierQuotationService SupplierQuotationService { get; }
        SupplierQuotationItemDetailService SupplierQuotationItemDetailService { get; }
        ProcessingStatusService ProcessingStatusService { get; }
        UserService UserService { get; }
        SupplierPOService SupplierPOService { get; }
        TaxService TaxService { get; }
        MaterialIssueService MaterialIssueService { get; }
        WorkCenterService WorkCenterService { get; }
        TransferService TransferService { get; }
        ProductionPlanService ProductionPlanService { get; }
        DashboardService DashboardService { get; }
        ReportsService ReportsService { get; }
        PaymentTermService PaymentTermService { get; }
        DailyProductionPlanService DailyProductionPlanService { get; }
        InwardItemService InwardItemService { get; }
        BrokerService BrokerService { get; }
        

        #endregion

        ////   
        ///New interface by ramesh take
        ///on 31-07-2019
        //
        AttributeService AttributeService { get; }
        ProcessInchargeService ProcessInchargeService { get; }
        AttributeValueService AttributeValueService { get; }
        ItemAttributeValueService ItemAttributeValueService { get; }
        CompanyService CompanyService { get; }
        CompanyBranchService CompanyBranchService { get; }
        InvoiceChallanService InvoiceChallanService { get; }
        ScreenService ScreenService { get; }
        RoleService RoleService { get; }
        AssignScreenService AssignScreenService { get; }
        /////
        /// Added interface for counter sales - by ramesh take on 10-Jan-2020
        /////
        ///
        CounterSalesService CounterSalesService { get; }
        JobWorkService JobWorkService { get; }
        FinishedGoodsService FinishedGoodsService { get; }
        SchemeService SchemeService { get; }
        FG_RetailService FG_RetailService { get; }
        ReturnOrderService ReturnOrderService { get; }

        //Shubham :- 29-07-2020 For Direct Supplier Purchase Request
        SupplierPRServices SupplierPRServices { get; }
    }
}
