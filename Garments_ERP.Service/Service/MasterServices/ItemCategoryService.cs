﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
    public class ItemCategoryService : IItemCategoryService
    {
        cl_ItemCategory obj = new cl_ItemCategory();
        public List<M_ItemCategoryMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_ItemCategoryMasterEntity> getRawSemifinsh()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        

        public long Insert(M_ItemCategoryMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_ItemCategoryMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CheckExistance(string ItemCategory, int? Id, int Itemgroupid,int companyid,int branchid)
        {
            try
            {
                return obj.CheckExistance(ItemCategory, Id, Itemgroupid, companyid, branchid);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
