﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class MachineTypeService
    {
        cl_Machinetypemaster obj = new cl_Machinetypemaster();
        public List<M_MachineTypeMasterEntity> Get()
        {
            try
            {
                return obj.Get();
            }
            catch (Exception)
            {
                
                throw;
            }
        }


      public  long Insert(M_MachineTypeMasterEntity entity)
     {
         try
         {
             return obj.Insert(entity);
         }
         catch (Exception)
         {

             throw;
         }

     }
      public  bool Update(M_MachineTypeMasterEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
       public bool Delete(long id)
      {
          try
          {
              return obj.Delete(id);
          }
          catch (Exception)
          {

              throw;
          }
      }
    }
}
