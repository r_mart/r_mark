﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System.Xml;
using Garments_ERP.Service.Interface.MasterInterfaces;
using Garments_ERP.Data.Classes.MasterClasses;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class CompanyService: ICompanyService
    {
        cl_MCompany obj = new cl_MCompany();
        public List<M_companyEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_companyEntity> getComdetail()
        {
            try
            {
                return obj.getComdetail();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string Insert(M_companyEntity entity,string password)
        {
            try
            {
                return obj.Insert(entity, password);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_companyEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_companyEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<M_BranchEntity> GetBranchListById(int compid)
        {
            try
            {
                return obj.GetBranchListById(compid);
            }
            catch(Exception)
            {
                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
