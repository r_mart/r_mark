﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class WorkCenterService
    {
        cl_WorkCenter obj = new cl_WorkCenter();
        public List<M_WorkCenterMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_WorkCenterMasterEntity GetById(int id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_WorkCenterMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_WorkCenterMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CheckWorkCenterExistance(string WorkCenter, int? Id,int companyid,int branchid)
        {
            try
            {
                return obj.CheckWorkCenterExistance(WorkCenter, Id, companyid, branchid);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
