﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class RoleService
    {
        cl_role obj = new cl_role();
        public List<M_RoleEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(M_RoleEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(M_RoleEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ActiveDeactive(int id, string Status)
        {
            try
            {
                return obj.ActiveDeactive(id, Status);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
