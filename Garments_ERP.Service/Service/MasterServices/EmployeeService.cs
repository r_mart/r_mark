﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
   public class EmployeeService
    {
       cl_Employee obj = new cl_Employee();
       public List<EmployeeEntity> get(int comid, int branchid)
       {
           try
           {
               return obj.get(comid, branchid);
           }
           catch (Exception)
           {
               
               throw;
           }
       }

        public EmployeeEntity getIdByUserData(int id)
        {
            try
            {
                return obj.getIdByUserData(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_Employee_TypeEntity> getEmployeeType()
        {
            try
            {
                return obj.getEmployeeType();
            }
            catch (Exception)
            {

                throw;
            }
        }

       public List<EmployeeAddressDetailEntity> getaddress(int comid,int branchid)
       {
           try
           {
               return obj.getaddress();
           }
           catch (Exception)
           {
               
               throw;
           }
       }
        public int Insert(EmployeeEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(EmployeeEntity entity,int DeptID, int RoleID)
        {
            try
            {
                return obj.Update(entity, DeptID, RoleID);
            }
            catch (Exception)
            {

                throw;
            }
        }



        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long InsertAddress(EmployeeAddressDetailEntity entity)
        {
            try
            {
                return obj.InsertAddress(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateAddress(EmployeeAddressDetailEntity entity)
        {
            try
            {
                return obj.UpdateAddress(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }



        public EmployeeAddressDetailEntity getaddressbyid(long id)
       {
           try
           {
               return obj.getaddressbyid(id);
           }
           catch (Exception)
           {
               throw;
           }
       }
    }
}
