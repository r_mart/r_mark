﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class DepartmentService
    {
        cl_Department obj = new cl_Department();
        public List<M_DepartmentMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_DepartmentMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(M_DepartmentMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckDepartmentNameExistance(string DepartmentName, int? Id)
        {
            try
            {
                return obj.CheckDepartmentNameExistance(DepartmentName, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
