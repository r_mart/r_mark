﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
    public class ItemSubCategoryService : IItemSubCategoryService
    {
        cl_ItemSubcategory obj = new cl_ItemSubcategory();


        public List<M_ItemMasterEntity> GetRawItemList(int subcatid)
        {
            try
            {
                return obj.GetRawItemList(subcatid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_ItemSubCategoryMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_ItemSubCategoryMasterEntity> getSubctegorybyCategoryid(long categoryid)
        {
            try
            {
                return obj.getSubctegorybyCategoryid(categoryid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_ItemSubCategoryMasterEntity> GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }


       public int Insert(M_ItemSubCategoryMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_ItemSubCategoryMasterEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception)
           {

               throw;
           }
       }

        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CheckExistance(string Itemsubcategory, int? Id, int itemcategoryid,int companyid,int branchid)
        {
            try
            {
                return obj.CheckExistance(Itemsubcategory, Id, itemcategoryid, companyid, branchid);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public M_ItemSubCategoryMasterEntity bysubcategoryid(long id)
        {
            try
            {
                return obj.bysubcategoryid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
