﻿using Garments_ERP.Data.Classes.MasterClasses;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class ItemAttributeValueService
    {
        cl_ItemAttributeValue obj = new cl_ItemAttributeValue();
        #region Methods
        public List<M_Item_Attribute_MasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public int Insert(M_Item_Attribute_MasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_Item_Attribute_MasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_Item_Attribute_MasterEntity GetById(int id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
