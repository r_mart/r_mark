﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class TransferService
    {
        cl_FGTransfer obj = new cl_FGTransfer();

        public List<FG_TransferMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<FG_TransferMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextTFNo()
        {
            try
            {
                return obj.getnextTFNo();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<FG_TransferMasterEntity> Get()
        {
            try
            {
                return obj.Get();
            }
            catch (Exception)
            {

                throw;
            }
        }
       public List<FG_TransferMasterEntity> Get(DateTime from,DateTime to)
        {
            try
            {
                return obj.Get(from,to);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public FG_TransferMasterEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(FG_TransferMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(FG_TransferMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
