﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class JobWorkService
    {
        cl_jobwork obj = new cl_jobwork();

        public M_JobworkEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_JobworkEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_JobworkEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public string getnextJWNo()
        {
            try
            {
                return obj.getnextJWNo();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(M_JobworkEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_ProcessMasterEntity> GetProcessList(long workorderID,int userid)
        {
            try
            {
                return obj.GetProcessList(workorderID, userid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ITEM_TRANSFEREntity> GetTransferNo(long workorderID, int userid)
        {
            try
            {
                return obj.GetTransferNo(workorderID, userid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MachineEntity> GetMachine(long ProcessId)
        {
            try
            {
                return obj.GetMachine(ProcessId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_BOM_ProcessCycleEntity GetOutItem(int ProcessId, long workorderID)
        {
            try
            {
                return obj.GetOutItem(ProcessId, workorderID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ITEM_TRANSFER_DETAILEntity> GetMaterialTransfer(string TransferNo)
        {
            try
            {
                return obj.GetMaterialTransfer(TransferNo);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
