﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Data.Classes.ProductionClasses;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;
using Garments_ERP.Service.Interface.ProductionInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service.ProductionServices
{
    public class FinishedGoodsService : IFinishedGoodsService
    {
        cl_FinishedGoodCreate obj = new cl_FinishedGoodCreate();

        public int Insert(M_ItemMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<M_ItemMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public M_ItemMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }




    }
}
