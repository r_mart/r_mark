﻿using Garments_ERP.Data.Classes;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class ReturnOrderService: IReturnOrderService
    {
        cl_ReturnOrder obj = new cl_ReturnOrder();
        public string getnextROno()
        {
            try
            {
                return obj.getnextROno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public T_ISSUE_RETURNSEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNSEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNS_DETAILEntity> GetInvoiceItem(long Id)
        {
            try
            {
                return obj.GetInvoiceItem(Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNS_DETAILEntity> GetSPOitem(long Id)
        {
            try
            {
                return obj.GetSPOitem(Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNS_DETAILEntity> GetTransferitem(long Id)
        {
            try
            {
                return obj.GetTransferitem(Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long insert(T_ISSUE_RETURNSEntity entity)
        {
            try
            {
                return obj.insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
