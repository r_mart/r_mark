﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class SupplierPRItemDetailService
    {
        cl_SupplierPRItemDetail obj = new cl_SupplierPRItemDetail();
        public bool Update(SRM_PurchaseRequestItemDetailEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_PurchaseRequestItemDetailEntity> getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_PurchaseRequestItemDetailEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(SRM_PurchaseRequestItemDetailEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
