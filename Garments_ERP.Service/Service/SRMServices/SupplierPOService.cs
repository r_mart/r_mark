﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
   public class SupplierPOService
    {
       cl_SupplierPurchaseOrder obj = new cl_SupplierPurchaseOrder();
       public string getnextPONo()
       {
           try
           {
               return obj.getnextPONo();
           }
           catch (Exception)
           {
               
               throw;
           }
       }

        public bool RejectedPO(long id, int Uid)
        {
            try
            {
                return obj.RejectedPO(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PurchaseOrderMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PurchaseOrderMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<SRM_PurchaseOrderMasterEntity> Get(DateTime from,DateTime to)
       {
           try
           {
               return obj.Get(from,to);
           }
           catch (Exception)
           {

               throw;
           }
       }
       public List<SRM_PurchaseOrderMasterEntity> Get()
       {
           try
           {
               return obj.Get();
           }
           catch (Exception)
           {

               throw;
           }
       }

       public List<SRM_PurchaseOrderMasterEntity> Getbysupplierid(long suppid)
       {
           try
           {
               return obj.Getbysupplierid(suppid);
           }
           catch (Exception)
           {

               throw;
           }
       }
       public SRM_PurchaseOrderMasterEntity getbyid(long id)
       {
           try
           {
               return obj.getbyid(id);
           }
           catch (Exception)
           {

               throw;
           }
       }
       public long Insert(SRM_PurchaseOrderMasterEntity entity)
       {
           try
           {
               return obj.Insert(entity);
           }
           catch (Exception)
           {

               throw;
           }
       }
       public bool Update(SRM_PurchaseOrderMasterEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception)
           {

               throw;
           }
       }

       public bool Delete(long id,int Userid)
       {
           try
           {
               return obj.Delete(id, Userid);
           }
           catch (Exception)
           {

               throw;
           }
       }

        public object getbyPONO(string PONO)
        {
            try
            {
                return obj.getbyPONO(PONO);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
