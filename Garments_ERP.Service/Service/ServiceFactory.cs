﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Service.Service.ProductionServices;
using Garments_ERP.Service.Service.MasterServices;
using Garments_ERP.Service.Service.Accounts;

namespace Garments_ERP.Service.Service
{
    public class ServiceFactory : IServiceFactory
    {

        private static IServiceFactory Instance;
        private ReturnOrderService ReturnOrderservice = new ReturnOrderService();
        private ProcessInchargeService ProcessInchargeservice = new ProcessInchargeService();
        private DailyProductionPlanEntryServices DailyProductionPlanEntryservices = new DailyProductionPlanEntryServices();
        private LedgerAccountServices LedgerAccountservice = new LedgerAccountServices();
        private CustomerService customerservice = new CustomerService();
        private EnquiryRawItemDetailService enquiryrawitemdetailservice = new EnquiryRawItemDetailService();
        private EnquiryService enquiryservice = new EnquiryService();
        private EnquiryStyleDetailService enquirystyledetailservice = new EnquiryStyleDetailService();
        private ItemCategoryService itemcategoryservice = new ItemCategoryService();
        private ItemService itemservice = new ItemService();
        private ItemSubCategoryService itemsubcategoryservice = new ItemSubCategoryService();
        private ItemSizeService itemsizeservice = new ItemSizeService();
        private SegmentTypeService segmenttypeservice = new SegmentTypeService();
        private StyleService styleservice = new StyleService();
        private ItemshadeService itemshadeservice = new ItemshadeService();
        private ItemColorService itemcolrservice = new ItemColorService();
        private QuotationService quotationservice = new QuotationService();
        private QuotationStyleDetailService quotationstyleservice = new QuotationStyleDetailService();
        private QuotationRawItemDetailService quotationrawitemdetailservice = new QuotationRawItemDetailService();
        private StyleSizeDetailService stylesizedetailservice = new StyleSizeDetailService();
        private SupplierBankService supplierbankservice = new SupplierBankService();
        private SupplierContactService suppliercontactservice = new SupplierContactService();
        private CountryService countryservice = new CountryService();
        private StateService stateservice = new StateService();
        private CityService cityservice = new CityService();
        private SRM_GRNInwardService srmgrninwardservice = new SRM_GRNInwardService();
        private WarehouseService warehouseservice = new WarehouseService();
        private SupplierService supplierservice = new SupplierService();
        private DepartmentService departmentservice = new DepartmentService();
        private UnitService unitservice = new UnitService();
        private SupplierTypeService suppliertypeservice = new SupplierTypeService();
        private CustomerContactService customercontactservice = new CustomerContactService();
        private CustomerBankService customerbankservice = new CustomerBankService();
        private CustomerTypeService customertypeservice = new CustomerTypeService();
        private RackService rackservice = new RackService();
        private PackingTypeService packingtypeservice = new PackingTypeService();
        //  private MachineTypeService machinetypeservice = new MachineTypeService();
        private ProcessService processservice = new ProcessService();
        private EmployeeService employeeservice = new EmployeeService();
        private ItemGroupService itemgroupservice = new ItemGroupService();
        private BillOfMaterialService billofmaterialservice = new BillOfMaterialService();
        private BillOfMaterialDetailService billofmaterialdetail = new BillOfMaterialDetailService();
        private BOMSizeDetailService bomdetailsevice = new BOMSizeDetailService();
        private ProcessCycleService processcycleservice = new ProcessCycleService();
        private MachineService machineservice = new MachineService();
        private InspectionTypeService inspectiontypeservice = new InspectionTypeService();
        private MachineTypeService machinetypeservice = new MachineTypeService();
        private WarehouseRoleService warehouseroleservice = new WarehouseRoleService();
        private WarehouseTypeService warehousetypeservice = new WarehouseTypeService();
        private CustomerPOService customerposervice = new CustomerPOService();
        private ShiftService shiftservice = new ShiftService();
        private SupplierPRService supplierprservice = new SupplierPRService();
        private SupplierPRItemDetailService supplierpritemdetailservice = new SupplierPRItemDetailService();
        private BrandService brandservice = new BrandService();
        private DiscountTypeService discounttypeservice = new DiscountTypeService();
        private RateIndicatorService rateindicatorservice = new RateIndicatorService();
        private WorkorderService workorderservice = new WorkorderService();
        private SupplierQuotationService supplierquotationservice = new SupplierQuotationService();
        private SupplierQuotationItemDetailService supplierquotationitemdetailservice = new SupplierQuotationItemDetailService();
        private ProcessingStatusService processingstatusservice = new ProcessingStatusService();
        private UserService userservice = new UserService();
        private SupplierPOService supplierposervice = new SupplierPOService();
        private TaxService taxservice = new TaxService();
        private MaterialIssueService materialissueservice = new MaterialIssueService();
        private WorkCenterService workcenterservice = new WorkCenterService();
        private TransferService transferservice = new TransferService();
        private ProductionPlanService productionplanservice = new ProductionPlanService();
        private DashboardService dashboardservice = new DashboardService();
        private ReportsService reportsservice = new ReportsService();
        private PaymentTermService paymentservice = new PaymentTermService();
        private DailyProductionPlanService dailyProductionPlanService = new DailyProductionPlanService();
        private InwardItemService inwarditemservice = new InwardItemService();
        private BrokerService brokerservice = new BrokerService();

        /// Product service object
        private AttributeService attrubuteservice = new AttributeService();
        private AttributeValueService attributevalueservice = new AttributeValueService();
        private ItemAttributeValueService itemattributeservice = new ItemAttributeValueService();
        private CompanyService companyservice = new CompanyService();
        private CompanyBranchService CompanyBranchservice = new CompanyBranchService();
        private InvoiceChallanService InvoiceChallanservice = new InvoiceChallanService();
        private ScreenService Screenservice = new ScreenService();
        private RoleService Roleservice = new RoleService();
        private AssignScreenService AssignScreenservice = new AssignScreenService();
        //Added for counter sales -by ramesh take on 10-jan-2020
        private CounterSalesService Countersalesservice = new CounterSalesService();

        //Added For Job Work -by Shubham on 13-jan-2020
        private JobWorkService JobWorkservice = new JobWorkService();
        private FinishedGoodsService FinishedGoodsservice = new FinishedGoodsService();
        //Added for scheme master -by ramesh take on 11-feb-2020
        private SchemeService Schemeservice = new SchemeService();
        //Added forFG transfer to retail -by ramesh take on 25-feb-2020
        private FG_RetailService FG_Retailservice = new FG_RetailService();

        //Shubham :- 29-07-2020 For Direct Supplier Purchase Request
        private SupplierPRServices SupplierPRservices = new SupplierPRServices();


        static ServiceFactory()
        {
            // Create the only instance of this class
            Instance = new ServiceFactory();

        }
        private ServiceFactory() { }
        public static IServiceFactory GetInstance()
        {
            return Instance;
        }
        #region 31-07-2019
        public CustomerService CustomerService
        {
            get { return customerservice; }
        }

        public LedgerAccountServices LedgerAccountServices
        {
            get { return LedgerAccountservice; }
        }
        public DailyProductionPlanEntryServices DailyProductionPlanEntryServices
        {
            get { return DailyProductionPlanEntryservices; }
        }

        public EnquiryRawItemDetailService EnquiryRawItemDetailService
        {
            get { return enquiryrawitemdetailservice; }
        }

        public EnquiryService EnquiryService
        {
            get { return enquiryservice; }
        }

        public EnquiryStyleDetailService EnquiryStyleDetailService
        {
            get { return enquirystyledetailservice; }
        }

        public ItemCategoryService ItemCategoryService
        {
            get { return itemcategoryservice; }
        }

        public ItemService ItemService
        {
            get { return itemservice; }
        }

        public ItemSizeService ItemSizeService
        {
            get { return itemsizeservice; }
        }

        public ItemSubCategoryService ItemSubCategoryService
        {
            get { return itemsubcategoryservice; }
        }
        public SegmentTypeService SegmentTypeService
        {
            get { return segmenttypeservice; }
        }
        public StyleService StyleService
        {
            get { return styleservice; }
        }
        public ItemshadeService ItemshadeService
        {
            get { return itemshadeservice; }
        }
        public ItemColorService ItemColorService
        {
            get { return itemcolrservice; }
        }
        public QuotationService QuotationService
        {
            get { return quotationservice; }
        }
        public QuotationRawItemDetailService QuotationRawItemDetailService
        {
            get { return quotationrawitemdetailservice; }
        }
        public QuotationStyleDetailService QuotationStyleDetailService
        {
            get { return quotationstyleservice; }
        }

        public StyleSizeDetailService StyleSizeDetailService
        {
            get { return stylesizedetailservice; }
        }



        public SRM_GRNInwardService SRMGRNInwardService
        {
            get { return srmgrninwardservice; }
        }
        public WarehouseService WarehouseService
        {
            get { return warehouseservice; }
        }
        public SupplierService SupplierService
        {
            get { return supplierservice; }
        }
        public DepartmentService DepartmentService
        {
            get { return departmentservice; }
        }
        public UnitService UnitService
        {
            get { return unitservice; }
        }
        public SupplierBankService SupplierBankService
        {
            get { return supplierbankservice; }
        }
        public SupplierContactService SupplierContactService
        {
            get { return suppliercontactservice; }
        }
        public CountryService CountryService
        {
            get { return countryservice; }
        }
        public StateService StateService
        {
            get { return stateservice; }
        }
        public CityService CityService
        {
            get { return cityservice; }
        }
        public SupplierTypeService SupplierTypeService
        {
            get { return suppliertypeservice; }
        }
        public CustomerContactService CustomerContactService
        {
            get { return customercontactservice; }
        }
        public CustomerBankService CustomerBankService
        {
            get { return customerbankservice; }
        }
        public CustomerTypeService CustomerTypeService
        {
            get { return customertypeservice; }
        }
        public RackService RackService
        {
            get { return rackservice; }
        }
        public PackingTypeService PackingTypeService
        {
            get { return packingtypeservice; }
        }
        public MachineTypeService MachineTypeService
        {
            get { return machinetypeservice; }
        }
        public ProcessService ProcesService
        {
            get { return processservice; }
        }
        public EmployeeService EmployeeService
        {
            get { return employeeservice; }
        }
        public ItemGroupService ItemGroupService
        {
            get
            {
                return itemgroupservice;
            }
        }
        public BillOfMaterialService BillOfMaterialService
        {
            get { return billofmaterialservice; }
        }
        public BillOfMaterialDetailService BillOfMaterialDetailService
        {
            get { return billofmaterialdetail; }
        }

        public ProcessCycleService ProcessCycleService
        {
            get
            {
                return processcycleservice;
            }
        }
        public BOMSizeDetailService BOMSizeDetailService
        {
            get { return bomdetailsevice; }
        }
        public MachineService MachineService
        {
            get { return machineservice; }
        }
        public InspectionTypeService InspectionTypeService
        {
            get { return inspectiontypeservice; }
        }
        public WarehouseRoleService WarehouseRoleService
        {
            get { return warehouseroleservice; }
        }
        public WarehouseTypeService WarehouseTypeService
        {
            get { return warehousetypeservice; }
        }
        public CustomerPOService CustomerPOService
        {
            get { return customerposervice; }
        }
        public ShiftService ShiftService
        {
            get { return shiftservice; }
        }
        public SupplierPRService SupplierPRService
        {
            get { return supplierprservice; }
        }
        public SupplierPRItemDetailService SupplierPRItemDetailService
        {
            get { return supplierpritemdetailservice; }
        }
        public BrandService BrandService
        {
            get { return brandservice; }
        }
        public DiscountTypeService DiscountTypeService
        {
            get { return discounttypeservice; }
        }
        public RateIndicatorService RateIndicatorService
        {
            get { return rateindicatorservice; }
        }
        public WorkorderService WorkorderService
        {
            get { return workorderservice; }
        }
        public SupplierQuotationService SupplierQuotationService
        {
            get { return supplierquotationservice; }
        }
        public SupplierQuotationItemDetailService SupplierQuotationItemDetailService
        {
            get { return supplierquotationitemdetailservice; }
        }
        public ProcessingStatusService ProcessingStatusService
        {
            get { return processingstatusservice; }
        }
        public UserService UserService
        {
            get { return userservice; }
        }
        public SupplierPOService SupplierPOService
        {
            get { return supplierposervice; }
        }
        public TaxService TaxService
        {
            get { return taxservice; }
        }
        public MaterialIssueService MaterialIssueService
        {
            get { return materialissueservice; }
        }
        public WorkCenterService WorkCenterService
        {
            get { return workcenterservice; }
        }
        public TransferService TransferService
        {
            get { return transferservice; }
        }
        public ProductionPlanService ProductionPlanService
        {
            get { return productionplanservice; }
        }
        public DashboardService DashboardService
        {
            get { return dashboardservice; }
        }
        public ReportsService ReportsService
        {
            get { return reportsservice; }
        }
        public PaymentTermService PaymentTermService
        {
            get { return paymentservice; }
        }
        public DailyProductionPlanService DailyProductionPlanService
        {
            get { return dailyProductionPlanService; }
        }
        public InwardItemService InwardItemService
        {
            get { return inwarditemservice; }
        }
        public BrokerService BrokerService
        {
            get { return brokerservice; }
        }
        #endregion

        /// <summary>
        /// Attribute service by ramesh take.
        /// on 31-07-2019
        /// </summary>
        public AttributeService AttributeService
        {
            get { return attrubuteservice; }
        }
        public ProcessInchargeService ProcessInchargeService
        {
            get { return ProcessInchargeservice; }
        }
        public AttributeValueService AttributeValueService
        {
            get { return attributevalueservice; }
        }
        public ItemAttributeValueService ItemAttributeValueService
        {
            get { return itemattributeservice; }
        }

        public CompanyService CompanyService
        {
            get { return companyservice; }
        }
        public CompanyBranchService CompanyBranchService
        {
            get { return CompanyBranchservice; }
        }

        public InvoiceChallanService InvoiceChallanService
        {
            get { return InvoiceChallanservice; }
        }

        public ScreenService ScreenService
        {
            get { return Screenservice; }
        }

        public RoleService RoleService
        {
            get { return Roleservice; }
        }

        public AssignScreenService AssignScreenService
        {
            get { return AssignScreenservice; }
        }
        //Added for counter sales by RST-10-jan2020
        public CounterSalesService CounterSalesService
        {
            get { return Countersalesservice; }
        }

        //Added By Job Work by Shubham 13-Jan-2020
        public JobWorkService JobWorkService
        {
            get { return JobWorkservice; }
        }

        public FinishedGoodsService FinishedGoodsService
        {
            get { return FinishedGoodsservice; }
        }
        public SchemeService SchemeService
        {
            get { return Schemeservice; }
        }
        public FG_RetailService FG_RetailService
        {
            get { return FG_Retailservice; }
        }

        public ReturnOrderService ReturnOrderService
        {
            get { return ReturnOrderservice; }
        }

        //Shubham :- 29-07-2020 For Direct Supplier Purchase Request
        public SupplierPRServices SupplierPRServices
        {
            get { return SupplierPRservices;  }
        }
    }
}
