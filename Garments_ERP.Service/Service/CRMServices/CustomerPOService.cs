﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System.Xml;

namespace Garments_ERP.Service.Service
{
 public   class CustomerPOService
    {
     cl_CustomerPO obj = new cl_CustomerPO();
        public List<M_POOtherChargesMasterEntity> getOtherCharge()
        {
            try
            {
                return obj.getOtherCharge();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CRM_CustomerPOMasterentity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CRM_CustomerPOMasterentity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool RejectedCPO(long id, int Uid)
        {
            try
            {
                return obj.RejectedCPO(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object EditAttributeValue(string MyJson, int type, XmlDocument doc)
        {
            try
            {
                return obj.EditAttributeValue(MyJson, type, doc);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CRM_CustomerPOMasterentity> Get()
     {
         try
         {
             return obj.Get();
         }
         catch (Exception)
         {

             throw;
         }
     }
     public List<CRM_CustomerPOMasterentity> Get(DateTime from,DateTime to)
     {
         try
         {
             return obj.Get(from, to);
         }
         catch (Exception)
         {

             throw;
         }
     }

    
      public  long Insert(CRM_CustomerPOMasterentity entity)
     {
         try
         {
             return obj.Insert(entity);
         }
         catch (Exception)
         {

             throw;
         }
     }
       public bool Update(CRM_CustomerPOMasterentity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
       public bool Delete(long id,int Userid)
       {
           try
           {
               return obj.Delete(id, Userid);
           }
           catch (Exception)
           {

               throw;
           }
       }
     public string getnextPONo()
       {
           try
           {
               return obj.getnextPONo();
           }
           catch (Exception)
           {

               throw;
           }
       }
     public CRM_CustomerPOMasterentity getbyid(long id)
     {
         try
         {
             return obj.getbyid(id);
         }
         catch (Exception)
         {

             throw;
         }

     }

     public List<CRM_CustomerPOMasterentity> Getbycustomerid(long custid)
     {
         try
         {
             return obj.Getbycustomerid(custid);
         }
         catch (Exception)
         {

             throw;
         }
     }

        public List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid)
        {
            try
            {
                return obj.getPOListbycustomerid(custid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object getbyPONO(string PONO)
        {
            try
            {
                return obj.getbyPONO(PONO);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
