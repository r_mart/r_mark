﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
  public  class QuotationRawItemDetailService:IQuotationRawItemDetailService
    {
      cl_QuotationRawItemDetail obj = new cl_QuotationRawItemDetail();
        public long Insert(CRM_QuotationRawItemDetailEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
      public  List<CRM_QuotationRawItemDetailEntity> getbyquotationid(long id)
        {
            try
            {
                return obj.getbyquotationid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
      public  bool Update(CRM_QuotationRawItemDetailEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public bool deleterawitem(long id)
      {
          try
          {
              return obj.deleterawitem(id);
          }
          catch (Exception)
          {

              throw;
          }
      }

    }
}
