﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
  public  class QuotationService:IQuotationService
    {
      cl_Quotation obj = new cl_Quotation();
        public long AddUpdate(List<string> Style_Img, List<string> RawItemImg, CRM_QuotationMasterEntity Entity, List<CRM_QuotationRawItemDetailEntity> RawItem)
        {
            try
            {
                return obj.AddUpdate(Style_Img, RawItemImg, Entity, RawItem);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CRM_QuotationStyleDetailEntity> GetQuoStyleData(long QuoId)
        {
            try
            {
                return obj.GetQuoStyleData(QuoId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_QuotationRawItemDetailEntity> GetQuoStyleRawItemData(long QuoId, long styleid)
        {
            try
            {
                return obj.GetQuoStyleRawItemData(QuoId, styleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_QuotationMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_QuotationMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool RejectedQuo(long id, int Uid)
        {
            try
            {
                return obj.RejectedQuo(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object _getGSTTaxlist()
        {
            try
            {
                return obj._getGSTTaxlist();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

      public string  getnextQuotatioNo()
      {
          try
          {
              return obj.getnextQuotatioNo();
          }
          catch (Exception)
          {

              throw;
          }

      }
      public List<CRM_QuotationMasterEntity> get(DateTime from,DateTime to)
      {
          try
          {
              return obj.get(from,to);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public bool Update(CRM_QuotationMasterEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }

      }
      public CRM_QuotationMasterEntity getbyid(long id)
      {
          try
          {
              return obj.getbyid(id);
          }
          catch (Exception)
          {

              throw;
          }
      }
        public List<CRM_QuotationMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
      public bool Delete(long id,int Userid)
        {
            try
            {
                return obj.Delete(id, Userid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(CRM_QuotationMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ee)
            {

                throw;
            }
        }
        public List<CRM_QuotationMasterEntity> getquotationnobycustomer(long customerid,int Company_ID,int BranchId)
        {
            try
            {
                return obj.getquotationnobycustomer(customerid, Company_ID, BranchId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public object QuoteGet(long id)
        {
            try
            {
                return obj.QuoteGet(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
