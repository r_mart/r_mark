﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service.MasterServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class SchemeController : Controller
    {
        //
        // GET: /Scheme/
        SchemeService scheService = new SchemeService();
        [RBAC]
        public ActionResult Index()
        {
            try
            {
                ViewBag.title = "Scheme";
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
                int comid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);
                List<M_SchemeMaster_Entity> entityList = new List<M_SchemeMaster_Entity>();
                entityList = scheService.get(comid, branchid);
                return View(entityList);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Login");
                throw;
            }
           
            //return View();
        }
        [RBAC]
        public ActionResult Create()
        {
            try
            {
                ViewBag.title = "Scheme";
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Login");
                throw;
            }
           
        }
        [RBAC]
        [HttpPost]
        public Int64 SaveScheme(M_SchemeMaster_Entity scheEnt)
        {
            return scheService.InsertUpdate(scheEnt);
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Scheme";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            int userid = Convert.ToInt32(collection.Id);
            M_SchemeMaster_Entity entity = new M_SchemeMaster_Entity();
            entity.Created_By = userid;
            entity.Branch_Id = branchid;
            entity.Scheme_ID = id;
            entity.Company_Id = comid;
            return View(entity);
        }
        public JsonResult Get_Scheme_ById(int id)
        {
            M_SchemeMaster_Entity schemeEnt = new M_SchemeMaster_Entity();
            schemeEnt = scheService.getById(id);
            return Json(schemeEnt);
        }
        public ActionResult delete(int id)
        {
            bool delete = scheService.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
        public JsonResult checkSchemewExists(int compid,int branchid,long itemid, string st_date,string ed_date,long scheme_id)
        {
            bool isExist = false;
            try
            {
                isExist = scheService.IsExists(compid, branchid,itemid,st_date,ed_date,scheme_id);
            }
            catch (Exception)
            {

                throw;
            }

            return Json(isExist);
        }
    }
}
