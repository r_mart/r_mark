﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.ProductionServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class DailyProductionEntryController : SuperController   
    {
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        CustomerService customerservice;
        WorkorderService workorderService;
        ProductionPlanService prodPlanService;
        MachineService machineService;
        WorkCenterService workCenterService = ServiceFactory.GetInstance().WorkCenterService;
        EmployeeService emloyeeService = ServiceFactory.GetInstance().EmployeeService;
        UnitService uniservice = ServiceFactory.GetInstance().UnitService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        DailyProductionPlanEntryServices DailyProductionPlanEntryServices = ServiceFactory.GetInstance().DailyProductionPlanEntryServices;
        public void Initialize(MfgDailyProductionPlanEntryEntity productionPlan)
        {
            int CBS = 1;//Customer ID
            customerservice = ServiceFactory.GetInstance().CustomerService;
            ViewBag.customerlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", 0);

            long wrkCenterID = 0, OprtorID = 0, Apprvd = 1, TlID = 0;

            //if (productionPlan.WorkCenterID != null)
            //    wrkCenterID = (long)productionPlan.WorkCenterID;

            //if (productionPlan.OperatorID != null)
            //    OprtorID = (long)productionPlan.OperatorID;

            //if (productionPlan.Approved != null)
            //    Apprvd = (int)productionPlan.Approved;

            //if (productionPlan.ToolID != null)
            //    TlID = (int)productionPlan.ToolID;

            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.WorkCenterList = DropDownList<M_WorkCenterMasterEntity>.LoadItems(workCenterService.get(), "Id", "WorkCenterName", wrkCenterID);
            ViewBag.OperatorList = DropDownList<EmployeeEntity>.LoadItems(emloyeeService.get(comid, branchid), "ID", "Name", OprtorID);

            var ApprovalstatusList = new List<NameValuePair>
            {
               new NameValuePair { ID=1, Name="Yes" },
               new NameValuePair { ID=0, Name="No" }
            };
            ViewBag.ApprovalstatusList = DropDownList<NameValuePair>.LoadItems(ApprovalstatusList, "ID", "Name", Apprvd);

            var RequiredToolsList = new List<NameValuePair>
            {
               new NameValuePair { ID=2000, Name="Sewing Tools" },
               new NameValuePair { ID=2001, Name="Cutting Tools" },
               new NameValuePair { ID=2002, Name="Weaving Tools" },
               new NameValuePair { ID=2003, Name="Stiching Tools" }
            };
            ViewBag.RequiredToolsList = DropDownList<NameValuePair>.LoadItems(RequiredToolsList, "ID", "Name", TlID);

            machineService = ServiceFactory.GetInstance().MachineService;
            ViewBag.MachinesList = DropDownList<MachineEntity>.LoadItems(machineService.Get(), "Id", "Machinename", 0);

            ViewBag.UnitList = DropDownList<M_UnitMasterEntity>.LoadItems(uniservice.get(), "Id", "ItemUnit", 0);
        }

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Daily Production Entry";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Daily Production Entry";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            Initialize(new MfgDailyProductionPlanEntryEntity());
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                MfgDailyProductionPlanEntryEntity entity = new MfgDailyProductionPlanEntryEntity();
                entity.MonthlyPlanID = Convert.ToInt32(frm["hdnMonthlyPlanID"]);
                entity.ProductionDate = DateTime.ParseExact(frm["DailyPlanDate"], Helper.Helper.DATE_FORMAT_FOR_EDIT, CultureInfo.InvariantCulture);
                entity.StyleID = Convert.ToInt64(frm["hdnStyleID"]);

                entity.DailyProduced_Qty = Convert.ToDecimal(frm["txtDailyProducedQty"]);
                entity.DailyPlanned_Qty = Convert.ToDecimal(frm["txtDailyPlannedQty"]);
                entity.ToolID = (frm["requiredToolID"] != null && frm["requiredToolID"] != "") ? Convert.ToInt32(frm["requiredToolID"]) : (int?)null;
                entity.WorkCenterID = (frm["workCenterID"] != null && frm["workCenterID"] != "") ? Convert.ToInt32(frm["workCenterID"]) : (int?)null;
                entity.OperatorID = (frm["operatorID"] != null && frm["operatorID"] != "") ? Convert.ToInt32(frm["operatorID"]) : (int?)null;
                entity.MachineID = (frm["MachineID"] != null && frm["MachineID"] != "") ? Convert.ToInt32(frm["MachineID"]) : (int?)null;
                entity.UnitID = (frm["StyleUnitID"] != null && frm["StyleUnitID"] != "") ? Convert.ToInt32(frm["StyleUnitID"]) : (int?)null;

                entity.CreatedBy = Convert.ToInt32(Helper.Helper.CurrentLoggedUser.Id);
                entity.CreatedDate = DateTime.Now;
                entity.Company_ID = companyid;
                entity.BranchId = branchid;
                List<DailyProductionEntryStyleSize> StyleSizeDetailsList = new List<DailyProductionEntryStyleSize>();
                string styleSizeDetailString = Convert.ToString(frm["hdnStyleSizeDetails"]); /// Collection of rows       
                if (styleSizeDetailString.Length > 0)
                {
                    string[] styleSizeRowDetails = styleSizeDetailString.Split('#');
                    foreach (string rwdetail in styleSizeRowDetails) // loop through each row
                    {
                        if (rwdetail != string.Empty)
                        {
                            string[] colDetails = rwdetail.Split('~'); // spit columns in a row

                            DailyProductionEntryStyleSize StyleSizeDetails = new DailyProductionEntryStyleSize();
                            //StyleSizeDetails.SizeID = Convert.ToInt64(colDetails[0].ToString());
                            StyleSizeDetails.Attribute_ID = colDetails[1].ToString();
                            StyleSizeDetails.Attribute_Value_ID = colDetails[2].ToString();
                            StyleSizeDetails.PlannedQuantity = Convert.ToDecimal(colDetails[3].ToString());
                            StyleSizeDetails.ProducedQuantity = Convert.ToDecimal(colDetails[4].ToString());
                            StyleSizeDetails.QC =Convert.ToString(colDetails[5]) == "Yes" ? true : false;
                            StyleSizeDetails.SizeID= Convert.ToInt32(colDetails[6].ToString());
                            StyleSizeDetails.Company_ID = companyid;
                            StyleSizeDetails.BranchId = branchid;
                            StyleSizeDetailsList.Add(StyleSizeDetails);
                        }
                    }
                }

                entity.StyleSizeDetails = StyleSizeDetailsList;

                var planservice = ServiceFactory.GetInstance().DailyProductionPlanEntryServices;
                long result = planservice.Insert(entity);
                if (result > 0)
                {
                    /// Redirect to Index Page
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Create");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Create");
            }

            return RedirectToAction("Create");
        }

        public JsonResult GetWorkOrderMonthlyPlans(long workorderID)
        {
            workorderService = ServiceFactory.GetInstance().WorkorderService;
            var prodPlansMonthlylist = workorderService.GetMonthlyPlansByWorkOrderID(workorderID);
            var workOrderQty = workorderService.GetWorkOrdersQuantity(workorderID);

            List<Object> objLst = new List<object>();
            objLst.Add(prodPlansMonthlylist);
            objLst.Add(workOrderQty);

            JsonResult res = Json(objLst);
            return res;
        }

        public JsonResult GetMonthlyPlanDetails(long monthlyPlanID)
        {
            prodPlanService = ServiceFactory.GetInstance().ProductionPlanService;
            var monthlylanObeject = prodPlanService.GetMonthlyPlanDetails(monthlyPlanID);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(monthlylanObeject);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"Planid\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = customerpo.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }
    }
    //public class NameValuePair
    //{
    //    public string Name { get; set; }
    //    public int ID { get; set; }
    //}
}
