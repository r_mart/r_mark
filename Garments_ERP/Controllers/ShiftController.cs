﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ShiftController : SuperController
    {
        //
        // GET: /Shift/
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        ShiftService shiftservice = ServiceFactory.GetInstance().ShiftService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Shift";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = shiftservice.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Shift";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            return View();
        }

        [HttpPost]
        public ActionResult Create(M_ShiftMasterEntity model)
        {
            try
            {
                model.CREATED_ON = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.CREATED_BY = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);
                model.IS_ENABLED = true;
                long insertedid = shiftservice.Insert(model);
                if (insertedid > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Shift";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_ShiftMasterEntity model = new M_ShiftMasterEntity();
            model = shiftservice.getbyid(id);
            if (model.DepartmentId != null)
            {
                ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)model.DepartmentId);
            }
            else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(M_ShiftMasterEntity model)
        {
            try
            {
                model.UPDATED_ON = DateTime.Now;

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.UPDATED_BY = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                model.IS_ENABLED = true;
                bool insertedid = shiftservice.Update(model);
                if (insertedid == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
            
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeleteShift(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = shiftservice.Delete(Id);
            return Json(ISDeleted);
        }

    }
}
