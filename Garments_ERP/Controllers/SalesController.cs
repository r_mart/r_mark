﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.Accounts;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class SalesController : Controller
    {
        //
        // GET: /Sales/
        EmployeeService employeeservice = ServiceFactory.GetInstance().EmployeeService;
        PaymentTermService paymenttermservice = ServiceFactory.GetInstance().PaymentTermService;
        CounterSalesService countersalesservice = ServiceFactory.GetInstance().CounterSalesService;
        DiscountTypeService discountTypeService = ServiceFactory.GetInstance().DiscountTypeService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Sales";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            List<Acc_Voucher_Entity> entityList = new List<Acc_Voucher_Entity>();
            entityList = countersalesservice.Get_AllCountersales(branchid);
            return View(entityList);
        }
        [RBAC]
        public ActionResult CounterSales()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            int userid = Convert.ToInt32(collection.Id);
            Acc_Voucher_Customer_Details_Entity entity = new Acc_Voucher_Customer_Details_Entity();
            entity.Created_By = userid;
            entity.Branch_Id = branchid;
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "Id", "Name", 0);
            ViewBag.paymenttermlist = DropDownList<M_PaymentTermMasterEntity>.LoadItems(paymenttermservice.get(), "Id", "PaymentTerm", 0);
            return View(entity);
        }
        public JsonResult Get_NameContact(string MyJson)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            var entityList = countersalesservice.Get_NameContact(comid, branchid, MyJson);
            return Json(entityList);
        }
        public JsonResult Get_ItemsWithHSN(string MyJson)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            var entityList = countersalesservice.Get_ItemsWithHSN(comid, branchid, MyJson);
            return Json(entityList);
        }
        public JsonResult Get_DiscountType()
        {
            var list = discountTypeService.get().ToList();
            return Json(list);
        }
        [HttpPost]
        public Int32 SaveCounterSales(Sales_Entity salesEntities)
        {
            if (salesEntities.Voucher.Voucher_ID == 0)
            {
                salesEntities.Voucher.Voucher_Date = DateTime.Now;
                string[] csno = countersalesservice.getnextcountersalesno().Split('-');
                string csno_ = csno[0] + "-" + csno[1] + "-" + csno[2] + "-" + csno[3] + "-" + csno[4];
                salesEntities.Voucher.Voucher_No = csno_;
            }
            return countersalesservice.SaveCounterSales(salesEntities.Voucher, salesEntities.VoucherItems, salesEntities.Voucher_Customer);
        }
        [RBAC]
        public ActionResult delete(int id)
        {
            bool delete = countersalesservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
        public ActionResult EditCounterSales(int v_Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            int userid = Convert.ToInt32(collection.Id);
            Acc_Voucher_Customer_Details_Entity entity = new Acc_Voucher_Customer_Details_Entity();
            entity.Created_By = userid;
            entity.Branch_Id = branchid;
            entity.Voucher_ID = v_Id;

            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "Id", "Name", 0);
            ViewBag.paymenttermlist = DropDownList<M_PaymentTermMasterEntity>.LoadItems(paymenttermservice.get(), "Id", "PaymentTerm", 0);
            return View(entity);
        }
        public JsonResult Get_Countersales_ById(int v_Id, int branchid)
        {
            Sales_Entity salesEntities = new Sales_Entity();
            salesEntities = countersalesservice.Get_Countersales_ById(v_Id, branchid);
            return Json(salesEntities);
        }
    }
}
