﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using Garments_ERP.Filters;
using Garments_ERP.Entity.ProductionEntities;
using System.Xml;
using Newtonsoft.Json;

namespace Garments_ERP.Controllers
{
    public class WorkOrderController : SuperController
    {
        //
        // GET: /WorkOrder/
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        WorkorderService woservice = ServiceFactory.GetInstance().WorkorderService;
        CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        SegmentTypeService segmenttypeservice = ServiceFactory.GetInstance().SegmentTypeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;
        QuotationService quotationservice = ServiceFactory.GetInstance().QuotationService;
        BillOfMaterialService bomservice = ServiceFactory.GetInstance().BillOfMaterialService;
        ProcessCycleService proccycleservice = ServiceFactory.GetInstance().ProcessCycleService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;


       [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Work Order";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Work Order";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            string[] wono = woservice.getnextWOno().Split('-');
            ViewBag.wono = wono[0] + "-" + wono[1] + "-" + wono[2] + "-" + wono[3] + "-XXXX";
            
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                entity.WorkOrderNo = frm["wono"];
                entity.StyleID = Convert.ToInt64(frm["styleid"]);

                entity.Date = DateTime.Now;
                entity.CustomerID = Convert.ToInt64(frm["customerid"]);
                entity.Comment = frm["comment"];

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ApproveDate = appdate;

                entity.RequiredQty = Convert.ToDecimal(frm["totalqty"]);
                if (frm["worevno"] != "")
                    entity.RevisionNo = Convert.ToDecimal(frm["worevno"]);
                entity.POID = Convert.ToInt64(frm["porefno"]);


                entity.Approved = Convert.ToInt16(frm["approvalstatus"]);
                // entity.CreatedDate=DateTime.Now;approvalstatus
                string sizeliststr = frm["sizeliststr"].ToString();
                string[] sizearr = sizeliststr.Split('#');
                List<MFG_WORK_ORDER_DETAILEntity> detaillist = new List<MFG_WORK_ORDER_DETAILEntity>();
                foreach (var size_ in sizearr)
                {
                    string[] sizeitem = size_.Split('~');
                    MFG_WORK_ORDER_DETAILEntity sizeentity = new MFG_WORK_ORDER_DETAILEntity();

                    sizeentity.SizeID = Convert.ToInt64(sizeitem[3]);
                    if (sizeitem[1] != "")
                    {
                        //  sizeentity = Convert.ToDecimal(sizeitem[1]);
                    }
                    if (sizeitem[2] != "")
                    {
                        var qty = Convert.ToDecimal(sizeitem[2]);
                        sizeentity.Quantity = Convert.ToInt32(qty);
                    }

                    //sizeentitylist.Add(sizeentity);
                    detaillist.Add(sizeentity);


                }
                entity.workorderdetaillist = detaillist;

                var resultid = woservice.Insert(entity);
                if (resultid > 0)
                {

                    TempData["alertwomsg"] = "Record got Inserted";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertpomsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }

            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Create2()
        {
            ViewBag.title = "Work Order";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                string[] wono = woservice.getnextWOno().Split('-');
                ViewBag.wono = wono[0] + "-" + wono[1] + "-" + wono[2] + "-" + wono[3] + "-XXXX";
                ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
                ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Create2(FormCollection frm)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
            var _hdnquotationid = Convert.ToInt32(frm["hdnquotationno"]);
            if (_hdnquotationid > 0)
            {
                entity.quotationdetails = quotationservice.getbyid(_hdnquotationid);
                entity.bomdetails = bomservice.GetByEnquiry(_hdnquotationid);
            }
            long _enquiryid = entity.quotationdetails.EnquiryID;

            int _processcycleid = Convert.ToInt32(entity.bomdetails.Prooce_Cycle_id);
            if (_processcycleid != 0)
            {
                entity.processtranlist = proccycleservice.Getbycycleid(_processcycleid);
            }

            try
            {
                entity.WorkOrderNo = frm["wono"];
                entity.StyleID = Convert.ToInt64(frm["hstyleid"]);
                entity.EnquiryID = _enquiryid;
                entity.ProcessCycleID = _processcycleid;
                entity.Date = DateTime.Now;
                entity.CustomerID = Convert.ToInt64(frm["customerid"]);

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ApproveDate = DateTime.Now;

                entity.RequiredQty = Convert.ToDecimal(frm["htotalqty"]);
                if (frm["worevno"] != "")
                    entity.RevisionNo = Convert.ToDecimal(frm["worevno"]);
                entity.POID = Convert.ToInt64(frm["porefno"]);

                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.Comment = Convert.ToString(frm["comment"]);

                roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.Approved = Convert.ToString(frm["approvalstatus"]) == "1" ? 1 : 0;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.ApproveDate = DateTime.Now;
                }
                else
                {
                    entity.Approved = 0;
                    entity.IsStatus = 2;
                }

                // entity.CreatedDate=DateTime.Now;approvalstatus
                string sizeliststr = frm["sizeliststr"].ToString();
                string[] sizearr = sizeliststr.Split('#');
                List<MFG_WORK_ORDER_DETAILEntity> detaillist = new List<MFG_WORK_ORDER_DETAILEntity>();
                foreach (var size_ in sizearr)
                {
                    if (size_ != "")
                    {
                        string[] sizeitem = size_.Split('~');
                        MFG_WORK_ORDER_DETAILEntity sizeentity = new MFG_WORK_ORDER_DETAILEntity();
                        sizeentity.ItemId = Convert.ToInt64(sizeitem[2]);
                        sizeentity.Attribute_ID = Convert.ToString(sizeitem[6]);
                        sizeitem[7] = Convert.ToString(sizeitem[7]).Substring(0, 1) == "," ? Convert.ToString(sizeitem[7]).Substring(1) : sizeitem[7];
                        sizeentity.Attribute_Value_ID = Convert.ToString(sizeitem[7]);
                        sizeentity.Quantity = Convert.ToInt64(sizeitem[8]);
                        sizeentity.BalanceQty = Convert.ToDecimal(sizeitem[9]);
                        sizeentity.ProductionQty = Convert.ToDecimal(sizeitem[10]);
                        sizeentity.rate = Convert.ToInt64(sizeitem[11]);
                        sizeentity.SizeID = Convert.ToInt64(sizeitem[12]);
                        sizeentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        sizeentity.BranchId = Convert.ToInt32(collection.BranchId);
                        detaillist.Add(sizeentity);
                    }
                }
                entity.workorderdetaillist = detaillist;
                var resultid = woservice.Insert(entity);
                if (resultid > 0)
                {
                    foreach (var item in entity.processtranlist)
                    {
                        MFG_WorkOrderProcessDetailsEntity detailentity = new MFG_WorkOrderProcessDetailsEntity();
                        detailentity.WO_ID = resultid;
                        detailentity.ProcessID = item.M_PROCESS_entity.Process_Id;
                        detailentity.ProcessInchargeID = item.M_PROCESS_entity.Process_Incharge_ID;
                        detailentity.IsActive = true;
                        detailentity.CreatedDate = DateTime.Now;
                        detailentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        detailentity.BranchId = Convert.ToInt32(collection.BranchId);
                        var WOPID = woservice.InsertWOProcesses(detailentity);
                    }
                    bool status = false;
                    status = woservice.InsertWOProcessMaterial(resultid);

                    TempData["alertmsg"] = "Work Order Created Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error! Something Went Wrong.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }

            catch (Exception ex)
            {
                TempData["alertmsg"] = "Error! Please Check Log";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            return View();
        }
        
        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Work Order";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                int CBS = 1;//Customer ID
                var rawitemlist = itemservice.get().Where(x => x.ItemCategoryid == 1).ToList();
                var record = woservice.GetbyId(id);


                ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)record.customerpo.styledetailentity.Segmenttypeid);
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", (long)record.customerpo.styledetailentity.Itemid);
                ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)record.customerpo.styledetailentity.Styleid);
                ViewBag.customerlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", (long)record.CustomerID);

                return View(record);
            }
        }
        [HttpPost]

        public ActionResult Edit(FormCollection frm)
        {
            MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            try
            {
                entity.WorkOrderID = Convert.ToInt64(frm["WorkOrderID"]);
                entity.RequiredQty = Convert.ToDecimal(frm["totalqty"]);

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ApproveDate = DateTime.Now;

                entity.RequiredQty = Convert.ToDecimal(frm["totalqty"]);
                if (frm["worevno"] != "")
                    entity.RevisionNo = Convert.ToDecimal(frm["worevno"]);

                entity.UpdatedBy = Convert.ToInt64(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.Comment = Convert.ToString(frm["comment"]);
                roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.Approved = Convert.ToString(frm["approvalstatus"]) == "1" ? 1 : 0;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.ApproveDate = DateTime.Now;
                }
                else
                {
                    entity.Approved = 0;
                    entity.IsStatus = 2;
                }

                entity.UpdatedDate = DateTime.Now;
                string sizeliststr = frm["sizeliststr"].ToString();
                string[] sizearr = sizeliststr.Split('#');
                List<MFG_WORK_ORDER_DETAILEntity> detaillist = new List<MFG_WORK_ORDER_DETAILEntity>();
                foreach (var size_ in sizearr)
                {
                    if (size_ != "")
                    {
                        string[] sizeitem = size_.Split('~');
                        MFG_WORK_ORDER_DETAILEntity sizeentity = new MFG_WORK_ORDER_DETAILEntity();
                        sizeentity.Attribute_ID = Convert.ToString(sizeitem[6]);
                        sizeitem[7] = Convert.ToString(sizeitem[7]).Substring(0, 1) == "," ? Convert.ToString(sizeitem[7]).Substring(1) : sizeitem[7];
                        sizeentity.Attribute_Value_ID = Convert.ToString(sizeitem[7]);
                        sizeentity.Quantity = Convert.ToInt64(sizeitem[8]);
                        sizeentity.BalanceQty = Convert.ToDecimal(sizeitem[9]);
                        sizeentity.ProductionQty = Convert.ToDecimal(sizeitem[10]);
                        sizeentity.rate = Convert.ToInt64(sizeitem[11]);
                        sizeentity.SizeID = Convert.ToInt64(sizeitem[12]);
                        sizeentity.ID = Convert.ToInt64(sizeitem[0]);
                        detaillist.Add(sizeentity);
                    }
                }
                entity.workorderdetaillist = detaillist;
              
                var updated = woservice.Update(entity);
                if (updated)
                {
                    TempData["alertmsg"] = "Record Update Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.Message);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");

        }

        
        public JsonResult delete(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);
            var deleted = woservice.Delete(id, Uid);
            return Json(deleted);

        }

        /// <summary>
        /// Created By : Shrikant M
        /// Created Date : 10-02-2018
        /// Purpose : get custmer's workOrder
        /// </summary>
        /// <param name="custid"></param>
        /// <returns></returns>
        public JsonResult GetCustomerWorkOrders(long customerID)
        {
            var list = woservice.GetbyCustomerId(customerID);
            return Json(list);
        }

        public JsonResult GetCompletedWorkOrdersCustomerWorkOrders(long customerID)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var list = woservice.GetCompletedWorkOrdersbyCustomerId(customerID).Where(x=>x.Company_ID==companyid && x.BranchId==branchid && x.IsStatus==3).OrderByDescending(x=>x.WorkOrderID);
            return Json(list);
        }

        public string GetWorkOrdersQuantity(long workOrderID)
        {
            var woQty = woservice.GetWorkOrdersQuantity(workOrderID);
            return woQty;
        }

        
        public JsonResult getWOSEBal(long id)
        {
            var list = woservice.getWOSEBal(id);
            return Json(list);
        }

        public JsonResult getPOListbycustomerid(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var list = woservice.getPOListbycustomerid(id).Where(x => x.IsStatus == 3 && x.Company_ID == companyid && x.BranchId == branchid && x.IsReadmade==0).OrderByDescending(x => x.Id).ToList();
            return Json(list);

        }

        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"WOId\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = customerpo.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        [HttpPost]
        public JsonResult GetItemList(int Model, int IAid)
        {
            var rawitemlist = itemservice.GetByCategoryId(Convert.ToInt64(Model)).ToList();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(rawitemlist);
            return Json(JSONString);
        }

        //Rejected WO Code
        public JsonResult RejectedWO(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = woservice.RejectedWO(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = woservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = woservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("IndexList", list);
            }
            else
            {
                var list = woservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("IndexList", list);
            }

        }
    }
}
