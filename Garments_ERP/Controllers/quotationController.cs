﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using System.IO;
using Garments_ERP.Filters;
using Newtonsoft.Json;
using SelectPdf;
using System.Xml;

namespace Garments_ERP.Controllers
{

    public class QuotationController : SuperController
    {
        //
        // GET: /Quotation/

        QuotationService quotationservice = ServiceFactory.GetInstance().QuotationService;
        // QuotationRawItemDetailService quotationrawitemdetailservice = ServiceFactory.GetInstance().QuotationRawItemDetailService;
        QuotationStyleDetailService quotationstyledetailservice = ServiceFactory.GetInstance().QuotationStyleDetailService;
        QuotationRawItemDetailService quotationrawitemservice = ServiceFactory.GetInstance().QuotationRawItemDetailService;

        CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        ItemSizeService itemsizeservice = ServiceFactory.GetInstance().ItemSizeService;
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        SegmentTypeService segmenttypeservice = ServiceFactory.GetInstance().SegmentTypeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;

        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        CustomerTypeService customertypeservice = ServiceFactory.GetInstance().CustomerTypeService;
        CustomerContactService customercontactservice = ServiceFactory.GetInstance().CustomerContactService;
        CustomerBankService customerbankservice = ServiceFactory.GetInstance().CustomerBankService;
        LedgerAccountServices ledgeraccountservices= ServiceFactory.GetInstance().LedgerAccountServices;
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;

        public JsonResult getcustomerlist()
        {
            int CBS = 1;//Customer ID
            var list = ledgeraccountservices.getCustList(CBS);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

        public JsonResult getItemlist()
        {
            var list = itemservice.getFinishedItem();
            return Json(list);
        }

        public JsonResult getItemcategorylist()
        {
            var list = itemcategoryservice.get();
            return Json(list);
        }

        public JsonResult getItemsubcategorylist()
        {
            var list = itemsubcategoryservice.get();
            return Json(list);
        }

        public JsonResult getSegmenttypelist()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var list = segmenttypeservice.get().Where(x=>x.Company_ID==companyid && x.Branch_ID==branchid).ToList();
            return Json(list);
        }

        public JsonResult getrawitemlist()
        {
            var list = itemservice.get().Where(x => x.ItemCategoryid == 2).ToList();
            return Json(list);
        }

        public JsonResult getstylelist()
        {
            var list = styleservice.get();
            return Json(list);
        }

        public JsonResult getshadelist()
        {
            var list = itemshadeservice.get();
            return Json(list);
        }

        public JsonResult getcolorlist()
        {
            var list = itemcolorservice.get();
            return Json(list);
        }

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Customer Quotation";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = quotationservice.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Customer Quotation";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int CB = 2;//Supplier ID
            string[] quotno = quotationservice.getnextQuotatioNo().Split('-');
            ViewBag.quotno = quotno[0] + "-" + quotno[1] + "-" + quotno[2] + "-" + quotno[3] + "-XXXX";
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            ViewBag.Supplierlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getSupplierList(CB), "Ledger_Id", "Ledger_Name", 0);
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Segmenttype", 0);

            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] rawitemimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            string styleimgpath = string.Empty;
            List<string> rawimglist = new List<string>();
            List<string> styleimglist = new List<string>();
            List<string> AttributeImage = new List<string>();
            try
            {
                if (Request.Files.Count > 0)
                {

                    if (styleimg.Count >0)
                    {
                        foreach (var stfile in styleimg)
                        {
                            if (stfile != null && stfile.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(stfile.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                stfile.SaveAs(path);
                                styleimgpath = newfilename;
                                styleimglist.Add(styleimgpath);
                            }
                        }

                    }
                    if (styleimglist.Count == 0)
                    {
                        if (Convert.ToString(frm["HstyleImageId"]) != "" && Convert.ToString(frm["HstyleImageId"]) != null)
                        {
                            styleimgpath = Convert.ToString(frm["HstyleImageId"]);
                            string[] styleimgesplit = styleimgpath.Split('#');
                            for (int i = 0; i < styleimgesplit.Length; i++)
                            {
                                styleimglist.Add(styleimgesplit[i]);
                            }
                        }

                    }

                    if (rawitemimg != null)
                    {
                        foreach (var rimg in rawitemimg)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                rimg.SaveAs(path);
                                rawimglist.Add(newfilename);

                            }
                        }
                    }
                    if (ItemAttrimages_id != null)
                    {
                        foreach (var rimg in ItemAttrimages_id)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                                string isExistImagepath = Server.MapPath("~/ItemImages/");
                                bool exists = System.IO.Directory.Exists(isExistImagepath);
                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistImagepath);
                                var path = Path.Combine(isExistImagepath, newfilename);
                                rimg.SaveAs(path);
                                AttributeImage.Add(newfilename);
                            }
                        }
                    }
                }

                CRM_QuotationMasterEntity quotentity = new CRM_QuotationMasterEntity();
                quotentity.QuotationNo = frm["quotno"].ToString();
                string quotdate = frm["quotdate"].ToString();
                DateTime quodate = DateTime.ParseExact(quotdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                quotentity.Quotationdate = quodate;
                quotentity.customerid = Convert.ToInt64(frm["customerid"]);
                quotentity.Enquiryrefno = frm["enquiryno"].ToString();
                //quotentity.narration = frm["narration"].ToString();
                quotentity.Addressid = Convert.ToInt64(frm["customeraddress"]);
                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                quotentity.Approvaldate = appdate;
                quotentity.Comment = frm["comment"].ToString();
                quotentity.narration = frm["narration"].ToString();
                quotentity.IsActive = true;
                quotentity.Createddate = DateTime.Now;
                string tentivdatestr = frm["tentivedate"].ToString();
                DateTime tentivedate = DateTime.ParseExact(tentivdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                quotentity.Tentivedate = tentivedate;
                quotentity.IsReadmade = Convert.ToString(frm["hidIsReadyMate"]) == "" ? 0 : Convert.ToInt32(frm["hidIsReadyMate"]);

                var sampling = frm["sam"].ToString();
                if (sampling == "yes")
                    quotentity.Sampling = true;
                else
                    quotentity.Sampling = false;
                

                quotentity.EnquiryID= Convert.ToInt32(frm["enquiryrefno"]);
                quotentity.SalePrice = Convert.ToDecimal(frm["saleprice"]);
                quotentity.Costprice = Convert.ToDecimal(frm["costprice"]);
                string subtot = Convert.ToString(frm["Hsubtotal"]);
                quotentity.Subtotal = Convert.ToDecimal(subtot);
                string tax = Convert.ToString(frm["GSTCharge"]);
                quotentity.Tax = Convert.ToDecimal(tax);
                string tot = Convert.ToString(frm["Htotal"]);
                quotentity.Total = Convert.ToDecimal(tot);
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                quotentity.CreatedBy = Convert.ToInt32(collection.Id);
                quotentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                quotentity.BranchId = Convert.ToInt32(collection.BranchId);
                var GrandTotal = frm["Grandtotal"];
                quotentity.GrandTotal = GrandTotal == "" ? 0 : Convert.ToDecimal(GrandTotal);

                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    quotentity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    quotentity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    quotentity.Approvaldate = DateTime.Now;
                }
                else
                {
                    quotentity.Approvalstatus = false;
                    quotentity.IsStatus = 2;
                }
                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10011; //Customer QUO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            oclist.Add(ent);

                        }
                    }
                    quotentity.POOtherChargeEntity = oclist;
                }


                long id = quotationservice.Insert(quotentity);
                if (id > 0)
                {
                    //code to save style detail
                    // var styleitemstrlist = frm["styleliststr"].ToString();
                    var styleitemstrlist = Convert.ToString(frm["styleliststr"]);
                    string[] stylearr = styleitemstrlist.Split('#');
                    //segment  item style brand stydesc color shade image reqqty sizeratio 
                    // string[] stylearr = styleitemstrlist.Split('#');
                    //  foreach (var arritem in stylearr)
                    //  {
                    // string[] styleitem = arritem.Split('~');
                    int ct = 0;
                    foreach (var arritem in stylearr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            CRM_QuotationStyleDetailEntity styleentity = new CRM_QuotationStyleDetailEntity();

                            //segment  item style brand stydesc color shade image reqqty sizeratio 
                            styleentity.Quotationid = id;
                            styleentity.Segmenttypeid = Convert.ToInt32(frm["segmenttypeid"]);
                            styleentity.Itemid = Convert.ToInt32(frm["itemid"]);
                            styleentity.Styleid = Convert.ToInt32(frm["styleid"]);
                            styleentity.Brand = frm["brand"].ToString();
                            styleentity.Styledesc = frm["styledesc"].ToString();
                            styleentity.Styleno = frm["styleno"].ToString();
                            styleentity.Totalqty = Convert.ToInt32(frm["reqqty"]);
                            styleentity.Attribute_ID = Convert.ToString(styleitem[6]);
                            styleitem[7] = Convert.ToString(styleitem[7]).Substring(0, 1) == "," ? Convert.ToString(styleitem[7]).Substring(1) : styleitem[7];
                            styleentity.Attribute_Value_ID = Convert.ToString(styleitem[7]);
                            styleentity.rate = Convert.ToDecimal(styleitem[9]);
                            styleentity.Sizeid = Convert.ToInt64(styleitem[10]);
                            styleentity.Createdby = Convert.ToInt32(collection.Id);
                            styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            styleentity.BranchId = Convert.ToInt32(collection.BranchId);
                            styleentity.SellRate = Convert.ToDecimal(styleitem[11]);
                            styleentity.GST = Convert.ToDecimal(styleitem[12]);
                            styleentity.Subtotal = Convert.ToDecimal(styleitem[14]);

                            if (frm["totalqty"].ToString() != "")
                            {
                                styleentity.Totalqty = Convert.ToDecimal(frm["totalqty"]);
                            }
                            if (styleitem[8] != "")
                            {
                                decimal qty = Convert.ToDecimal(styleitem[8]);
                                styleentity.Reqqty = Convert.ToInt32(qty);
                            }

                            styleentity.Styleimage = "";

                            int ICount = Convert.ToInt32(styleitem[15]);
                            int k = 0;
                            var Attrimgarray = AttributeImage.ToList();
                            List<string> imgdata = new List<string>();
                            foreach (var img in Attrimgarray)
                            {
                                if (k != ICount)
                                {
                                    if (img != "")
                                    {
                                        imgdata.Add(img.ToString());
                                        AttributeImage.RemoveAt(0);
                                    }
                                    else
                                    {
                                        AttributeImage.RemoveAt(0);
                                    }
                                }
                                else
                                {
                                    break;
                                }
                                k++;
                            }
                            styleentity.itemimg_list = imgdata;
                            ct++;
                            styleentity.IsActive = true;
                            
                            long insertedid = quotationstyledetailservice.Insert(styleentity);
                        }
                    }

                    foreach(var imgdata in styleimglist)
                    {
                        CRM_QuotationStyleImageEntity imgent = new CRM_QuotationStyleImageEntity();
                        imgent.Quotationid = id;
                        imgent.Quotationimg = Convert.ToString(imgdata);
                        imgent.CreatedBy = Convert.ToInt32(collection.Id);
                        imgent.Company_ID = Convert.ToInt32(collection.Company_ID);
                        imgent.BranchId = Convert.ToInt32(collection.BranchId);
                        long insertedid = quotationstyledetailservice.InsertStyleImage(imgent);

                    }


                    //code to save raw item details
                    var rimgarray = rawimglist.ToArray();
                    var rawitemstrlist = frm["rawitemliststr"].ToString();
                    string[] rawarr = rawitemstrlist.Split('#');
                    int countofitem = 0;
                    foreach (var item in rawarr)
                    {
                        if (item != "")
                        {
                            string[] rawitem = item.Split('~');
                            CRM_QuotationRawItemDetailEntity rawitementity = new CRM_QuotationRawItemDetailEntity();
                            rawitementity.Quotationid = id;
                            rawitementity.Itemcategoryid = Convert.ToInt32(rawitem[1]);
                            rawitementity.Itemsubcategoryid = Convert.ToInt32(rawitem[2]);
                            rawitementity.Itemdesc = rawitem[3].ToString();
                            // rawitementity.Itemid = Convert.ToInt32(rawitem[2]);
                            rawitementity.Supplier = rawitem[4].ToString();
                            rawitementity.Createdate = DateTime.Now;
                            rawitementity.IsActive = true;
                            rawitementity.Createdby = Convert.ToInt32(collection.Id);
                            rawitementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            rawitementity.BranchId = Convert.ToInt32(collection.BranchId);
                            int RICount = Convert.ToInt32(rawitem[5]);
                            int k = 0;
                            var Rawimgarray = rawimglist.ToList();
                            List<string> rawimgdata = new List<string>();
                            if (Rawimgarray.Count > 0 && RICount>0)
                            {
                                foreach (var img in Rawimgarray)
                                {
                                    if (k != RICount)
                                    {
                                        if (img != "")
                                        {
                                            rawimgdata.Add(img.ToString());
                                            rawimglist.RemoveAt(0);
                                        }
                                        else
                                        {
                                            rawimglist.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    k++;
                                }
                            }
                            else
                            {
                                string[] RImgList = Convert.ToString(rawitem[6]).Split(',');
                                if(RImgList.Length>0)
                                {
                                    foreach(var img in RImgList)
                                    {
                                        if (img != "")
                                        {
                                            rawimgdata.Add(img.ToString());
                                        }
                                    }
                                }
                            }

                            rawitementity.rawitemimg_list = rawimgdata;
                            countofitem++;
                            //long rawitemid = enquiryrawitemservice.Insert(rawitementity);
                            long rawitemid = quotationrawitemservice.Insert(rawitementity);
                        }
                        
                    }


                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }


        //New Code Add Update Enquery
        [HttpPost]
        public ActionResult AddUpdate(List<HttpPostedFileBase> Style_Img, List<HttpPostedFileBase> RawItemImg, string data, string RawItemData)
        {
            long id = 0;
            try
            {
                string styleimgpath = string.Empty;
                List<string> styleimglist = new List<string>();
                List<string> rawimglist = new List<string>();
                List<string> AttributeImage = new List<string>();
                if (Request.Files.Count > 0)
                {
                    foreach (var stfile in Style_Img)
                    {
                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);

                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);
                        }
                    }

                    if (RawItemImg != null)
                    {
                        foreach (var rimg in RawItemImg)
                        {

                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);

                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);

                                var path = Path.Combine(isExistCompanypath, newfilename);
                                rimg.SaveAs(path);
                                rawimglist.Add(newfilename);
                            }
                        }
                    }
                }


                List<CRM_QuotationRawItemDetailEntity> RawItem = JsonConvert.DeserializeObject<List<CRM_QuotationRawItemDetailEntity>>(RawItemData);
                CRM_QuotationMasterEntity Entity = JsonConvert.DeserializeObject<CRM_QuotationMasterEntity>(data);
                Entity.IsActive = true;
                Entity.Createddate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                Entity.CreatedBy = Convert.ToInt32(collection.Id);
                Entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                Entity.BranchId = Convert.ToInt32(collection.BranchId);
                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    Entity.IsStatus = Entity.Approvalstatus == true ? 3 : 5;
                    Entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    Entity.IsStatus = 2;
                }
                id = quotationservice.AddUpdate(styleimglist, rawimglist, Entity, RawItem);

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
            }
            return Json(id);

        }

        public JsonResult editrawitem(long id, long itemcatid, int itemsubid, string itemdesc, string supplier)
        {
            CRM_QuotationRawItemDetailEntity rawentity = new CRM_QuotationRawItemDetailEntity();
            rawentity.Id = id;
            //rawentity.Enquiryid = enquiryid;
            rawentity.Itemcategoryid = itemcatid;
            rawentity.Itemsubcategoryid = itemsubid;
            rawentity.Itemdesc = itemdesc;
            rawentity.Supplier = supplier;
            bool isupdate = quotationrawitemservice.Update(rawentity);
            return Json(isupdate);

        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Customer Quotation";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int CBS = 1;//Customer ID
            int CB = 2;//Supplier ID
            CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
            entity = quotationservice.getbyid(id);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.style.Segmenttypeid);
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            return View(entity);

        }


        [HttpPost]
        public ActionResult Edit(FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] rawitemimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            List<string> rawimglist = new List<string>();
            List<string> AttributeImage = new List<string>();
            List<CRM_QuotationStyleImageEntity> styleimglist = new List<CRM_QuotationStyleImageEntity>();
            List<string> styleimglistold = new List<string>();
            string styleimgpath = "";
            try
            {

                if (Request.Files.Count > 0)
                {
                    if (styleimg != null)
                    {
                        foreach (var stfile in styleimg)
                        {
                            CRM_QuotationStyleImageEntity imgentity = new CRM_QuotationStyleImageEntity();
                            if (stfile != null && stfile.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(stfile.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                stfile.SaveAs(path);
                                imgentity.Quotationimg = newfilename;
                                styleimglist.Add(imgentity);
                                //  styleimglist.Add(newfilename);
                            }
                        }
                    }

                    if (rawitemimg != null)
                    {
                        foreach (var rimg in rawitemimg)
                        {

                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                rimg.SaveAs(path);
                                rawimglist.Add(newfilename);
                                //styleimgpath = newfilename;
                                //styleimglist.Add(styleimgpath);
                            }
                        }
                    }
                    if (ItemAttrimages_id != null)
                    {
                        foreach (var rimg in ItemAttrimages_id)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                                string isExistImagepath = Server.MapPath("~/ItemImages/");
                                bool exists = System.IO.Directory.Exists(isExistImagepath);
                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistImagepath);
                                var path = Path.Combine(isExistImagepath, newfilename);
                                rimg.SaveAs(path);
                                AttributeImage.Add(newfilename);
                            }
                        }
                    }

                }

                var quotid = Convert.ToInt64(frm["Id"]);

                CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
                entity.Id = quotid;
                entity.narration = frm["narration"].ToString();
                entity.Comment = frm["comment"].ToString();
                var rvalue = frm["sample"];

                if (rvalue == "yes")
                    entity.Sampling = true;
                else
                    entity.Sampling = false;

                entity.imgelist = styleimglist;
                entity.Costprice = Convert.ToDecimal(frm["costprice"]);
                entity.SalePrice = Convert.ToDecimal(frm["saleprice"]);
                entity.Subtotal = Convert.ToDecimal(frm["subtotal"]);
                entity.Tax = Convert.ToDecimal(frm["GSTCharge"]);
                entity.Total = Convert.ToDecimal(frm["total"]);
                entity.Approvaldate = DateTime.Now;
                if (roleid != 4)
                {
                    entity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.IsStatus = 2;
                }
                entity.UpdatedBy = userid;
                entity.Company_ID = companyid;
                entity.BranchId = branchid;
                var GrandTotal = frm["Grandtotal"];
                entity.GrandTotal = GrandTotal == "" ? 0 : Convert.ToDecimal(GrandTotal);
                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10010; //Supplier QUO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            ent.Id = Convert.ToInt64(data[5]);
                            oclist.Add(ent);

                        }
                    }
                    entity.POOtherChargeEntity = oclist;
                }


                bool isenqupdate = quotationservice.Update(entity);


                var styleitemstrlist = Convert.ToString(frm["styleliststr"]);
                //id type sizeper sizeqty
                string[] stylearr = styleitemstrlist.Split('#');
                foreach (var arritem in stylearr)
                {
                    if (arritem != "")
                    {
                        string[] styleitem = arritem.Split('~');
                        CRM_QuotationStyleDetailEntity styleentity = new CRM_QuotationStyleDetailEntity();

                        styleentity.Quotationid = quotid;
                        styleentity.Id = Convert.ToInt64(styleitem[0]);
                        styleentity.Segmenttypeid = Convert.ToInt32(styleitem[1]);
                        styleentity.Itemid = Convert.ToInt32(styleitem[3]);
                        styleentity.Styleid = Convert.ToInt32(styleitem[2]);
                        styleentity.Brand = frm["brand"].ToString();
                        styleentity.Styledesc = frm["styledesc"].ToString();
                        styleentity.Styleno = frm["styleno"].ToString();
                        styleentity.Attribute_ID = Convert.ToString(styleitem[7]);
                        styleitem[8] = Convert.ToString(styleitem[8]).Substring(0, 1) == "," ? Convert.ToString(styleitem[8]).Substring(1) : styleitem[8];
                        styleentity.Attribute_Value_ID = Convert.ToString(styleitem[8]);
                        styleentity.rate = Convert.ToDecimal(styleitem[10]);
                        styleentity.Sizeid = Convert.ToInt64(styleitem[11]);
                        styleentity.SellRate = Convert.ToDecimal(styleitem[12]);
                        styleentity.GST = Convert.ToDecimal(styleitem[13]);
                        styleentity.Subtotal = Convert.ToDecimal(styleitem[15]);

                        styleentity.Updatedby = userid;
                        styleentity.Updateddate = DateTime.Now;

                        if (frm["totalqty"].ToString() != "")
                        {
                            styleentity.Totalqty = Convert.ToDecimal(frm["reqqty"]);
                        }
                        if (styleitem[9] != "")
                        {
                            decimal qty = Convert.ToDecimal(styleitem[9]);
                            styleentity.Reqqty = Convert.ToInt32(qty);
                        }
                        int ICount = Convert.ToInt32(styleitem[16]);
                        int k = 0;
                        List<string> imgdata = new List<string>();
                        var Attrimgarray = AttributeImage.ToList();
                        foreach (var img in Attrimgarray)
                        {
                            if (k != ICount)
                            {
                                if (img != "")
                                {
                                    imgdata.Add(img.ToString());
                                    AttributeImage.RemoveAt(0);
                                }
                                else
                                {
                                    AttributeImage.RemoveAt(0);
                                }
                            }
                            else
                            {
                                break;
                            }
                            k++;
                        }
                        styleentity.itemimg_list = imgdata;

                        styleentity.IsActive = true;
                        styleentity.Updateddate = DateTime.Now;
                        bool isupdatestyle = quotationstyledetailservice.Update(styleentity);
                    }
                }
                
                var rawitemliststr = frm["rawitemliststr"].ToString();


                //code to update rawitem
                string[] rawitemstr = rawitemliststr.Split('#');
                string[] rawimgarray = rawimglist.ToArray();
                int count = 0;
                foreach (var rawitem in rawitemstr)
                {
                    if (rawitem != "")
                    {
                        string[] raw = rawitem.Split('~');
                        //id itemcat itemsub itemdesc supplier
                        CRM_QuotationRawItemDetailEntity rawentity = new CRM_QuotationRawItemDetailEntity();
                        rawentity.Id = Convert.ToInt64(raw[0]);
                        rawentity.Quotationid = quotid;
                        rawentity.Itemcategoryid = Convert.ToInt64(raw[1]);
                        rawentity.Itemsubcategoryid = Convert.ToInt32(raw[2]);
                        rawentity.Itemdesc = raw[3].ToString();
                        rawentity.Supplier = raw[4].ToString();
                        int RICount = Convert.ToInt32(rawitem[5]);
                        int k = 0;
                        var Rawimgarray = rawimglist.ToList();
                        List<string> rawimgdata = new List<string>();
                        foreach (var img in Rawimgarray)
                        {
                            if (k != RICount)
                            {
                                if (img != "")
                                {
                                    rawimgdata.Add(img.ToString());
                                    rawimglist.RemoveAt(0);
                                }
                                else
                                {
                                    rawimglist.RemoveAt(0);
                                }
                            }
                            else
                            {
                                break;
                            }
                            k++;
                        }
                        rawentity.rawitemimg_list = rawimgdata;
                        rawentity.Updatedby = userid;
                        rawentity.Createdby = userid;
                        rawentity.Updateddate = DateTime.Now;
                        rawentity.Createdate = DateTime.Now;
                        if (rawentity.Id != 0)
                        {
                            bool updated = quotationrawitemservice.Update(rawentity);
                        }
                        else
                        {
                            //long rinsertid = enquiryrawitemservice.Insert(rawentity);
                            long rinsertid = quotationrawitemservice.Insert(rawentity);
                        }

                        count++;
                    }
                }


                TempData["alertmsg"] = "Record Update Successfully";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
           
        }

        public JsonResult getcustomerdetails(int id)
        {
            var list = ledgeraccountservices.getAddInfo(id);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

        public JsonResult getquotnobycustomerid(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var list = quotationservice.getquotationnobycustomer(id, Company_ID, BranchId);
            return Json(list);

        }
        public JsonResult getquotationdetails(long id)
        {
            var entity = quotationservice.getbyid(id);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }
        public ActionResult InvoicePrint()
        {
            return View();
        }
        [RBAC]
        public JsonResult Delete(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            var result = quotationservice.Delete(id, Userid);
            return Json(result);
        }
        public JsonResult deleterawitem(long id)
        {
            var isdelete = quotationrawitemservice.deleterawitem(id);
            return Json(isdelete);

        }


        [HttpPost]
        public JsonResult getGSTTaxlist()
        {
            var list = quotationservice._getGSTTaxlist();
            string JSONString = string.Empty;            
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

        [RBAC]
        public ActionResult QuotationReport(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var list = quotationservice.QuoteGet(id);
            return View(list);

        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {

            // read parameters from the webpage
            string htmlString = "<html><head><title>Quotation</title><link rel='stylesheet' href='http://localhost:2423/Content/dist/css/AdminLTE.min.css'><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'></head><body><div class='row'><div class='col-sm-12 text-center'><h3>Quotation</h3></div></div>" + GridHtml + "</body></html>";
            string baseUrl = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            int webPageHeight = 0;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            converter.Options.DisplayFooter = true;
            converter.Options.MarginTop = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginBottom = 10;
            converter.Options.PdfCompressionLevel = PdfCompressionLevel.Best;
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfStandard = PdfStandard.Full;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.DrawBackground = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Print;
            PdfTextSection text = new PdfTextSection(0, 10, "Page: {page_number} of {total_pages} ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;

        }


        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"QtyId\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = quotationstyledetailservice.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }


        //Rejected Quotation Code
        public JsonResult RejectedQuo(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = quotationservice.RejectedQuo(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);

            var list = quotationservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = quotationservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = quotationservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

        public JsonResult GetQuoStyleData(long QuoId)
        {
            var list = quotationservice.GetQuoStyleData(QuoId);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

        public JsonResult GetQuoStyleRawItemData(long QuoId, long styleid)
        {
            var list = quotationservice.GetQuoStyleRawItemData(QuoId, styleid);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }
    }
}
