﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.ProductionServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class FG_RetailController : SuperController
    {
        //
        // GET: /FG_Retail/
        FG_RetailService FG_Retailservice = ServiceFactory.GetInstance().FG_RetailService;
        BillOfMaterialService billofmaterialservice = ServiceFactory.GetInstance().BillOfMaterialService;
        WorkorderService workorderservice = ServiceFactory.GetInstance().WorkorderService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        TransferService transferservice = ServiceFactory.GetInstance().TransferService;
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        QuotationService quotationservice = ServiceFactory.GetInstance().QuotationService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "FG Retail";
            List<FG_Transfer_To_RetailEntity> entity = new List<FG_Transfer_To_RetailEntity>();
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
                int comid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);
                entity = FG_Retailservice.Get(comid, branchid);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Login");
                throw;
            }
            return View(entity);
        }

        [RBAC]
        public ActionResult Create()
        {
            try
            {
                ViewBag.title = "FG Retail";
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Login");
                throw;
            }
            return View();
        }

        [RBAC]
        public JsonResult Get_WorkOrders(string userInp, long CustID)
        {
            if (CustID > 0)
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                int comid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);
                var entityList = FG_Retailservice.Get_WorkOrders(comid, branchid, userInp, CustID);
                return Json(entityList);
            }
            return Json(null);
        }

        [RBAC]
        public JsonResult Get_FGTransfers(string userInp, long woID)
        {
            if (woID > 0)
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                int comid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);
                var entityList = FG_Retailservice.Get_FGTransfers(comid, branchid, userInp, woID);
                return Json(entityList);
            }
            return Json(null);
        }

        [RBAC]
        public JsonResult Get_FGTransferDetails(long fg_transferid)
        {
            if (fg_transferid > 0)
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                int comid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);
                var entityList = FG_Retailservice.Get_FGTransferDetails(comid, branchid, fg_transferid);
                return Json(entityList);
            }
            return Json(null);
        }

        [RBAC]
        [HttpPost]
        public Int64 SaveFG_Retail(FG_Transfer_To_RetailEntity FG_TREntity)
        {
            return FG_Retailservice.SaveFG_Retail(FG_TREntity);

        }

        [RBAC]
        public JsonResult Get_FG_TRDetails(long fg_trid)
        {
            if (fg_trid > 0)
            {
                var entityList = FG_Retailservice.getbyid(fg_trid);
                return Json(entityList);
            }
            return Json(null);
        }

        [RBAC]
        public ActionResult delete(long Id)
        {
            bool delete = FG_Retailservice.Delete(Id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }

        [RBAC]
        public JsonResult getPOdetailsbyid(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);

            List<object> list = new List<object>();
            List<M_BillOfMaterialMasterEntity> BomEnt = new List<M_BillOfMaterialMasterEntity>();
            Base_BillOfMaterialMasterEntity BomBaseEnt = new Base_BillOfMaterialMasterEntity();
            var Podata = customerpo.getbyid(id);
            long Quoid = Podata.QuotationID;
            var Quodata= quotationservice.getbyid(Quoid);
            long Enq = Quodata.EnquiryID;
            BomBaseEnt = billofmaterialservice.getBase(ViewBag.companyid, ViewBag.branchid);
            BomEnt = BomBaseEnt.bomlist.ToList();
            var bomdata = BomEnt.Where(x =>(x.EnquiryId== Enq || x.Poid == id )&& x.IsActive==true).FirstOrDefault();
            list.Add(bomdata.IsReadymate);
            if (bomdata.IsReadymate == 1)
            {
                var GRNNo = FG_Retailservice.GetGRNList(bomdata.BOM_ID);
                list.Add(GRNNo);
            }
            else
            {
                var WONo = workorderservice.Get().Where(x => x.Company_ID == ViewBag.companyid && x.BranchId == ViewBag.branchid && x.IsStatus == 3 && x.POID== id).ToList();
                list.Add(WONo);
            }
            list.Add(Podata);
            return Json(list);
        }


        [RBAC]
        public JsonResult GetGRNData(long id)
        {
            var record = srmgrnservice.getbyid(id);
            return Json(record);
        }

        [RBAC]
        public JsonResult GetFGTransfer(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            var list = transferservice.Get().Where(x=>x.Company_ID== ViewBag.companyid && x.BranchId == ViewBag.branchid && x.RefWorkOrderId== id).ToList();
            return Json(list);
        }
    }

}
