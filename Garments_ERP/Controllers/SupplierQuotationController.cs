﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class SupplierQuotationController : SuperController
    {
        //
        // GET: /SupplierQuotation/
        SupplierPRItemDetailService supplierpritemdetailservice = ServiceFactory.GetInstance().SupplierPRItemDetailService;
        SupplierService supplierservice = ServiceFactory.GetInstance().SupplierService;
        SupplierPRService supplierprservice = ServiceFactory.GetInstance().SupplierPRService;
        SupplierQuotationService supplierquotationservice = ServiceFactory.GetInstance().SupplierQuotationService;
        SupplierQuotationItemDetailService supplierquotationitemdetailservice = ServiceFactory.GetInstance().SupplierQuotationItemDetailService;
        RateIndicatorService rateindicatorservice = ServiceFactory.GetInstance().RateIndicatorService;
        DiscountTypeService discountypeservice = ServiceFactory.GetInstance().DiscountTypeService;
        TaxService taxservice = ServiceFactory.GetInstance().TaxService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;

        [RBAC]
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.title = "Supplier Quotation";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Supplier Quotation";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
            string[] QuotationNo = supplierquotationservice.getnextQuotatioNo().Split('-');
            entity.QuotationNo = QuotationNo[0] + "-" + QuotationNo[1] + "-" + QuotationNo[2] + "-" + QuotationNo[3] + "-XXXX";

            int CBS = 2;//Supplier ID
            ViewBag.supplierlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", 0);
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            ViewBag.prnolist = DropDownList<SRM_PurchaseRequestMasterEntity>.LoadItems(supplierprservice.getList(ViewBag.companyid, ViewBag.branchid), "Id", "PR_No", 0);
            entity.OtherTax = 0;
            entity.TotalCost = 0;
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(SRM_QuotationMasterEntity entity, FormCollection frm)
        {
            try
            {
                //var itemlist = frm["QuotationItemList"].ToString();
                var GrandTotal = frm["Grandtotal"];
                entity.TotalCost = Convert.ToDecimal(frm["total"]);
                entity.GrandTotal = GrandTotal == "" ? 0 : Convert.ToDecimal(GrandTotal);

                string PR_Date = frm["QuotationDate_"];
                DateTime quotationdate = DateTime.ParseExact(PR_Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.QuotationDate = quotationdate;

                string validitydatestr = frm["Validity_"];
                DateTime validitydate = DateTime.ParseExact(validitydatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Validity = validitydate;

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Approvaldate = appdate;

                string tax = Convert.ToString(frm["GSTCharge"]);
                entity.GSTTax = tax == "select" ? 0 : Convert.ToDecimal(tax);
                entity.PR_No = frm["purchaserequestno"].ToString();
                entity.PR_Id = Convert.ToInt32(frm["PRid"].ToString());
                entity.IsActive = true;
                entity.CreatedDate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.Comment = Convert.ToString(frm["comment"]);
                entity.IsReadymade = Convert.ToString(frm["IsReadymate"]) == "" ? 0 : Convert.ToInt32(frm["IsReadymate"]);

                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.Approvalstatus = false;
                    entity.IsStatus = 2;
                }
                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10010; //Supplier QUO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            oclist.Add(ent);

                        }
                    }
                    entity.POOtherChargeEntity = oclist;
                }

                long id = supplierquotationservice.Insert(entity);
                if (id > 0)
                {

                    var itemdetaillist = frm["QuotationItemList"].ToString();
                    long insertedid = 0;
                    //code to split raw items
                    string[] itemarr = itemdetaillist.Split('#');
                    foreach (var arritem in itemarr)
                    {
                        if (arritem != "")
                        {
                            string[] contact = arritem.Split('~');
                            SRM_QuotationItemDetailEntity pritementity = new SRM_QuotationItemDetailEntity();
                            pritementity.QuotationId = id;
                            pritementity.ItemId = Convert.ToInt32(contact[0]);
                            pritementity.Item_Attribute_ID = Convert.ToInt32(contact[1]);
                            pritementity.UnitId = Convert.ToInt32(contact[2]);
                            pritementity.TotalQty = Convert.ToDecimal(contact[3]);
                            pritementity.ItemRate = Convert.ToDecimal(contact[4]);
                            pritementity.TotalPrice = Convert.ToDecimal(contact[5]);
                            pritementity.DiscountTypeId = Convert.ToInt32(contact[6]);
                            pritementity.RateIndicatorId = Convert.ToInt32(contact[7]);
                            pritementity.DiscountRate = Convert.ToDecimal(contact[8]);
                            pritementity.IsActive = true;
                            pritementity.CreatedDate = DateTime.Now;
                            pritementity.CreatedBy = Convert.ToInt32(collection.Id);
                            pritementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            pritementity.BranchId = Convert.ToInt32(collection.BranchId);
                            insertedid = supplierquotationitemdetailservice.Insert(pritementity);
                        }
                    }
                    if (insertedid > 0)
                    {
                        TempData["alertmsg"] = "Supplier Quotation Created Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
            
        }

        [RBAC]
        public JsonResult getPRItemDetails(long id)
        {
            SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
            entity.IsReadymade = supplierprservice.getbyid(id).IsReadymade;
            entity.rawitemlist = supplierpritemdetailservice.getbyid(id);
            entity.rateindicatorlist = rateindicatorservice.get();
            entity.discounttypelist = discountypeservice.get();
            return Json(entity);
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Supplier Quotation";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
            entity = supplierquotationservice.getbyid(id);
            entity.SRM_QuotationItemDetail = supplierquotationitemdetailservice.getbyquotationid(id);
            entity.rateindicatorlist = rateindicatorservice.get();
            entity.discounttypelist = discountypeservice.get();
            int CBS = 2;//Supplier ID
            if (entity.SupplierId != null)
            {
                ViewBag.supplierlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", (int)entity.SupplierId);
            }
            else { ViewBag.supplierlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", 0); }
            //if (entity.PR_Id != null)
            //{
            //    ViewBag.prnolist = DropDownList<SRM_PurchaseRequestMasterEntity>.LoadItems(supplierprservice.get(), "Id", "PR_No", (int)entity.PR_Id);
            //}
            //else { ViewBag.prnolist = DropDownList<SRM_PurchaseRequestMasterEntity>.LoadItems(supplierprservice.get(), "Id", "PR_No", 0); }
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);

            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(long id, SRM_QuotationMasterEntity entity, FormCollection frm)
        {
            try
            {
                //entity.TotalCost = Convert.ToDecimal(frm["GrandTotal"]);
                //string PR_Date = frm["QuotationDate"];
                //DateTime quotationdate = DateTime.ParseExact(PR_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //entity.QuotationDate = quotationdate;
                //entity.PR_No = frm["purchaserequestno"].ToString();

                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                entity.GSTTax = Convert.ToDecimal(frm["GSTCharge"]);
                entity.IsActive = true;
                entity.UpdatedDate = DateTime.Now;
                entity.Comment = frm["comment"].ToString();

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Approvaldate = appdate;

                if (roleid != 4)
                {
                    entity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.IsStatus = 2;
                }
                entity.UpdatedBy = userid;
                entity.Company_ID = companyid;
                entity.BranchId = branchid;
                var GrandTotal = frm["Grandtotal"];
                entity.TotalCost = Convert.ToDecimal(frm["total"]);
                entity.GrandTotal = GrandTotal == "" ? 0 : Convert.ToDecimal(GrandTotal);
                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10010; //Supplier QUO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            ent.Id = Convert.ToInt64(data[5]);
                            oclist.Add(ent);

                        }
                    }
                    entity.POOtherChargeEntity = oclist;
                }

                bool updateid = supplierquotationservice.Update(entity);
                if (updateid == true)
                {

                    var itemdetaillist = frm["QuotationItemList"].ToString();
                    bool updatedid = false;
                    //code to split raw items
                    string[] itemarr = itemdetaillist.Split('#');
                    foreach (var arritem in itemarr)
                    {
                        if (arritem != "")
                        {
                            string[] contact = arritem.Split('~');
                            SRM_QuotationItemDetailEntity pritementity = new SRM_QuotationItemDetailEntity();
                            pritementity.QuotationId = id;
                            pritementity.ItemId = Convert.ToInt32(contact[0]);
                            pritementity.Item_Attribute_ID = Convert.ToInt32(contact[1]);
                            pritementity.UnitId = Convert.ToInt32(contact[2]);
                            pritementity.TotalQty = Convert.ToDecimal(contact[3]);
                            pritementity.ItemRate = Convert.ToDecimal(contact[4]);
                            pritementity.TotalPrice = Convert.ToDecimal(contact[5]);
                            pritementity.DiscountTypeId = Convert.ToInt32(contact[6]);
                            pritementity.RateIndicatorId = Convert.ToInt32(contact[7]);
                            pritementity.DiscountRate = Convert.ToDecimal(contact[8]);
                            pritementity.Id = Convert.ToInt32(contact[9]);
                            pritementity.IsActive = true;
                            pritementity.CreatedDate = DateTime.Now;
                            pritementity.UpdatedBy = userid;
                            updatedid = supplierquotationitemdetailservice.Update(pritementity);
                        }
                    }
                    if (updatedid == true)
                    {
                        TempData["alertmsg"] = "Supplier Quotation Updated Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        //Delete Supplier Quotation
        [RBAC]
        [HttpPost]
        public JsonResult DeleteQuotation(long Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            bool ISDeleted = false;
            ISDeleted = supplierquotationservice.Delete(Id, Userid);
            return Json(ISDeleted);
        }

        [RBAC]
        public ActionResult QuotationReport(long id)
        {

            var list = supplierquotationservice.QuoteGet(id);
            return View(list);

        }

        [RBAC]
        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {

            // read parameters from the webpage
            string htmlString = "<html><head><title>Quotation</title><link rel='stylesheet' href='http://localhost:2423/Content/dist/css/AdminLTE.min.css'><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'></head><body><div class='row'><div class='col-sm-12 text-center'><h3>Quotation</h3></div></div>" + GridHtml + "</body></html>";
            string baseUrl = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            int webPageHeight = 0;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            converter.Options.DisplayFooter = true;
            converter.Options.MarginTop = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginBottom = 10;
            converter.Options.PdfCompressionLevel = PdfCompressionLevel.Best;
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfStandard = PdfStandard.Full;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.DrawBackground = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Print;
            PdfTextSection text = new PdfTextSection(0, 10, "Page: {page_number} of {total_pages} ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;

        }

        //Rejected Quotation Code
        [RBAC]
        public JsonResult RejectedQuo(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = supplierquotationservice.RejectedQuo(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = supplierquotationservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = supplierquotationservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = supplierquotationservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

    }
}
