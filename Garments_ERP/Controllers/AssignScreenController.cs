﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class AssignScreenController : SuperController
    {
        //
        // GET: /AssignScreen/
        RoleService RoleSer = ServiceFactory.GetInstance().RoleService;
        AssignScreenService AssignScrSer= ServiceFactory.GetInstance().AssignScreenService;

        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Assign Screen";

            var screenLst = AssignScrSer.get();
            return View(screenLst);
        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Assign Screen";

            return View();
        }

        [RBAC]
        public JsonResult GetScreenData(long RoleId)
        {
            var list = AssignScrSer.GetScreenData(RoleId);
            return Json(list);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                bool status = false;
                long roleid = Convert.ToInt64(frm["Aroleid"]);
                string[] AllData=Convert.ToString(frm["AssignScreendataId"]).Split('#');
                foreach(var data in AllData)
                {
                    if (data != "")
                    {
                        string[] screenData = data.Split('~');
                        status = AssignScrSer.Insert(screenData[0], screenData[1], roleid);
                    }
                }

                if (status == true)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.Message.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        public JsonResult ActiveDeactive(int id, string Status)
        {
            var status = AssignScrSer.ActiveDeactive(id, Status);
            return Json(status);
        }

        [RBAC]
        public JsonResult RoleList()
        {
            var list = RoleSer.get().Where(x => x.IsActive == true);
            return Json(list);
        }
    }
}
