﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class TransferController : SuperController
    {
        //
        // GET: /Transfer/
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        RackService rackservice = ServiceFactory.GetInstance().RackService;
        WorkorderService workorderservice = ServiceFactory.GetInstance().WorkorderService;
        TransferService transferservice = ServiceFactory.GetInstance().TransferService;
        EnquiryStyleDetailService enquirystyledetailservice = ServiceFactory.GetInstance().EnquiryStyleDetailService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "FG";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = transferservice.Get();
            return View(list);
        }

        [HttpPost]
        public JsonResult DeleteTransfer(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = transferservice.Delete(Id);
            return Json(ISDeleted);
        }
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "FG";

            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
            string[] enqno = transferservice.getnextTFNo().Split('-');
            entity.TransferNo = enqno[0] + "-" + enqno[1] + "-" + enqno[2] + "-" + enqno[3] + "-XXXX";

            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(ViewBag.companyid, ViewBag.branchid), "ID", "SHORT_NAME", 0);
            ViewBag.racklist = DropDownList<M_RackMasterEntity>.LoadItems(rackservice.get(), "Id", "Rack", 0);
            ViewBag.workordernolist = DropDownList<MFG_WORK_ORDEREntity>.LoadItems(workorderservice.Get().Where(x=>x.Company_ID== ViewBag.companyid && x.BranchId== ViewBag.branchid && x.IsStatus==3).ToList(), "WorkOrderID", "WorkOrderNo", 0);
            return View(entity);
        }
      
        [RBAC]
        [HttpPost]
        public ActionResult Create(FG_TransferMasterEntity entity, FormCollection frm)
        {
            try
            {

                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                string tfDate = frm["TransferDate"].ToString();
                DateTime tranferdate = DateTime.ParseExact(tfDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.TransferDate = tranferdate;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.IsStatus = 3;
                entity.IsActive = true;
                entity.ItemId = Convert.ToInt64(frm["Itemid"]);
                entity.Item_Attribute_Id = Convert.ToInt32(frm["ItemAttriid"]);
                entity.HSNCode = Convert.ToString(frm["HSNCodeid"]);
                entity.Unit = Convert.ToInt64(frm["Unitid"]);
                entity.WOQty = Convert.ToDecimal(frm["WOQty"]);
                entity.TotBatchQty = Convert.ToDecimal(frm["totBatchQty"]);
                entity.ProducedQty = Convert.ToDecimal(frm["totProducedQty"]);
                entity.BalanceQty = Convert.ToDecimal(frm["totBalancedQty"]);
                
                string sizelist = frm["Bomliststr"].ToString();
                string[] wosizearr = sizelist.Split('#');
                List<FG_TransferDetailEntity> wosizelist = new List<FG_TransferDetailEntity>();
                foreach (var arritem in wosizearr)
                {
                    if (arritem != "")
                    {
                        string[] item = arritem.Split('~');
                        FG_TransferDetailEntity tfdetailentity = new FG_TransferDetailEntity();
                        tfdetailentity.batchlotid = Convert.ToInt64(item[0]);
                        tfdetailentity.BatchLotQty = Convert.ToDecimal(item[1]);
                        tfdetailentity.ProducedQty = Convert.ToDecimal(item[2]);
                        tfdetailentity.BalanceQty = Convert.ToDecimal(item[3]);
                        tfdetailentity.IsQC_Done = Convert.ToString(item[4]);
                        tfdetailentity.Company_ID = companyid;
                        tfdetailentity.BranchId = branchid;
                        tfdetailentity.CreatedBy = userid;
                        wosizelist.Add(tfdetailentity);
                    }
                }
                entity.FG_TransferDetail = wosizelist;
                long insertedid = transferservice.Insert(entity);
                if (insertedid > 0)
                {
                    TempData["alertmsg"] = "FG Transfer Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "FG";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
            entity = transferservice.getbyid(id);
            if (entity.DepartmentId != null)
            {
                ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)entity.DepartmentId);
            }
            else { ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
            if (entity.WarehouseId != null)
            {
                ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(ViewBag.companyid, ViewBag.branchid), "ID", "SHORT_NAME", (int)entity.WarehouseId);
            }
            else { ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(ViewBag.companyid, ViewBag.branchid), "ID", "SHORT_NAME", 0); }
           
            //ViewBag.workordernolist = DropDownList<MFG_WORK_ORDEREntity>.LoadItems(workorderservice.Get(), "WorkOrderID", "WorkOrderNo", 0);
            return View(entity);
        }
      
        [RBAC]
        [HttpPost]
        public ActionResult Edit(long id, FG_TransferMasterEntity entity,FormCollection frm)
        {

            try
            {
                
                entity.UpdatedDate = DateTime.Now;
                entity.IsActive = true;
                string sizelist = frm["sizeliststr"].ToString();
                string[] wosizearr = sizelist.Split('#');
                List<FG_TransferDetailEntity> wosizelist = new List<FG_TransferDetailEntity>();
                foreach (var arritem in wosizearr)
                {
                    if (arritem != "")
                    {
                        string[] item = arritem.Split('~');
                        FG_TransferDetailEntity tfdetailentity = new FG_TransferDetailEntity();
                        tfdetailentity.Id = Convert.ToInt64(item[0]);
                        tfdetailentity.ProducedQty = Convert.ToDecimal(item[4]);
                        tfdetailentity.BalanceQty = Convert.ToDecimal(item[5]);
                        tfdetailentity.IsQC_Done = Convert.ToString(item[6]);
                        wosizelist.Add(tfdetailentity);
                    }
                }
                entity.FG_TransferDetail = wosizelist;
                bool updatedid = transferservice.Update(entity);
                if (updatedid == true)
                {
                    TempData["alertmsg"] = "FG Transfer Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
            
        }

        [RBAC]
        public JsonResult getWOdetailsbyid(long id)
        {
            List<object> List = new List<object>();
            MFG_WORK_ORDEREntity entity = workorderservice.GetbyId(id);
            entity.wodate = Convert.ToString(string.Format("{0:MM/dd/yyyy}", entity.Date));
            List.Add(entity.wodate);
            var ItemData = workorderservice.GetItemData(id);
            List.Add(ItemData);
            long itemid = Convert.ToInt64(ItemData.ItemId);
            int itemAttr = Convert.ToInt32(ItemData.Item_Attribute_ID);
            var ItemBatchData = workorderservice.GetBatchData(id, itemid, itemAttr);
            List.Add(ItemBatchData);
            return Json(List);
        }

        [RBAC]
        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"FGTId\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = customerpo.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = transferservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);

            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = transferservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = transferservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }


        [RBAC]
        public JsonResult GetTransferData(long Id)
        {
            var record = transferservice.getbyid(Id);
            return Json(record);
        }
    }
}
