﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class JobworkController : SuperController
    {
        //
        // GET: /Jobwork/
        WorkorderService workorderservice = ServiceFactory.GetInstance().WorkorderService;
        JobWorkService jobworkservice = ServiceFactory.GetInstance().JobWorkService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Job Work";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Job Work";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            ViewBag.workordernolist = DropDownList<MFG_WORK_ORDEREntity>.LoadItems(workorderservice.Get().Where(x => x.Company_ID == ViewBag.companyid && x.BranchId == ViewBag.branchid && x.IsStatus == 3).ToList(), "WorkOrderID", "WorkOrderNo", 0);
            M_JobworkEntity entity = new M_JobworkEntity();
            string[] jobno = jobworkservice.getnextJWNo().Split('-');
            entity.jobworkno = jobno[0] + "-" + jobno[1] + "-" + jobno[2] + "-" + jobno[3] + "-XXXX";
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_JobworkEntity entity, FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                entity.processInchargeId = Convert.ToInt64(userid);
                entity.woid = Convert.ToInt64(frm["woid"]);
                entity.transferId = Convert.ToString(frm["TransferNo"]);
                entity.JobStatus = Convert.ToString(frm["jobstatus"]);
                string startdate = Convert.ToString(frm["startdate"]);
                DateTime JBstartDate = DateTime.ParseExact(startdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.startjobdate = JBstartDate;
                entity.startjobtime = Convert.ToString(frm["starttime"]);
                string Enddate = Convert.ToString(frm["enddate"]);
                DateTime JBEndDate = DateTime.ParseExact(Enddate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.endjobtime = Convert.ToString(frm["endtime"]);
                entity.MachineId = Convert.ToInt64(frm["MachineId"]);
                entity.ProcessId = Convert.ToInt64(frm["ProcessId"]);
                entity.OutItemName = Convert.ToInt64(frm["outItemid"]);
                entity.outAttributeId = Convert.ToInt64(frm["outItemAttribId"]);
                entity.OutQty = Convert.ToDecimal(frm["outItemQty"]);
                entity.CreatedBy =Convert.ToString(userid);
                entity.Company_ID = companyid;
                entity.BranchId = branchid;
                entity.IsStatus = 3;

                string itemdata = Convert.ToString(frm["jbrawmaterial"]);
                if (itemdata != "")
                {
                    string[] spltdata = itemdata.Split('#');
                    List<M_Jobwork_MaterialEntity> Material = new List<M_Jobwork_MaterialEntity>();
                    foreach (var item in spltdata)
                    {
                        if (item != "")
                        {
                            string[] rawitem = item.Split('~');
                            M_Jobwork_MaterialEntity ent = new M_Jobwork_MaterialEntity();
                            ent.ItemId = Convert.ToInt64(rawitem[0]);
                            ent.ItemAttributeId = Convert.ToInt32(rawitem[1]);
                            ent.unitid = Convert.ToInt64(rawitem[2]);
                            ent.BatchId = Convert.ToInt64(rawitem[3]);
                            ent.BatchQty = Convert.ToDecimal(rawitem[4]);
                            ent.ConsumedQty = Convert.ToDecimal(rawitem[5]);
                            ent.BalanceQty = Convert.ToDecimal(rawitem[6]);
                            ent.BalanceQty = Convert.ToDecimal(rawitem[6]);
                            ent.CreatedBy =Convert.ToString(userid);
                            ent.Company_ID = companyid;
                            ent.BranchId = branchid;
                            Material.Add(ent);

                        }
                    }
                    entity.JBmaterial = Material;

                    long id = jobworkservice.Insert(entity);

                    if (id > 0)
                    {
                        TempData["alertmsg"] = "Record Added Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["alertmsg"] = "Something Wrong..!";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Job Work";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            M_JobworkEntity entity = new M_JobworkEntity();
            entity = jobworkservice.GetById(id);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            ViewBag.workordernolist = DropDownList<MFG_WORK_ORDEREntity>.LoadItems(workorderservice.Get().Where(x => x.Company_ID == ViewBag.companyid && x.BranchId == ViewBag.branchid && x.IsStatus == 3).ToList(), "WorkOrderID", "WorkOrderNo", (int)entity.woid);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);

            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_JobworkEntity entity, FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);
                bool isupdate = false;
                using (var db = new GarmentERPDBEntities())
                {
                    long inwarditemid = Convert.ToInt64(frm["jobid"]);
                    string itemPIval = Convert.ToString(frm["productinfo"]);
                    string itemPIId = Convert.ToString(frm["producttype"]);
                    if (itemPIval != "" && itemPIId != "")
                    {
                        string[] itemPI = itemPIval.Split('#');
                        string PIids = itemPIId;

                        foreach (var myPIdata in itemPI)
                        {
                            if (inwarditemid > 0)
                            {
                                string[] PIsplt = myPIdata.Split('~');
                                string[] PIidsplt = PIids.Split(',');
                                int len = PIsplt.Length;
                                M_ItemInwardMaster inwardrecord = new M_ItemInwardMaster();
                                inwardrecord.ItemInward_ID = inwarditemid;
                                inwardrecord.Jobwork_ID = inwarditemid;
                                inwardrecord.Item_ID = Convert.ToInt64(frm["outItemid"]);
                                inwardrecord.Item_Attribute_ID = Convert.ToInt32(frm["outItemAttribId"]);
                                inwardrecord.ItemQty = Convert.ToDecimal(PIsplt[len - 1]);
                                inwardrecord.UOM = Convert.ToInt32(frm["Unit"]);
                                inwardrecord.BalanceQty = 0;
                                inwardrecord.IsActive = true;
                                inwardrecord.InventoryModeId = 4;
                                inwardrecord.CreatedDate = DateTime.Now;
                                inwardrecord.CreatedBy = userid;
                                inwardrecord.Company_ID = companyid;
                                inwardrecord.BranchId = branchid;


                                db.M_ItemInwardMaster.Add(inwardrecord);
                                db.SaveChanges();
                                long IIid = inwardrecord.ID;
                                int k = 0;
                                foreach (var pidata in PIidsplt)
                                {
                                    M_Product_Info prodinfo = new M_Product_Info();
                                    prodinfo.ItemInward_ID = IIid;
                                    prodinfo.Dynamic_Controls_Id = Convert.ToInt64(pidata);
                                    prodinfo.Dynamic_Controls_value = Convert.ToString(PIsplt[k]);
                                    prodinfo.Created_Date = DateTime.Now;
                                    prodinfo.Created_By = userid;
                                    prodinfo.Company_ID = companyid;
                                    prodinfo.BranchId = branchid;
                                    db.M_Product_Info.Add(prodinfo);
                                    db.SaveChanges();
                                    k++;
                                }
                            }
                        }

                        string Enddate = Convert.ToString(frm["enddate"]);
                        DateTime JBEndDate = DateTime.ParseExact(Enddate, "MM/dd/yyyy", CultureInfo.InvariantCulture);


                        var record = db.M_Jobwork.Find(inwarditemid);
                        record.endjobdate = JBEndDate;
                        record.endjobtime = entity.endjobtime;
                        record.endjobtime = Convert.ToString(frm["endtime"]);
                        record.ModifyBy = Convert.ToString(userid);
                        record.ModifyDate = DateTime.Now;
                        record.IsStatus = 4;
                        record.AcceptQty = Convert.ToDecimal(frm["AcceptQty"]);
                        record.QC = Convert.ToInt32(frm["hideqcdone"]);
                        db.SaveChanges();
                        isupdate = true;

                    }
                    else
                    {
                        TempData["alertmsg"] = "Batch Info Data Not Set Proper";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                if (isupdate == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Please Check Error Log...!";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            
        }



        //Get Workorder And User Against Process List And Material Transfer No.
        [RBAC]
        public JsonResult getWOdetailsbyid(long workorderID)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var ProcessList = jobworkservice.GetProcessList(workorderID, userid);
            var TransferList = jobworkservice.GetTransferNo(workorderID, userid).Where(x=>x.Company_ID==companyid && x.BranchId==branchid).ToList();

            List<Object> objLst = new List<object>();
            objLst.Add(ProcessList);
            objLst.Add(TransferList);

            JsonResult res = Json(objLst);
            return res;
        }

        //Get Machine List By Process Wise
        [RBAC]
        public JsonResult GetMachine(long ProcessId)
        {
            var machinelist = jobworkservice.GetMachine(ProcessId);
            JsonResult res = Json(machinelist);
            return res;
        }


        //Get Out Item Details
        [RBAC]
        public JsonResult GetOutItem(int ProcessId, long workorderID)
        {
            var machinelist = jobworkservice.GetOutItem(ProcessId, workorderID);
            JsonResult res = Json(machinelist);
            return res;
        }

        [RBAC]
        public JsonResult GetMaterialTransfer(string TransferNo)
        {
            var ItemDetails = jobworkservice.GetMaterialTransfer(TransferNo);
            JsonResult res = Json(ItemDetails);
            return res;
        }


        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = jobworkservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = jobworkservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = jobworkservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }
    }
}
