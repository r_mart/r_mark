﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.CommonEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    [RBAC]
    public class CustomerPOController : SuperController
    {
        //
        // QuotationService quotationservice = ServiceFactory.GetInstance().QuotationService;
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        SegmentTypeService segmenttypeservice = ServiceFactory.GetInstance().SegmentTypeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        CustomerTypeService customertypeservice = ServiceFactory.GetInstance().CustomerTypeService;
        CustomerContactService customercontactservice = ServiceFactory.GetInstance().CustomerContactService;
        CustomerBankService customerbankservice = ServiceFactory.GetInstance().CustomerBankService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        // GET: /CustomerPO/
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Customer PO";

            var list = customerpo.Get();
            return View(list);
        }


        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Customer PO";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            ViewBag.OtherCharge= DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            string[] pono = customerpo.getnextPONo().Split('-');
            ViewBag.pono = pono[0] + "-" + pono[1] + "-" + pono[2] + "-" + pono[3] + "-XXXX";

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            string styleimgpath = string.Empty;
            List<string> AttributeImage = new List<string>();
            List<string> styleimglist = new List<string>();
            try
            {
                
                if (Request.Files.Count > 0)
                {

                    if (styleimg != null)
                    {
                        foreach (var stfile in styleimg)
                        {
                            if (stfile != null && stfile.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(stfile.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                stfile.SaveAs(path);
                                styleimgpath = newfilename;
                                styleimglist.Add(styleimgpath);
                            }
                        }

                    }
                    if (styleimglist.Count == 0)
                    {
                        if (Convert.ToString(frm["HstyleImageId"]) != "" && Convert.ToString(frm["HstyleImageId"]) != null)
                        {
                            styleimgpath = Convert.ToString(frm["HstyleImageId"]);
                            string[] styleimgesplit = styleimgpath.Split('#');
                            for (int i = 0; i < styleimgesplit.Length; i++)
                            {
                                styleimglist.Add(styleimgesplit[i]);
                            }
                        }
                    }

                    if (ItemAttrimages_id != null)
                    {
                        foreach (var rimg in ItemAttrimages_id)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                                string isExistImagepath = Server.MapPath("~/ItemImages/");
                                bool exists = System.IO.Directory.Exists(isExistImagepath);
                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistImagepath);
                                var path = Path.Combine(isExistImagepath, newfilename);
                                rimg.SaveAs(path);
                                AttributeImage.Add(newfilename);
                            }
                        }
                    }
                }



                CRM_CustomerPOMasterentity custpoentity = new CRM_CustomerPOMasterentity();
                custpoentity.POno = Convert.ToString(frm["POno"]);
                string podatestr = Convert.ToString(frm["POdate"]);
                DateTime POdate = DateTime.ParseExact(podatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                custpoentity.PODate = POdate;
                custpoentity.customerid = Convert.ToInt64(frm["customerid"]);
                var Quotrefno_ = Convert.ToString(frm["quotrefno"]);
                if (Quotrefno_ != "0")
                {
                    custpoentity.Quotrefno = Quotrefno_;
                    custpoentity.QuotationID = Convert.ToInt32(Quotrefno_);
                }

                string quotdate = Convert.ToString(frm["quotdate"]);
                if (quotdate != "")
                {
                    DateTime quodate = DateTime.ParseExact(quotdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    custpoentity.Quotdate = quodate;
                }

                custpoentity.Addressid = Convert.ToInt64(frm["custaddid"]);
                string approvdate = Convert.ToString(frm["approvaldate"]);
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                custpoentity.Approvaldate = appdate;

                custpoentity.Comments = Convert.ToString(frm["comment"]);
                custpoentity.Remark = Convert.ToString(frm["remark"]);
                custpoentity.IsActive = true;
                custpoentity.Createddate = DateTime.Now;
                string tentivdatestr = Convert.ToString(frm["tentivedate"]);
                DateTime tentivedate = DateTime.ParseExact(tentivdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                custpoentity.Tentivedate = tentivedate;
                custpoentity.imglist = styleimglist;

                var sampling = frm["issampling"].ToString();
                if (sampling == "yes")
                    custpoentity.Sampling = true;
                else
                    custpoentity.Sampling = false;

                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                custpoentity.Craetedby = Convert.ToInt32(collection.Id);
                custpoentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                custpoentity.BranchId = Convert.ToInt32(collection.BranchId);
                custpoentity.IsReadmade = Convert.ToString(frm["hidIsReadyMate"]) == "" ? 0 : Convert.ToInt32(frm["hidIsReadyMate"]);

                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    custpoentity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    custpoentity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    custpoentity.Approvaldate = DateTime.Now;
                }
                else
                {
                    custpoentity.Approvalstatus = false;
                    custpoentity.IsStatus = 2;
                }

                custpoentity.Saleprice = Convert.ToDecimal(frm["saleprice"]);
                custpoentity.Costprice = Convert.ToDecimal(frm["costprice"]);
                custpoentity.Subtotal = Convert.ToDecimal(frm["subtotal"]);
                custpoentity.Tax = Convert.ToDecimal(frm["tax"]);
                custpoentity.Total = Convert.ToDecimal(frm["total"]);
                custpoentity.GrandTotal = Convert.ToDecimal(frm["Grandtotal"]);

                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if(otherchargeStr!="")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach(var ocdata in OCSplit)
                    {
                        if(ocdata!="")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10007; //Customer PO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            oclist.Add(ent);

                        }
                    }
                    custpoentity.POOtherChargeEntity = oclist;
                }
                
                //quotentity.
                //enquiryentity.Sampling=frm["sampling"];
                CRM_CustomerPOStyleDetailEntity styleentity = new CRM_CustomerPOStyleDetailEntity();

                //segment  item style brand stydesc color shade image reqqty sizeratio 

                styleentity.Segmenttypeid = Convert.ToInt32(frm["Hsegmenttypeid"]);
                styleentity.Itemid = Convert.ToInt32(frm["itemid"]);
                styleentity.Styleid = Convert.ToInt32(frm["styleid"]);
                styleentity.Brand = Convert.ToString(frm["brand"]);
                styleentity.Styledesc = Convert.ToString(frm["styledesc"]);
                styleentity.Styleno = Convert.ToString(frm["styleno"]);
                styleentity.Createdby = Convert.ToInt32(collection.Id);
                styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                styleentity.BranchId = Convert.ToInt32(collection.BranchId);

                if (frm["reqqty"].ToString() != "")
                {
                    styleentity.Totalqty = Convert.ToDecimal(frm["reqqty"]);
                }

                string sizeliststr = frm["styleliststr"];
                string[] sizearr = sizeliststr.Split('#');
                List<CRM_CustoemrPOStyleSizeDetailEntity> sizeentitylist = new List<CRM_CustoemrPOStyleSizeDetailEntity>();
                foreach (var size_ in sizearr)
                {
                    if (size_ != "")
                    {
                        string[] styleitem = size_.Split('~');
                        CRM_CustoemrPOStyleSizeDetailEntity sizeentity = new CRM_CustoemrPOStyleSizeDetailEntity();
                        sizeentity.SIzepcs = Convert.ToDecimal(styleitem[8]);
                        sizeentity.Attribute_ID = Convert.ToString(styleitem[6]);
                        styleitem[7] = Convert.ToString(styleitem[7]).Substring(0, 1) == "," ? Convert.ToString(styleitem[7]).Substring(1) : styleitem[7];
                        sizeentity.Attribute_Value_ID = Convert.ToString(styleitem[7]);
                        sizeentity.rate = Convert.ToDecimal(styleitem[9]);
                        sizeentity.Sizeid = Convert.ToInt64(styleitem[10]);
                        sizeentity.Createdby = Convert.ToInt32(collection.Id);
                        sizeentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        sizeentity.BranchId = Convert.ToInt32(collection.BranchId);
                        sizeentity.SellRate = Convert.ToDecimal(styleitem[11]);
                        sizeentity.GST = Convert.ToDecimal(styleitem[12]);
                        sizeentity.Subtotal = Convert.ToDecimal(styleitem[14]);
                        sizeentity.IsActive = true;

                        int ICount = Convert.ToInt32(styleitem[15]);
                        int k = 0;
                        var Attrimgarray = AttributeImage.ToList();
                        List<string> imgdata = new List<string>();
                        foreach (var img in Attrimgarray)
                        {
                            if (k != ICount)
                            {
                                if (img != "")
                                {
                                    imgdata.Add(img.ToString());
                                    AttributeImage.RemoveAt(0);
                                }
                                else
                                {
                                    AttributeImage.RemoveAt(0);
                                }
                            }
                            else
                            {
                                break;
                            }
                            k++;
                        }
                        sizeentity.itemimg_list = imgdata;
                        sizeentitylist.Add(sizeentity);
                    }
                }
                styleentity.sizelist = sizeentitylist;
                custpoentity.styledetailentity = styleentity;

                //Code to add PO delivery details   -Ramesh Take on 24-Jan-2020
                string poschedules = frm["po_delivery_schedules"];
                string[] scheduleArr = poschedules.Split('#');
                List<MFG_PO_DeliveryDetail_Entity> po_delivery_Schedules = new List<MFG_PO_DeliveryDetail_Entity>();
                foreach (var schedule in scheduleArr)
                {
                    if (schedule != "")
                    {
                        string[] record_ = schedule.Split('~');
                        MFG_PO_DeliveryDetail_Entity po_delentity = new MFG_PO_DeliveryDetail_Entity();
                        po_delentity.Ledger_Id = Convert.ToInt32(frm["customerid"]);
                        DateTime delDate_ = DateTime.ParseExact(record_[0], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        po_delentity.Delivery_Date = delDate_;
                        po_delentity.Delivery_Qty = Convert.ToDecimal(record_[1]);
                        po_delentity.Comments = record_[2];
                        if (record_[4] !="")
                        {
                            po_delentity.Item_ID = Convert.ToInt64(record_[4]);
                        }
                        po_delentity.Item_Attribute_Id = record_[5] != "" ? Convert.ToInt32(record_[5]) : 0;
                        po_delentity.Created_By = Convert.ToInt32(collection.Id);
                        po_delentity.Created_Date = DateTime.Now;
                        po_delentity.IsActive = true;
                        //po_delentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        //po_delentity.BranchId = Convert.ToInt32(collection.BranchId);
                        po_delivery_Schedules.Add(po_delentity);
                    }
                }
                custpoentity.MFG_PO_DeliveryDetail = po_delivery_Schedules;


                long id = customerpo.Insert(custpoentity);

                if (id > 0)
                {

                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");

                }
                else
                {
                    TempData["alertmsg"] = "Record Not Save!";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");


        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Customer PO";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int CBS = 1;//Customer ID
            CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
            entity = customerpo.getbyid(id);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.styledetailentity.Segmenttypeid);
            ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", (long)entity.styledetailentity.Itemid);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.styledetailentity.Styleid);
            ViewBag.customerlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", (long)entity.customerid);
            ViewBag.custaddresslist = DropDownList<M_Ledger_BillingDetailsEntity>.LoadItems(ledgeraccountservices.GetAddressById((long)entity.customerid), "Id", "Address", (long)entity.Addressid);
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var poId = Convert.ToInt64(frm["Id"]);
            string styleimgpath = string.Empty;
            List<string> AttributeImage = new List<string>();
            List<CRM_CustomerPOStyleImageEntity> styleimglist = new List<CRM_CustomerPOStyleImageEntity>();
            if (Request.Files.Count > 0)
            {

                if (styleimg != null)
                {
                    foreach (var stfile in styleimg)
                    {
                        CRM_CustomerPOStyleImageEntity imgentity = new CRM_CustomerPOStyleImageEntity();
                        if (stfile != null && stfile.ContentLength > 0)
                        {

                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            imgentity.styleimg = newfilename;
                            styleimglist.Add(imgentity);
                        }
                    }

                }

                if (ItemAttrimages_id != null)
                {
                    foreach (var rimg in ItemAttrimages_id)
                    {
                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                            string isExistImagepath = Server.MapPath("~/ItemImages/");
                            bool exists = System.IO.Directory.Exists(isExistImagepath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistImagepath);
                            var path = Path.Combine(isExistImagepath, newfilename);
                            rimg.SaveAs(path);
                            AttributeImage.Add(newfilename);
                        }
                    }
                }

            }

            CRM_CustomerPOMasterentity custpoentity = new CRM_CustomerPOMasterentity();
            CRM_CustomerPOStyleDetailEntity styleentity = new CRM_CustomerPOStyleDetailEntity();

            custpoentity.Id = poId;
            if (frm["saleprice"] != "")
                custpoentity.Saleprice = Convert.ToDecimal(frm["saleprice"]);
            if (frm["costprice"] != "")
                custpoentity.Costprice = Convert.ToDecimal(frm["costprice"]);

            if (frm["subtotal"] != "")
                custpoentity.Subtotal = Convert.ToDecimal(frm["subtotal"]);
            if (frm["tax"] != "")
                custpoentity.Tax = Convert.ToDecimal(frm["tax"]);

            if (frm["total"] != "")
                custpoentity.Total = Convert.ToDecimal(frm["total"]);

            custpoentity.GrandTotal = Convert.ToDecimal(frm["Grandtotal"]);

            string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
            if (otherchargeStr != "")
            {
                string[] OCSplit = otherchargeStr.Split('#');
                List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                foreach (var ocdata in OCSplit)
                {
                    if (ocdata != "")
                    {
                        var data = ocdata.Split('~');
                        T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                        ent.OtherChargeId = Convert.ToInt32(data[0]);
                        ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                        ent.GST = Convert.ToDecimal(data[2]);
                        ent.InventoryModeId = 10007; //Customer PO
                        ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                        ent.BranchId = Convert.ToInt32(collection.BranchId);
                        ent.CreatedBy = Convert.ToInt32(collection.Id);
                        ent.Id = Convert.ToInt64(data[5]);
                        oclist.Add(ent);

                    }
                }
                custpoentity.POOtherChargeEntity = oclist;
            }

            custpoentity.Remark = frm["comment"];
            custpoentity.styleimglist = styleimglist;
            string approvdate = Convert.ToString(frm["approvaldate"]);
            if (approvdate != "")
            {
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                custpoentity.Approvaldate = appdate;
            }
            if (roleid != 4)
            {
                custpoentity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                custpoentity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                custpoentity.Approvaldate = DateTime.Now;
            }
            else
            {
                custpoentity.IsStatus = 2;
            }
            custpoentity.Updatedby = userid;
            custpoentity.Company_ID = companyid;
            custpoentity.BranchId = branchid;



            string tentivdatestr = Convert.ToString(frm["tentivedate"]);
            DateTime tentivedate = DateTime.ParseExact(tentivdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            custpoentity.Tentivedate = tentivedate;

            styleentity.Totalqty = Convert.ToDecimal(frm["reqqty"]);

            string sizeliststr = Convert.ToString(frm["styleliststr"]);
            string[] sizearr = sizeliststr.Split('#');
            List<CRM_CustoemrPOStyleSizeDetailEntity> sizeentitylist = new List<CRM_CustoemrPOStyleSizeDetailEntity>();

            //code for update size details
            foreach (var size_ in sizearr)
            {
                if (size_ != "")
                {
                    string[] styleitem = size_.Split('~');
                    CRM_CustoemrPOStyleSizeDetailEntity sizeentity = new CRM_CustoemrPOStyleSizeDetailEntity();
                    sizeentity.Id = Convert.ToInt64(styleitem[0]);
                    sizeentity.SIzepcs = Convert.ToDecimal(styleitem[9]);
                    sizeentity.Attribute_ID = Convert.ToString(styleitem[7]);
                    styleitem[8] = Convert.ToString(styleitem[8]).Substring(0, 1) == "," ? Convert.ToString(styleitem[8]).Substring(1) : styleitem[8];
                    sizeentity.Attribute_Value_ID = Convert.ToString(styleitem[8]);
                    sizeentity.rate = Convert.ToDecimal(styleitem[10]);
                    sizeentity.Sizeid = Convert.ToInt64(styleitem[11]);
                    sizeentity.SellRate = Convert.ToDecimal(styleitem[12]);
                    sizeentity.GST = Convert.ToDecimal(styleitem[13]);
                    sizeentity.Subtotal = Convert.ToDecimal(styleitem[15]);
                    int ICount = Convert.ToInt32(styleitem[16]);
                    int k = 0;
                    List<string> imgdata = new List<string>();
                    var Attrimgarray = AttributeImage.ToList();
                    foreach (var img in Attrimgarray)
                    {
                        if (k != ICount)
                        {
                            if (img != "")
                            {
                                imgdata.Add(img.ToString());
                                AttributeImage.RemoveAt(0);
                            }
                            else
                            {
                                AttributeImage.RemoveAt(0);
                            }
                        }
                        else
                        {
                            break;
                        }
                        k++;
                    }
                    sizeentity.itemimg_list = imgdata;

                    sizeentity.IsActive = true;
                    sizeentity.Updatedby = userid;
                    sizeentitylist.Add(sizeentity);
                }
            }

            styleentity.sizelist = sizeentitylist;
            custpoentity.styledetailentity = styleentity;

            //Code to add PO delivery details   -Ramesh Take on 24-Jan-2020
            string poschedules = frm["po_delivery_schedules"];
            string[] scheduleArr = poschedules.Split('#');
            List<MFG_PO_DeliveryDetail_Entity> po_delivery_Schedules = new List<MFG_PO_DeliveryDetail_Entity>();
            foreach (var schedule in scheduleArr)
            {
                if (schedule != "")
                {
                    string[] record_ = schedule.Split('~');
                    MFG_PO_DeliveryDetail_Entity po_delentity = new MFG_PO_DeliveryDetail_Entity();
                    po_delentity.Ledger_Id = Convert.ToInt32(frm["customerid"]);
                    DateTime delDate_ = DateTime.ParseExact(record_[0], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    po_delentity.Delivery_Date = delDate_;
                    //po_delentity.Delivery_Date = Convert.ToDateTime(record_[0]);
                    po_delentity.Delivery_Qty = Convert.ToDecimal(record_[1]);
                    po_delentity.Comments = record_[2];
                    if (record_[4] != "")
                    {
                        po_delentity.Item_ID = Convert.ToInt64(record_[4]);
                    }
                    po_delentity.Item_Attribute_Id = record_[5] != "" ? Convert.ToInt32(record_[5]) : 0;
                    po_delentity.PO_DelSchedule_ID = Convert.ToInt64(record_[3]);
                    po_delentity.Created_By = Convert.ToInt32(collection.Id);
                    po_delentity.Modified_By = Convert.ToInt32(collection.Id);
                    po_delentity.Modified_Date = DateTime.Now;
                    po_delentity.IsActive = true;
                    //po_delentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                    //po_delentity.BranchId = Convert.ToInt32(collection.BranchId);
                    po_delivery_Schedules.Add(po_delentity);
                }
            }
            custpoentity.MFG_PO_DeliveryDetail = po_delivery_Schedules;

            //  long id = customerpo.Insert(custpoentity);
            var updated = customerpo.Update(custpoentity);
            if (updated == true)
            {
                TempData["alertmsg"] = "Record Update Successfully";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
            }
            else
            {
                TempData["alertmsg"] = "Record Not Update";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public JsonResult getPObycustomerid(long id)
        {
            var list = customerpo.Getbycustomerid(id);
            return Json(list);

        }

        [RBAC]
        public JsonResult getPOListbycustomerid(long id)
        {
            var list = customerpo.getPOListbycustomerid(id);
            return Json(list);

        }

        [RBAC]
        public JsonResult getPOdetailsbyid(long id)
        {
            CRM_CustomerPOMasterentity entity = customerpo.getbyid(id);
            entity.podatestr = Convert.ToString(string.Format("{0:MM/dd/yyyy}", entity.PODate));

            return Json(entity);
        }

        public ActionResult Delete(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            var deleted = customerpo.Delete(id, Userid);
            return Json(deleted);
        }

        [RBAC]
        public ActionResult CustomerPOReport(long Id)
        {

            var list = customerpo.getbyPONO(Convert.ToString(Id));
            return View(list);
        }



        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {

            // read parameters from the webpage
            string htmlString = "<html><head><title>CustomerPO</title><link rel='stylesheet' href='http://localhost:2423/Content/dist/css/AdminLTE.min.css'><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'></head><body><div class='row'><div class='col-sm-12 text-center'><h3>Customer PO</h3></div></div>" + GridHtml + "</body></html>";
            string baseUrl = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            int webPageHeight = 0;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            converter.Options.DisplayFooter = true;
            converter.Options.MarginTop = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginBottom = 10;
            converter.Options.PdfCompressionLevel = PdfCompressionLevel.Best;
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfStandard = PdfStandard.Full;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.DrawBackground = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Print;
            PdfTextSection text = new PdfTextSection(0, 10, "Page: {page_number} of {total_pages} ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;


            //using (MemoryStream stream = new System.IO.MemoryStream())
            //{
            //    StringReader sr = new StringReader(GridHtml);
            //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            //    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);

            //try
            //{
            //    pdfDoc.Open();
            //    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            //    pdfDoc.Close();
            //}
            //catch (Exception ex)
            //{

            //}

            //return File(stream.ToArray(), "application/pdf", "Grid.pdf");
            //}


        }
        //public JsonResult CustomerPOReportToPdf(string collection)
        //{
        //    string htmlString = collection;
        //    string baseUrl = collection;

        //    string pdf_page_size = collection;
        //    PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
        //        pdf_page_size, true);

        //    string pdf_orientation = collection;
        //    PdfPageOrientation pdfOrientation =(PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
        //        pdf_orientation, true);

        //    int webPageWidth = 1024;
        //    try
        //    {
        //        webPageWidth = Convert.ToInt32(collection);
        //    }
        //    catch { }

        //    int webPageHeight = 0;
        //    try
        //    {
        //        webPageHeight = Convert.ToInt32(collection);
        //    }
        //    catch { }

        //    // instantiate a html to pdf converter object
        //    HtmlToPdf converter = new HtmlToPdf();

        //    // set converter options
        //    converter.Options.PdfPageSize = pageSize;
        //    converter.Options.PdfPageOrientation = pdfOrientation;
        //    converter.Options.WebPageWidth = webPageWidth;
        //    converter.Options.WebPageHeight = webPageHeight;

        //    // create a new pdf document converting an url
        //    PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

        //    // save pdf document
        //    byte[] pdf = doc.Save();

        //    // close pdf document
        //    doc.Close();

        //    // return resulted pdf document
        //    FileResult fileResult = new FileContentResult(pdf, "application/pdf");
        //    fileResult.FileDownloadName = "Document.pdf";

        //    return Json(fileResult);
        //}


        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"POStyleId\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = customerpo.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        //Rejected Customer PO Code
        public JsonResult RejectedCPO(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = customerpo.RejectedCPO(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = customerpo.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = customerpo.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = customerpo.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

    }
}
