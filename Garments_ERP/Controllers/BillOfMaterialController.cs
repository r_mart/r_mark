﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class BillOfMaterialController : SuperController
    {
        //
        // GET: /BillOfMaterial/
        AttributeService attributeservice = ServiceFactory.GetInstance().AttributeService;
        ItemGroupService itemgroupservice = ServiceFactory.GetInstance().ItemGroupService;
        CustomerPOService CustomerPoservice = ServiceFactory.GetInstance().CustomerPOService;
        QuotationRawItemDetailService quotationrawitemservice = ServiceFactory.GetInstance().QuotationRawItemDetailService;
        CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;
        EnquiryService enquiryservice = ServiceFactory.GetInstance().EnquiryService;
        EnquiryStyleDetailService enquirystyledetailservice = ServiceFactory.GetInstance().EnquiryStyleDetailService;
        EnquiryRawItemDetailService enquiryrawitemservice = ServiceFactory.GetInstance().EnquiryRawItemDetailService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        SegmentTypeService segmenttypeservice = ServiceFactory.GetInstance().SegmentTypeService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        ItemSizeService itemsizeservice = ServiceFactory.GetInstance().ItemSizeService;
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        SupplierService supplierservice = ServiceFactory.GetInstance().SupplierService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        BillOfMaterialService billofmaterialservice = ServiceFactory.GetInstance().BillOfMaterialService;
        BillOfMaterialDetailService billmaterialdetailservice = ServiceFactory.GetInstance().BillOfMaterialDetailService;
        BOMSizeDetailService bomsizedetailservice = ServiceFactory.GetInstance().BOMSizeDetailService;
        ProcessCycleService processcycleservice = ServiceFactory.GetInstance().ProcessCycleService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        SupplierPRService supplierprservice = ServiceFactory.GetInstance().SupplierPRService;
        SupplierPRItemDetailService supplierpritemdetailservice = ServiceFactory.GetInstance().SupplierPRItemDetailService;
        BillOfMaterialService bomservice = ServiceFactory.GetInstance().BillOfMaterialService;

        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Bill Of Material";

            var list = billofmaterialservice.getBase(ViewBag.companyid, ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public JsonResult DeleteBOM(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = billofmaterialservice.Delete(Id);
            return Json(ISDeleted);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Index(string enqfromdate, string enqtodate)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.title = "Bill Of Material";

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            if (enqfromdate != "" && enqtodate != "")
            {
                var fdate = DateTime.ParseExact(enqfromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(enqtodate, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                var list = billofmaterialservice.get(fdate, tdate, ViewBag.companyid, ViewBag.branchid);

                ViewBag.fromdate = enqfromdate;
                ViewBag.todate = enqtodate;

                return View(list);
            }
            else
            {
                var list = billofmaterialservice.getBase(ViewBag.companyid, ViewBag.branchid);
                return View(list);
            }
        }

        [RBAC]
        [HttpGet]
        public ActionResult Create(long Enqid, long POid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.title = "Bill Of Material";

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.bomlist = DropDownList<M_BillOfMaterialMasterEntity>.LoadItems(billofmaterialservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.BranchId == ViewBag.branchid).ToList(), "BOM_ID", "BOM_No", 0);

            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            ViewBag.EnqId = Enqid;
            ViewBag.POId = POid;

            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", 0);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", 0);
            ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Prooce_Cycle_id", "Process_cycle_Name", 0);
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            ViewBag.Attribute = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get(), "Attribute_ID", "Attribute_Name", 0);
            string[] BOM_No = billofmaterialservice.getnextbomno().Split('-');
            entity.BOM_No = BOM_No[0] + "-" + BOM_No[1] + "-" + BOM_No[2] + "-" + BOM_No[3] + "-XXXX";

            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] rawitemimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            try
            {
                string styleimgpath = string.Empty;
                List<string> styleimglist = new List<string>();
                List<string> rawimglist = new List<string>();
                List<string> AttributeImage = new List<string>();

                int IsReadyMate = Convert.ToInt32(frm["hidIsReadyMate"]);


                if (Request.Files.Count > 0)
                {
                    foreach (var stfile in styleimg)
                    {
                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);
                        }
                    }
                    
                    if(IsReadyMate==0)
                    {
                        if (rawitemimg != null)
                        {
                            foreach (var rimg in rawitemimg)
                            {

                                if (rimg != null && rimg.ContentLength > 0)
                                {
                                    var filename = Path.GetFileName(rimg.FileName);
                                    var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                    string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                    bool exists = System.IO.Directory.Exists(isExistCompanypath);

                                    if (!exists)
                                        System.IO.Directory.CreateDirectory(isExistCompanypath);

                                    var path = Path.Combine(isExistCompanypath, newfilename);
                                    rimg.SaveAs(path);
                                    rawimglist.Add(newfilename);
                                }
                                else
                                {
                                    rawimglist.Add("");
                                }
                            }
                        }
                    }
                   

                    if (ItemAttrimages_id != null)
                    {
                        foreach (var rimg in ItemAttrimages_id)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                                string isExistImagepath = Server.MapPath("~/ItemImages/");
                                bool exists = System.IO.Directory.Exists(isExistImagepath);
                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistImagepath);
                                var path = Path.Combine(isExistImagepath, newfilename);
                                rimg.SaveAs(path);
                                AttributeImage.Add(newfilename);
                            }
                        }
                    }

                }
                if (styleimglist.Count == 0)
                {
                    if (Convert.ToString(frm["HstyleImageId"]) != "" && Convert.ToString(frm["HstyleImageId"]) != null)
                    {
                        styleimgpath = Convert.ToString(frm["HstyleImageId"]);
                        string[] styleimgesplit = styleimgpath.Split('#');
                        for (int i = 0; i < styleimgesplit.Length; i++)
                        {
                            styleimglist.Add(styleimgesplit[i]);
                        }
                    }

                }
                entity.RevisionNo = Convert.ToDecimal(frm["RevisionNo"]);
                entity.IsReadymate = IsReadyMate;
                if (IsReadyMate == 0)
                {
                    entity.Prooce_Cycle_id = Convert.ToInt32(frm["Prooce_Cycle_id"]);
                }
                int isPR= Convert.ToInt32(frm["IsPRid"]);
                entity.IsPRDone = isPR == 0 ? false : true;
                var rawitemstrlist = Convert.ToString(frm["BOMitemList"]);
                long Enqid = Convert.ToInt64(frm["HidEnqid"]);
                long POid = Convert.ToInt64(frm["HidPOid"]);
                //var customeraddress = Convert.ToInt64(frm["customeraddress"]);
                //entity.CustomerAddressId = customeraddress;
                var styleid = Convert.ToInt64(frm["Styleid"]);
                entity.StyleId = styleid;
                var BOM_No = frm["BOM_No"];
                entity.BOM_No = BOM_No;
                entity.EnquiryId = Enqid > 0 ? Enqid : 9999;
                entity.Poid = POid;
                entity.CreatedDate = DateTime.Now;
                entity.CustomerId = Convert.ToInt32(frm["customerid"]);
                entity.CustomerAddressId = Convert.ToInt32(frm["customeraddress"]);
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);

                entity.IsActive = true;
                entity.styleimglist = styleimglist;
                var processcyclelist = Convert.ToString(frm["bomprocessCyclelist"]);
                if (processcyclelist != null && processcyclelist != "")
                {
                    string[] BOMPCarr = processcyclelist.Split('#');
                    List<M_BOM_ProcessCycleEntity> BOMpclist = new List<M_BOM_ProcessCycleEntity>();
                    foreach (var arritem in BOMPCarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BOM_ProcessCycleEntity styleentity = new M_BOM_ProcessCycleEntity();
                            styleentity.ProcessId = Convert.ToInt32(styleitem[0]);
                            styleentity.Processsequence = Convert.ToInt32(styleitem[1]);
                            styleentity.ItemOutId = Convert.ToInt64(styleitem[2]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[3]);
                            styleentity.PerQty = Convert.ToDecimal(styleitem[4]);
                            styleentity.RequiredQty = Convert.ToDecimal(styleitem[5]);
                            styleentity.IsActive = true;
                            styleentity.CreatedBy = Convert.ToInt32(collection.Id);
                            styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            styleentity.BranchId = Convert.ToInt32(collection.BranchId);
                            BOMpclist.Add(styleentity);
                        }
                    }
                    entity.BOM_ProcessCycle = BOMpclist;
                }
                long insertedid = billofmaterialservice.Insert(entity);
                if (insertedid > 0)
                {
                    string[] rawitemarr = rawitemstrlist.Split('#');
                    // "64~65~66~#12~12~12~#"
                    var sizelist = Convert.ToString(frm["BOMSizeList"]);

                    string[] sizearr = sizelist.Split('#');
                    var j = 0;
                    decimal bomsizeid = 0;
                    long insertid = 0;
                    foreach (var arritem in rawitemarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
                            styleentity.BOM_ID = insertedid;
                            styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
                            styleentity.ItemId = Convert.ToInt32(styleitem[1]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[2]);
                            styleentity.SupplierName = Convert.ToString(styleitem[3]);
                            styleentity.RequiredQty = Convert.ToDecimal(styleitem[5]);
                            styleentity.UnitId = Convert.ToInt32(styleitem[7]);
                            styleentity.PerQty = Convert.ToInt32(styleitem[4]);
                            styleentity.Comment = Convert.ToString(frm["comment"]);
                            styleentity.IsActive = true;
                            styleentity.CreatedDate = DateTime.Now;
                            styleentity.CreatedBy = Convert.ToInt32(collection.Id);
                            styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            styleentity.BranchId = Convert.ToInt32(collection.BranchId);
                            int RICount = Convert.ToInt32(styleitem[8]);
                            int k = 0;
                            var Rawimgarray = rawimglist.ToList();
                            List<string> rawimgdata = new List<string>();
                            if (Rawimgarray.Count > 0 && RICount > 0)
                            {
                                foreach (var img in Rawimgarray)
                                {
                                    if (k != RICount)
                                    {
                                        if (img != "")
                                        {
                                            rawimgdata.Add(img.ToString());
                                            rawimglist.RemoveAt(0);
                                        }
                                        else
                                        {
                                            rawimglist.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    k++;
                                }
                            }
                            else
                            {
                                string[] RImgList = Convert.ToString(styleitem[9]).Split(',');
                                if (RImgList.Length > 0)
                                {
                                    foreach (var img in RImgList)
                                    {
                                        if (img != "" && img!="-")
                                        {
                                            rawimgdata.Add(img.ToString());
                                        }
                                    }
                                }
                            }

                            styleentity.rawitemimg_list = rawimgdata;
                            
                            insertid = billmaterialdetailservice.Insert(styleentity);

                            j++;
                        }
                    }

                    if (insertedid > 0)
                    {
                        foreach (var data in sizearr)
                        {
                            if (data != "")
                            {
                                string[] sizerecord = data.Split('~');
                                M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
                                bomentity.BOMD_Id = insertedid;
                                bomentity.SizeId = Convert.ToInt64(sizerecord[10]);
                                bomentity.Quantity = sizerecord[8] != "" ? Convert.ToDecimal(sizerecord[8]) : 0;
                                bomentity.IsActive = true;
                                bomentity.Attribute_ID = Convert.ToString(sizerecord[6]);
                                sizerecord[7] = Convert.ToString(sizerecord[7]).Substring(0, 1) == "," ? Convert.ToString(sizerecord[7]).Substring(1) : sizerecord[7];
                                bomentity.Attribute_Value_ID = Convert.ToString(sizerecord[7]);
                                bomentity.rate = sizerecord[9] != "" ? Convert.ToDecimal(sizerecord[9]) : 0;
                                bomentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                bomentity.BranchId = Convert.ToInt32(collection.BranchId);
                                bomentity.Segmenttypeid = Convert.ToInt32(sizerecord[0]);
                                bomentity.Styleid = Convert.ToInt32(frm["styleid"]);
                                bomentity.Itemid = Convert.ToInt32(frm["itemid"]);
                                bomentity.Styleno = frm["styleno"].ToString();
                                bomentity.Styledesc = frm["styledesc"].ToString();
                                bomentity.Brand = frm["brand"].ToString();
                                bomentity.Totalqty = Convert.ToString(frm["reqqty"]) != "" ? Convert.ToInt32(frm["reqqty"]) : 0;
                                bomentity.Createdby = Convert.ToInt32(collection.Id);
                                bomentity.SellRate = sizerecord[11] != "" ? Convert.ToDecimal(sizerecord[11]) : 0;
                                bomentity.GST = sizerecord[12] != "" ? Convert.ToDecimal(sizerecord[12]) : 0;
                                bomentity.Subtotal = sizerecord[14] != "" ? Convert.ToDecimal(sizerecord[14]) : 0;
                                int ICount = Convert.ToInt32(sizerecord[15]);
                                int k = 0;
                                var Attrimgarray = AttributeImage.ToList();
                                List<string> imgdata = new List<string>();
                                foreach (var img in Attrimgarray)
                                {
                                    if (k != ICount)
                                    {
                                        if (img != "")
                                        {
                                            imgdata.Add(img.ToString());
                                            AttributeImage.RemoveAt(0);
                                        }
                                        else
                                        {
                                            AttributeImage.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    k++;
                                }
                                bomentity.itemimg_list = imgdata;
                                
                                bomsizeid = bomsizedetailservice.Insert(bomentity);
                            }
                        }


                    }

                    if(isPR>0)
                    {
                        SRM_PurchaseRequestMasterEntity PRentity = new SRM_PurchaseRequestMasterEntity();
                        PRentity.PR_Date = DateTime.Now;
                        PRentity.BOM_Date = DateTime.Now;
                        var StyleId = frm["hdnStyleId"];
                        PRentity.EnquiryId = Enqid > 0 ? Enqid : 9999;
                        PRentity.POid = POid;
                        PRentity.StyleId = styleid;
                        var priority = "Normal";
                        PRentity.Priority = priority;
                        PRentity.IsActive = true;
                        PRentity.CreatedDate = DateTime.Now;
                        PRentity.Approvaldate = DateTime.Now;
                        PRentity.CreatedBy = Convert.ToInt32(collection.Id);
                        PRentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        PRentity.BranchId = Convert.ToInt32(collection.BranchId);
                        PRentity.IsReadymade = IsReadyMate;

                        int roleid = Convert.ToInt32(collection.RoleID);
                        PRentity.ApprovalStatus = false;
                        if (roleid != 4)
                        {
                            PRentity.ApprovalStatus =  true;
                            PRentity.IsStatus = 3;
                            PRentity.Approvaldate = DateTime.Now;
                        }
                        else
                        {
                            PRentity.ApprovalStatus = false;
                            PRentity.IsStatus = 2;
                        }
                        
                        PRentity.BOM_Id = insertedid;
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = insertedid;
                        long id = 0;
                        id = supplierprservice.Insert(PRentity);
                        long prrid = 0;
                        if (id > 0)
                        {
                            
                            foreach (var arritem in rawitemarr)
                            {
                                if (arritem != "")
                                {   
                                    string[] styleitem = arritem.Split('~');
                                    SRM_PurchaseRequestItemDetailEntity pritementity = new SRM_PurchaseRequestItemDetailEntity();
                                    pritementity.PR_ID = id;
                                    pritementity.ItemCategoryId = Convert.ToInt32(styleitem[0]);
                                    pritementity.ItemId = Convert.ToInt32(styleitem[1]);
                                    pritementity.Item_Attribute_ID = Convert.ToInt32(styleitem[2]);
                                    pritementity.UnitId = Convert.ToInt32(styleitem[7]);
                                    pritementity.orderQty = Convert.ToDecimal(styleitem[5]);
                                    pritementity.RequiredQty= Convert.ToDecimal(styleitem[5]);
                                    pritementity.IsActive = true;
                                    pritementity.CreatedDate = DateTime.Now;
                                    pritementity.CreatedBy = Convert.ToInt32(collection.Id);
                                    pritementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                    pritementity.BranchId = Convert.ToInt32(collection.BranchId);
                                    prrid = supplierpritemdetailservice.Insert(pritementity);
                                    j++;
                                }
                            }
                            
                            if (prrid > 0)
                            {
                                bool updatestatus = false;
                                bomentity.IsPRDone = true;
                                updatestatus = bomservice.UpdatePRDone(bomentity);
                                TempData["alertmsg"] = "Auto PR Create Successfully!";
                                TempData["alertclass"] = "alert alert-success alert-dismissible";
                                return RedirectToAction("Index");
                            }
                        }
                    }

                    if (insertedid > 0)
                    {
                        TempData["alertmsg"] = "Record Save Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        [HttpGet]
        public ActionResult Edit(long bomid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.title = "Bill Of Material";

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            entity = billofmaterialservice.GetById(bomid);
            //entity.M_BillOfMaterialDetails = billmaterialdetailservice.GetById(bomid);
            entity.M_BillOfMaterialDetail = billmaterialdetailservice.GetById(bomid);
            // entity.M_BOMSizeDetails = bomsizedetailservice.GetById(entity.M_BillOfMaterialDetail);
            var rawitemlist = itemservice.get().ToList();
            int CBS = 1;
            ViewBag.customerlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", (long)entity.CustomerId);
            ViewBag.addresslist = DropDownList<M_Ledger_BillingDetailsEntity>.LoadItems(ledgeraccountservices.GetAddressById((long)entity.CustomerId), "Id", "Address", (long)entity.CustomerAddressId);
            if (entity.M_BOMSizeDetails.Styleid != null)
            {
                ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.M_BOMSizeDetails.Styleid);
            }
            else
            {
                ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)0);
            }
            if (entity.M_BOMSizeDetails.Segmenttypeid != null)
            {
                ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.M_BOMSizeDetails.Segmenttypeid);
            }
            else
            {
                ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)0);
            }
            if (entity.Prooce_Cycle_id != null) { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Prooce_Cycle_id", "Process_cycle_Name", (int)entity.Prooce_Cycle_id); }
            else { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Prooce_Cycle_id", "Process_cycle_Name", 0); }
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            entity.itemlist_ = rawitemlist;
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            entity.unitlist_ = unitservice.get();
            ViewBag.Attribute = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get(), "Attribute_ID", "Attribute_Name", 0);

            return View(entity);
        }

        //[HttpPost]
        //public ActionResult Edit(long id, FormCollection frm, M_BillOfMaterialMasterEntity entity)
        //{
        //    var rawitemstrlist = frm["BOMitemList"].ToString();
        //    var styleid = Convert.ToInt64(frm["Styleid"]);
        //    entity.StyleId = styleid;
        //    var BOM_No = frm["BOM_No"];
        //    entity.BOM_No = BOM_No;
        //    entity.EnquiryId = id;
        //    entity.UpdatedDate = DateTime.Now;
        //    entity.IsActive = true;
        //    bool updatedid = billofmaterialservice.Update(entity);
        //    if (updatedid == true)
        //    {
        //        string[] rawitemarr = rawitemstrlist.Split('#');
        //        "64~65~66~#12~12~12~#"
        //        var sizelist = frm["BOMSizeList"].ToString();
        //        var qtylist = frm["BOMQtyList"].ToString();
        //        var sizeidlist = frm["BOMSizeIdList"].ToString();

        //        string[] sizearr = sizelist.Split('#');
        //        string[] qtyarr = qtylist.Split('#');
        //        string[] sizeidarr = sizeidlist.Split('#');
        //        var j = 0;
        //        var k = 0;
        //        bool bomsizeid = false;
        //        foreach (var arritem in rawitemarr)
        //        {
        //            string[] styleitem = arritem.Split('~');
        //            M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
        //            styleentity.BOM_ID = entity.BOM_ID;
        //            styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
        //            styleentity.ItemId = Convert.ToInt32(styleitem[1]);
        //            styleentity.SupplierName = Convert.ToString(styleitem[2]);
        //            styleentity.RequiredQty = Convert.ToDecimal(styleitem[3]);
        //            styleentity.UnitId = Convert.ToInt32(styleitem[4]);
        //            styleentity.ID = Convert.ToInt32(styleitem[5]);
        //            styleentity.Comment = frm["comment"].ToString();
        //            styleentity.IsActive = true;
        //            styleentity.CreatedDate = DateTime.Now;
        //            styleentity.UpdatedDate = DateTime.Now;
        //            bool updatetid = billmaterialdetailservice.Update(styleentity);
        //            if (updatetid == true)
        //            {
        //                string[] qtysingle = { };
        //                qtysingle = qtyarr[j].Split('~');


        //                string[] sizeidsingle = { };
        //                sizeidsingle = sizeidarr[k].Split('~');

        //                int i = 0;

        //                string[] sizerecord = sizearr[j].Split('~');


        //                foreach (var sizeitem in sizerecord)
        //                {
        //                    M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
        //                    bomentity.BOMD_Id = styleentity.ID;
        //                    bomentity.Id = Convert.ToInt32(sizeidsingle[k]);
        //                    bomentity.SizeId = Convert.ToInt32(sizeitem);
        //                    bomentity.Quantity = Convert.ToDecimal(qtysingle[i]);
        //                    bomentity.IsActive = true;
        //                    bomsizeid = bomsizedetailservice.Update(bomentity);
        //                    i++;
        //                }
        //            }
        //            j++;
        //            k++;
        //        }
        //        if (bomsizeid == true)
        //        {
        //            TempData["alertbommsg"] = "Bill of Material Updated Successfully.";
        //            TempData["alertclass"] = "alert alert-success alert-dismissible";
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    entity = billofmaterialservice.GetById(entity.BOM_ID);
        //    entity.CRM_EnquiryMaster = enquiryservice.getbyid(id);
        //    entity.style = enquirystyledetailservice.getbyenquiryid(id);
        //    entity.rawitemlist = enquiryrawitemservice.getbyenquiryid(id);
        //    entity.M_BillOfMaterialDetail = billmaterialdetailservice.GetById(entity.BOM_ID);
        //    var rawitemlist = itemservice.get().ToList();
        //    ViewBag.colorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", (long)entity.style.Stylecolorid);
        //    ViewBag.customerlist = DropDownList<M_CustomerMasterEntity>.LoadItems(customerservice.get(), "Id", "contactname", (int)entity.CRM_EnquiryMaster.customerid);
        //    ViewBag.addresslist = DropDownList<M_ContactAddressEntity>.LoadItems(customerservice.getAddresssbycustomerid((long)entity.CRM_EnquiryMaster.customerid), "Id", "Addess", (int)entity.CRM_EnquiryMaster.Addressid);
        //    ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.style.Styleid);
        //    ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.style.Segmenttypeid);
        //    if (entity.Prooce_Cycle_id != null) { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", (int)entity.Prooce_Cycle_id); }
        //    else { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", 0); }
        //    ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
        //    entity.itemlist_ = rawitemlist;
        //    ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
        //    entity.unitlist_ = unitservice.get();
        //    return View(entity);
        //}

        [RBAC]
        public JsonResult getItems(long categoryid)
        {
            var list = itemservice.GetByCategoryId(categoryid);
            return Json(list);
        }

        [RBAC]
        public JsonResult getUnit(long itemid, string IAid)
        {
            string AttibId = IAid;
            int type = 2;
            string myjson = "";
            myjson = "[{\"itemid\":\"" + itemid + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = billofmaterialservice.InsertAutoPR(doc, type, AttibId);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);

            //var list = itemservice.GetById(itemid);
            //return Json(list);
        }


        //Create by Kiran 30-07-2019 start
        [RBAC]
        [HttpGet]
        public ActionResult BOM_by_PO(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int CBS = 1;//Customer ID
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            entity.CRM_POMaster = CustomerPoservice.getbyid(id);
            var dd = entity.CRM_POMaster.Quotrefno;
            var qut = Convert.ToInt32(dd);
            entity.porawitemlist = quotationrawitemservice.getbyquotationid(qut);
            ViewBag.Attribute = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get(), "Attribute_ID", "Attribute_Name", 0);
            ViewBag.customerlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", (long)entity.CRM_POMaster.customerid);
            ViewBag.addresslist = DropDownList<M_Ledger_BillingDetailsEntity>.LoadItems(ledgeraccountservices.GetAddressById((long)entity.CRM_POMaster.customerid), "Id", "Address", (long)entity.CRM_POMaster.Addressid);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.CRM_POMaster.styledetailentity.Styleid);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.CRM_POMaster.styledetailentity.Segmenttypeid);
            ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Prooce_Cycle_id", "Process_cycle_Name", 0);
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);

            string[] BOM_No = billofmaterialservice.getnextbomno().Split('-');
            entity.BOM_No = BOM_No[0] + "-" + BOM_No[1] + "-" + BOM_No[2] + "-" + BOM_No[3] + "-XXXX";

            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult BOM_by_PO(long id, FormCollection frm)
        {
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            try
            {
                entity.RevisionNo = Convert.ToDecimal(frm["RevisionNo"]);
                entity.Prooce_Cycle_id = Convert.ToInt32(frm["Prooce_Cycle_id"]);
                entity.IsPRDone = false;

                var rawitemstrlist = Convert.ToString(frm["BOMitemList"]);
                var styleid = Convert.ToInt64(frm["Styleid"]);
                entity.StyleId = styleid;
                var BOM_No = frm["BOM_No"];
                entity.BOM_No = BOM_No;
                entity.Poid = id;
                entity.CreatedDate = DateTime.Now;
                entity.CustomerId = Convert.ToInt32(frm["Customer_id"]);
                entity.CustomerAddressId = Convert.ToInt32(frm["customeraddress"]);
                entity.IsActive = true;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);

                var processcyclelist = Convert.ToString(frm["bomprocessCyclelist"]);
                if (processcyclelist != null || processcyclelist != "")
                {
                    string[] BOMPCarr = processcyclelist.Split('#');
                    List<M_BOM_ProcessCycleEntity> BOMpclist = new List<M_BOM_ProcessCycleEntity>();
                    foreach (var arritem in BOMPCarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BOM_ProcessCycleEntity styleentity = new M_BOM_ProcessCycleEntity();
                            styleentity.ProcessId = Convert.ToInt32(styleitem[0]);
                            styleentity.Processsequence = Convert.ToInt32(styleitem[1]);
                            styleentity.ItemOutId = Convert.ToInt64(styleitem[2]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[3]);
                            styleentity.PerQty = Convert.ToDecimal(styleitem[4]);
                            styleentity.RequiredQty = Convert.ToDecimal(styleitem[5]);
                            styleentity.IsActive = true;
                            styleentity.CreatedBy = Convert.ToInt32(collection.Id);
                            styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            styleentity.BranchId = Convert.ToInt32(collection.BranchId);
                            BOMpclist.Add(styleentity);
                        }
                    }
                    entity.BOM_ProcessCycle = BOMpclist;
                }
                long insertedid = billofmaterialservice.Insert(entity);
                if (insertedid > 0)
                {
                    string[] rawitemarr = rawitemstrlist.Split('#');
                    // "64~65~66~#12~12~12~#"
                    var sizelist = Convert.ToString(frm["BOMSizeList"]);

                    string[] sizearr = sizelist.Split('#');
                    var j = 0;
                    decimal bomsizeid = 0;
                    long insertid = 0;
                    foreach (var arritem in rawitemarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
                            styleentity.BOM_ID = insertedid;
                            styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
                            styleentity.ItemId = Convert.ToInt32(styleitem[1]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[2]);
                            styleentity.SupplierName = Convert.ToString(styleitem[3]);
                            styleentity.RequiredQty = Convert.ToDecimal(styleitem[7]) == 0 ? Convert.ToDecimal(styleitem[5]) : Convert.ToDecimal(styleitem[7]);
                            styleentity.UnitId = Convert.ToInt32(styleitem[8]);
                            styleentity.PerQty = Convert.ToDecimal(styleitem[4]);
                            styleentity.Comment = Convert.ToString(frm["comment"]);
                            styleentity.IsActive = true;
                            styleentity.CreatedDate = DateTime.Now;
                            styleentity.CreatedBy = Convert.ToInt32(collection.Id);
                            styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            styleentity.BranchId = Convert.ToInt32(collection.BranchId);
                            insertid = billmaterialdetailservice.Insert(styleentity);

                            j++;
                        }
                    }
                    if (insertid > 0)
                    {
                        foreach (var data in sizearr)
                        {
                            if (data != "")
                            {
                                string[] sizerecord = data.Split('~');
                                M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
                                bomentity.BOMD_Id = insertedid;
                                bomentity.Quantity = Convert.ToDecimal(sizerecord[5]);
                                bomentity.Attribute_ID = Convert.ToString(sizerecord[1]);
                                sizerecord[2] = Convert.ToString(sizerecord[2]).Substring(0, 1) == "," ? Convert.ToString(sizerecord[2]).Substring(1) : sizerecord[2];
                                bomentity.Attribute_Value_ID = Convert.ToString(sizerecord[2]);
                                bomentity.rate = Convert.ToDecimal(sizerecord[6]);
                                bomentity.SizeId = Convert.ToInt64(sizerecord[7]);
                                bomentity.IsActive = true;
                                bomentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                bomentity.BranchId = Convert.ToInt32(collection.BranchId);
                                bomsizeid = bomsizedetailservice.Insert(bomentity);
                            }
                        }
                    }
                    if (bomsizeid > 0)
                    {
                        TempData["alertmsg"] = "Record Save Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";

                        return RedirectToAction("Index");

                    }
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");

        }

        [RBAC]
        [HttpGet]
        public ActionResult BOM_PO_Edit(long id, long bomid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int CBS = 1;//Customer ID
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            entity = billofmaterialservice.GetById(bomid);
            entity.CRM_POMaster = CustomerPoservice.getbyid(id);
            var dd = entity.CRM_POMaster.Quotrefno;
            var qut = Convert.ToInt32(dd);
            entity.porawitemlist = quotationrawitemservice.getbyquotationid(qut);
            //entity.M_BillOfMaterialDetails = billmaterialdetailservice.GetById(bomid);
            entity.M_BillOfMaterialDetail = billmaterialdetailservice.GetById(bomid);
            // entity.M_BOMSizeDetails = bomsizedetailservice.GetById(entity.M_BillOfMaterialDetail);
            var rawitemlist = itemservice.get().ToList();

            ViewBag.customerlist = DropDownList<M_LedgersEntity>.LoadItems(ledgeraccountservices.getCustList(CBS), "Ledger_Id", "Ledger_Name", (long)entity.CRM_POMaster.customerid);
            ViewBag.addresslist = DropDownList<M_Ledger_BillingDetailsEntity>.LoadItems(ledgeraccountservices.GetAddressById((long)entity.CRM_POMaster.customerid), "Id", "Address", (long)entity.CRM_POMaster.Addressid);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.CRM_POMaster.styledetailentity.Styleid);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.CRM_POMaster.styledetailentity.Segmenttypeid);
            if (entity.Prooce_Cycle_id != null) { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Prooce_Cycle_id", "Process_cycle_Name", (int)entity.Prooce_Cycle_id); }
            else { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Prooce_Cycle_id", "Process_cycle_Name", 0); }
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            entity.itemlist_ = rawitemlist;
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            entity.unitlist_ = unitservice.get();

            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(long bomid, FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] rawitemimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            try
            {
                string styleimgpath = string.Empty;
                List<string> styleimglist = new List<string>();
                List<string> rawimglist = new List<string>();
                List<string> AttributeImage = new List<string>();
                int IsReadyMate = Convert.ToInt32(frm["hidIsReadyMate"]);
                if (Request.Files.Count > 0)
                {
                    foreach (var stfile in styleimg)
                    {
                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);
                        }
                    }

                    if(IsReadyMate==0)
                    {
                        if (rawitemimg != null)
                        {
                            foreach (var rimg in rawitemimg)
                            {

                                if (rimg != null && rimg.ContentLength > 0)
                                {
                                    var filename = Path.GetFileName(rimg.FileName);
                                    var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                    string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                    bool exists = System.IO.Directory.Exists(isExistCompanypath);

                                    if (!exists)
                                        System.IO.Directory.CreateDirectory(isExistCompanypath);

                                    var path = Path.Combine(isExistCompanypath, newfilename);
                                    rimg.SaveAs(path);
                                    rawimglist.Add(newfilename);
                                }
                                else
                                {
                                    rawimglist.Add("");
                                }
                            }
                        }

                    }

                    if (ItemAttrimages_id != null)
                    {
                        foreach (var rimg in ItemAttrimages_id)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                                string isExistImagepath = Server.MapPath("~/ItemImages/");
                                bool exists = System.IO.Directory.Exists(isExistImagepath);
                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistImagepath);
                                var path = Path.Combine(isExistImagepath, newfilename);
                                rimg.SaveAs(path);
                                AttributeImage.Add(newfilename);
                            }
                        }
                    }

                }
                entity.IsReadymate = IsReadyMate;
                entity.RevisionNo = Convert.ToDecimal(frm["RevisionNo"]);
                if (IsReadyMate == 0)
                {
                    entity.Prooce_Cycle_id = Convert.ToInt32(frm["Prooce_Cycle_id"]);
                }
                var rawitemstrlist = Convert.ToString(frm["BOMitemList"]);

                var styleid = Convert.ToInt64(frm["styleid"]);
                entity.StyleId = styleid;
                var BOM_No = frm["BOM_No"];
                entity.BOM_No = BOM_No;
                entity.EnquiryId = bomid;
                entity.UpdatedDate = DateTime.Now;
                entity.IsActive = true;
                entity.CustomerId = Convert.ToInt32(frm["customerid"]);
                entity.CustomerAddressId = Convert.ToInt32(frm["customeraddress"]);
                entity.BOM_ID = Convert.ToInt64(frm["BOMid"]);
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.UpdatedBy = Convert.ToInt32(collection.Id);
                
                var processcyclelist = Convert.ToString(frm["bomprocessCyclelist"]);
                if (processcyclelist != null && processcyclelist != "")
                {
                    string[] BOMPCarr = processcyclelist.Split('#');
                    List<M_BOM_ProcessCycleEntity> BOMpclist = new List<M_BOM_ProcessCycleEntity>();
                    foreach (var arritem in BOMPCarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BOM_ProcessCycleEntity styleentity = new M_BOM_ProcessCycleEntity();
                            styleentity.ProcessId = Convert.ToInt32(styleitem[0]);
                            styleentity.Processsequence = Convert.ToInt32(styleitem[1]);
                            styleentity.ItemOutId = Convert.ToInt64(styleitem[2]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[3]);
                            styleentity.ID = Convert.ToInt64(styleitem[4]);
                            styleentity.IsActive = true;
                            styleentity.UpdatedBy = Convert.ToInt32(collection.Id);
                            BOMpclist.Add(styleentity);
                        }
                    }
                    entity.BOM_ProcessCycle = BOMpclist;
                }

                bool updatedid = billofmaterialservice.Update(entity);
                if (updatedid == true)
                {
                    string[] rawitemarr = rawitemstrlist.Split('#');
                    // "64~65~66~#12~12~12~#"
                    var sizelist = Convert.ToString(frm["BOMSizeList"]);

                    string[] sizearr = sizelist.Split('#');
                    var j = 0;
                    var k = 0;
                    bool bomsizeid = false;
                    bool updatetid = false;
                    if (IsReadyMate == 0)
                    {
                        foreach (var arritem in rawitemarr)
                        {
                            if (arritem != "")
                            {
                                string[] styleitem = arritem.Split('~');
                                M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
                                styleentity.BOM_ID = entity.BOM_ID;
                                styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
                                styleentity.ItemId = Convert.ToInt32(styleitem[1]);
                                styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[2]);
                                styleentity.SupplierName = Convert.ToString(styleitem[3]);
                                styleentity.RequiredQty = Convert.ToDecimal(styleitem[5]);
                                styleentity.UnitId = Convert.ToInt32(styleitem[7]);
                                styleentity.PerQty = Convert.ToInt32(styleitem[4]);
                                styleentity.Comment = Convert.ToString(frm["comment"]);
                                styleentity.IsActive = true;

                                styleentity.UpdatedDate = DateTime.Now;
                                styleentity.UpdatedBy = Convert.ToInt32(collection.Id);
                                int RICount = Convert.ToInt32(styleitem[9]);
                                int m = 0;
                                var Rawimgarray = rawimglist.ToList();
                                List<string> rawimgdata = new List<string>();
                                if (Rawimgarray.Count > 0 && RICount > 0)
                                {
                                    foreach (var img in Rawimgarray)
                                    {
                                        if (m != RICount)
                                        {
                                            if (img != "")
                                            {
                                                rawimgdata.Add(img.ToString());
                                                rawimglist.RemoveAt(0);
                                            }
                                            else
                                            {
                                                rawimglist.RemoveAt(0);
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        m++;
                                    }
                                }
                                styleentity.rawitemimg_list = rawimgdata;

                                if (Convert.ToInt32(styleitem[8]) != 0)
                                {
                                    updatetid = billmaterialdetailservice.Update(styleentity);
                                }
                                else
                                {
                                    billmaterialdetailservice.Insert(styleentity);
                                    updatetid = true;
                                }

                                j++;
                                k++;
                            }
                        }
                    }
                    if (updatedid == true)
                    {
                        foreach (var data in sizearr)
                        {
                            if (data != "")
                            {
                                string[] sizerecord = data.Split('~');
                                M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
                                bomentity.BOMD_Id = entity.BOM_ID;
                                bomentity.Id = Convert.ToInt32(sizerecord[0]);
                                bomentity.SizeId = Convert.ToInt64(sizerecord[11]);
                                bomentity.Quantity = sizerecord[9] != "" ? Convert.ToDecimal(sizerecord[9]) : 0;
                                bomentity.IsActive = true;
                                bomentity.Attribute_ID = Convert.ToString(sizerecord[7]);
                                sizerecord[8] = Convert.ToString(sizerecord[8]).Substring(0, 1) == "," ? Convert.ToString(sizerecord[8]).Substring(1) : sizerecord[8];
                                bomentity.Attribute_Value_ID = Convert.ToString(sizerecord[8]);
                                bomentity.rate = sizerecord[10] != "" ? Convert.ToDecimal(sizerecord[10]) : 0;
                                bomentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                bomentity.BranchId = Convert.ToInt32(collection.BranchId);
                                bomentity.Segmenttypeid = Convert.ToInt32(sizerecord[1]);
                                bomentity.Styleid = Convert.ToInt32(frm["styleid"]);
                                bomentity.Itemid = Convert.ToInt32(frm["itemid"]);
                                bomentity.Styleno = frm["styleno"].ToString();
                                bomentity.Styledesc = frm["styledesc"].ToString();
                                bomentity.Brand = frm["brand"].ToString();
                                bomentity.Totalqty = Convert.ToString(frm["reqqty"]) != "" ? Convert.ToDecimal(frm["reqqty"]) : 0;
                                bomentity.Createdby = Convert.ToInt32(collection.Id);
                                bomentity.SellRate = sizerecord[12] != "" ? Convert.ToDecimal(sizerecord[12]) : 0;
                                bomentity.GST = sizerecord[13] != "" ? Convert.ToDecimal(sizerecord[13]) : 0;
                                bomentity.Subtotal = sizerecord[15] != "" ? Convert.ToDecimal(sizerecord[15]) : 0;
                                int ICount = Convert.ToInt32(sizerecord[16]);
                                int m = 0;
                                var Attrimgarray = AttributeImage.ToList();
                                List<string> imgdata = new List<string>();
                                foreach (var img in Attrimgarray)
                                {
                                    if (m != ICount)
                                    {
                                        if (img != "")
                                        {
                                            imgdata.Add(img.ToString());
                                            AttributeImage.RemoveAt(0);
                                        }
                                        else
                                        {
                                            AttributeImage.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    m++;
                                }
                                bomentity.itemimg_list = imgdata;
                                bomsizeid = bomsizedetailservice.Update(bomentity);
                            }
                        }
                    }
                    if (updatedid == true)
                    {
                        TempData["alertmsg"] = "Record Update Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        [HttpPost]
        public ActionResult BOM_PO_Edit(long id, FormCollection frm)
        {
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            try
            {
                entity.RevisionNo = Convert.ToDecimal(frm["RevisionNo"]);
                entity.Prooce_Cycle_id = Convert.ToInt32(frm["Prooce_Cycle_id"]);

                var rawitemstrlist = Convert.ToString(frm["BOMitemList"]);
                var styleid = Convert.ToInt64(frm["Styleid"]);
                entity.StyleId = styleid;
                var BOM_No = frm["BOM_No"];
                entity.BOM_No = BOM_No;
                entity.Poid = id;
                entity.UpdatedDate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.UpdatedBy = Convert.ToInt32(collection.Id);

                entity.CustomerId = Convert.ToInt32(frm["Customer_id"]);
                entity.CustomerAddressId = Convert.ToInt32(frm["customeraddress"]);
                entity.IsActive = true;
                entity.BOM_ID = Convert.ToInt64(frm["BOMid"]);
                var processcyclelist = Convert.ToString(frm["bomprocessCyclelist"]);
                if (processcyclelist != null || processcyclelist != "")
                {
                    string[] BOMPCarr = processcyclelist.Split('#');
                    List<M_BOM_ProcessCycleEntity> BOMpclist = new List<M_BOM_ProcessCycleEntity>();
                    foreach (var arritem in BOMPCarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BOM_ProcessCycleEntity styleentity = new M_BOM_ProcessCycleEntity();
                            styleentity.ProcessId = Convert.ToInt32(styleitem[0]);
                            styleentity.Processsequence = Convert.ToInt32(styleitem[1]);
                            styleentity.ItemOutId = Convert.ToInt64(styleitem[2]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[3]);
                            styleentity.ID = Convert.ToInt64(styleitem[4]);
                            styleentity.IsActive = true;
                            entity.UpdatedBy = Convert.ToInt32(collection.Id);
                            BOMpclist.Add(styleentity);
                        }
                    }
                    entity.BOM_ProcessCycle = BOMpclist;
                }
                bool updatedid = billofmaterialservice.Update(entity);
                if (updatedid == true)
                {
                    string[] rawitemarr = rawitemstrlist.Split('#');
                    // "64~65~66~#12~12~12~#"
                    var sizelist = Convert.ToString(frm["BOMSizeList"]);

                    string[] sizearr = sizelist.Split('#');
                    var j = 0;
                    var k = 0;
                    bool bomsizeid = false;
                    bool updatetid = false;
                    long insertid = 0;
                    foreach (var arritem in rawitemarr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');
                            M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
                            styleentity.BOM_ID = entity.BOM_ID;
                            styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
                            styleentity.ItemId = Convert.ToInt32(styleitem[1]);
                            styleentity.Item_Attribute_ID = Convert.ToInt32(styleitem[2]);
                            styleentity.SupplierName = Convert.ToString(styleitem[3]);
                            styleentity.RequiredQty = Convert.ToDecimal(styleitem[7]) == 0 ? Convert.ToDecimal(styleitem[5]) : Convert.ToDecimal(styleitem[7]);
                            styleentity.UnitId = Convert.ToInt32(styleitem[8]);
                            styleentity.ID = Convert.ToInt32(styleitem[9]);
                            styleentity.PerQty = Convert.ToInt32(styleitem[4]);
                            // styleentity.Comment = frm["comment"].ToString();
                            styleentity.IsActive = true;
                            styleentity.CreatedDate = DateTime.Now;
                            styleentity.UpdatedDate = DateTime.Now;
                            entity.UpdatedBy = Convert.ToInt32(collection.Id);

                            if (Convert.ToInt32(styleitem[9]) == 0)
                            {
                                insertid = billmaterialdetailservice.Insert(styleentity);
                            }
                            else
                            {
                                updatetid = billmaterialdetailservice.Update(styleentity);
                            }


                            j++;
                            k++;
                        }
                    }

                    if (updatetid == true)
                    {
                        foreach (var data in sizearr)
                        {
                            if (data != "")
                            {
                                string[] sizerecord = data.Split('~');
                                M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
                                bomentity.BOMD_Id = entity.BOM_ID;
                                bomentity.Id = Convert.ToInt32(sizerecord[0]);
                                bomentity.Quantity = Convert.ToDecimal(sizerecord[5]);
                                bomentity.Attribute_ID = Convert.ToString(sizerecord[1]);
                                sizerecord[2] = Convert.ToString(sizerecord[2]).Substring(0, 1) == "," ? Convert.ToString(sizerecord[2]).Substring(1) : sizerecord[2];
                                bomentity.Attribute_Value_ID = Convert.ToString(sizerecord[2]);
                                bomentity.rate = Convert.ToDecimal(sizerecord[6]);
                                bomentity.SizeId = Convert.ToInt64(sizerecord[7]);
                                bomentity.IsActive = true;
                                bomsizeid = bomsizedetailservice.Update(bomentity);
                            }
                        }
                    }
                    if (bomsizeid == true)
                    {
                        TempData["alertmsg"] = "Record Update Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }
        //Create by Kiran 30-07-2019 END

        [RBAC]
        [HttpPost]
        public JsonResult SaveAutoPR(string Model, int type)
        {
            string AttibId = "";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + Model + "}", "root"); // JSON needs to be an objec
            var insertedid = billofmaterialservice.InsertAutoPR(doc, type, AttibId);
            DataSet dt = new DataSet();
            dt = (DataSet)insertedid;
            foreach (DataTable table in dt.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                    if (Convert.ToInt32(dr["save"]) == 200)
                    {
                        TempData["alertmsg"] = Convert.ToString(dr["msg"]);
                    }
                    else
                    {
                        TempData["alertmsg"] = dr["ErrorMessage"];
                    }
                }
                break;
            }
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(insertedid);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult GetRawItemList(int subCatid)
        {
            var insertedid = itemsubcategoryservice.GetRawItemList(subCatid);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(insertedid);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult Getitemgroup()
        {
            var insertedid = itemgroupservice.Get();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(insertedid);
            return Json(JSONString);
        }

        [RBAC]
        public JsonResult Getunitlist()
        {
            var insertedid = unitservice.get();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(insertedid);
            return Json(JSONString);
        }


        [RBAC]
        public JsonResult getenquiryList(long custid)
        {
            List<Object> list = new List<object>();
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var Enquery = billofmaterialservice.getenquiryList(Company_ID, BranchId, custid).OrderByDescending(x => x.EnquiryId).ToList();
            list.Add(Enquery);
            var PO = billofmaterialservice.POList(Company_ID, BranchId, custid).OrderByDescending(x => x.Id).ToList();
            list.Add(PO);
            return Json(list);
        }

        [RBAC]
        public JsonResult getPOList(long EnqId)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var PO = billofmaterialservice.EnqPOList(Company_ID, BranchId, EnqId).OrderByDescending(x => x.Id).ToList();
            return Json(PO);
        }


        [RBAC]
        [HttpPost]
        public JsonResult GetBOMList(long itemid)
        {
            List<object> Bomdata = new List<object>();
            var styleData = billmaterialdetailservice.GetStyleDetails(itemid);
            Bomdata.Add(styleData);
            if (styleData.Id > 0)
            {
                var list = billmaterialdetailservice.GetBOMList(styleData.Id);
                Bomdata.Add(list);
            }
            return Json(Bomdata);
        }

        [RBAC]
        [HttpPost]
        public JsonResult getBOMDetails(long bomid)
        {
            string JSONString = string.Empty;
            try
            {
                List<object> list = new List<object>();
                var BOMStyle = billmaterialdetailservice.GetBOMStyle(bomid);
                list.Add(BOMStyle);
                var Materiallist = billmaterialdetailservice.GetById(bomid);
                list.Add(Materiallist);

                JSONString = JsonConvert.SerializeObject(list);
                return Json(JSONString);
            }
            catch (Exception ex)
            {
                TempData["alertprmsg"] = "Something went wrong, Please try again.";
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                JSONString = JsonConvert.SerializeObject(ex);
                return Json(JSONString);
            }
        }

        [RBAC]
        [HttpPost]
        public JsonResult getenquiryDetails(long EnqId,long POid)
        {
            string JSONString = string.Empty;
            try
            {
                List<object> list = new List<object>();
                var enqlist = enquiryservice.getbyid(EnqId);
                list.Add(enqlist);
                if (POid > 0)
                {
                    var polist = CustomerPoservice.getbyid(POid);
                    list.Add(polist);
                }
                JSONString = JsonConvert.SerializeObject(list);
                return Json(JSONString);
            }
            catch (Exception ex)
            {
                TempData["alertprmsg"] = "Something went wrong, Please try again.";
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                JSONString = JsonConvert.SerializeObject(ex);
                return Json(JSONString);
            }
        }
    }
}
