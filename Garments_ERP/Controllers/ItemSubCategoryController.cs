﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class ItemSubCategoryController : SuperController
    {
        //
        // GET: /ItemSubCategory/
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Item Sub Category";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = itemsubcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public JsonResult CheckItemSubCategory(string Itemsubcategory, int? Id, int itemcategoryid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            bool value = itemsubcategoryservice.CheckExistance(Itemsubcategory, Id, itemcategoryid, companyid, branchid);
            return Json(value, JsonRequestBehavior.AllowGet);
        }


        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Item Sub Category";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_ItemSubCategoryMasterEntity entity)
        {
            try
            {
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Createdby = Convert.ToInt32(collection.Id);
                entity.Createddate = DateTime.Now;
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                long id = itemsubcategoryservice.Insert(entity);
                if (id > 0)
                {

                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");

                }
            }
            catch(Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Item Sub Category";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_ItemSubCategoryMasterEntity entity = itemsubcategoryservice.bysubcategoryid(id);
             ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory",(long)entity.itemcategoryid);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_ItemSubCategoryMasterEntity entity)
        {
            try
            {
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Updatedby = Convert.ToInt32(collection.Id);
                entity.Updateddate = DateTime.Now;
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                bool isupdate = itemsubcategoryservice.Update(entity);
                if (isupdate == true)
                {

                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }


        [RBAC]
        public ActionResult delete(int id)
        {
            bool delete = itemsubcategoryservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }


    }
}
