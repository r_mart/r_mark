﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ItemAttributeController : Controller
    {
        //
        // GET: /ItemAttribute/
        ItemService itemService = ServiceFactory.GetInstance().ItemService;
        AttributeValueService attributevalservice = ServiceFactory.GetInstance().AttributeValueService;
        AttributeService attributeservice = ServiceFactory.GetInstance().AttributeService;
        ItemAttributeValueService itemattributeservice= ServiceFactory.GetInstance().ItemAttributeValueService;
        //
        // GET: /Attribute/

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Item Attribute";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = itemattributeservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.BranchId == ViewBag.branchid).ToList();
            return View(list);
        }


        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Item Attribute";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemService.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid).ToList(), "Id", "ItemName", 0);
            //ViewBag.attributevallist = DropDownList<M_Attribute_Value_MasterEntity>.LoadItems(attributevalservice.get(), "Attribute_Value_ID", "Attribute_Value", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_Item_Attribute_MasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        entity.Is_Active = true;
                        entity.Created_Date = DateTime.Now;
                        entity.Created_By = Helper.Helper.CurrentLoggedUser.Id;
                        int id = itemattributeservice.Insert(entity);
                        if (id > 0)
                        {

                            TempData["alertItemAttrmsg"] = "Record inserted successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");

        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Item Attribute";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_Item_Attribute_MasterEntity model = new M_Item_Attribute_MasterEntity();
            model = itemattributeservice.GetById(id);
            if (model.Item_ID != null)
            {
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemService.get(), "Id", "ItemName", (int)model.Item_ID);
            }
            else
            {
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemService.get(), "Id", "ItemName", 0);
            }

            return View(model);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_Item_Attribute_MasterEntity model)
        {
            try
            {
                model.Modified_Date = DateTime.Now;
                model.Is_Active = true;
                model.Modified_By = Helper.Helper.CurrentLoggedUser.Id;
                bool updatedId = itemattributeservice.Update(model);

                if (updatedId == true)
                {
                    TempData["alertAttrvalmsg"] = "Record updated successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");

        }

        [HttpPost]
        public JsonResult DeleteAttribute(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = itemattributeservice.Delete(Id);
            return Json(ISDeleted);

        }


    }
}
