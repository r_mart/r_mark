﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ScreenController : SuperController
    {
        //
        // GET: /Screen/
        ScreenService screenSer = ServiceFactory.GetInstance().ScreenService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Screen";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            var screenLst = screenSer.get();
            return View(screenLst);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Screen";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }
        
        public JsonResult GetScreen()
        {
            var list = screenSer.get().Where(x=>x.IsActive==true && x.ParentId==0);
            return Json(list);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm) {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                M_ScreenEntity Screenentity = new M_ScreenEntity();
                Screenentity.ScreenName = Convert.ToString(frm["screenName"]);
                Screenentity.Discription = Convert.ToString(frm["screenDis"]);
                Screenentity.URL = Convert.ToString(frm["screenurl"]);
                Screenentity.ParentId = Convert.ToString(frm["parentid"]) == "" ? 0 : Convert.ToInt64(frm["parentid"]);
                Screenentity.IsSub = Screenentity.ParentId == 0 ? 0 : 1;
                Screenentity.CreatedBy = Convert.ToInt32(collection.Id);
                
                long id = screenSer.Insert(Screenentity);
                if(id>0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch(Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Screen";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            M_ScreenEntity entity = screenSer.get().Where(x => x.screenId == id).FirstOrDefault();
            return View(entity);
        }

        [RBAC]
        public JsonResult ActiveDeactive(int id,string Status)
        {
            var status = screenSer.ActiveDeactive(id, Status);
            return Json(status);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                M_ScreenEntity Screenentity = new M_ScreenEntity();
                Screenentity.screenId= Convert.ToInt64(frm["ScreenId"]);
                Screenentity.ScreenName = Convert.ToString(frm["screenName"]);
                Screenentity.Discription = Convert.ToString(frm["screenDis"]);
                Screenentity.URL = Convert.ToString(frm["screenurl"]);
                Screenentity.ParentId = Convert.ToString(frm["parentid"]) == "" ? 0 : Convert.ToInt64(frm["parentid"]);
                Screenentity.IsSub = Screenentity.ParentId == 0 ? 0 : 1;
                Screenentity.UpdateBy = Convert.ToInt32(collection.Id);
                bool id = screenSer.Update(Screenentity);
                if (id==true)
                {
                    TempData["alertmsg"] = "Record Update Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
        }
    }
}
