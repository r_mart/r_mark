﻿function GetBTpp(id) {
    debugger;
    $("#BatchInfoTable").attr('style', 'width: 100%;display:none;');
    $("#BatchInfoTable > tbody").html('');
    $("#BatchInfoTable > thead").html('');
    $("#TotalCount").html('');

    var no = id.match(/\d+/);
    $("#Rowid").val(no);
    GetBatchList();
    
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    $('#BatchInfoModal').modal('toggle');
    
}

function GetItemQuantity() {
    var itemid = $("#InItem").val();
    var MIid = $("#Id").val();
    if (itemid != "" && itemid != "0") {
        $.ajax({
            type: "POST",
            url: '/MaterialIssue/getSRMitemdetail/',
            data: "{'itemid':" + itemid + ",'MIid':" + MIid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#ItemTotQty").val(result.IssueQty);
                GetBatchList(); 
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }
}


function GetBatchList() {
    debugger;
    var ItemId = $("#InItem").val();
    var itemAttr = $("#rawIAid").val();
    var companyid = $("#companyid").val();
    var branchid = $("#branchid").val();
    var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'companyid': companyid, 'branchid': branchid };
    var MyJson = JSON.stringify(Josndata);

    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':8}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            debugger;
            result = JSON.parse(result);
            $("#Batchnoid").empty();
            $.each(result.Table, function (key, BT) {
                $("#Batchnoid").append('<option value="' + BT.ItemInward_ID + '">' + BT.Dynamic_Controls_value + '</option>');
            });
            $("#Batchnoid").multiselect('reload');
            $("#Batchnoid").multiselect({
                columns: 1,
                placeholder: 'Select Batch No',
                search: true,
                searchOptions: {
                    'default': 'Search Batch No'
                },
                selectAll: true
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function GetInwardQty() {
    debugger;
    var no = $("#Rowid").val();
    var ItemId = $("#InItem").val();
    var itemAttr = $("#rawIAid").val();
    var inwardid = $('#Batchnoid option:selected').toArray().map(item => item.value).join();

    var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'inwardid': inwardid };
    var MyJson = JSON.stringify(Josndata);
    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':9}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            debugger;
            result = JSON.parse(result);
            $.each(result.Table, function (key, BT) {
                $("#batchTotQty").val(BT.TotalQty);
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function SetBatchNo() {
    var ErrorMessage = "";
    var isvalid = true;
    var inwardid = $('#Batchnoid option:selected').toArray().map(item => item.value).join();
    var BatchQty = $("#batchTotQty").val();
    if (inwardid == "") {
        ErrorMessage = "Please Select Batch No!";
        isvalid = false;
    }
    if (BatchQty == "0") {
        ErrorMessage = "Your Stock is Less!";
        isvalid = false;
    }
    var inwardvalue = $('#Batchnoid option:selected').toArray().map(item => item.text).join();
    var $dataRows = $("#BatchInfoTable tr:not('.titlerow')");
    $dataRows.each(function () {
        $(this).find('.Batchnor').each(function (i) {
            var batchno = ($(this).val());
            $.each(inwardvalue.split(','), function (i, dt) {
                if (batchno == dt) {
                    ErrorMessage = batchno+" This Batch No. All Ready in Row!";
                    isvalid = false;
                }
            });
        });
    });


    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    } else {

        var tabstr = "";
        $("#BatchInfoTable > thead").html("");
        tabstr = tabstr + "<tr  class='titlerow'>";
        tabstr = tabstr + "<th style='width:16.5em;'>Item Name</th><th style='width:10.5em;'>Required Qty</th><th style='width:10.5em;'>Batch No</th><th style='width:10.5em;'>Batch Qty</th><th style='width:10.5em;'>Issue Qty</th>";
        tabstr = tabstr + "<th style='width:10.5em;'>Balance Qty</th><th style='width:6em;'>Action</th>";
        tabstr = tabstr + "</tr>";
        $("#BatchInfoTable > thead").append(tabstr);

        var no = $("#Rowid").val();
        var ItemId = $("#InItem").val();
        var Itemname = $("#MIrawitemnameid").val();
        var itemAttr = $("#rawIAid").val();
        var isubcat = $("#hiddenitemcategoryid").val();
        var itemQty = $("#ItemTotQty").val();
        

        var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'inwardid': inwardid };
        var MyJson = JSON.stringify(Josndata);
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/SaveAutoPR/',
            data: "{'Model':'" + MyJson + "','type':10}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);
                $("#BatchInfoTable").attr('style', 'width: 100%;display:block;');
                var $dataRows = $("#BatchInfoTable tr:not('.titlerow')");
                var len = $dataRows.length;
                $.each(result.Table, function (key, BT) {
                    len = len + 1;
                    tabstr = "";
                    tabstr = tabstr + "<tr>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow itemd' value='" + Itemname + "' readonly style='width:12.5em;' /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmid" + len + "' value='" + ItemId + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmAttrid" + len + "' value='" + itemAttr + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmsubcatid" + len + "' value='" + isubcat + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmUOMid" + len + "' value='" + BT.UOM + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmUOMval" + len + "' value='" + BT.Unitdesc + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow totQty itemd' value='" + itemQty + "' id='ItemQty"+len+"' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow itemd' value='" + BT.Dynamic_Controls_value + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow Batchnor itemd' value='" + BT.ItemInward_ID + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow ItemQty itemd'  id='BTItemQty" + len + "' value='" + BT.ItemQty + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow IssueQty itemd' id='IssueQty" + len + "' name='IssueQty" + len + "' value='0' onkeypress='return isNumber(event)' onchange=GetTotalCal('" + len + "')  /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow itemd' value='0' id='BalQty" + len + "' Name='BalQty" + len + "' readonly /></td>";
                    tabstr = tabstr + "<td style='width:5em;'><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
                    tabstr = tabstr + "</tr>";
                    $("#BatchInfoTable > tbody").append(tabstr);
                    //SetTotalQty();
                });

                $("#InItem").val('');
                $("#rawitemnameid").val('');
                $("#rawIAid").val('');
                $("#hiddenitemcategoryid").val('');
                $("#ItemTotQty").val('');
                $("#batchTotQty").val('');
                GetBatchList();

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function SetTotalQty() {
    var tabstr = "";
    $("#TotalCount").html("");
    tabstr = tabstr + "<tr  class=''>";
    tabstr = tabstr + "<th style='width:16.5em;'></th><th style='width:10.5em;'></th><th style='width:10.5em;'></th><th style='width:10.5em;'>Total Issue</th><th style='width:10.5em;'><input type='text' class='form-control totqtycl' value='0' id='Totalissue' Name='Totalissue' readonly /></th>";
    tabstr = tabstr + "<th style='width:10.5em;'></th><th style='width:8em;'></th>";
    tabstr = tabstr + "</tr>";
    $("#TotalCount").append(tabstr);
}

function GetTotalCal(id) {
    debugger;
    var no = id;
    var itemQty = $("#ItemQty" + no).val();
    var IssueQty = $("#IssueQty" + no).val();

    if (parseFloat(IssueQty) > parseFloat(itemQty)) {
        msgshow("You Have Required Only " + itemQty + " Quantity!");
        $("#IssueQty" + no).val("");
    }
    else {
        var bal = 0;
        var batchQty = $("#BTItemQty" + no).val();

        if (parseFloat(IssueQty) > parseFloat(batchQty)) {
            msgshow("You Have Required Only " + itemQty + " Quantity!");
            $("#IssueQty" + no).val("");
        }
        else {
            bal = parseFloat(batchQty) - parseFloat(IssueQty);
            $("#BalQty" + no).val(bal);
        }
    }
}


function msgshow(alrtmsg) {
    swal({
        title: 'Error',
        text: alrtmsg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}

$(document).on('click', 'i.pluxtrash', function () {

    $(this).closest('tr').remove();

    return false;
});

function removetr() {

    $(this).closest('tr').remove();
   
    return false;

}

function removeItem(no) {
    debugger;
    $("#tabinitem" + no).html('');
    $("#tabInItemUnit" + no).html('');
    $("#tabAvailableQty" + no).html('');
    $("#tabtargetQty" + no).html('');
}

$('#SaveBatchInfo').on('click', function () {

    var MyJsonarr = [];
    var MyJsondata = {};
    var no = $("#Rowid").val();
    var $dataRows = $("#BatchInfoTable tr:not('.titlerow')");
    var Idata = "";
    $dataRows.each(function () {
        $(this).find('.itemd').each(function (i) {
            var itname = ($(this).val());
            Idata += itname + "^";
        });
        Idata = Idata.substring(0, Idata.length - 1);
        Idata += "#";
    });
    var olddata = "";
    var initem = "";
    var unitdata = "";
    var issuedata = "";
    var ReqqtyData = "";
    var Qty = 0;
    Idata = Idata.substring(0, Idata.length - 1);
    $.each(Idata.split('#'), function (i, dt) {
        debugger;
        var mydata = dt.split('^');
        if (MyJsonarr.length > 0) {
            var count = 0;
            $.each(MyJsonarr, function (i, data) {
                debugger;
                if (parseFloat(mydata[1]) == parseFloat(data.ItemID) && parseFloat(mydata[2]) == parseFloat(data.ItemAttrib)) {
                    Qty = parseFloat(mydata[10]) + parseFloat(data.Qty);
                    data.Qty = Qty;
                    data.BatchInfo = data.BatchInfo + "#" + dt;
                    count++;
                }
            });
            if (count == 0) {
                count = 0;
                MyJsondata = { 'ItemName': mydata[0], 'ItemID': mydata[1], 'ItemAttrib': mydata[2], 'ItemSubcat': mydata[3], 'UnitID': mydata[4], 'UnitName': mydata[5], 'ReqQty': mydata[6], 'BatchID': mydata[7], 'BatchNo': mydata[8], 'BatchQty': mydata[9], 'Qty': mydata[10], 'BalQty': mydata[11], 'BatchInfo': dt };
                MyJsonarr.push(MyJsondata);
            }
        }
        else {
            MyJsondata = { 'ItemName': mydata[0], 'ItemID': mydata[1], 'ItemAttrib': mydata[2], 'ItemSubcat': mydata[3], 'UnitID': mydata[4], 'UnitName': mydata[5], 'ReqQty': mydata[6], 'BatchID': mydata[7], 'BatchNo': mydata[8], 'BatchQty': mydata[9], 'Qty': mydata[10], 'BalQty': mydata[11], 'BatchInfo': dt };
            MyJsonarr.push(MyJsondata);
        }
    });

    var ct = 0;
    $.each(MyJsonarr, function (i, data) {
        debugger;
        if (parseFloat(data.ReqQty) == parseFloat(data.Qty)) {
            initem = "<div style='position:relative;width:205px;'><i class='fa fa-close close' onclick=removeItem('" + no + "')></i><input type='text'  datavalue='" + data.ItemID + "' datasubval='" + data.ItemSubcat + "' dataIA='" + data.ItemAttrib + "' dataBatch='" + data.BatchInfo + "'  value='" + data.ItemName + "' class='form-control initemdata' style='margin-bottom:0.5em;width:12em;' readonly/></div>";
            $("#tabinitem" + no).append(initem);

            unitdata = "<input type='text' datavalue='" + data.UnitID + "' value='" + data.UnitName + "' class='form-control InItemUnitdata' style='margin-bottom:0.5em;' readonly/>";
            $("#tabInItemUnit" + no).append(unitdata);
            
            issuedata = "<input type='text' datavalue='" + data.Qty + "' value='" + data.Qty + "' class='form-control AvailableQtydata' style='margin-bottom:0.5em;' readonly/>";
            $("#tabAvailableQty" + no).append(issuedata);

            ReqqtyData = "<input type='text'  class='form-control reqqtydata' style='margin-bottom:0.5em;' onkeypress='return isNumber(event)' onchange=createOUTItem('" + no + "');  />";
            $("#tabtargetQty" + no).append(ReqqtyData);
            
        }
        else {
            swal({
                title: 'Error',
                text: "Please Enter Proper Issue Quantity!",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
            ct++;

        }
    });

    if (ct== 0) {
        $('.modal').modal('hide');
        $("#BatchInfoTable").attr('style', 'width: 100%;display:none;');
    }
    

   
});

function pophide() {
    $('.modal').modal('hide');
    $("#BatchInfoTable").attr('style', 'width: 100%;display:none;');
}

function GetPIpp(id) {
    debugger;
    $("#Dynamic_ID option:selected").prop("selected", false);

    $("#ProductInfoTable > tbody").html('');
    $("#ProductInfoTable > thead").html('');
    $("#TotalCount").html('');
    $("#ProductInfoTable1").html('');

    var no = id.match(/\d+/);
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    $('#ProdutInfoModal').modal('toggle');

    var MyPIstr = $("#productinfo" + no).val();
    var name = $("#rawoutitemnameid" + no).val();
    $("#proditemname").val(name);
    $("#proditemname").attr('dataval', no);
    var acptqty = $("#OutItemQty" + no).val();
    $("#proAcptQty").val(acptqty);

    if (MyPIstr != "") {
        var protype = $("#producttype" + no).val();
        if (protype != "") {
            $.each(protype.split(","), function (i, e) {
                $("#Dynamic_ID option[value='" + e + "']").prop("selected", true);
            });
            $("#Dynamic_ID").select2().trigger('change');
            var pid = document.getElementById('Dynamic_ID');
            GetProductvalue(pid);
        }
        $(".attrbtab1").removeAttr('style');
        $(".attrbtab2").attr('style', 'display:none;');

        $.each(MyPIstr.split('#'), function (i, data) {

            var lenchk = data.split('~');
            lenchk = lenchk.length;
            var tabstr = "";
            tabstr = tabstr + "<tr class='attrtabval'>";
            $.each(data.split('~'), function (i, data) {

                if (i == (lenchk - 1)) {
                    tabstr = tabstr + "<td style='width:8em;' dataval='" + data + "'>" + data + "<input type='hidden' class='itemqtycl' value='" + data + "' /> </td>";

                    var path = window.location.pathname;
                    path = path.split('/');
                    if (path[2] == "Edit") {
                        tabstr = tabstr + "<td style='width:5em;'><i class='fa fa-trash pluxtrashdis' disabled></i></td>";
                    }
                    else {
                        tabstr = tabstr + "<td style='width:5em;'><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
                    }
                }
                else {
                    tabstr = tabstr + "<td dataval='" + data + "' style='width:14em;'><input type='hidden' value='" + data + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + data + "'  />" + data + "</td>";
                }

            });
            tabstr = tabstr + "</tr>";
            $("#ProductInfoTable > tbody").append(tabstr);


        });
        GetTotalQty();
        $("#ProductInfoTable").attr('style', 'width:100%;display:block;');
        var path = window.location.pathname;
        path = path.split('/');
        if (path[2] == "Edit") {
            $("#ProductInfoTable1").attr('style', 'display:none;');
            $(".bodyscol").attr('style', 'max-height:300px');
        }
    }
    else {
        $("#ProductInfoTable").attr('style', 'width:100%;display:none;');
    }
}


function GetProductvalue(aid) {
    debugger;
    var AttribId = $(aid).val();
    if (AttribId != "" && AttribId != null) {
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',');
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        var tabstr = "";
        $("#ProductInfoTable > thead").html("");
        tabstr = tabstr + "<tr  class='attrbtab1' style='display:none;'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr = tabstr + "<th style='width:14em;'>" + Data[i] + "</th>";
            }
        }
        tabstr = tabstr + "<th style='width:8em;'>Quantity</th><th style='width:6.5em;'>Action</th>";
        tabstr = tabstr + "</tr>";
        $("#ProductInfoTable > thead").append(tabstr);


        var tabstr1 = "";
        $("#ProductInfoTable1").html("");
        tabstr1 = tabstr1 + "<tr  class='attrbtab2'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr1 = tabstr1 + "<th style='width:14em;'>" + Data[i] + "</th>";
            }
        }
        tabstr1 = tabstr1 + "<th  style='width:8em;'>Quantity</th><th style='width:6.5em;'>Action</th>";
        tabstr1 = tabstr1 + "</tr>";
        tabstr1 = tabstr1 + "<tr  class='attrbtab_val2'>";
        var index2 = AttribId.indexOf(',');
        if (index2 == -1) {
            AttribId = AttribId + ",";
        }
        AttribId = AttribId.split(',');

        for (i = 0; i < AttribId.length; i++) {
            var option = "";
            if (AttribId[i] != "") {
                if (Data[i] == "Expiry") {
                    tabstr1 = tabstr1 + "<td style='width:14em;'><div class='input-group date hasDatepicker' id='bs_datepicker_component_container4' style='border:1px solid #ccc;border-radius:5px;'><input type='text' class='form-control  pull-right MyExpire hidval'  placeholder='Ex:30/07/2016' value='' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' onclick='changedateStyle()' /><span class='input-group-addon'><i class='material-icons'>date_range</i></span></div></td>";
                }
                else if (Data[i] == "Lot No" || Data[i] == "Batch") {
                    var PIname = $("#proditemname").val();
                    var iname = "";
                    $.each(PIname.split(' '), function (i, data) {
                        iname = iname + data[0];
                    });
                    var radno = Math.random().toFixed(3).split(".").pop();
                    radno = parseInt(radno);
                   
                    radno = (radno.toString().length == 2) ? radno + "1" : radno;
                    var no = Data[i].split(" ").join("")[0] + "-ML-PR-" + iname + radno + "-" + Getreturn();

                    tabstr1 = tabstr1 + "<td style='width:14em;'><input type='text' class='form-control hidval' readonly value='" + no + "' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' /></td>";
                }
                else {

                    tabstr1 = tabstr1 + "<td style='width:14em;'><input type='text' class='form-control hidval' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' /></td>";
                }


            }
        }
        tabstr1 = tabstr1 + "<td  style='width:8em;'><input type='text' id='itemQty1'  class='form-control' onkeypress='return isNumber(event)' /></td><td style='width:5em;'><i class='fa fa-plus pluxad' onclick='SetProductValue()'></i></td>";
        tabstr1 = tabstr1 + "</tr>";
        $("#ProductInfoTable1").append(tabstr1);

        var cnt = 0;
        $(".MyExpire").datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: 0
        });
    }
    else {
        debugger;
        $("#ProductInfoTable1").html("");
        $("#ProductInfoTable").html("");
        $("#Dynamic_ID  option:selected").val("");
    }
}

function changedateStyle() {

    $("#ui-datepicker-div").css("z-index", "10000");
}

function Getreturn() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = + dd + mm + yyyy;
    return today;
}

function SetProductValue() {
    debugger;
    var comm = {};
    var savedata = {};
    itemQty = $("#itemQty1").val();
    var isvalid = true;
    var ErrorMessage = "";
    //validations of item master

    $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
        var chkatrval = this.value;
        if (chkatrval == "" || chkatrval == "0") {
            ErrorMessage = "Please Enter Details!";
            isvalid = false;
        }
    });


    if (itemQty == "") {
        ErrorMessage = "Please Enter Item Quantity";
        isvalid = false;
    }
    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {

        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',');
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        debugger
        var count = 0;
        var temprray = "";
        var atrval = "";
        var item1 = {};
        $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            atrval = atrval + "," + this.value;
        });

        $("#ProductInfoTable").find(".attrtabval").each(function () {
            temprray = "";
            $(this).children('td').find(".hidval2").each(function () {
                temprray = temprray + "," + this.value;
            });
            if (atrval == temprray) {
                count++;
            }
        });



        if (count == 0) {
            debugger;
            $("#ProductInfoTable").attr('style', 'width: 100%;display:block;');
            var atrid;
            $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
                atrid = atrid + "," + this.value;
            });
            atrid = atrid.split(',');
            atrid.splice(0, 1);
            atrval = atrval.split(',');
            atrval.splice(0, 1);


            $(".attrbtab1").removeAttr('style');
            $(".attrbtab2").attr('style', 'display:none;');
            var tabstr = "";
            var len1 = $("#ProductInfoTable tr").length;
            tabstr = tabstr + "<tr class='attrtabval'>";
            for (i = 0; i < Data.length; i++) {
                if (Data[i] != "" && atrval[i] != "" && (atrid[i] != "" || atrid[i] !== "undefined")) {
                    tabstr = tabstr + "<td dataval='" + atrid[i] + "' style='width:14em;'><input type='hidden' value='" + atrid[i] + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + atrval[i] + "'  />" + atrval[i] + "</td>";
                }
            }
            tabstr = tabstr + "<td style='width:8em;' dataval='" + itemQty + "'>" + itemQty + "<input type='hidden' class='itemqtycl' value='" + itemQty + "' /> </td><td style='width:5em;'><i class='fa fa-trash pluxtrash1' onclick='removetr1()'></i></td>";
            tabstr = tabstr + "</tr>";
            $("#ProductInfoTable > tbody").append(tabstr);

            GetTotalQty();
        }
        else {
            swal({
                title: 'Error',
                text: "This Combination All Ready Add In Rows",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
        }

        $("#itemQty1").val("");
        $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            var idname = this.getAttribute("id");

            var PIname = $("#proditemname").val();
            var iname = "";
            $.each(PIname.split(' '), function (i, data) {
                iname = iname + data[0];
            });
            var myval = "";
            var myvalueno = "";
            if (idname == "LotNo1" || idname == "Batch1") {
               
                myval = this.value.split('-');
                myvalueno = myval[3].substr(myval[3].length - 3);
                myvalueno = parseInt(myvalueno) + parseInt(1);
                this.value = myval[0] + "-" + myval[1] + "-" + myval[2] + "-" + iname + myvalueno + "-" + myval[4];
            }
            else {
                this.value = "";
            }
           

        });

    }
}

function GetTotalQty() {
    debugger;
    var tabstr = "";
    var ItemQty = 0;
    var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
    var index = Data.indexOf(',');
    if (index == -1) {
        Data = Data + ",";
    }
    Data = Data.split(',');
    $("#ProductInfoTable tr.attrtabval").children('td').find(".itemqtycl").each(function () {
        ItemQty = parseFloat(ItemQty) + parseFloat(this.value);
    });



    $("#TotalCount").html('');
    var len1 = $("#ProductInfoTable1 tr > td").length;
    tabstr = tabstr + "<tr class='attrtabval'>";
    for (i = 1; i <= Data.length; i++) {
        if (i == Data.length) {
            tabstr = tabstr + "<td style='width:14em;'><b>Total Quantity</b></td>";
        }
        else {
            tabstr = tabstr + "<td style='width:14em;'></td>";
        }

    }
    tabstr = tabstr + "<td style='width:8em;'  dataval='" + ItemQty + "'>" + ItemQty + "<input type='hidden' class='totqtycl' value='" + ItemQty + "' /></td><td style='width:5em;'></td>";
    tabstr = tabstr + "</tr>";
    $("#TotalCount").append(tabstr);
}

$(document).on('click', 'i.pluxtrash1', function () {

    $(this).closest('tr').remove();
    GetTotalQty();
    return false;
});

function removetr1() {

    $(this).closest('tr').remove();
    GetTotalQty();
    return false;

}

$('#SaveProdInfo').on('click', function () {

    var TotalQty = "";
    var AcptQty = $("#proAcptQty").val();
    $("#TotalCount tr").children('td').find('.totqtycl').each(function (i) {

        TotalQty = this.value;  //here get td value
    });

    if (parseFloat(TotalQty) == parseFloat(AcptQty)) {
        var Astr = "";
        var $AdataRows = $("#ProductInfoTable tr:not('.attrbtab1')");
        var Arowlenght = $AdataRows.length;
        var Astylerowcount = 0;
        //var stylerowCount = $('#maintable2 tr').length;
        $AdataRows.each(function () {
            debugger;
            Acount = 0;
            Astylerowcount++
            var Aleng = $(this).children('td').length;
            $(this).children('td').each(function (i) {
                debugger;
                var Aitemlist = ($(this).attr('dataval'));  //here get td value
                Acount++;

                if (Aitemlist != undefined) {

                    if (Acount == (Aleng - 1)) {
                        Astr += Aitemlist;
                    }
                    else {
                        Astr += Aitemlist + "~";
                    }
                }

            });

            if (Astylerowcount == Arowlenght) {
            }
            else {
                Astr += "#";
            }

        });
        var no = $("#proditemname").attr('dataval');
        $("#productinfo" + no).val(Astr);
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.value).join();
        $("#producttype" + no).val(Data);
        $('.modal').modal('hide');
    }
    else {
        swal({
            title: 'Error',
            text: "Accept Quantity and Product Total Qunatity Does Not Match!",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
});


function GetTIpp(id) {
    $('#TransferInfoModal').modal('toggle');
    var no = id.match(/\d+/);
    $("#rowid").val(no);
    var moddata = $("#transferinfo" + no).val();
    if (moddata != "") {
        moddata = moddata.split('~');
        $("#tmode").val(moddata[1]);
        $("#transpoterNameId").val(moddata[2]);
        $("#PersonNameId").val(moddata[3]);
        $("#PersonMobNoId").val(moddata[4]);
        $("#TFromWarehouseID").val(moddata[5]);
        $("#TFromWarehouse").val(moddata[6]);
        $("#TTOWarehouse").val(moddata[7]);
        $("#TTOWarehouse").val(moddata[8]);
        $("#transferdate").val(moddata[9]);
        $("#Deliverydate").val(moddata[10]);
        $("#senderbyID").val(moddata[11]);
        $("#senderby").val(moddata[12]); 
    }
}

$("#SaveTransferInfo").on('click', function () {
    var mode = $("#tmode").val();
    var transpotername = $("#transpoterNameId").val();
    var PersonName = $("#PersonNameId").val();
    var permobno = $("#PersonMobNoId").val();
    var formWarH = $("#TFromWarehouseID").val();
    var formWarHN = $("#TFromWarehouse").val();
    var toWarH = $("#TTOWarehouseID").val();
    var toWarHN = $("#TTOWarehouse").val();
    var transferDt = $("#transferdate").val();
    var deliveryDt = $("#Deliverydate").val();
    var SenderBy = $("#senderbyID").val(); 
    var SenderByN = $("#senderby").val(); 

    if (transpotername == "") {
        msgshow("Enter Transpoter Name !");
    }
    else if (PersonName == "") {
        msgshow("Enter Person Name !");
    }
    else if (formWarH == "0" || formWarH == "" || formWarHN == "") {
        msgshow("Please Select Form Warehouse !");
    }
    else if (toWarH == "0" || toWarH == "" || toWarHN == "") {
        msgshow("Please Select To Warehouse !");
    }
    else if (SenderBy == "0" || SenderBy == "" || SenderByN == "") {
        msgshow("Please Select Sender By !");
    }
    else {

        var str = 'T-2019-11-19-XXXX' + '~' + mode + '~' + transpotername + '~' + PersonName + '~' + permobno + '~' + formWarH + '~' + formWarHN + '~' + toWarH + '~' + toWarHN + '~' + transferDt + '~' + deliveryDt + '~' + SenderBy + '~' + SenderByN;
        var no = $("#rowid").val();
        $("#transferinfo" + no).val(str);
        $("#btnSaveItem" + no).removeAttr('disabled');
        $('.modal').modal('hide');
    }
});