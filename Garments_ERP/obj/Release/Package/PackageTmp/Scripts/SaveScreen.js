﻿
GetScreen();

function SetParent() {
    var STval = $("#screenType").val();
    if (parseInt(STval) == 2) {
        $("#parentSHid").show();
    }
    else {
        $("#parentSHid").hide();
    }
}

function GetScreen() {
    var startrow = "0";
    var startrowvalue = "---Select---";
    $.ajax({
        type: "POST",
        url: "/Screen/GetScreen/",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (subcates) {
            $('#parentid').empty();
            $("#parentid").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
            $.each(subcates, function (i, subcate) {
                $("#parentid").append('<option  value="' + subcate.screenId + '">' + subcate.ScreenName + '</option>');
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

$('#SaveScreen').on("click", function () {
    var isvalid = true;
    var screenName = $('#screenName').val();
    var screenurl = $('#screenurl').val();
    var screenType = $('#screenType').val();
    if (screenName == "") {
        ErrorMsg("Please Enter Screen Name!");
        isvalid = false;
    }
    else if (screenType == 0) {
        ErrorMsg("Please Select Screen Type!");
        isvalid = false;
    } else if (screenType > 1) {
        var parentid = $('#parentid').val();
        if (parentid == 0) {
            ErrorMsg("Please Select Parent Screen!");
            isvalid = false;
        }
    }

    return isvalid;
});

function ErrorMsg(msg) {
    swal({
        title: "Error",
        text: msg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}