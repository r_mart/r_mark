﻿using System.Web;
using System.Web.Optimization;

namespace Garments_ERP
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //  bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/site.css"));

            // bundles.Add(new StyleBundle("~/Content/Maincss").Include(
            //    "~/Content/app-assets/css/bootstrap.css",
            //    "~/Content/app-assets/fonts/icomoon.css",
            //    "~/Content/app-assets/fonts/flag-icon-css/css/flag-icon.min.css",
            //    "~/Content/app-assets/vendors/css/extensions/pace.css",
            //    "~/Content/app-assets/css/bootstrap-extended.css",
            //    "~/Content/app-assets/css/app.css",
            //    "~/Content/app-assets/css/colors.css",
            //    "~/Content/app-assets/css/core/menu/menu-types/vertical-menu.css",
            //    "~/Content/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css",
            //    "~/Content/app-assets/css/plugins/animate/animate.css",
            //    "~/Content/app-assets/css/core/colors/palette-gradient.css",
            //    "~/Content/assets/css/style.css"


            //));

            bundles.Add(new StyleBundle("~/Content/Maincss").Include(
                "~/Content/bower_components/bootstrap/dist/css/bootstrap.min.css",
                "~/Content/bower_components/font-awesome/css/font-awesome.min.css",
                "~/Content/bower_components/Ionicons/css/ionicons.min.css",
                "~/Content/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css",
                "~/Content/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                "~/Content/bower_components/bootstrap-daterangepicker/daterangepicker.css",
                "~/Content/bower_components/select2/dist/css/select2.min.css",
                "~/Content/dist/css/skins/_all-skins.min.css",
                "~/Content/bower_components/jvectormap/jquery-jvectormap.css",
                "~/Content/plugins/iCheck/all.css",
                "~/Content/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css",
                "~/Content/plugins/timepicker/bootstrap-timepicker.min.css",
                "~/Content/dist/css/AdminLTE.min.css",
                "~/Content/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));


            bundles.Add(new ScriptBundle("~/bundles/MainJs").Include(
                "~/Content/bower_components/jquery/dist/jquery.min.js",
                "~/Content/bower_components/bootstrap/dist/js/bootstrap.min.js",
                "~/Content/bower_components/select2/dist/js/select2.full.min.js",
                "~/Content/bower_components/jquery-ui/jquery-ui.min.js",
                "~/Content/bower_components/datatables.net/js/jquery.dataTables.min.js",
                "~/Content/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js",
                "~/Content/plugins/input-mask/jquery.inputmask.js",
                "~/Content/plugins/input-mask/jquery.inputmask.date.extensions.js",
                "~/Content/plugins/input-mask/jquery.inputmask.extensions.js",
                "~/Content/bower_components/moment/min/moment.min.js",
                "~/Content/bower_components/bootstrap-daterangepicker/daterangepicker.js",
                "~/Content/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                "~/Content/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js",
                "~/Content/bower_components/raphael/raphael.min.js",
                "~/Content/bower_components/morris.js/morris.min.js",
                "~/Content/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                "~/Content/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                "~/Content/bower_components/jquery-knob/dist/jquery.knob.min.js",
                "~/Content/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                "~/Content/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
                "~/Content/bower_components/fastclick/lib/fastclick.js",
                "~/Content/dist/js/adminlte.min.js",
                "~/Content/dist/js/pages/dashboard.js",
                "~/Content/dist/js/demo.js"

                ));


            bundles.Add(new ScriptBundle("~/bundles/exportJs").Include(
                 "~/Content/exportpugin/tableExport.js",
                 "~/Content/exportpugin/jquery.base64.js",
                 "~/Content/exportpugin/html2canvas.js",
                 "~/Content/exportpugin/jspdf/libs/base64.js",
                 "~/Content/exportpugin/jspdf/libs/sprintf.js",
                 "~/Content/exportpugin/jspdf/jspdf.js"
               ));
        }
    }
}