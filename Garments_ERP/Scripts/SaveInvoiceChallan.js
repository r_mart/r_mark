﻿function getCustomerdetails() {
    var startrow = 0;
    var startrowvalue = "---Select----";
    var custid = $("#customerid").val();
    $("#porefno").empty();
    $.ajax({
        type: "POST",
        url: '/InvoiceChallan/getPOListbycustomerid/',
        data: "{'id':" + custid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            $("#porefno").append('<option value="0">' + startrowvalue + '</option>');
            $.each(result, function (i, bussiness) {
                $("#porefno").append('<option value="' + bussiness.Id + '">' + bussiness.POno + '</option>');
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });

    getAddress();
    //getinfo();

}

function getAddress() {
    var custid = $("#customerid").val();
    $.ajax({
        type: "POST",
        url: '/CustomerEnquiry/getaddress/',
        data: "{'custid':" + custid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            result = JSON.parse(result);
            $.each(result, function (i, item) {
                if (i == 0) {
                    $("#CustAddId").val(item.Address);
                }
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function GetPODetails() {
    var POId = $("#porefno").val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: '/InvoiceChallan/EditAttributeValue/',
        data: "{'MyJson':'','type':35,'id':" + POId + "}",
        contentType: "application/json;",
        success: function (Result) {
            debugger;
            Result = JSON.parse(Result);
            $("#itemtable > tbody").html('');
            var Str = "";
            $.each(Result.Table, function (e, data) {
                Str = "";
                if (data.save > 0) {
                    Str = Str + "<tr><td>" + data.ItemName + "<input type='hidden' id='Itemid" + e + "' value='" + data.ItemName + "' dataID='" + data.ItemID + "' dataAttr='" + data.ItemAttributeId + "' class='form-control itemraw' readonly='readonly' /></td>";
                    Str = Str + "<td style='display:none;'><input type='hidden'  value='" + data.ItemID + "' class='itemraw' /><input type='hidden'  value='" + data.ItemAttributeId + "' class='itemraw' /></td>";
                    Str = Str + "<td>" + data.HScode+"<input type='hidden' value='" + data.HScode + "' class='form-control itemraw' readonly='readonly' /></td>";
                    Str = Str + "<td><input type='text' value='" + data.POQty + "' id='poqtyid" + e + "' class='form-control itemraw' readonly='readonly' /></td>";
                    Str = Str + "<td><input type='text' value='' id='invqtyid" + e + "' onkeypress='return isNumber(event);' onchange=GetTotalCal('" + e + "') class='form-control itemraw' /></td>";
                    Str = Str + "<td><input type='text' value='' id='BalQtyid" + e + "' readonly='readonly'  class='form-control itemraw' /></td>";
                    Str = Str + "<td><input type='text' value='" + data.rate + "' id='rateid" + e + "' readonly='readonly'  class='form-control itemraw' /></td>";
                    Str = Str + "<td><input type='text' value='" + data.UOM + "' dataID='" + data.UnitId + "'  class='form-control' readonly  /><input type='hidden' value='" + data.UnitId + "' class='itemraw' readonly  /></td>";
                    Str = Str + "<td><input type='text' value='' id='totalamtid" + e + "'  class='form-control itemraw' readonly='readonly' /></td>";
                    Str = Str + "<td><input type='text' value='" + data.GST + "'  class='form-control itemraw'  readonly='readonly' /></td>";
                    Str = Str + "<td><input type='text' value='" + data.TotInvQty + "' id='totinvqtyid" + e + "'  class='form-control itemraw'  readonly='readonly' /></td>";
                    Str = Str + "<td><input type='hidden' value='' id='productInfo" + e + "' Name='productInfo" + e + "' class='itemPIdata' /><input type='hidden' value='' id='inwardid" + e + "' Name='inwardid" + e + "' class='itemraw' /><button type='button' id='btnSetProductInfo" + e + "' disabled class='btn btn-danger btn-xs' onclick=GetBLpp(this.id)>Batch/Lot Info</button></td><tr>";

                    $("#itemtable > tbody").append(Str);
                }
                else {
                    $("#itemtable > tbody").html('Data Not Fount!');
                }
            });

            GetOtherCharge();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function GetOtherCharge() {
    var poid = $('#porefno').val();
    if (poid > 0) {
        $.ajax({
            type: "POST",
            url: '/CustomerPO/getPOdetailsbyid/',
            data: "{'id':" + poid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result != null) {
                    if (result.POOtherChargeEntity != null) {
                        if (result.POOtherChargeEntity.length > 0) {
                            var j = 0;
                            $("#OtherChargeDetails").show();
                            $("#OC_GrandTotal_id").show();
                            $('#OtherChargeDetails > tbody').empty();
                            $.each(result.POOtherChargeEntity, function (k, ocdata) {
                                var otherchargeid = "otherchargeid" + j;
                                var OC_Amount = "OC_Amount" + j;
                                var OC_GSTid = "OC_GSTid" + j;
                                var hidgst = "hidgst" + j;
                                var OC_GSTAmt = "OC_GSTAmt" + j;
                                var OC_total = "OC_total" + j;
                                var ocTid = "ocTid" + j;
                                var GST = ocdata.GST == "" || ocdata.GST == null ? 0 : parseFloat(ocdata.GST);
                                var OC_amount = parseFloat(ocdata.OtherChargeValue);
                                var gstamount = parseFloat(OC_amount) * parseFloat(GST) / 100;
                                var tot = parseFloat(gstamount) + parseFloat(OC_amount);

                                var row = '<tr><td>' + ocdata.OCMaster.POOtherChargeName + '<input type="hidden" class="ocdata" id="' + otherchargeid + '" name="' + otherchargeid + '" value="' + ocdata.OtherChargeId + '" /></td>' +
                                    '<td>' + OC_amount + '<input type="hidden" class="form-control ocdata" value="' + ocdata.OtherChargeValue + '" id="' + OC_Amount + '" name="' + OC_Amount + '" onchange="OC_Cal(this.id)" onkeypress="return isNumber(event)" style="display:none;" readonly /></td>' +
                                    '<td>' + GST + '<input type="hidden" class="ocdata" value="' + GST + '" id="' + hidgst + '" name="' + hidgst + '" /></td>' +
                                    '<td>' + gstamount + '<input type="hidden" value="' + gstamount + '" class="form-control ocdata" id="' + OC_GSTAmt + '" name="' + OC_GSTAmt + '" onkeypress="return isNumber(event)" readonly /></td>' +
                                    '<td>' + tot + '<input type="hidden" value="' + tot + '" class="form-control ocdata" id="' + OC_total + '" name="' + OC_total + '" onkeypress="return isNumber(event)" readonly /></td>' +
                                    '<td><a id="btnOCDelete" class="deleteOC btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></a><input type="hidden" class="ocdata" id="' + ocTid + '" name="' + ocTid + '" value="' + ocdata.Id + '" /></td></tr>';
                                $('#OtherChargeDetails > tbody').append(row);
                                j++;
                            });
                        }
                        else {
                            $("#OtherChargeDetails").hide();
                        }
                    }
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
 }

function GetTotalCal(no) {
    var POQty = $("#poqtyid" + no).val();
    var TotInvQty = $("#totinvqtyid" + no).val();
    var qty = $("#invqtyid" + no).val();
    var rate = $("#rateid" + no).val();
    qty = parseFloat(qty) + parseFloat(TotInvQty);
    var balQty = parseFloat(POQty) - parseFloat(TotInvQty);
    if (parseFloat(qty) > parseFloat(POQty)) {
        swal({
            title: 'Error',
            text: 'You Have Remainig ' + balQty + ' Quantity',
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        $("#invqtyid" + no).val("");
        $("#btnSetProductInfo" + no).attr('disabled', 'disabled');
    }
    else {
        balQty = parseFloat(POQty) - parseFloat(qty);
        $("#BalQtyid" + no).val(balQty);
        qty = $("#invqtyid" + no).val();
        total = parseFloat(qty) * parseFloat(rate);
        $("#totalamtid" + no).val(total);
        $("#btnSetProductInfo" + no).removeAttr('disabled');
        GSTCal();
    }

}

function GSTCal() {
    debugger;
    var gstcharge = $("#GSTCharge").val() == "0" || $("#GSTCharge").val() == "select" ? 0 : $("#GSTCharge").val();
    var totalAmt = $("#totalamtid0").val() == "" ? 0 : $("#totalamtid0").val();
    var GSTamount = (parseFloat(totalAmt) * parseFloat(gstcharge)) / 100;
    $("#GSTamount").val(GSTamount);
    var Gtot = parseFloat(GSTamount) + parseFloat(totalAmt);
    $("#total").val(Gtot);
    GetGrandTot();
}

function GetGrandTot() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var len = $dataRows.length;
    var OC_tot1 = 0;
    var potot = $("#total").val();
    if (len > 0) {
        $dataRows.each(function (i) {
            $(this).find('.ocdata').each(function (i) {
                if (i == 4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });
        
        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#Grandtotal").val(Grandtot);
    }
    else {
        $("#Grandtotal").val(potot);
    }
}

function GetBLpp(id) {
    var no = id.match(/\d+/);
    $("#rawno").val(no);
    var ItemId = $("#Itemid" + no).attr('dataID');
    var ItemAttrId = $("#Itemid" + no).attr('dataAttr');
    var poid = $("#porefno").val();
    var param = { 'ItemId': ItemId, 'ItemAttrId': ItemAttrId, 'poid': poid };
    $.ajax({
        type: "POST",
        url: '/InvoiceChallan/GetBatachLot/',
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            $("#BatchLotModal").modal('toggle');
            $("#BatchLotid").empty();
            if (result != null) {
                $("#BatchLotid").append('<option value="0">Select</option>');
                $.each(result, function (i, data) {
                    $("#BatchLotid").append('<option value="' + data.ItemInward_ID + '">' + data.Dynamic_Controls_value + '</option>');
                });
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
    $("#batchlottab > tbody").empty();
    var batchdata = $("#productInfo" + no).val();
    if (batchdata != "" && batchdata != null) {
        $.each(batchdata.split('#'), function (i, bt) {
            if (bt != "" && bt != null) {
                var batch = bt.split('~');
                var str = '<tr><td>' + batch[0] + '<input type="hidden" value="' + batch[0] + '" datavalue="' + batch[1] + '" class="rackdata Batchnor"><input type="hidden" value="' + batch[1] + '" class="rackdata"></td>' +
                    '<td>' + batch[2] + '<input type="hidden" value="' + batch[2] + '" class="rackdata"></td>' +
                    '<td>' + batch[3] + '<input type="hidden" value="' + batch[3] + '" class="rackdata batissue"></td>' +
                    '<td>' + batch[4] + '<input type="hidden" value="' + batch[4] + '" class="rackdata"></td>' +
                    '<td><i class="fa fa-trash pluxtrash" onclick="removetr()"></i></td><tr>';
                $("#batchlottab > tbody").append(str);
            }
        });
    }
}

function GetInwardQty() {
    debugger;
    var no = $("#rawno").val();
    var ItemId = $("#Itemid" + no).attr('dataID');
    var ItemAttrId = $("#Itemid" + no).attr('dataAttr');
    var inwardid = $('#BatchLotid option:selected').val();

    var Josndata = { 'ItemId': ItemId, 'ItemAttr': ItemAttrId, 'inwardid': inwardid };
    var MyJson = JSON.stringify(Josndata);
    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':9}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            result = JSON.parse(result);
            $.each(result.Table, function (key, BT) {
                $("#batclotQty").val(BT.TotalQty);
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function setBatLotCal() {
    var batlotQty = $("#batclotQty").val();
    var IssueQty = $("#IssueQty").val();
    if (parseFloat(IssueQty) > parseFloat(batlotQty)) {
        swal({
            title: 'Error',
            text: 'You Have Remainig ' + batlotQty + ' Quantity',
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else {
        var balQty = parseFloat(batlotQty) - parseFloat(IssueQty);
        $("#balncedQty").val(parseFloat(balQty).toFixed(2));
    }
}

function SetBatchData() {
    var inwardid = $('#BatchLotid option:selected').val();
    var inwardvalue = $('#BatchLotid option:selected').text();
    var batchlotQty = $("#batclotQty").val();
    var IssueQty = $("#IssueQty").val();
    var balQty = $("#balncedQty").val();
    var $dataRows = $("#batchlottab > tbody > tr:not('.titlerow')");
    var len = $dataRows.length;
    var ErrorMessage = "";
    if (len > 0) {
        $dataRows.each(function () {
            $(this).find('.Batchnor').each(function (i) {
                var batchno = ($(this).attr("datavalue"));
                if (batchno == inwardvalue) {
                    ErrorMessage = inwardvalue + " This Batch No. All Ready in Row!";
                    isvalid = false;
                }
            });
        });
    }

    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    } else {
        var str = '<tr><td>' + inwardvalue + '<input type="hidden" value="' + inwardid + '" datavalue="' + inwardvalue + '" class="rackdata Batchnor"><input type="hidden" value="' + inwardvalue + '" class="rackdata"></td>' +
            '<td>' + batchlotQty + '<input type="hidden" value="' + batchlotQty + '" class="rackdata"></td>' +
            '<td>' + IssueQty + '<input type="hidden" value="' + IssueQty + '" class="rackdata batissue"></td>' +
            '<td>' + balQty + '<input type="hidden" value="' + balQty + '" class="rackdata"></td>' +
            '<td><i class="fa fa-trash pluxtrash" onclick="removetr()"></i></td><tr>';
        $("#batchlottab > tbody").append(str);
    }
    $('#BatchLotid').val(0);
    $("#batclotQty").val("");
    $("#IssueQty").val("");
    $("#balncedQty").val("");
    $("#BatchLotid").trigger('change');
    var totisue = $("#totBatchQtyIssue").val() == "" ? 0 : $("#totBatchQtyIssue").val();
    totisue = parseFloat(totisue) + parseFloat(IssueQty);
    $("#totBatchQtyIssue").val(totisue);
}

$(document).on('click', 'i.pluxtrash', function () {
    $(this).closest('tr').remove();
    totIssueCal();
    return false;
});

function removetr() {
    $(this).closest('tr').remove();
    totIssueCal();
    return false;
}
function pophide() {
    $('.modal').modal('hide');
}

function totIssueCal() {
    var $dataRows = $("#batchlottab > tbody > tr:not('.titlerow')");
    var len = $dataRows.length;
    var ErrorMessage = "";
    var tot = 0;
    if (len > 0) {
        $dataRows.each(function () {
            $(this).find('.batissue').each(function (i) {
                var batchno = ($(this).val());
                tot = parseFloat(tot) + parseFloat(batchno);
            });
        });
    }
    $("#totBatchQtyIssue").val(tot);
}

$("#SaveProdInfo").on('click', function () {
    debugger;
    var str = "";
    var $dataRows = $("#batchlottab > tbody > tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var rackrowcount = 0;
    var batchidss = "";
    //var stylerowCount = $('#maintable2 tr').length;
    $dataRows.each(function () {
        count = 0;
        rackrowcount++;
        var Aleng = $(this).children('td').length;
        $(this).find('.rackdata').each(function (i) {
            var itemlist = ($(this).val());  //here get td value
            count++;
            if (count == 1) {
                batchidss = itemlist;
            }

            if (itemlist != undefined) {

                if (count == Aleng) {
                    str += itemlist;
                }
                else {
                    str += itemlist + "~";
                }
            }

        });

        if (rackrowcount == rowlenght) {

        }
        else {
            str += "#";
            batchidss += ",";
        }

    });

    var totisue = $("#totBatchQtyIssue").val() == "" ? 0 : $("#totBatchQtyIssue").val();
    var no = $("#rawno").val();
    var InvQty = $("#invqtyid" + no).val();
    if (parseFloat(totisue) > parseFloat(InvQty)) {
        swal({
            title: 'Error',
            text: 'Please Issue Only ' + InvQty + ' Quantity',
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else {
        $("#productInfo" + no).val(str);
        $("#inwardid" + no).val(batchidss);
        $("#BatchLotModal").modal('hide');
    }
   
});

var ErrorMessage = "";

$("#saveInvoice").on('click', function () {
    debugger;
    ErrorMessage = "";
    var invdate = $("#InvoiceDate").val();
    var isvalid = false;
    if (invdate == "") {
        setswal("Select Invoice Date");
        isvalid = false;
    }
    var custid = $("#customerid").val();
    if (custid == "" || custid == "0") {
        setswal("Select Customer");
        isvalid = false;
    }

    var custNameid = $("#custnameid").val();
    if (custNameid == "") {
        setswal("Select Customer");
        isvalid = false;
    }

    var porefno = $("#porefno").val();
    if (porefno == "" || porefno == "0" || porefno == "select") {
        setswal("Select PO");
        isvalid = false;
    }

    var deliveryadd = $("#deliveryaddid").val();
    if (deliveryadd == "") {
        setswal("Please Enter Delivery Address.");
        isvalid = false;
    }
    
    var tot = $("#total").val();
    if (tot == "" || parseFloat(tot)==0) {
        setswal("Please Enter Invoice Quantity");
        isvalid = false;
    }
    
    var $dataRows1 = $("#itemtable tr:not('.titlerow')");
    $dataRows1.each(function () {
        $(this).find('.itemPIdata').each(function (i) {
            var itemlist1 = ($(this).val());
            if (itemlist1 == "" || itemlist1 == null) {
                setswal("Please Add Batch/Lot Data");
                isvalid = false;
            }
        });
        
    });
    
    if (ErrorMessage.length > 0) {

        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {
        isvalid = true;
        var rawsizerowcount = 0;
        var $dataRows = $("#itemtable tr:not('.titlerow')");
        var sizerowlenght = $dataRows.length;
        var sizerowstr = "";
        $dataRows.each(function () {
            count = 0;
            rawsizerowcount++;
            $(this).find('.itemraw').each(function (i) {
                var itemlist = ($(this).val());
                count++;

                if (count == 13) {
                    sizerowstr += itemlist;
                }
                else {
                    sizerowstr += itemlist + "~";
                }

            });

            if (rawsizerowcount == sizerowlenght) {

            }
            else {
                sizerowstr += "#";
            }

        });
        $('#Invitemstr').val(sizerowstr);



        var rawsizerowcount1 = 0;
        var sizerowlenght1 = $dataRows1.length;
        var sizerowstr1 = "";
        $dataRows1.each(function () {
            count = 0;
            rawsizerowcount1++;
            $(this).find('.itemPIdata').each(function (i) {
                var itemlist1 = ($(this).val());
                count++;

                if (count == 1) {
                    sizerowstr1 += itemlist1;
                }
                else {
                    sizerowstr1 += itemlist1 + "^";
                }

            });

            if (rawsizerowcount1 == sizerowlenght1) {

            }
            else {
                sizerowstr1 += "|";
            }

        });
        $('#InvitemPIstr').val(sizerowstr1);


        var $OC_dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
        var OClenght = $OC_dataRows.length;
        var Ocrowcount = 0;
        var str = "";
        if (OClenght > 0) {
            $OC_dataRows.each(function (i) {
                count = 0;
                Ocrowcount++;
                var Aleng = $(this).children('td').length;
                $(this).find('.ocdata').each(function (i) {
                    var itemlist = ($(this).val());
                    count++;
                    itemlist = itemlist == "select" || itemlist == "" ? 0 : itemlist;
                    if (itemlist != undefined) {

                        if (count == Aleng) {
                            str += itemlist;
                        }
                        else {
                            str += itemlist + "~";
                        }
                    }
                });

                if (Ocrowcount == OClenght) {
                }
                else {
                    str += "#";
                }
            });
            $("#otherchargestr").val(str);
        }
        else {
            $("#otherchargestr").val("");
        }
    }
    return isvalid;
});

function setswal(msg) {
    if (ErrorMessage.length > 0) {
        ErrorMessage += "\n" + msg;
    } else { ErrorMessage = msg; }
}