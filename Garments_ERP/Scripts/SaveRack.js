﻿
$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");
});


$('#saveRack').on('click', function () {
    var Rack = $("#Rack").val();
    var DepartmentId = $('#DepartmentId').val();
    var WarehouseId = $('#WarehouseId').val();
    var isvalid = true;
    //validations of Rack details 
    if (WarehouseId == "" || WarehouseId == 0) {
        swal("Please select Warehouse");
        isvalid = false;
    }
    else if (Rack == "") {
        swal("Please enter rack name");
        isvalid = false;
    }
    else if (DepartmentId == "" || DepartmentId == 0) {
        swal("Please select department");
        isvalid = false;
    }

    return isvalid;
});