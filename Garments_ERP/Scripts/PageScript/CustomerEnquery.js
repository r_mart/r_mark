﻿var MyItemArra = [];
var $ItemArra = [];
var MyRawItemArray = [];
var RawItemArray = [];
$(document).ready(function () {
    $("#bs_datepicker_component_container > .ui-datepicker-inline").attr("style", "Display:none;");
    $('.select2').select2();
    $('.select2').width("100%");
    var roleid = $("#roleid").val();
    if (parseInt(roleid) != 4) {
        $(".approval").show();
        $(".approvalComment").show();
    }
    else {
        $(".approval").hide();
        $(".approvalComment").hide();
    }

    $(".remove").click(function () {
        $(this).parent(".imgpreview").remove();
    });


    debugger;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat.length == 2) {
        pathsdat[2] = "Index";
    }


    if (pathsdat[2] == "Edit") {
        var hidenqtype = $("#hidenqtype").val();
        $("#EnquiryType").val(hidenqtype);
        $("#EnquiryType").select2().trigger('change');

        getBrokerDetail();
        GetEnqryStyleData();
        getAddress();
    }
    else if (pathsdat[2] == "Index") {
        $("#bs_datepicker_component_container > .ui-datepicker-inline").attr('style', 'display:none;');
        $("#bs_datepicker_component_container1 > .ui-datepicker-inline").attr('style', 'display:none;');

        GetData(2, 'Pending');

        var param = { 'Type': 1, 'attribute': '' };
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/StatusCount/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result.Table, function (i, data) {
                    $("#Pendingcount").text(data.pending);
                    $("#InProgresscount").text(data.inprogress);
                    $("#Completedcount").text(data.completed);
                    $("#Rejectedcount").text(data.Rejected);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        debugger;
        
        $("#Enquirydate").datepicker({
            //setDate: currentDate,
            format: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            },
            maxDate: 0

        });

        $("#approvaldate").datepicker({
            //setDate: currentDate,
            format: 'mm/dd/yy'
            // maxDate: 0
        });
        $("#Tentivedate").datepicker({
            //setDate: currentDate,
            format: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            }

            //maxDate: 0
        });

        var tempdate = new Date();
        var today = new Date();

        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        // var today2 = today.setMonth(mm + 1);
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = mm + '/' + dd + '/' + yyyy;

        var actualDate = new Date();
        var nextMonth = new Date(actualDate.getFullYear(), actualDate.getMonth() + 1, actualDate.getDate());
        //alert(nextMonth);


        dd = nextMonth.getDate();
        mm = nextMonth.getMonth() + 1;
        yyyy = nextMonth.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var nexttentivedate = mm + '/' + dd + '/' + yyyy;


        $("#Enquirydate").val(today);
        $("#approvaldate").val(today);
        $("#Tentivedate").val(nexttentivedate);
    }
});


$("#sampleyes").on("change", function () {
    //  var value = $("#Non-Taxable").val();
    if ($('#sampleyes').prop("checked") == true) {
        $("issampling").val("yes");
    }
});
$("#sampleno").on("change", function () {
    //  var value = $("#Non-Taxable").val();
    if ($('#sampleno').prop("checked") == true) {
        $("issampling").val("no");
    }
});


function ListVadation() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;

    if ($("#Segmenttypeid").val() == "" || $("#Segmenttypeid").val() == undefined || $("#Segmenttypeid").val() == "Select") {
        msg = msg + "* Please Select Segment Type. \n";
        blnFlag = false;
    }
    if ($("#Itemid").val() == "" || $("#Itemid").attr("data-ID") == "0" || $("#Itemid").attr("data-ID") == "") {
        msg = msg + "* Please Select Item. \n";
        blnFlag = false;
    }
    if ($("#Styleid").val() == "" || $("#Styleid").attr("data-ID") == "0" || $("#Styleid").attr("data-ID") == "") {
        msg = msg + "* Please Select Style. \n";
        blnFlag = false;
    }
    if ($("#Reqqty").val() == "") {
        msg = msg + "* Please Enter Item Req. Quantity. \n";
        blnFlag = false;
    }
    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "error");
        return false;
    }

    return true;
}

function AddtoList() {
    if (ListVadation() == true) {
        var $dataRow = $("#AttributeTable > tbody > tr:not('.attrbtab1')");
        var rowlen = $dataRow.length;
        if (rowlen > 0) {
            var len = 0;
            $dataRow.each(function (i) {
                len = len + 1;
                var Rate = "";
                var sizeid = "";
                var attributeval = "";
                $(this).children('td').each(function (i) {
                    $(this).find(".hid2").each(function () {
                        if (i == 0) {
                            attributeval = this.value;
                        }
                        else {
                            attributeval = attributeval + "," + this.value;
                        }
                    });

                    $(this).find(".IARate").each(function () {
                        Rate = this.value;
                    });

                    $(this).find(".IAsizeid").each(function () {
                        sizeid = this.value;
                    });
                });
                var SegTId = $("#Segmenttypeid").val();
                var SegTName = $("#Segmenttypeid option:selected").text();
                var itemid = $("#Itemid").attr("data-ID");
                var itemName = $("#Itemid").val();
                var styleid = $("#Styleid").attr("data-ID");
                var styleName = $("#Styleid").val();
                var styleDesc = $("#Styledesc").val();
                var styleNo = $("#Styleno").val();
                var Brand = $("#Brand").val();
                var AttrSizeId = $("#AttrSizeId").val();
                var attributeid = $("#AttributeId").val();
                var $MdataRow = $("#MultiItemTab > tbody > tr");
                var file1 = document.querySelector('#styleimg');
                var path = $("#Attrimages" + len).val();
                var ReqQty = $("#Reqqty").val();
                var ItemUnit = $("#ItemUnit").val();

                var Mlen = $MdataRow.length;
                var str = '<tr data="' + Mlen+'">';
                str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                str = str + '<td>' + ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + ItemUnit + '" /></td>';
                if (file1.files.length > 0) {
                    str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                    str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                }
                else {
                    str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                    str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                }
                str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                $("#MultiItemTab > tbody").append(str);
                debugger;
                MyRawItemArray = [];
                var myListJosn = {
                    "Id": "0", "Enquiryid":"0", "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                    "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "SizeId": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                    "Attribute_ID": attributeid, "Attribute_Value_ID": attributeval, "Totalqty": ReqQty, "ItemUnit": ItemUnit, "MyRawItem": MyRawItemArray
                };

                var $myListJosn2 = {
                    "Id": "0", "Enquiryid": "0", "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                    "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "SizeId": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                    "Attribute_ID": attributeid, "Attribute_Value_ID": attributeval, "Totalqty": ReqQty, "ItemUnit": ItemUnit
                };

                var file2 = document.querySelector('#Style_Img_' + Mlen);
                file2.files = file1.files;
                var emptyFile = document.createElement('input');
                emptyFile.type = 'file';
                file1.files = emptyFile.files;

                MyItemArra.push(myListJosn);
                $ItemArra.push($myListJosn2);
                GetRawItemData();
            });

            $("#Segmenttypeid").val("");
            $("#Segmenttypeid").select2().trigger('change');
            $("#Itemid").attr("data-ID","0");
            $("#Itemid").val("");
            $("#Styleid").attr("data-ID","0");
            $("#Styleid").val("");
            $("#Styledesc").val("");
            $("#Styleno").val(""); 
            $("#Brand").val("");
            $("#Reqqty").val("");
            $("#ItemUnit").val("");
            $("#AttributeTable > tbody").empty();
        }
    }
}

function GetRawItemData() {
    if (MyItemArra.length) {
        $("#rawMaterialMultiIdtab").empty();
        for (var i = 0; i < MyItemArra.length; i++) {
            var str = "<div class='box-body table-responsive no-padding'><table class='table table-striped table-bordered' id='rawitemtable_" + i + "'>";
            str = str + "<caption class='tblcaptn'>" + MyItemArra[i].ItemName + "</caption><thead><tr style='background-color:#d5d5d5;'><th>Item Category</th>";
            str = str + "<th>Sub Category</th><th>Item Desc</th><th>Supplier</th><th>Image</th><th>Action</th></tr><tr><th>";
            str = str + "<input type='text' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='itemcatNameid_" + i + "' name='itemcatNameid' onkeypress='GetCategoryList(" + i + ")' /></th>"
            str = str + "<th><input type='text' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='itemsubcategoryNameid_" + i + "' name='itemsubcategoryNameid'  onkeypress='GetSubCategoryList(" + i + ")' /></th>";
            str = str + "<th><input type='text' class='form-control alphaonly' id='itemdesc_" + i + "' name='itemdesc' /></th>";
            str = str + "<th><input type='text' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='supplierNameid_" + i + "' name='supplierNameid'  onkeypress='GetSupplierList(" + i + ")' /></th>";
            str = str + "<th><input type='file' class='form-control' name='RawImg' id='RawImg_" + i + "' style='display:none;' multiple><input type='button' class='btn btn-info' style='margin-top:0em;' value='Upload Image' onclick=ImageUpload(this.id,'upload','Raw','RawImg_" + i + "') /></th>";
            str = str + "<th><button type='button' class='btn btn-info' onclick='AddRawItam(" + i + ")'>Add</button></th></tr></thead><tbody></tbody></table>";
            str = str + "<table id='hiderawitemtable_" + i + "'><tbody></tbody></table></div>";
            $("#rawMaterialMultiIdtab").append(str);
            MyRawItemsArr(i);
        }
    }
}

function validrawitem_chk(no) {
    var valid = true;

    var itemcategoryid = $("#itemcatNameid_" + no).attr("data-ID");
    var itemsubcategoryid = $("#itemsubcategoryNameid_" + no).attr("data-ID");


    $('#rawitemtable_' + no + ' tr').each(function () {

        if ($(this).find('.validitemcatrow').val() == itemcategoryid && $(this).find('.validitemsubrow').val() == itemsubcategoryid) {
            valid = false;
        }
    });

    return valid;
}


function AddRawItam(no) {
    var selecteditemcategory = $("#itemcatNameid_" + no).val();
    var itemcategoryid = $("#itemcatNameid_" + no).attr("data-ID");

    var itemsubcategoryid = $("#itemsubcategoryNameid_" + no).attr("data-ID");
    var selecteditemsubcategory = $("#itemsubcategoryNameid_" + no).val();

    var itemdesc_ = $('#itemdesc_' + no).val();



    var supplier = $("#supplierNameid_" + no).val();
    var supplierID = $("#supplierNameid_" + no).attr("data-ID");

    var i = $('#rawitemtable_' + no + ' tr').length;
    var rowCount = 0;

    if (itemcategoryid == "") {
        swal({
            title: 'Error',
            text: "Please Select Item Category.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else if (itemsubcategoryid == "") {
        swal({
            title: 'Error',
            text: "Please Select Item Subcategory.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else if (validrawitem_chk(no) == false) {
        swal("selected item category or item sub category already added.");
    }
    else {
        debugger;
        var file1 = document.querySelector('#RawImg_' + no);
        var fcount = file1.files.length;
        var len1 = $("#rawitemtable_" + no + " tr").length;
        len1 = len1 + 1;

        var rawitemJson = {
            "Id": 0,"Enquiryid":0,
            "Itemcategoryid": itemcategoryid, "ItemCategoryName": selecteditemcategory, "Itemsubcategoryid": itemsubcategoryid, "SubCategoryName": selecteditemsubcategory, "Itemdesc": itemdesc_,
            "Supplier": supplier, "SupplierId": supplierID, "RawItemImage": file1.files, "RawImgCount": fcount
        };
        var rawitemJson1 = {
            "Id": 0, "Enquiryid": 0,
            "Itemcategoryid": itemcategoryid, "ItemCategoryName": selecteditemcategory, "Itemsubcategoryid": itemsubcategoryid, "SubCategoryName": selecteditemsubcategory, "Itemdesc": itemdesc_,
            "Supplier": supplier, "SupplierId": supplierID, "RawImgCount": fcount
        };
        RawItemArray.push(rawitemJson1);
        MyItemArra[no].MyRawItem.push(rawitemJson);
        MyRawItemsArr(no);

        var emptyFile = document.createElement('input');
        emptyFile.type = 'file';
        file1.files = emptyFile.files;
        $("#itemcatNameid_" + no).val("");
        $("#itemcatNameid_" + no).attr("data-ID", "");

        $("#itemsubcategoryNameid_" + no).attr("data-ID", "");
        $("#itemsubcategoryNameid_" + no).val("");

        $('#itemdesc_' + no).val("");
        $("#supplierNameid_" + no).val("");
        $("#supplierNameid_" + no).attr("data-ID", "");
    }
}

function MyRawItemsArr(no) {
    debugger
    if (MyItemArra[no].MyRawItem.length > 0) {
        var len1 = 0;
        $('#rawitemtable_' + no +" > tbody").empty();
        $.each(MyItemArra[no].MyRawItem, function (i, data) {
            debugger;
            len1 = len1 + 1;
            rawdiv = '<tr class="data-contact-person odd" id="' + no + '_' + len1 + '" value="' + no + '_' +len1 + '" data="' + no + '" dataIndex="' + i + '">' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemcategoryid + '" class="form-control validitemcatrow"><p class="">' + data.ItemCategoryName + '</p></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + data.SubCategoryName + '</p></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemdesc + '" class="form-control "><p class="">' + data.Itemdesc + '</p></td>' +
                '<td style="padding-right:10px;"><p class="">' + data.Supplier + '</p></td><td style="padding-right:10px;">';
            if (parseInt(data.RawItemImage.length) > 0) {
                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Preview Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
            }
            else {
                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Upload Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
            }
            rawdiv = rawdiv + '<input type="file" style="display:none;" id="RawItemImg_' + no + '_' + len1 + '" name="RawItemImg" class="form-control" multiple /></td>';
            rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitemArry btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
                '</tr>';
            $('#rawitemtable_' + no + " > tbody").append(rawdiv);


            if (parseInt(data.RawItemImage.length) > 0) {
                var file2 = document.querySelector('#RawItemImg_' + no + '_' + len1);
                file2.files = data.RawItemImage;
            }
        })

        
    }
}


$(document).on("click", ".deleteitemArr", function () {
    var id = $(this).closest("tr").attr('data');
    MyItemArra.splice(id, 1);
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#MultiItemTab > tbody > tr#' + id).remove();
    GetRawItemData();


});

$(document).on("click", ".deleterawitemArry", function () {
    debugger;
    var id = $(this).closest("tr").attr('dataIndex');
    var no = $(this).closest("tr").attr('data');
    MyItemArra[no].MyRawItem.splice(id, 1);
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#rawitemtable_' + no + ' > tbody > tr#' + id).remove();
    MyRawItemsArr(no);
});



function ImageUpload(id, name, type,fileid) {
    debugger;
    $("#img_preview").html('');
    $("#typeid").val(type);

    if (name == "preview") {
        $("#uploadid").hide();
    }
    else {
        $("#uploadid").show();
    }

    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    $("#noid").val(no);
    $("#nameid").val(name);
    $("#fileid").val(fileid);
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat[2] == "Edit") {
        $('#UploadPreviewModal').modal('toggle');
        if (type == "Style") {
            LoadImageData(fileid, "", no);
        }
        if (type == "Raw") {
            LoadImageData(fileid, "", no);
        }
        
    }
    else {
        $('#UploadPreviewModal').modal('toggle');
        if (type == "Style") {
            LoadImageData(fileid, "", no);
        }
        if (type == "Raw") {
            LoadImageData(fileid, "", no);
        }
       
    }
    $('#UploadPreviewModal').attr('style', 'z-index: 10001;');
    $(".modal-backdrop").attr('style', 'z-index: 10000;');
}

function LoadImageData(id, pathid, no) {
    debugger;
    $("#img_preview").html("");

    var strd1 = '<div id="background"></div>';
    $("#img_preview").append(strd1);
    var file1 = document.querySelector('#' + id + no);
    var file2 = document.querySelector('#ItemImages');
    file2.files = file1.files;

    var filesLength = file2.files.length;
    var wid = parseFloat(filesLength) * 80;
    var Rid = $("#img_preview").find("input:radio").attr("id");
    var m = 1;
    if (Rid == undefined) {
        m = 0;
    }

    if (pathid != "") {
        var path = $("#" + pathid + no).val();
        if (path != "" && path != undefined && filesLength == 0) {

            $.each(path.split(','), function (k, d) {
                var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + d + "' style='height:100%;width:100%' /> </label><img src='" + d + "' class='DefaultBigStyle1'>";
                $("#img_preview").append(imgstr);
                $('#id0').trigger('click');
            });
        }
    }
    for (var i = 0; i < filesLength; i++) {
        var f = file2.files[i];
        var fileReader = new FileReader();
        var k = 0;
        fileReader.onload = (function (e) {
            debugger;
            k = m + k;
            var file = e.target;
            var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /> </label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
            $("#img_preview").append(imgstr);
            $('#id0').trigger('click');
            k++;
        });

        fileReader.readAsDataURL(f);
    }
}


$("#ItemImages").on("change", function (e) {
    debugger;
    $("#img_preview").html('');
    var strd = '<div id="background"></div>';
    $("#img_preview").append(strd);
    var files = e.target.files,
        filesLength = files.length;
    var wid = parseFloat(filesLength) * 80;

    for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        var k = 0;
        fileReader.onload = (function (e) {
            var file = e.target;
            var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /></label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
            $("#img_preview").append(imgstr);
            $('#id0').trigger('click');
            k++;
        });

        fileReader.readAsDataURL(f);
    }

});


function SaveImage() {
    debugger;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    var file1 = document.querySelector('#ItemImages');
    var emptyFile = document.createElement('input');
    emptyFile.type = 'file';

    var no = $("#noid").val();
    var name = $("#nameid").val();
    var typeid = $("#typeid").val();
    if (parseInt(no) > 0) {
        if (typeid == "Item") {
            var file2 = document.querySelector('#ItemImages' + no);
        }
        if (typeid == "RawItem") {
            file2 = document.querySelector('#rawitemimg' + no);
        }
        var filesLength = file1.files.length;
        if (parseInt(filesLength) > 0) {
            file2.files = file1.files;
            $("#img_preview").html("");
        }
        file1.files = emptyFile.files;
        $('.modal').modal('hide');
    }
    else {
        var fileid=$("#fileid").val();
        if (typeid == "Style") {
            file2 = document.querySelector('#' + fileid);
        }
        if (typeid == "Raw") {
            file2 = document.querySelector('#' + fileid);
        }
      
        filesLength = file1.files.length;
        if (parseInt(filesLength) > 0) {
            file2.files = file1.files;
            $("#img_preview").html("");
        }
        file1.files = emptyFile.files;
        $('.modal').modal('hide');
    }

}


function hidepop() {
    $('.modal').modal('hide');
}

function ImagePreview(PreviewId) {
    $('#UploadPreviewModal').modal('toggle');
    $("#uploadid").hide();

    var path = $("#" + PreviewId).val();
    if (path != "" && path != undefined) {

        $.each(path.split(','), function (k, d) {
            var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + d + "' style='height:100%;width:100%' /> </label><img src='" + d + "' class='DefaultBigStyle1'>";
            $("#img_preview").append(imgstr);
            $('#id0').trigger('click');
        });
    }
}

function getItemdetail() {
    $("#AttributeTable").empty();
    var itemid = $("#Itemid").attr("data-ID");
    if (itemid != null && itemid != "" && itemid != "0" && itemid != undefined) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '/Item/getAttributeValue/',
            data: "{'MyJson':'" + itemid + "','type':4}",
            contentType: "application/json;",
            success: function (Result) {
                debugger;
                Result = JSON.parse(Result);
                var tabstr = "";
                var len1 = 1;
                tabstr = tabstr + "<tr  class='attrbtab1'>";
                $.each(Result.Table, function (a, data) {
                    debugger
                    $("#AttributeId").val(data.Attribute_ID);

                    $.each(data.AttributName.split(","), function (b, edata) {

                        tabstr = tabstr + "<th>" + edata + "</th>";
                    });
                });
                tabstr = tabstr + "<th>Required Quantity</th><th>Rate Per Qty</th><th>Image</th>";
                tabstr = tabstr + "</tr>";


                $.each(Result.Table1, function (i, subcate) {
                    debugger;
                    len1 = len1 + i;
                    var attributename = subcate.AttributeValue;
                    var index1 = attributename.indexOf(',');
                    if (index1 == -1) {
                        attributename = attributename + ",";
                    }
                    attributename = attributename.split(',');
                    tabstr = tabstr + "<tr class='attrtabval'>";
                    var ck = 0;
                    $.each(subcate.Attribute_Value_ID.split("~"), function (k, e) {
                        if (e != "") {
                            tabstr = tabstr + "<td><input type='hidden' value='" + e + "' class='hid2'  />"
                            if (attributename[ck] != "") {
                                tabstr = tabstr + "<input type='hidden' class='hidval2' value='" + attributename[ck] + "' /> " + attributename[ck] + "";
                            }
                            tabstr = tabstr + "</td>";
                            ck = ck + 1;
                        }
                    });

                    var path = "";
                    var n = 0;
                    if (subcate.AttributeImage != "" && subcate.AttributeImage != null) {
                        $.each(subcate.AttributeImage.split(','), function (c, d) {
                            if (n == 0) {
                                path = "@itemimgpath_/" + d;
                            }
                            else {
                                path = path + ',' + "@itemimgpath_/" + d;
                            }
                            n = n + 1;
                        });
                    }
                    $("#ItemUnit").val(subcate.ItemUnit);

                    tabstr = tabstr + "<td><input type='text' class='form-control IAReqQty' onkeypress='return isNumber(event)' onkeyup='GetTotQty()' /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IARate' value='" + subcate.rate + "'  readonly /><input type='hidden' class='form-control IAsizeid' id='AttrSizeId' value='" + subcate.Item_Attribute_ID + "'  readonly /></td>";
                    tabstr = tabstr + "<td dataval='" + subcate.ImgCount + "'><input type='hidden' class='IAImgcount' id='IAImg_count" + len1 + "' value='" + subcate.ImgCount + "' />";
                    tabstr = tabstr + "<input type='button' class='btn btn-info' value='View Image' id='viewimage" + len1 + "'  onclick=GetImageUpload(this.id,'preview','Item') />";
                    tabstr = tabstr + "<input type='file' style='display:none;' id='ItemAttrimages" + len1 + "' name='ItemAttrimages_id' class='form-control' multiple /> <input type='hidden' id='Attrimages" + len1 + "' name='Attrimages" + len1 + "' value='" + path + "' class='form-control' /></td>";
                    tabstr = tabstr + "</tr>"

                });
                tabstr = "<tbody>" + tabstr + "</tbody>"
                $("#AttributeTable").append(tabstr);

                var tdlen = document.getElementById('AttributeTable').rows[0].cells.length;
                if (parseInt(tdlen) > 10) {
                    var wid = parseInt(tdlen) * 140;
                    $("#AttributeTable").attr('style', 'width:' + wid + 'px');
                    $("#AttributeTable").parent('div').attr('style', 'overflow-x:scroll;');
                }

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}


function GetTotQty() {
    var TotQty = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAReqQty").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
        TotQty = TotQty + parseFloat(Qty);
    });
    $("#Reqqty").val(TotQty);
}


function getAddress() {
    var startrow = 0;
    var startrowvalue = "Select";
    var custid = $("#customerid").attr("data-ID");
    if (custid != null && custid != "" && custid != "0" && custid != undefined) {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/getaddress/',
            data: "{'custid':" + custid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {

                result = JSON.parse(result);
                $('#Addressid').empty();
                $("#Addressid").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
                $.each(result, function (i, item) {

                    $("#Addressid").append('<option value="' + item.Id + '">' + item.Address + '</option>');
                });
                $("#Addressid").val($("#Addressid option:eq(1)").val());
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}


$("#saveenquiry").on("click", function () {

    debugger;
    ErrorMessage = "";
    var isvalid = true;
    if (MyItemArra.length == 0) {
        ErrorMessage += "Please Check Style Details \n";
        isvalid = false;
    }

    var enqtype = $("#Enquirytype").val();
    if (enqtype == "select" || enqtype == "") {
        ErrorMessage += "Please Select Enquiry Type \n";
        isvalid = false;
    }

    var custid = $('#customerid').attr("data-ID");
    var addressid = $('#Addressid').val();

    if (custid == "") {
        ErrorMessage += "Please Select Customer \n";
        isvalid = false;
    }
    if (addressid == "0" || addressid == "") {
        ErrorMessage += "Please Select Customer Address. \n";
        isvalid = false;
    }
    var borkerid = $("#BrokerId").val();
    if (borkerid == "0" || borkerid == "") {
        ErrorMessage += "Please Select Broker Name. \n";
        isvalid = false;
    }

    var enqdt = $("#Enquirydate").val();
    if (enqdt == "") {
        ErrorMessage += "Please Select Enquiry Date. \n";
        isvalid = false;
    }

    var Delivrdt = $("#Tentivedate").val();
    if (Delivrdt == "") {
        ErrorMessage += "Please Select Expected Delivery Date. \n";
        isvalid = false;
    }

    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {
        SaveEnquery();
    }

    return isvalid;
});

function SaveEnquery() {
    var EnquiryId = $("#EnquiryId").val();
    var Enquiryno = $("#Enquiryno").val();
    var Enquirydate = $("#Enquirydate").val();
    var customerid = $("#customerid").attr("data-ID");
    var narration = $("#narration").val();
    var Approvalstatus = $("#Approvalstatus").val();
    if (parseInt(Approvalstatus) == 1) {
        Approvalstatus = true;
    }
    else if (parseInt(Approvalstatus) == 0) {
        Approvalstatus = false;
    }

    var Tentivedate = $("#Tentivedate").val();
    var Sampling = false;
    if ($("#sampleyes").prop("checked") == true) {
        Sampling = true;
    }
    var Addressid = $("#Addressid").val();
    var BrokerId = $("#BrokerId").attr("data-ID");
    var Enquirytype = $("#Enquirytype").val();
    var hidIsReadyMate = $("#hidIsReadyMate").val();
    var Comment = $("#Comment").val();

    var $data = {
        "EnquiryId": EnquiryId, "Enquiryno": Enquiryno, "Enquirydate": Enquirydate, "customerid": customerid, "narration": narration, "Approvalstatus": Approvalstatus, "Tentivedate": Tentivedate, "Sampling": Sampling,
        "Addressid": Addressid, "BrokerId": BrokerId, "Enquirytype": Enquirytype, 'hidIsReadyMate': hidIsReadyMate, 'Comment': Comment, "StyleEnt": $ItemArra
    }
    debugger;
    var formData = new FormData();
    var totalFiles = MyItemArra.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.querySelector('#Style_Img_' + i);
        for (var k = 0; k < file.files.length; k++) {
            formData.append("Style_Img", file.files[k]);
        }
        
        var RawItms = MyItemArra[i].MyRawItem;
        for (var j = 0; j < RawItms.length; j++) {
            var file2 = RawItms[j].RawItemImage;
            for (var m = 0; m < file2.length; m++) {
                formData.append("RawItemImg", file2[m]);
            }
        }
       
    }
    
    formData.append("data", JSON.stringify($data));
    formData.append("RawItemData", JSON.stringify(RawItemArray));

    

    //var $jsondata = JSON.stringify($data);

    debugger;
    $.ajax({
        url: '/CustomerEnquiry/AddUpdate/',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            if (data != undefined) {
                swal({
                    title: 'Success',
                    text: 'Enquery Submit Successfully.',
                    type: 'success',
                    confirmButtonText: "OK"
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = '/CustomerEnquiry/Index';
                        }
                    });
            }
        }
    });
} 


function GetStyleData() { 
    var styleid = $('#Styleid').attr("data-ID");

    if (styleid != null && styleid != "" && styleid != "0" && styleid != undefined) {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/GetStyleData/',
            data: "{'styleid':" + styleid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $("#Styledesc").val(result.Styledescription == null ? "" : result.Styledescription);
                $("#Styleno").val(result.StyleNo == null ? "" : result.StyleNo);

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}



function getBrokerDetail() {
    $('#BrokerPhone').val("");
    var brokerid = $('#BrokerId').attr("data-ID");
    if (brokerid != "0" && brokerid != null && brokerid != "" && brokerid != undefined) {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/getaddress/',
            data: "{'custid':" + brokerid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result, function (i, item) {
                    if (i == 0) {
                        $('#BrokerPhone').val(item.Contact_No);
                    }
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }

}

//Create Code Complte

///Edite Code Start
function GetEnqryStyleData() {
    var EnquiryId = $("#EnquiryId").val();
    if (EnquiryId != "") {
        if (parseInt(EnquiryId) > 0) {
            $.ajax({
                type: "POST",
                url: '/CustomerEnquiry/GetEnqryStyleData/',
                data: "{'EnquiryId':" + EnquiryId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $.each(result, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var AttributeValId = dat.Attribute_Value_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td>';
                            if (file1.files.length > 0) {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            else {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                            str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                            $("#MultiItemTab > tbody").append(str);
                            debugger;
                            MyRawItemArray = [];
                            var myListJosn = {
                                "Id": dat.Id, "Enquiryid": dat.Enquiryid, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "SizeId": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "MyRawItem": MyRawItemArray
                            };

                            var $myListJosn2 = {
                                "Id": dat.Id, "Enquiryid": dat.Enquiryid, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "SizeId": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit
                            };

                            var file2 = document.querySelector('#Style_Img_' + Mlen);
                            file2.files = file1.files;
                            var emptyFile = document.createElement('input');
                            emptyFile.type = 'file';
                            file1.files = emptyFile.files;

                            MyItemArra.push(myListJosn);
                            $ItemArra.push($myListJosn2);
                            GetRawItemData();
                            GetEnqryStyleRawItemData(dat.Id, Mlen);
                        });
                    }                    
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}


function GetEnqryStyleRawItemData(styleid, no) {
    var EnquiryId = $("#EnquiryId").val();
    if (EnquiryId != "") {
        if (parseInt(EnquiryId) > 0) {
            $.ajax({
                type: "POST",
                url: '/CustomerEnquiry/GetEnqryStyleRawItemData/',
                data: "{'EnquiryId':" + EnquiryId + ",'styleid':" + styleid+"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        var len1 = 0;
                        $('#rawitemtable_' + no + " > tbody").empty();
                        $.each(result, function (i, data) {
                            debugger;
                            len1 = len1 + 1;
                            rawdiv = '<tr class="data-contact-person odd" id="' + no + '_' + len1 + '" value="' + no + '_' + len1 + '" data="' + no + '" dataIndex="' + i + '">' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemcategoryid + '" class="form-control validitemcatrow"><p class="">' + data.ItemCategoryName + '</p></td>' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + data.SubCategoryName + '</p></td>' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemdesc + '" class="form-control "><p class="">' + data.Itemdesc + '</p></td>' +
                                '<td style="padding-right:10px;"><p class="">' + data.Supplier + '</p></td><td style="padding-right:10px;">';
                            if (parseInt(data.RawItemImg.length) > 0) {
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Preview Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            }
                            else {
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Upload Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            }
                            rawdiv = rawdiv + '<input type="file" style="display:none;" id="RawItemImg_' + no + '_' + len1 + '" name="RawItemImg" class="form-control" multiple /></td>';
                            rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitemArry btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
                                '</tr>';
                            $('#rawitemtable_' + no + " > tbody").append(rawdiv);


                            if (parseInt(data.RawItemImg.length) > 0) {
                                var file2 = document.querySelector('#RawItemImg_' + no + '_' + len1);
                                file2.files = data.RawItemImage;
                            }

                            var rawitemJson = {
                                "Id": data.Id, "Enquiryid": data.Enquiryid,
                                "Itemcategoryid": data.Itemcategoryid, "ItemCategoryName": data.ItemCategoryName, "Itemsubcategoryid": data.Itemsubcategoryid, "SubCategoryName": data.SubCategoryName, "Itemdesc": data.Itemdesc,
                                "Supplier": data.Supplier, "RawImgCount": data.RawItemImg.length
                            };
                            var rawitemJson1 = {
                                "Id": data.Id, "Enquiryid": data.Enquiryid,
                                "Itemcategoryid": data.Itemcategoryid, "ItemCategoryName": data.ItemCategoryName, "Itemsubcategoryid": data.Itemsubcategoryid, "SubCategoryName": data.SubCategoryName, "Itemdesc": data.Itemdesc,
                                "Supplier": data.Supplier, "RawImgCount": data.RawItemImg.length
                            };
                            RawItemArray.push(rawitemJson1);
                            MyItemArra[no].MyRawItem.push(rawitemJson);



                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}


//Index Page 
function deleteenquiry(id) {
    var enquiryid = id;
    var param = { 'id': enquiryid };

    swal({
        title: 'Confirmation',
        text: 'Are You sure delete enquiry?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/CustomerEnquiry/delteenquiry/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {

                        if (result == true) {
                            swal({
                                title: 'Confirmation',
                                text: 'Record Delete Successfully!',
                                showConfirmButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true,
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#8CD4F5',
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                }
                            )

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("Cancelled", ")", "error");
            }
        });


}

function Cancleenquiry(id) {
    var enquiryid = id;
    var param = { 'id': enquiryid };

    swal({
        title: 'Confirmation',
        text: 'Are You Sure Rejected This Enquiry?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/CustomerEnquiry/Rejectedenquiry/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {

                        if (result == true) {
                            swal({
                                title: 'Confirmation',
                                text: 'Record Rejected Successfully!',
                                showConfirmButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true,
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#8CD4F5',
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                }
                            )

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("Cancelled", ")", "error");
            }
        });


}

function GetData(Status, Name) {

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/CustomerEnquiry/GetStatusData", { 'IsStatus': Status });

}

function setDateformat(Dt) {

    var dt1 = Dt.substring(6, Dt.length);
    dt1 = dt1.substring(0, dt1.length - 2);
    var today = new Date(parseInt(dt1));
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    // var today2 = today.setMonth(mm + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = mm + '/' + dd + '/' + yyyy;

    return today;
}


function FilterDataShow() {
    debugger;
    var formDate = $("#fromdate").val();
    var toDate = $("#todate").val();
    var Name = $("#Tablist > .active").attr('id');
    var Status = $("#" + Name).attr("datavalue")

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/CustomerEnquiry/GetFilterStatusData/", { 'fromdate': formDate, 'todate': toDate, 'IsStatus': Status });

}

function loadingHTML() {
    return $("#divLoadingContent").html();
}

function GetEnqProduct(EnquiryId) {
    if (EnquiryId != "") {
        if (parseInt(EnquiryId) > 0) {
            $("#EnqProductmodel").modal("show");
            $.ajax({
                type: "POST",
                url: '/CustomerEnquiry/GetEnqryStyleData/',
                data: "{'EnquiryId':" + EnquiryId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $("#EnqProductTab > tbody").empty();
                        $.each(result, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td></tr>';
                            $("#EnqProductTab > tbody").append(str);
                            
                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}
