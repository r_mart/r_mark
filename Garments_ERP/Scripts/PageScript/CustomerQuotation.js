﻿var MyItemArra = [];
var $ItemArra = [];
var MyRawItemArray = [];
var RawItemArray = [];
var ErrorMessage = "";

function setswal(msg) {
    if (ErrorMessage.length > 0) {
        ErrorMessage += "\n" + msg;
    } else { ErrorMessage = msg; }
}
$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");

    $("#bs_datepicker_component_container > .ui-datepicker-inline").attr("style", "Display:none;");

    var roleid = $("#roleid").val();
    if (parseInt(roleid) != 4) {
        $(".approval").show();
        $(".approvalComment").show();
    }
    else {
        $(".approval").hide();
        $(".approvalComment").hide();
    }

    debugger;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat.length == 2) {
        pathsdat[2] = "Index";
    }

    if (pathsdat[2] == "Edit") {
        GetQuotationStyleData();
        getAddress();
        othercharge1();
    }
    else if (pathsdat[2] == "Index") {
        $("#bs_datepicker_component_container > .ui-datepicker-inline").attr('style', 'display:none;');
        $("#bs_datepicker_component_container1 > .ui-datepicker-inline").attr('style', 'display:none;');

        GetData(2, 'Pending');

        var param = { 'Type': 2, 'attribute': '' };
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/StatusCount/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result.Table, function (i, data) {
                    $("#Pendingcount").text(data.pending);
                    $("#InProgresscount").text(data.inprogress);
                    $("#Completedcount").text(data.completed);
                    $("#Rejectedcount").text(data.Rejected);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {

        $("#Quotationdate").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy',
            maxDate: 0
        });
        $("#approvaldate").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy'
            // maxDate: 0
        });
        $("#Tentivedate").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy'
            //  maxDate: 0
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = mm + '/' + dd + '/' + yyyy;
        document.getElementById("Quotationdate").value = today;
        document.getElementById("approvaldate").value = today;
        document.getElementById("Tentivedate").value = today;
    }

});

function getCustomerdetails() {
    var startrow = 0;
    var startrowvalue = "---Select----";
    var custid = $("#customerid").attr("data-id");
    $("#Enquiryrefno").empty();
    $.ajax({
        type: "POST",
        url: '/CustomerEnquiry/getenquiry/',
        data: "{'custid':" + custid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            $("#Enquiryrefno").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
            $.each(result, function (i, bussiness) {
                $("#Enquiryrefno").append('<option value="' + bussiness.EnquiryId + '">' + bussiness.Enquiryno + '</option>');
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });

    getAddress();
}

function getAddress() {
    var custid = $("#customerid").attr("data-id");
    $.ajax({
        type: "POST",
        url: '/CustomerEnquiry/getaddress/',
        data: "{'custid':" + custid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            result = JSON.parse(result);
            $('#Addressid').empty();
            $("#Addressid").append('<option value="0">Select</option>');
            $.each(result, function (i, item) {

                $("#Addressid").append('<option value="' + item.Id + '">' + item.Address + '</option>');
            });
            $("#Addressid").val($("#Addressid option:eq(1)").val());

            getinfo();

            
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function getinfo() {
    var custaddid = $("#Addressid").val();
    if (custaddid != "") {
        $.ajax({
            type: "POST",
            url: '/quotation/getcustomerdetails/',
            data: "{'id':" + custaddid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result, function (i, item) {
                    $('#email').val(item.Email);
                    $("#phone").val(item.Contact_No);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

}



function getenquirydetails(id) {
    debugger;
    var enquiryid = $("#Enquiryrefno").val();
    var enq = $("#Enquiryrefno option:selected").text();

    $('#enquiryno').val(enq);
    if (enquiryid != "" && enquiryid != "0") {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/getenquirydetails/',
            data: "{'id':" + enquiryid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);
                if (result != null) {
                    GetDateFormateChage(result.Createddate);
                    result.Createddate = GetDateFormateChage(result.Createddate);
                    result.Enquirydate = GetDateFormateChage(result.Enquirydate);
                    result.Tentivedate = GetDateFormateChage(result.Tentivedate);
                    var Isreadymade = result.IsReadmade != null && result.IsReadmade > 0 ? result.IsReadmade : 0;
                    $("#hidIsReadyMate").val(Isreadymade);
                    $("#Tentivedate").val(result.Tentivedate);
                    //var result_ = JSON.stringify(result);
                    if (result.Sampling == true) {
                        $("input[name=sam][value='yes']").prop('checked', true);
                    }
                    else {
                        $("input[name=sam][value='no']").prop('checked', true);
                    }
                    var len = 0;
                    if (result.StyleEnt != null) {
                        $.each(result.StyleEnt, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var AttributeValId = dat.Attribute_Value_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td>';
                            str = str + '<td><input type="text" class="form-control" id="M_Rate_' + Mlen + '" name="M_Rate" value="' + dat.rate + '"  readonly /></td>';
                            str = str + '<td><input type="text" class="form-control" value=""  id="M_SellRate_' + Mlen + '" name="M_SellRate" onkeypress="return isNumber(event);" onkeyup="GetTotQty(this.id)" /></td>';
                            str = str + '<td><select class="form-control select2" style="width: 115px;" id="M_GSTCharge_' + Mlen + '" name="M_GSTCharge" onchange="GetTotQty(this.id)"></select></td>';
                            str = str + '<td><input type="text" class="form-control" id="M_Tax_' + Mlen + '" name="M_Tax" value=""  readonly /></td>';
                            str = str + '<td><input type="text" class="form-control" value=""   id="M_total_' + Mlen + '" name="M_total" onkeypress="return isNumber(event);" readonly /></td>';
                            if (file1.files.length > 0) {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            else {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                            str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                            $("#MultiItemTab > tbody").append(str);
                            debugger;
                            MyRawItemArray = [];
                            var myListJosn = {
                                "Id": 0, "Quotationid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": 0, "GST": 0, "Subtotal": 0,
                                "MyRawItem": MyRawItemArray
                            };

                            var $myListJosn2 = {
                                "Id": 0, "Quotationid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": 0, "GST": 0, "Subtotal": 0
                            };

                            var file2 = document.querySelector('#Style_Img_' + Mlen);
                            file2.files = file1.files;
                            var emptyFile = document.createElement('input');
                            emptyFile.type = 'file';
                            file1.files = emptyFile.files;

                            MyItemArra.push(myListJosn);
                            $ItemArra.push($myListJosn2);
                            GetRawItemData();
                            GetEnqryStyleRawItemData(dat.Id, Mlen);

                            //Get Tax List
                            $.ajax({
                                type: "POST",
                                url: '/quotation/getGSTTaxlist/',
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (gstsdata) {
                                    var result_ = JSON.parse(gstsdata);
                                    $('#M_GSTCharge_' + Mlen).empty();
                                    $('#M_GSTCharge_' + Mlen).append('<option value="select">Select</option>');
                                    $.each(result_, function (i, item) {
                                        $('#M_GSTCharge_' + Mlen).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                                    });
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        });
                    }

                    $('#isselectenquiry').val("0");
                }

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetRawItemData() {
    if (MyItemArra.length) {
        $("#rawMaterialMultiIdtab").empty();
        for (var i = 0; i < MyItemArra.length; i++) {
            var str = "<div class='box-body table-responsive no-padding'><table class='table table-striped table-bordered' id='rawitemtable_" + i + "'>";
            str = str + "<caption class='tblcaptn'>" + MyItemArra[i].ItemName + "<input type='hidden' value='" + MyItemArra[i].Itemid + "' id='RawitemId_" + i + "' /></caption><thead><tr style='background-color:#d5d5d5;'><th>Item Category</th>";
            str = str + "<th>Sub Category</th><th>Item Desc</th><th>Supplier</th><th>Image</th><th>Action</th></tr><tr><th>";
            str = str + "<input type='text' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='itemcatNameid_" + i + "' name='itemcatNameid' onkeypress='GetCategoryList(" + i + ")' /></th>"
            str = str + "<th><input type='text' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='itemsubcategoryNameid_" + i + "' name='itemsubcategoryNameid'  onkeypress='GetSubCategoryList(" + i + ")' /></th>";
            str = str + "<th><input type='text' class='form-control alphaonly' id='itemdesc_" + i + "' name='itemdesc' /></th>";
            str = str + "<th><input type='text' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='supplierNameid_" + i + "' name='supplierNameid'  onkeypress='GetSupplierList(" + i + ")' /></th>";
            str = str + "<th><input type='file' class='form-control' name='RawImg' id='RawImg_" + i + "' style='display:none;' multiple><input type='button' class='btn btn-info' style='margin-top:0em;' value='Upload Image' onclick=ImageUpload(this.id,'upload','Raw','RawImg_" + i + "') /></th>";
            str = str + "<th><button type='button' class='btn btn-info' onclick='AddRawItam(" + i + ")'>Add</button></th></tr></thead><tbody></tbody></table>";
            str = str + "<table id='hiderawitemtable_" + i + "'><tbody></tbody></table></div>";
            $("#rawMaterialMultiIdtab").append(str);
            MyRawItemsArr(i);
        }
    }
}

function MyRawItemsArr(no) {
    debugger
    if (MyItemArra[no].MyRawItem.length > 0) {
        var len1 = 0;
        $('#rawitemtable_' + no + " > tbody").empty();
        $.each(MyItemArra[no].MyRawItem, function (i, data) {
            debugger;
            len1 = len1 + 1;
            rawdiv = '<tr class="data-contact-person odd" id="' + no + '_' + len1 + '" value="' + no + '_' + len1 + '" data="' + no + '" dataIndex="' + i + '">' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemcategoryid + '" class="form-control validitemcatrow"><p class="">' + data.ItemCategoryName + '</p></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + data.SubCategoryName + '</p></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemdesc + '" class="form-control "><p class="">' + data.Itemdesc + '</p></td>' +
                '<td style="padding-right:10px;"><p class="">' + data.Supplier + '</p></td><td style="padding-right:10px;">';
            if (parseInt(data.RawItemImage.length) > 0) {
                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Preview Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
            }
            else {
                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Upload Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
            }
            rawdiv = rawdiv + '<input type="file" style="display:none;" id="RawItemImg_' + no + '_' + len1 + '" name="RawItemImg" class="form-control" multiple /></td>';
            rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitemArry btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
                '</tr>';
            $('#rawitemtable_' + no + " > tbody").append(rawdiv);


            if (parseInt(data.RawItemImage.length) > 0) {
                var file2 = document.querySelector('#RawItemImg_' + no + '_' + len1);
                file2.files = data.RawItemImage;
            }
        })


    }
}

function GetEnqryStyleRawItemData(styleid, no) {
    var EnquiryId = $("#Enquiryrefno").val();
    if (EnquiryId != "") {
        if (parseInt(EnquiryId) > 0) {
            $.ajax({
                type: "POST",
                url: '/CustomerEnquiry/GetEnqryStyleRawItemData/',
                data: "{'EnquiryId':" + EnquiryId + ",'styleid':" + styleid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        var len1 = 0;
                        $('#rawitemtable_' + no + " > tbody").empty();
                        $.each(result, function (i, data) {
                            debugger;
                            len1 = len1 + 1;
                            rawdiv = '<tr class="data-contact-person odd" id="' + no + '_' + len1 + '" value="' + no + '_' + len1 + '" data="' + no + '" dataIndex="' + i + '">' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemcategoryid + '" class="form-control validitemcatrow"><p class="">' + data.ItemCategoryName + '</p></td>' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + data.SubCategoryName + '</p></td>' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemdesc + '" class="form-control "><p class="">' + data.Itemdesc + '</p></td>' +
                                '<td style="padding-right:10px;"><p class="">' + data.Supplier + '</p></td><td style="padding-right:10px;">';
                            if (parseInt(data.RawItemImg.length) > 0) {
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Preview Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            }
                            else {
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Upload Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            }
                            rawdiv = rawdiv + '<input type="file" style="display:none;" id="RawItemImg_' + no + '_' + len1 + '" name="RawItemImg" class="form-control" multiple /></td>';
                            rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitemArry btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
                                '</tr>';
                            $('#rawitemtable_' + no + " > tbody").append(rawdiv);


                            if (parseInt(data.RawItemImg.length) > 0) {
                                var file2 = document.querySelector('#RawItemImg_' + no + '_' + len1);
                                file2.files = data.RawItemImage;
                            }

                            var rawitemJson = {
                                "Id": 0, "Quotationid": 0,
                                "Itemcategoryid": data.Itemcategoryid, "ItemCategoryName": data.ItemCategoryName, "Itemsubcategoryid": data.Itemsubcategoryid, "SubCategoryName": data.SubCategoryName, "Itemdesc": data.Itemdesc,
                                "Supplier": data.Supplier, "Itemid": data.Itemid, "RawItemImage": data.RawItemImg.length, "RawImgCount": data.RawItemImg.length
                            };
                            var rawitemJson1 = {
                                "Id": 0, "Quotationid": 0,
                                "Itemcategoryid": data.Itemcategoryid, "ItemCategoryName": data.ItemCategoryName, "Itemsubcategoryid": data.Itemsubcategoryid, "SubCategoryName": data.SubCategoryName, "Itemdesc": data.Itemdesc,
                                "Supplier": data.Supplier, "Itemid": data.Itemid, "RawItemImage": data.RawItemImg.length,"RawImgCount": data.RawItemImg.length
                            };
                            RawItemArray.push(rawitemJson1);
                            MyItemArra[no].MyRawItem.push(rawitemJson);



                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}


function GetDateFormateChage(DateString) {
    DateString = DateString.substr(0, DateString.lastIndexOf("T"));
    var splitString = DateString.split('-');
    DateString = splitString[1] + "/" + splitString[2] + "/" + splitString[0];
    return DateString
}

function SetTotQty() {
    var TotQty = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAReqQty").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
        TotQty = TotQty + parseFloat(Qty);
    });
    $("#Reqqty").val(TotQty);
}

// Create By Shubham QuationCal() 18-07-2019 Start
function GetTotQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    var RQty = $("#M_ReqQty_" + no).val();
    RQty = RQty != "" ? RQty : 0;
    var SellRate = $("#M_SellRate_" + no).val();
    SellRate = SellRate != "" ? SellRate : 0;
    var GST = $("#M_GSTCharge_" + no).val();
    GST = GST != "" && GST != "select" ? GST : 0;
    var tot = parseFloat(RQty) * parseFloat(SellRate);
    var taxv = (parseFloat(tot) * parseFloat(GST)) / 100;
    $("#M_Tax_" + no).val(taxv);
    tot = parseFloat(tot) + parseFloat(taxv);
    $("#M_total_" + no).val(parseFloat(tot).toFixed(2));
    MyItemArra[no].SellRate = SellRate;
    MyItemArra[no].GST = GST;
    MyItemArra[no].Subtotal = tot;

    $ItemArra[no].SellRate = SellRate;
    $ItemArra[no].GST = GST;
    $ItemArra[no].Subtotal = tot;


    QuationCal();
}

function QuationCal() {
    debugger;
    var totRQ = 0;
    var salP = 0;
    var subtot = 0;
    var st = 0;
    var $dataRow = $("#MultiItemTab > tbody > tr");
    var rowlen = $dataRow.length;
    if (rowlen > 0) {
        var len = 0;
        for (var i = 0; i < rowlen; i++) {
            var tot = $("#M_total_" + i).val();
            tot = tot == "" ? 0 : tot;
            subtot = parseFloat(subtot) + parseFloat(tot);
        }
    }
    subtot = parseFloat(subtot).toFixed(2);
    $("#Total").val(subtot);
    $("#Htotal").val(subtot);
    $("#GrandTotal").val(subtot);
    GetGrandTot();
}


function GetGrandTot() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var len = $dataRows.length;
    var OC_tot1 = 0;
    var potot = $("#Total").val();
    if (len > 0) {
        $dataRows.each(function (i) {
            $(this).find('.ocdata').each(function (i) {
                if (i == 4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });

        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#GrandTotal").val(Grandtot);
    }
    else {
        $("#GrandTotal").val(potot);
    }
}

// Create By Shubham QuationCal() 18-07-2019 END

function getItemdetail() {
    $("#AttributeTable").empty();
    var itemid = $("#Itemid").attr("data-ID");
    if (itemid != null && itemid != "" && itemid != "0" && itemid != undefined) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '/Item/getAttributeValue/',
            data: "{'MyJson':'" + itemid + "','type':4}",
            contentType: "application/json;",
            success: function (Result) {
                debugger;
                Result = JSON.parse(Result);
                var tabstr = "";
                var len1 = 1;
                tabstr = tabstr + "<tr  class='attrbtab1'>";
                $.each(Result.Table, function (a, data) {
                    debugger
                    $("#AttributeId").val(data.Attribute_ID);

                    $.each(data.AttributName.split(","), function (b, edata) {

                        tabstr = tabstr + "<th>" + edata + "</th>";
                    });
                });
                tabstr = tabstr + "<th>Required Quantity</th><th>Cost Price Per Qty</th><th><p class='required'>Sell Price Per Qty</p></th><th><p class='required'>GST Charge(%)</p></th><th>Tax</th><th>Sub Total</th><th>Image</th>";
                tabstr = tabstr + "</tr>"


                $.each(Result.Table1, function (i, subcate) {
                    debugger;
                    len1 = len1 + i;
                    var attributename = subcate.AttributeValue;
                    var index1 = attributename.indexOf(',');
                    if (index1 == -1) {
                        attributename = attributename + ",";
                    }
                    attributename = attributename.split(',');
                    tabstr = tabstr + "<tr class='attrtabval'>";
                    var ck = 0;
                    $.each(subcate.Attribute_Value_ID.split("~"), function (k, e) {
                        if (e != "") {
                            tabstr = tabstr + "<td><input type='hidden' value='" + e + "' class='hid2'  />"
                            if (attributename[ck] != "") {
                                tabstr = tabstr + "<input type='hidden' class='hidval2' value='" + attributename[ck] + "' /> " + attributename[ck] + "";
                            }
                            tabstr = tabstr + "</td>";
                            ck = ck + 1;
                        }
                    });


                    var path = "";
                    var n = 0;
                    if (subcate.AttributeImage != "" && subcate.AttributeImage != null) {
                        $.each(subcate.AttributeImage.split(','), function (c, d) {
                            if (n == 0) {
                                path = "@itemimgpath_/" + d;
                            }
                            else {
                                path = path + ',' + "@itemimgpath_/" + d;
                            }
                            n = n + 1;
                        });
                    }
                    $("#ItemUnit").val(subcate.ItemUnit);

                    tabstr = tabstr + "<td><input type='text' class='form-control IAReqQty' value='' id='IAReqQty" + len1 + "' onkeypress='return isNumber(event)' onkeyup='SetTotQty()' /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IARate' value='" + subcate.rate + "'  readonly /><input type='hidden' class='form-control IAsizeid' value='" + subcate.Item_Attribute_ID + "'  readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IASellRate' value=''  id='IASellRate" + len1 + "' onkeypress='return isNumber(event);' onkeyup='A_GetTotQty(this.id)' /></td>";
                    tabstr = tabstr + "<td><select class='form-control select2 IAGSTCharge' style='width:115px;' id='GSTCharge" + len1 + "' name='GSTCharge" + len1 + "' onchange='A_GetTotQty(this.id)'></select></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IATax' value='' dataGST='" + subcate.Taxrate + "' id='IATax" + len1 + "' onkeypress='return isNumber(event);' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IAtotal' value='' id='IAtotal" + len1 + "' onkeypress='return isNumber(event);' readonly /></td>";
                    tabstr = tabstr + "<td dataval='" + subcate.ImgCount + "'><input type='hidden' class='IAImgcount' id='IAImg_count" + len1 + "' value='" + subcate.ImgCount + "' />";
                    tabstr = tabstr + "<input type='button' class='btn btn-info' value='View Image' id='viewimage" + len1 + "'  onclick=GetImageUpload(this.id,'preview','Item') />";
                    tabstr = tabstr + "<input type='file' style='display:none;' id='ItemAttrimages" + len1 + "' name='ItemAttrimages_id' class='form-control' multiple /> <input type='hidden' id='Attrimages" + len1 + "' name='Attrimages" + len1 + "' value='" + path + "' class='form-control' /></td>";
                    tabstr = tabstr + "</tr>"

                });
                tabstr = "<tbody>" + tabstr + "</tbody>"
                $("#AttributeTable").append(tabstr);

                var tdlen = document.getElementById('AttributeTable').rows[0].cells.length;
                if (parseInt(tdlen) > 10) {
                    var wid = parseInt(tdlen) * 140;
                    $("#AttributeTable").attr('style', 'width:' + wid + 'px');
                    $("#AttributeTable").parent('div').attr('style', 'overflow-x:scroll;');
                }

                //Get Tax List
                $.ajax({
                    type: "POST",
                    url: '/quotation/getGSTTaxlist',
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (gstsdata) {

                        var result_ = JSON.parse(gstsdata);
                        $('.IAGSTCharge').empty();
                        $(".IAGSTCharge").append('<option value="select">Select</option>');
                        $.each(result_, function (i, item) {
                            $(".IAGSTCharge").append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                        });

                        var i = 1;
                        $("#AttributeTable tr.attrtabval").children('td').find(".IATax").each(function () {
                            var GST = this.getAttribute('dataGST');
                            $("#GSTCharge" + i).val(GST);
                            i++;
                        });

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetStyleData() {
    var styleid = $('#Styleid').attr("data-ID");

    if (styleid != null && styleid != "" && styleid != "0" && styleid != undefined) {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/GetStyleData/',
            data: "{'styleid':" + styleid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $("#Styledesc").val(result.Styledescription == null ? "" : result.Styledescription);
                $("#Styleno").val(result.StyleNo == null ? "" : result.StyleNo);

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function A_GetTotQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    var RQty = $("#IAReqQty" + no).val();
    RQty = RQty != "" ? RQty : 0;
    var SellRate = $("#IASellRate" + no).val();
    SellRate = SellRate != "" ? SellRate : 0;
    var GST = $("#GSTCharge" + no).val();
    GST = GST != null && GST != "" && GST != "select" ? GST : 0;
    var tot = parseFloat(RQty) * parseFloat(SellRate);
    var taxv = (parseFloat(tot) * parseFloat(GST)) / 100;
    $("#IATax" + no).val(taxv);
    tot = parseFloat(tot) + parseFloat(taxv);
    $("#IAtotal" + no).val(parseFloat(tot).toFixed(2));
}

function ListVadation() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;

    if ($("#Segmenttypeid").val() == "" || $("#Segmenttypeid").val() == undefined || $("#Segmenttypeid").val() == "Select") {
        msg = msg + "* Please Select Segment Type. \n";
        blnFlag = false;
    }
    if ($("#Itemid").val() == "" || $("#Itemid").attr("data-ID") == "0" || $("#Itemid").attr("data-ID") == "") {
        msg = msg + "* Please Select Item. \n";
        blnFlag = false;
    }
    if ($("#Styleid").val() == "" || $("#Styleid").attr("data-ID") == "0" || $("#Styleid").attr("data-ID") == "") {
        msg = msg + "* Please Select Style. \n";
        blnFlag = false;
    }
    if ($("#Reqqty").val() == "") {
        msg = msg + "* Please Enter Item Req. Quantity. \n";
        blnFlag = false;
    }
    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "error");
        return false;
    }

    return true;
}

function AddtoList() {
    if (ListVadation() == true) {
        var $dataRow = $("#AttributeTable > tbody > tr:not('.attrbtab1')");
        var rowlen = $dataRow.length;
        if (rowlen > 0) {
            var len = 0;
            $dataRow.each(function (i) {
                len = len + 1;
                //get row of sizetable
                var attributeval = "";
                var ReqQty = "";
                var Rate = "";
                var sizeid = "";
                var Sellrate = "";
                var GST = "";
                var taxval = "";
                var tot = "";
                var imgcount = "";
                $(this).children('td').each(function (i) {
                    debugger;
                    $(this).find(".hid2").each(function () {
                        if (i == 0) {
                            attributeval = this.value;
                        }
                        else {
                            attributeval = attributeval + "," + this.value;
                        }
                    });

                    $(this).find(".IAReqQty").each(function () {
                        ReqQty = this.value;
                    });

                    $(this).find(".IARate").each(function () {
                        Rate = this.value;
                    });

                    $(this).find(".IAsizeid").each(function () {
                        sizeid = this.value;
                    });
                    $(this).find(".IASellRate").each(function () {
                        Sellrate = this.value;
                    });
                    $(this).find(".IAGSTCharge ").each(function () {
                        GST = this.value;
                    });
                    $(this).find(".IATax").each(function () {
                        taxval = this.value;
                    });
                    $(this).find(".IAtotal").each(function () {
                        tot = this.value;
                    });
                    $(this).find(".IAImgcount").each(function () {
                        imgcount = this.value;
                    });

                });
                var SegTId = $("#Segmenttypeid").val();
                var SegTName = $("#Segmenttypeid option:selected").text();
                var itemid = $("#Itemid").attr("data-ID");
                var itemName = $("#Itemid").val();
                var styleid = $("#Styleid").attr("data-ID");
                var styleName = $("#Styleid").val();
                var styleDesc = $("#Styledesc").val();
                var styleNo = $("#Styleno").val();
                var Brand = $("#Brand").val();
                var AttrSizeId = $("#AttrSizeId").val();
                var attributeid = $("#AttributeId").val();
                var $MdataRow = $("#MultiItemTab > tbody > tr");
                var file1 = document.querySelector('#styleimg');
                var path = $("#Attrimages" + len).val();
                var ReqQty = $("#Reqqty").val();
                var ItemUnit = $("#ItemUnit").val();

                var Mlen = $MdataRow.length;
                var str = '<tr data="' + Mlen + '">';
                str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                str = str + '<td>' + ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + ItemUnit + '" /></td>';
                str = str + '<td><input type="text" class="form-control" id="M_Rate_' + Mlen + '" name="M_Rate" value="' + Rate + '"  readonly /></td>';
                str = str + '<td><input type="text" class="form-control" value="' + Sellrate+'"  id="M_SellRate_' + Mlen + '" name="M_SellRate" onkeypress="return isNumber(event);" onkeyup="GetTotQty(this.id)" /></td>';
                str = str + '<td><select class="form-control select2" style="width: 115px;" id="M_GSTCharge_' + Mlen + '" name="M_GSTCharge" onchange="GetTotQty(this.id)"></select></td>';
                str = str + '<td><input type="text" class="form-control" id="M_Tax_' + Mlen + '" name="M_Tax" value="' + taxval+'"  readonly /></td>';
                str = str + '<td><input type="text" class="form-control" value="' + tot+'"   id="M_total_' + Mlen + '" name="M_total" onkeypress="return isNumber(event);" readonly /></td>';
                if (file1.files.length > 0) {
                    str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                    str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                }
                else {
                    str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                    str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                }
                str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                $("#MultiItemTab > tbody").append(str);
                debugger;
                MyRawItemArray = [];
                var myListJosn = {
                    "Id": 0, "Quotationid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                    "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                    "Attribute_ID": attributeid, "Attribute_Value_ID": attributeval, "Totalqty": ReqQty, "ItemUnit": ItemUnit, "SellRate": Sellrate, "GST": GST, "Subtotal": tot,
                    "MyRawItem": MyRawItemArray
                };

                var $myListJosn2 = {
                    "Id": 0, "Quotationid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                    "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                    "Attribute_ID": attributeid, "Attribute_Value_ID": attributeval, "Totalqty": ReqQty, "ItemUnit": ItemUnit, "SellRate": Sellrate, "GST": GST, "Subtotal": tot
                };

                var file2 = document.querySelector('#Style_Img_' + Mlen);
                file2.files = file1.files;
                var emptyFile = document.createElement('input');
                emptyFile.type = 'file';
                file1.files = emptyFile.files;

                MyItemArra.push(myListJosn);
                $ItemArra.push($myListJosn2);
                GetRawItemData();

                //Get Tax List
                $.ajax({
                    type: "POST",
                    url: '/quotation/getGSTTaxlist',
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (gstsdata) {

                        var result_ = JSON.parse(gstsdata);
                        $('#M_GSTCharge_' + Mlen).empty();
                        $('#M_GSTCharge_' + Mlen).append('<option value="select">Select</option>');
                        $.each(result_, function (i, item) {
                            $('#M_GSTCharge_' + Mlen).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                        });
                        $('#M_GSTCharge_' + Mlen).val(GST);

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            });

            $("#Segmenttypeid").val("");
            $("#Segmenttypeid").select2().trigger('change');
            $("#Itemid").attr("data-ID", "0");
            $("#Itemid").val("");
            $("#Styleid").attr("data-ID", "0");
            $("#Styleid").val("");
            $("#Styledesc").val("");
            $("#Styleno").val("");
            $("#Brand").val("");
            $("#Reqqty").val("");
            $("#ItemUnit").val("");
            $("#AttributeTable > tbody").empty();
            QuationCal();
        }
    }
}

function AddRawItam(no) {
    var selecteditemcategory = $("#itemcatNameid_" + no).val();
    var itemcategoryid = $("#itemcatNameid_" + no).attr("data-ID");

    var itemsubcategoryid = $("#itemsubcategoryNameid_" + no).attr("data-ID");
    var selecteditemsubcategory = $("#itemsubcategoryNameid_" + no).val();

    var itemdesc_ = $('#itemdesc_' + no).val();



    var supplier = $("#supplierNameid_" + no).val();
    var supplierID = $("#supplierNameid_" + no).attr("data-ID");

    var i = $('#rawitemtable_' + no + ' tr').length;
    var rowCount = 0;

    if (itemcategoryid == "") {
        swal({
            title: 'Error',
            text: "Please Select Item Category.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else if (itemsubcategoryid == "") {
        swal({
            title: 'Error',
            text: "Please Select Item Subcategory.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else if (validrawitem_chk(no) == false) {
        swal("selected item category or item sub category already added.");
    }
    else {
        debugger;
        var file1 = document.querySelector('#RawImg_' + no);
        var fcount = file1.files.length;
        var len1 = $("#rawitemtable_" + no + " tr").length;
        len1 = len1 + 1;
        var RawitemId = $("#RawitemId_" + no).val();
        var rawitemJson = {
            "Id": 0, "Quotationid": 0,
            "Itemcategoryid": itemcategoryid, "ItemCategoryName": selecteditemcategory, "Itemsubcategoryid": itemsubcategoryid, "SubCategoryName": selecteditemsubcategory, "Itemdesc": itemdesc_,
            "Supplier": supplier, "SupplierId": supplierID, "Itemid": RawitemId, "RawItemImage": file1.files, "RawImgCount": fcount
        };
        var rawitemJson1 = {
            "Id": 0, "Quotationid": 0,
            "Itemcategoryid": itemcategoryid, "ItemCategoryName": selecteditemcategory, "Itemsubcategoryid": itemsubcategoryid, "SubCategoryName": selecteditemsubcategory, "Itemdesc": itemdesc_,
            "Supplier": supplier, "SupplierId": supplierID, "Itemid": RawitemId, "RawItemImage": file1.files,"RawImgCount": fcount
        };
        RawItemArray.push(rawitemJson1);
        MyItemArra[no].MyRawItem.push(rawitemJson);
        MyRawItemsArr(no);

        var emptyFile = document.createElement('input');
        emptyFile.type = 'file';
        file1.files = emptyFile.files;
        $("#itemcatNameid_" + no).val("");
        $("#itemcatNameid_" + no).attr("data-ID", "");

        $("#itemsubcategoryNameid_" + no).attr("data-ID", "");
        $("#itemsubcategoryNameid_" + no).val("");

        $('#itemdesc_' + no).val("");
        $("#supplierNameid_" + no).val("");
        $("#supplierNameid_" + no).attr("data-ID", "");
    }
}

$(document).on("click", ".deleteitemArr", function () {
    var id = $(this).closest("tr").attr('data');
    MyItemArra.splice(id, 1);
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#MultiItemTab > tbody > tr#' + id).remove();
    GetRawItemData();


});

$(document).on("click", ".deleterawitemArry", function () {
    debugger;
    var id = $(this).closest("tr").attr('dataIndex');
    var no = $(this).closest("tr").attr('data');
    MyItemArra[no].MyRawItem.splice(id, 1);
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#rawitemtable_' + no + ' > tbody > tr#' + id).remove();
    MyRawItemsArr(no);
});

function validrawitem_chk(no) {
    var valid = true;

    var itemcategoryid = $("#itemcatNameid_" + no).attr("data-ID");
    var itemsubcategoryid = $("#itemsubcategoryNameid_" + no).attr("data-ID");


    $('#rawitemtable_' + no + ' tr').each(function () {

        if ($(this).find('.validitemcatrow').val() == itemcategoryid && $(this).find('.validitemsubrow').val() == itemsubcategoryid) {
            valid = false;
        }
    });

    return valid;
}


$("#savequotation").on("click", function () {

    debugger;
    ErrorMessage = "";
    var isvalid = true;
    if (MyItemArra.length == 0) {
        ErrorMessage += "Please Check Style Details \n";
        isvalid = false;
    }
    var custid = $('#customerid').attr("data-ID");
    var addressid = $('#Addressid').val();

    if (custid == "") {
        ErrorMessage += "Please Select Customer \n";
        isvalid = false;
    }

    if (addressid == "0" || addressid == "") {
        ErrorMessage += "Please Select Customer Address. \n";
        isvalid = false;
    }

    var Enquiryrefno = $("#Enquiryrefno").val();
    if (Enquiryrefno == "0") {
        ErrorMessage += "Please Select Enquiry No. \n";
        isvalid = false;
    }
    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {
        SaveQuotation();
    }
    return isvalid;
});

function SaveQuotation() {
    var Id = $("#Id").val();
    var QuotationNo = $("#QuotationNo").val();
    var Quotationdate = $("#Quotationdate").val();
    var customerid = $("#customerid").attr("data-ID");
    var Addressid = $("#Addressid").val();
    var Enquiryrefno = $("#Enquiryrefno option:selected").text();
    var EnquiryID = $("#Enquiryrefno").val();
    var Sampling = false;
    if ($("#sampleyes").prop("checked") == true) {
        Sampling = true;
    }
    var Tentivedate = $("#Tentivedate").val();
    var narration = $("#narration").val();
    var Approvalstatus = $("#Approvalstatus").val();
    if (parseInt(Approvalstatus) == 1) {
        Approvalstatus = true;
    }
    else if (parseInt(Approvalstatus) == 0) {
        Approvalstatus = false;
    }
    var Comment = $("#Comment").val();
    var Total = $("#Total").val();
    var hidIsReadyMate = $("#hidIsReadyMate").val();
    var GrandTotal = $("#GrandTotal").val();

    var OtherChare = [];
    var $OC_dataRows = $("#OtherChargeDetails > tbody > tr");
    var OClenght = $OC_dataRows.length;
    for (var k = 0; k < OClenght; k++) {
        var otherchargeid = $("#otherchargeid" + k).val();
        var OC_Amount = $("#OC_Amount" + k).val();
        var OC_GSTid = $("#OC_GSTid" + k).val();
        var OC_GSTAmt = $("#OC_GSTAmt" + k).val();
        var OC_total = $("#OC_total" + k).val();
        var ocTid = $("#ocTid").val();
        var jsonaOC = { "Id": ocTid, "TXID": 0, "OtherChargeId": otherchargeid, "OtherChargeValue": OC_Amount, "GST": OC_GSTid };
        OtherChare.push(jsonaOC);
    }

    var $data = {
        "Id": Id, "Enquiryrefno": Enquiryrefno, "EnquiryID": EnquiryID, "QuotationNo": QuotationNo, "Quotationdate": Quotationdate, "customerid": customerid, "narration": narration, "Approvalstatus": Approvalstatus,
        "Comment": Comment, "Costprice": 0, "SalePrice": 0, 'Tax': 0, 'Tentivedate': Tentivedate, "Sampling": Sampling, "Total": Total, "Addressid": Addressid,
        'IsReadmade': hidIsReadyMate, "GrandTotal": GrandTotal, "StyleEnt": $ItemArra, "POOtherChargeEntity": OtherChare
    }


    debugger;
    var formData = new FormData();
    var totalFiles = MyItemArra.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.querySelector('#Style_Img_' + i);
        for (var k = 0; k < file.files.length; k++) {
            formData.append("Style_Img", file.files[k]);
        }

        var RawItms = MyItemArra[i].MyRawItem;
        for (var j = 0; j < RawItms.length; j++) {
            var file2 = RawItms[j].RawItemImage;
            for (var m = 0; m < file2.length; m++) {
                formData.append("RawItemImg", file2[m]);
            }
        }

    }

    formData.append("data", JSON.stringify($data));
    formData.append("RawItemData", JSON.stringify(RawItemArray));


    debugger;
    $.ajax({
        url: '/Quotation/AddUpdate/',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            if (data != undefined) {
                swal({
                    title: 'Success',
                    text: 'Quotation Submit Successfully.',
                    type: 'success',
                    confirmButtonText: "OK"
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = '/Quotation/Index';
                        }
                    });
            }
        }
    });
}

//////////Edit Page Code Start///////////////

function GetQuotationStyleData() {
    var Id = $("#Id").val();
    if (Id != "") {
        if (parseInt(Id) > 0) {
            $.ajax({
                type: "POST",
                url: '/Quotation/GetQuoStyleData/',
                data: "{'QuoId':" + Id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $.each(result, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var AttributeValId = dat.Attribute_Value_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td>';
                            str = str + '<td><input type="text" class="form-control" id="M_Rate_' + Mlen + '" name="M_Rate" value="' + dat.rate + '"  readonly /></td>';
                            str = str + '<td><input type="text" class="form-control" value="' + dat.SellRate+'"  id="M_SellRate_' + Mlen + '" name="M_SellRate" onkeypress="return isNumber(event);" onkeyup="GetTotQty(this.id)" /></td>';
                            str = str + '<td><select class="form-control select2" style="width: 115px;" id="M_GSTCharge_' + Mlen + '" name="M_GSTCharge" onchange="GetTotQty(this.id)"></select></td>';
                            str = str + '<td><input type="text" class="form-control" id="M_Tax_' + Mlen + '" name="M_Tax" value="0"  readonly /></td>';
                            str = str + '<td><input type="text" class="form-control" value="' + dat.Subtotal+'"   id="M_total_' + Mlen + '" name="M_total" onkeypress="return isNumber(event);" readonly /></td>';
                            if (file1.files.length > 0) {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            else {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                            str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                            $("#MultiItemTab > tbody").append(str);
                            debugger;
                            MyRawItemArray = [];
                            var myListJosn = {
                                "Id": dat.Id, "Quotationid": dat.Quotationid, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": dat.SellRate, "GST": dat.GST, "Subtotal": dat.Subtotal,
                                "MyRawItem": MyRawItemArray
                            };

                            var $myListJosn2 = {
                                "Id": dat.Id, "Quotationid": dat.Quotationid, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": dat.SellRate, "GST": dat.GST, "Subtotal": dat.Subtotal
                            };

                            var file2 = document.querySelector('#Style_Img_' + Mlen);
                            file2.files = file1.files;
                            var emptyFile = document.createElement('input');
                            emptyFile.type = 'file';
                            file1.files = emptyFile.files;

                            MyItemArra.push(myListJosn);
                            $ItemArra.push($myListJosn2);
                            GetRawItemData();
                            GetQuoStyleRawItemData(dat.Id, Mlen);

                            //Get Tax List
                            $.ajax({
                                type: "POST",
                                url: '/quotation/getGSTTaxlist/',
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (gstsdata) {
                                    var result_ = JSON.parse(gstsdata);
                                    $('#M_GSTCharge_' + Mlen).empty();
                                    $('#M_GSTCharge_' + Mlen).append('<option value="select">Select</option>');
                                    $.each(result_, function (i, item) {
                                        $('#M_GSTCharge_' + Mlen).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                                    });
                                    $('#M_GSTCharge_' + Mlen).val(dat.GST);
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}


function GetQuoStyleRawItemData(styleid, no) {
    var Id = $("#Id").val();
    if (Id != "") {
        if (parseInt(Id) > 0) {
            $.ajax({
                type: "POST",
                url: '/Quotation/GetQuoStyleRawItemData/',
                data: "{'QuoId':" + Id + ",'styleid':" + styleid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        var len1 = 0;
                        $('#rawitemtable_' + no + " > tbody").empty();
                        $.each(result, function (i, data) {
                            debugger;
                            len1 = len1 + 1;
                            rawdiv = '<tr class="data-contact-person odd" id="' + no + '_' + len1 + '" value="' + no + '_' + len1 + '" data="' + no + '" dataIndex="' + i + '">' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemcategoryid + '" class="form-control validitemcatrow"><p class="">' + data.ItemCategoryName + '</p></td>' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + data.SubCategoryName + '</p></td>' +
                                '<td style="padding-right:10px;"><input type="hidden" value="' + data.Itemdesc + '" class="form-control "><p class="">' + data.Itemdesc + '</p></td>' +
                                '<td style="padding-right:10px;"><p class="">' + data.Supplier + '</p></td><td style="padding-right:10px;">';
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Upload Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            //if (parseInt(data.RawItemImg.length) > 0) {
                            //    rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Preview Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            //}
                            //else {
                            //    rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="Upload Image"  onclick=ImageUpload(this.id,"preview","Raw","RawItemImg_' + no + '_' + len1 + '") />';
                            //}
                            rawdiv = rawdiv + '<input type="file" style="display:none;" id="RawItemImg_' + no + '_' + len1 + '" name="RawItemImg" class="form-control" multiple /></td>';
                            rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitemArry btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
                                '</tr>';
                            $('#rawitemtable_' + no + " > tbody").append(rawdiv);


                            if (parseInt(data.RawItemImg.length) > 0) {
                                var file2 = document.querySelector('#RawItemImg_' + no + '_' + len1);
                                file2.files = data.RawItemImage;
                            }

                            var rawitemJson = {
                                "Id": 0, "Quotationid": 0,
                                "Itemcategoryid": data.Itemcategoryid, "ItemCategoryName": data.ItemCategoryName, "Itemsubcategoryid": data.Itemsubcategoryid, "SubCategoryName": data.SubCategoryName, "Itemdesc": data.Itemdesc,
                                "Supplier": data.Supplier, "RawItemImage": data.RawItemImg.length, "RawImgCount": data.RawItemImg.length
                            };
                            var rawitemJson1 = {
                                "Id": 0, "Quotationid": 0,
                                "Itemcategoryid": data.Itemcategoryid, "ItemCategoryName": data.ItemCategoryName, "Itemsubcategoryid": data.Itemsubcategoryid, "SubCategoryName": data.SubCategoryName, "Itemdesc": data.Itemdesc,
                                "Supplier": data.Supplier, "RawItemImage": data.RawItemImg.length, "RawImgCount": data.RawItemImg.length
                            };
                            RawItemArray.push(rawitemJson1);
                            MyItemArra[no].MyRawItem.push(rawitemJson);



                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}


function othercharge1() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var sizerowlenght = $dataRows.length;

    if (sizerowlenght > 0) {

        $("#OtherChargeDetails").show();
        $("#OC_GrandTotal_id").show();
        //Get Tax List
        $.ajax({
            type: "POST",
            url: '/quotation/getGSTTaxlist/',
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (gstsdata) {
                var result_ = JSON.parse(gstsdata);
                $('.OC_GST').empty();
                $(".OC_GST").append('<option value="select">Select</option>');
                $.each(result_, function (i, item) {
                    $(".OC_GST").append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                });
                $dataRows.each(function (i) {
                    var gstval = $("#hidgst" + i).val();
                    gstval = parseInt(gstval);
                    $("#OC_GSTid" + i).attr("style", "width:115px;");
                    $("#OC_GSTid" + i).val(gstval);
                    $("#OC_GSTid" + i).select2().trigger('change');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });


    }
    else {
        $("#OtherChargeDetails").hide();
        $("#OC_GrandTotal_id").hide();
    }

}

////////Edit Page Code End //////////////////


/////Index Page Code Start////////////////////////

function deletequotation(id) {
    var quotid = id;
    var param = { 'id': quotid };

    swal({
        title: 'Confirmation',
        text: 'Are You sure delete quotation?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/Quotation/Delete/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result == true) {
                            swal({
                                title: 'Confirmation',
                                text: 'Record Delete Successfully!',
                                showConfirmButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true,
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#8CD4F5',
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        var path = window.location.pathname;
                                        window.location = path;
                                    }
                                }
                            )

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("warning", "Something Missing...", "error");
            }
        });
}

function Canclequo(id) {
    var quoid = id;
    var param = { 'id': quoid };

    swal({
        title: 'Confirmation',
        text: 'Are You Sure Rejected This Quotation?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/Quotation/RejectedQuo/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {

                        if (result == true) {
                            swal({
                                title: 'Confirmation',
                                text: 'Record Rejected Successfully!',
                                showConfirmButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true,
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#8CD4F5',
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                }
                            )

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("warning", "Something Missing...", "error");
            }
        });


}

function GetData(Status, Name) {

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/Quotation/GetStatusData/", { 'IsStatus': Status });

}

function loadingHTML() {
    return $("#divLoadingContent").html();
}


function setDateformat(Dt) {

    var dt1 = Dt.substring(6, Dt.length);
    dt1 = dt1.substring(0, dt1.length - 2);
    var today = new Date(parseInt(dt1));
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    // var today2 = today.setMonth(mm + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = mm + '/' + dd + '/' + yyyy;

    return today;
}

function FilterDataShow() {
    debugger;
    var formDate = $("#fromdate").val();
    var toDate = $("#todate").val();
    var Name = $("#Tablist > .active").attr('id');
    var Status = $("#" + Name).attr("datavalue")

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/Quotation/GetFilterStatusData/", { 'fromdate': formDate, 'todate': toDate, 'IsStatus': Status });

}

function GetQuoProduct(QuoId) {
    if (QuoId != "") {
        if (parseInt(QuoId) > 0) {
            $("#QuoProductmodel").modal("show");
            $.ajax({
                type: "POST",
                url: '/Quotation/GetQuoStyleData/',
                data: "{'QuoId':" + QuoId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $("#EnqProductTab > tbody").empty();
                        $.each(result, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td></tr>';
                            $("#EnqProductTab > tbody").append(str);

                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}

////Index Page Code End/////////////////////////////////////