﻿//Code to validate data before adding into grid- ramesh take
function validateSchedule() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;

    var totalqty = $('#reqqty').val();
    var deliveryDate = $("#Delivery_Schedule_Date").val();
    var deliveryQty = $('#Delivery_Qty').val();
    var item_id = $('#Item_ID').val();
    var item_name = $("#Item_ID option:selected").text();
    if (item_id == "0" || item_id == undefined) {
        msg = msg + "* Please select delivery item. \n";
        blnFlag = false;
    }
    if (totalqty == "" || totalqty == undefined) {
        msg = msg + "* Product required quantity should not be null. \n";
        blnFlag = false;
    }
    if (deliveryDate == "" || deliveryDate == undefined) {
        msg = msg + "* Please select delivery date. \n";
        blnFlag = false;
    }
    if (deliveryQty == "" || deliveryQty == undefined) {
        msg = msg + "* Please enter delivery quantiy. \n";
        blnFlag = false;
    }

    var $rows = $('#DeliveryScheduleDetails').find('tbody tr');
    var delQty_grid = 0;
    if ($rows.length > 0) {
        $.each($rows, function (key, row) {
            var delDate = $(row).find('[name=txtDelSchDate]').val();
            var delQty = $(row).find('[name=txtDelQty]').val();
            var delItem = $(row).find('[name=txtDeliveryItem]').val();
            if (item_name == delItem && deliveryDate == delDate && deliveryQty == delQty) {
                msg = msg + "* Delivery Item, Date and Quantiy already exists in the grid. \n";
                blnFlag = false;
            }
            delQty_grid = parseFloat(delQty_grid) + parseFloat(delQty);
            //delQty_grid = delQty_grid + delQty;
        });
    }
    if (delQty_grid > 0 && deliveryQty > 0) {
        //delQty_grid = delQty_grid + deliveryQty;
        delQty_grid = parseFloat(delQty_grid) + parseFloat(deliveryQty);
        //delQty_grid = parseFloat(delQty_grid).toFixed(2);
        //totalqty = parseFloat(totalqty).toFixed(2);
        if (delQty_grid > totalqty) {
            msg = msg + "* Total delivery quantity should not be greater than sales order quantity. \n";
            blnFlag = false;
        }
    }

    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "warning");
        return false;
    }
    return true;
}
//Code to add delivery schedule in grid
$("#btnAddDelivery").on("click", function () {

    if (validateSchedule() == true) {
        var rowCnt = 0;
        var item_id = $('#Item_ID').val();
        var item_name = $("#Item_ID option:selected").text();
        var AttribId = $("#Item_ID option:selected").attr('dataAttr');
        var deliveryDate = $("#Delivery_Schedule_Date").val();
        var deliveryQty = $('#Delivery_Qty').val();
        var deliveryCom = $('#Delivery_Comment').val();
        if (deliveryDate != "" || deliveryQty != "") {
            var $rows_ = $('#DeliveryScheduleDetails').find('tbody tr');
            if ($rows_.length > 0) {
                rowCnt = $rows_.length;
                rowCnt = rowCnt + 1;
            }
            else {
                rowCnt = rowCnt + 1;
            }
            var tblrow = '<tr id="rowid' + rowCnt + '">' +
                '<td class="DeliveryItem"><input id="txtDeliveryItem' + rowCnt + '" type="text" value="' + item_name + '" name="txtDeliveryItem" class="form-control" autocomplete="off" style="display:none" readonly>' + item_name+'</td>' +
                '<td class="DeliveryDate"><input id="txtDelSchDate' + rowCnt + '" type="text" value="' + deliveryDate + '" onfocus="applyDatepicker(' + rowCnt + ');" name="txtDelSchDate" class="form-control delrow" autocomplete="off"></td>' +
                '<td class="DeliverQty"><input id="txtDelQty' + rowCnt + '" value="' + deliveryQty + '" type="text" name="txtDelQty" onchange="validateQty(this.id);" onkeypress="return isNumberKey(event,this.id)" class="form-control text-right delrow" autocomplete="off" style="display:none;" readonly>' + deliveryQty+'</td>' +
                '<td class="DeliverComment"><textarea class="form-control delrow" id="txtDelivery_Comments' + rowCnt + '" name="txtDelivery_Comments"></textarea></td>' +
                '<td class="action text-center" style="width: 100px"><input type="hidden" id="PO_DelSchedule_ID' + rowCnt + '" class="form-control delrow" name="PO_DelSchedule_ID" value="0"/><input type="hidden" id="Item_ID' + rowCnt + '" class="form-control delrow" name="Item_ID" value="' + item_id + '"/><a id="' + rowCnt + '" class="fa fa-fw fa-remove" onclick="removeRow(' + rowCnt + ');"></a></td>' +
                '<td style="display:none;"></td><td style="display:none;"><input type="hidden" id="AttribId' + rowCnt + '" class="form-control delrow" name="AttribId" value="' + AttribId + '"/></td>' +
                '</tr>';
            if (rowCnt == 1) {
                $('#DeliveryScheduleDetails').append(tblrow);
            } else {
                $('#DeliveryScheduleDetails > tbody > tr:first').before(tblrow);
            }
            $("textarea#txtDelivery_Comments" + rowCnt).html(deliveryCom);
        }
        $("#Delivery_Schedule_Date").val('');
        $('#Delivery_Qty').val('');
        $('#Delivery_Comment').val('');
        $("#Delivery_Schedule_Date").focus();
    }

});
//Accept only decimal values
function isNumberKey(evt, id) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) {
            var txt = document.getElementById(id).value;
            if (!(txt.indexOf(".") > -1)) {

                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    } catch (w) {
        alert(w);
    }
}
//Apply date picker
function applyDatepicker(id) {
    $("#txtDelSchDate" + id).datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: 0
    });
    $("#txtDelSchDateE" + id).datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: 0
    });
}
//Code to create string of delivery schedule
function createPODeliveryStr() {
    var sche_str = "";
    var $dataRows_ = $('#DeliveryScheduleDetails').find('tbody tr');
    var rowlen = $dataRows_.length;
    var scherowcount = 0;
    //var stylerowCount = $('#maintable2 tr').length;
    $dataRows_.each(function () {
        var count = 0;
        scherowcount++;
        var Aleng = $(this).children('td').length;
        $(this).find('.delrow').each(function (i) {
            debugger;
            var inpVal_ = ($(this).val());  //here get td value
            count++;

            if (inpVal_ != undefined) {

                if (count == Aleng) {
                    sche_str += inpVal_;
                }
                else {
                    sche_str += inpVal_ + "~";
                }
            }

        });
        if (scherowcount == rowlen) {

        }
        else {
            sche_str += "#";
        }
    });
    $('#po_delivery_schedules').val(sche_str);

}
//code to validate product quantity
function validateQty(id) {
    var totalqty = $('#reqqty').val();
    if (totalqty != "") {
        var $rows = $('#DeliveryScheduleDetails').find('tbody tr');
        var delQty_grid = 0;
        if ($rows.length > 0) {
            $.each($rows, function (key, row) {
                var delQty = $(row).find('[name=txtDelQty]').val();
                delQty_grid = parseFloat(delQty_grid) + parseFloat(delQty);
            });
        }
        if (delQty_grid > 0) {
            //delQty_grid = parseFloat(delQty_grid).toFixed(2);
            //totalqty = parseFloat(totalqty).toFixed(2);
            if (delQty_grid > totalqty) {
                swal("warning", "Total delivery quantity should not be greater than sales order quantity.", "warning");
                $('#' + id).val(0);
            }
        }
    }
}
//code to prevent user to enter delivery qty
function preventUser() {
    var totalqty = $('#reqqty').val();
    var deliveryQty = $('#Delivery_Qty').val();
    totalqty = parseFloat(totalqty);
    deliveryQty = parseFloat(deliveryQty);
    if (totalqty < deliveryQty) {
        swal("warning", "Total delivery quantity should not be greater than sales order quantity.", "warning");
        $('#Delivery_Qty').val('');
    }
}
//function to remove row
function removeRow(id) {
    var rowid = "rowid" + id;
    $("#DeliveryScheduleDetails").find("#" + rowid).remove();
}

//Code to Append item list to delivery Item DDL
function AppendItems() {
    var startrow = 0;
    var startrowvalue = "---Select Item---";
    $("#Item_ID").empty();
    $("#Item_ID").append('<option value="' + startrow + '">' + startrowvalue + '</option>');

    var item = $('#itemnameid').val();
    var item_id = $('#itemid').val();
    if (item != "" && item_id != "") {
        var rawsizerowcount = 0;
        var $dataRows3 = $("#AttributeTable tr:not('.attrbtab1')");
        var sizerowlenght = $dataRows3.length;
        var sizerowstr = "";

        $dataRows3.each(function () {
            debugger;
            var sizearray = [];
            //get row of sizetable
            var AName = "";
            var sizeid = 0;
            $(this).children('td').find(".hidval2").each(function (i) {
                    AName = this.value;
            });

            $(this).children('td').find(".IAsizeid").each(function (i) {
                
                    sizeid = this.value;
            });
            $("#Item_ID").append('<option  dataAttr="' + sizeid + '" value="' + item_id + '">' + item + "-" + AName + '</option>');
            
        });
        
    }

    //var $rows = $('#rawitemtable').find('tbody tr');
    //if ($rows.length > 0) {
    //    $("#Item_ID").empty();
    //    $("#Item_ID").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
    //    $.each($rows, function (key, row) {
    //        var item_id = $(row).find('[name=rawitem]').val();
    //        var item = $(row).find('[name=txtItem]').val();
    //        if (item_id != undefined && item != undefined) {
    //            if (item_id != "" && item != "") {
    //                $("#Item_ID").append('<option value="' + item_id + '">' + item + '</option>');
    //                //$("#Item_ID").select2();
    //            }
    //        }


    //    });
    //}
}