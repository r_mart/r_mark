﻿$(function () {
    //List Of Customer
    $("#custnameid").autocomplete({
        source: function (request, response) {
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid};
            myjson.push(mydata);

            var custtype =1;
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':1,'id':'" + custtype + "','screenId':"+ screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.Ledger_Name, value: item.Ledger_Name, dataid: item.Ledger_Id };

                        }));
                       
                    }
                  
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            var path = window.location.pathname;
            path = path.split('/');
            if (path[1] == "DailyProductionPlan" || path[1] == "DailyProductionEntry") {
                $("#CustomerID").val(i.item.dataid);
            }
            else {
                $("#customerid").val(i.item.dataid);
            }

            
        },
        minLength: 1
    });

    //List Of Supplier
    $("#supplierNameid").autocomplete({
        source: function (request, response) {
          
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            var custtype = 2;
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':1,'id':'" + custtype + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.Ledger_Name, value: item.Ledger_Name, dataid: item.Ledger_Id };
                            
                        }));
                        
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            
            var path = window.location.pathname;
            path = path.split('/');
            if (path[1] == "SupplierQuotation" || path[1] == "MaterialStock") {
                $("#SupplierId").val(i.item.dataid);
            }
            else if (path[1] == "SupplierPO" || path[1] == "SupplierGRN") {
                $("#supplierid").val(i.item.dataid);
            }
            else{
                $("#supplier").val(i.item.value);
            }
        },

        minLength: 1
    });




    //List Of Style Name
    $("#stylenameid1").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();	
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':2,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Stylename, value: item.Stylename, dataid: item.Id };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#styleid1").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Item Category Name
    $("#itemcatNameid").autocomplete({
        source: function (request, response) {
         
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':4,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                   
                    if (data.hasOwnProperty('Table')) {
                     
                        response($.map(data.Table, function (item) {
                            return { label: item.Itemcategory, value: item.Itemcategory, dataid: item.Id };
                           
                        }));
                       
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemcategoryid").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Item Category Name
    $("#itemcat_Nameid").autocomplete({
        source: function (request, response) {
            if (request.term == "") {
                $("#itemcatlist").hide();
            }
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            var Groupid = $("#Itemgroupid").val();

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':14,'id':'" + Groupid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.Itemcategory, value: item.Itemcategory, dataid: item.Id };

                        }));
                       
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {

            $("#itemcategoryid").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Sub Category List
    $("#itemsubcategoryNameid").autocomplete({
        source: function (request, response) {
          
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            var itemcatid = $("#itemcategoryid").val();
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':5,'id':'" + itemcatid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                  
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.Itemsubcategory, value: item.Itemsubcategory, dataid: item.Id };
                            
                        }));
                       
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemsubcategory").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Sub Category List In BOM
    $("#itemsubcategoryidNameid").autocomplete({
        source: function (request, response) {
           
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':6,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Itemsubcategory, value: item.Itemsubcategory, dataid: item.Id };
                            
                        }));
                        
                    }
                    
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemsubcategoryid").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Raw Item Name In BOM
    $("#rawitemnameid").autocomplete({
        source: function (request, response) {
           
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            var itemsubcatid = $("#itemsubcategoryid").val();
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':7,'id':'" + itemsubcatid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    
                    if (data.hasOwnProperty('Table')) {
                      
                        response($.map(data.Table, function (item) {
                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID };
                            
                        }));
                       
                    }
                   
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemrawid").val(i.item.dataid);
            $("#itemrawAid").val(i.item.dataIA); 
        },
        minLength: 1
    });

    
    //List Of Department
    $("#DepartmentnameId").autocomplete({
        source: function (request, response) {
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':8,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.Departmentname, value: item.Departmentname, dataid: item.Id };
                            
                        }));
                        
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            var path = window.location.pathname;
            path = path.split('/');
            if (path[1] == "SupplierGRN" || path[1] == "LedgerAccount") {
                $("#departmentid").val(i.item.dataid);
            }
            else {
                $("#DepartmentId").val(i.item.dataid);
            }

            
        },
        minLength: 1
    });

    //List Of Account Head
    $("#AccountHeadnameId").autocomplete({
        source: function (request, response) {
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':9,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.Name, value: item.Name, dataid: item.ID };
                        }));
             
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            var path = window.location.pathname;
            path = path.split('/');
            if (path[1] == "MaterialIssue") {
                $("#EmpID").val(i.item.dataid);
            }
            else {
                $("#AccountHeadId").val(i.item.dataid);
            }


            
        },
        minLength: 1
    });

    //List Of Account Head
    $("#senderby").autocomplete({
        source: function (request, response) {
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':9,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.Name, value: item.Name, dataid: item.ID };
                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#senderbyID").val(i.item.dataid);
        },
        minLength: 1
    });


    //List Of Warehouse
    $("#warehousenameid").autocomplete({
        source: function (request, response) {
          
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':10,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                   
                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.SHORT_NAME, value: item.SHORT_NAME, dataid: item.Id };
                            
                        }));
                   
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            var path = window.location.pathname;
            path = path.split('/');
            if (path[1] == "SupplierGRN") {
                $("#warehouseid").val(i.item.dataid);
            }
            else if (path[1] == "MaterialIssue") {
                $("#WarehouseID").val(i.item.dataid);
            }
            else if (path[1] == "Transfer") {
                $("#WarehouseId").val(i.item.dataid);
            }
            else {
                $("#DeliveryLoaction").val(i.item.dataid);
            }

        },
        minLength: 1
    });

    //List Of Warehouse
    $("#TFromWarehouse").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':10,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.SHORT_NAME, value: item.SHORT_NAME, dataid: item.Id };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#TFromWarehouseID").val(i.item.dataid);
        },
        minLength: 1
    });


    //List Of Warehouse
    $("#TTOWarehouse").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':10,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.SHORT_NAME, value: item.SHORT_NAME, dataid: item.Id };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#TTOWarehouseID").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Work Center
    $("#workCenternameID").autocomplete({
        source: function (request, response) {
           
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':11,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                    
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.WorkCenterName, value: item.WorkCenterName, dataid: item.Id };
                        }));
                       
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#workCenterID").val(i.item.dataid);

        },
        minLength: 1
    });

    //List Of Batch No
    $("#BatchNo").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':16,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Dynamic_Controls_value, value: item.Dynamic_Controls_value, dataid: item.ItemInward_ID };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#batchnoid").val(i.item.dataid);
        },
        minLength: 1
    });

    //List Of Lot No.
    $("#LotNo").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':17,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Dynamic_Controls_value, value: item.Dynamic_Controls_value, dataid: item.ItemInward_ID };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#LotNoId").val(i.item.dataid);
        },
        minLength: 1
    });


    $("#FGItemNameId").autocomplete({
        source: function (request, response) {
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            var itemsubcatid = null;
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':18,'id':'" + itemsubcatid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID, dataisc: item.Itemsubcategoryid };
                        }));
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#FGItemId").val(i.item.dataid);
            $("#hidFGitemcatid").val(i.item.dataisc);
            $("#FGrawIAid").val(i.item.dataIA);
        },
        minLength: 1
    });

    //List Of Item Name and Semi-finished In BOM
    $("#itemnameid_SF").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':19,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {

                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id };
                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemid").val(i.item.dataid);
        },
        minLength: 1
    });


    //List Of All Item Name
    $("#Inv_itemnameid").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var Groupid = $("#Itemgroupid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':20,'id':'" + Groupid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {

                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID,};
                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#Invitemid").val(i.item.dataid);
            $("#InvitemAttrid").val(i.item.dataIA);
        },
        minLength: 1
    });

    //List Of Customer, Supplier, Operator Name in Return Stock Screen
    $("#BSOnameId").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var Groupid = $("#typeid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':21,'id':'" + Groupid + "','screenId':" + screenId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {

                            return { label: item.name, value: item.name, dataid: item.id };
                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#BSOid").val(i.item.dataid);
        },
        minLength: 1
    });
});

//List Of Raw Item Name in BOM 2
function Getrawitemlist(no) {
    //List Of Raw Item Name In BOM
    $("#rawitemnameid" + no).autocomplete({
        source: function (request, response) {
          
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            var itemsubcatid = $("#hiddenitemcategoryid" + no).val();
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':7,'id':'" + itemsubcatid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    
                    if (data.hasOwnProperty('Table')) {
                      
                        response($.map(data.Table, function (item) {
                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID };
                        }));
                        
                    }
                    
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        }, select: function (e, i) {
            $("#rawitemid" + no).val(i.item.dataid);
            $("#rawIAid" + no).val(i.item.dataIA);
        },
        minLength: 1
    });
}


//List Of Raw Item Name in Material Issue
function GetOutrawitemlist(no) {
   
    //List Of Raw Item Name In BOM
    $("#rawoutitemnameid" + no).autocomplete({
        source: function (request, response) {
            debugger;
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':12,'id':'','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID, dataisc: item.Itemsubcategoryid };
                           
                        }));
                        
                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            debugger;
            $("#OutItem" + no).val(i.item.dataid);
            $("#outitemcategoryid" + no).val(i.item.dataisc);
            $("#rawIAoutid" + no).val(i.item.dataIA);
        },
        minLength: 1
    });
}


//List Of Raw Item Name in Material Issue
function GetInrawitemlist() {
    //List Of Raw Item Name In BOM
    $("#MIrawitemnameid").autocomplete({
        source: function (request, response) {
            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            var Woid = $("#wono").val();
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':13,'id':'" + Woid + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                       
                        response($.map(data.Table, function (item) {
                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID, dataisc: item.Itemsubcategoryid };
                            
                        }));
                        
                    }

                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#InItem").val(i.item.dataid);
            $("#hiddenitemcategoryid").val(i.item.dataisc);
            $("#rawIAid").val(i.item.dataIA);
        },
        minLength: 1
    });
}


//List Of Attribute Name in Item Master
function GetAttrList(name,id) {
    debugger;
    var namid = name.id;
    $("#" + namid).autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':15,'id':'" + id + "','screenId':" + screenId +"}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Attribute_Value, value: item.Attribute_Value, dataid: item.Attribute_Value_ID, datavalue: item.Attribute_Name };
                        }));

                    }

                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#" + namid).attr('datavalue', i.item.datavalue);
            $("#Hid" + namid).val(i.item.dataid);
            $("#Hv" + namid).val(i.item.value);
            
        },
        minLength: 1
    });
}

$(document).mouseup(function (e) {

    if ($(e.target).closest(".searchlist").length=== 0) {
        $(".searchlist").hide();
    } 
    
});


function GetCategoryList(no) {
    $("#itemcatNameid_" + no).autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':4,'id':'','screenId':" + screenId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Itemcategory, value: item.Itemcategory, dataid: item.Id };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemcatNameid_" + no).attr("data-ID",i.item.dataid);
        },
        minLength: 1
    });
}

function GetSubCategoryList(no) {
    $("#itemsubcategoryNameid_" + no).autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            var itemcatid = $("#itemcatNameid_" + no).attr("data-ID");
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':5,'id':'" + itemcatid + "','screenId':" + screenId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.Itemsubcategory, value: item.Itemsubcategory, dataid: item.Id };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#itemsubcategoryNameid_" + no).attr("data-ID", i.item.dataid);
        },
        minLength: 1
    });
}


function GetSupplierList(no) {
    $("#supplierNameid_" + no).autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var screenId = $("#screenId").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);

            var custtype = 2;
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':1,'id':'" + custtype + "','screenId':" + screenId + "}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);
                    if (data.hasOwnProperty('Table')) {
                        response($.map(data.Table, function (item) {
                            return { label: item.Ledger_Name, value: item.Ledger_Name, dataid: item.Ledger_Id };

                        }));

                    }
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#supplierNameid_" + no).attr("data-ID", i.item.dataid);
        },

        minLength: 1
    });
}

//List Of Customer
$("#customerid").autocomplete({
    source: function (request, response) {
        var myjson = [];
        var companyid = $("#companyid").val();
        var branchid = $("#branchid").val();
        var screenId = $("#screenId").val();
        var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
        myjson.push(mydata);

        var custtype = 1;
        $.ajax({
            url: '/CustomerEnquiry/SearchList/',
            data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':1,'id':'" + custtype + "','screenId':" + screenId + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                data = JSON.parse(data);

                if (data.hasOwnProperty('Table')) {
                    response($.map(data.Table, function (item) {
                        return { label: item.Ledger_Name, value: item.Ledger_Name, dataid: item.Ledger_Id };

                    }));
                }
            },
            error: function (response) {
                //alert(response.responseText);
            },
            failure: function (response) {
                //alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        $("#customerid").attr("data-ID", i.item.dataid);
    },
    minLength: 1
});

//List Of Broker
$("#BrokerId").autocomplete({
    source: function (request, response) {
        var myjson = [];
        var companyid = $("#companyid").val();
        var branchid = $("#branchid").val();
        var screenId = $("#screenId").val();
        var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
        myjson.push(mydata);

        var custtype = 3;
        $.ajax({
            url: '/CustomerEnquiry/SearchList/',
            data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':1,'id':'" + custtype + "','screenId':" + screenId + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                data = JSON.parse(data);
                if (data.hasOwnProperty('Table')) {
                    response($.map(data.Table, function (item) {
                        return { label: item.Ledger_Name, value: item.Ledger_Name, dataid: item.Ledger_Id };

                    }));
                }
            },
            error: function (response) {
                //alert(response.responseText);
            },
            failure: function (response) {
                //alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        $("#BrokerId").attr("data-ID",i.item.dataid);
    },
    minLength: 1
});


//List Of Item Name Finished Good Item and readymade itemFinished Good Item and readymade item
$("#Itemid").autocomplete({
    source: function (request, response) {

        var myjson = [];
        var companyid = $("#companyid").val();
        var branchid = $("#branchid").val();
        var screenId = $("#screenId").val();
        var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
        myjson.push(mydata);

        $.ajax({
            url: '/CustomerEnquiry/SearchList/',
            data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':18,'id':'','screenId':" + screenId + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                data = JSON.parse(data);

                if (data.hasOwnProperty('Table')) {

                    response($.map(data.Table, function (item) {

                        return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataAid: item.Item_Attribute_ID, datagroup: item.Itemgroup };
                    }));

                }
            },
            error: function (response) {
                //alert(response.responseText);
            },
            failure: function (response) {
                //alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        $("#Itemid").attr("data-ID", i.item.dataid);
        $("#Itemid").attr("data-AID", i.item.dataAid);
        var n = i.item.datagroup.search("Ready");
        if (parseInt(n) >= 0) {
            $("#hidIsReadyMate").val(1);
        }
        else {
            $("#hidIsReadyMate").val(0);
        }
    },
    minLength: 1
});



//List Of Style Name
$("#Styleid").autocomplete({
    source: function (request, response) {

        var myjson = [];
        var companyid = $("#companyid").val();
        var branchid = $("#branchid").val();
        var screenId = $("#screenId").val();
        var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
        myjson.push(mydata);

        $.ajax({
            url: '/CustomerEnquiry/SearchList/',
            data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':2,'id':'','screenId':" + screenId + "}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                data = JSON.parse(data);

                if (data.hasOwnProperty('Table')) {

                    response($.map(data.Table, function (item) {
                        return { label: item.Stylename, value: item.Stylename, dataid: item.Id };

                    }));

                }
            },
            error: function (response) {
                //alert(response.responseText);
            },
            failure: function (response) {
                //alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        $("#Styleid").attr("data-ID",i.item.dataid);
    },
    minLength: 1
});

