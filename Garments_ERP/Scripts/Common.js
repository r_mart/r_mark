﻿$(document).ready(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});

$(document).ready(function () {

    var alrtmsg = $("#altmsg").val() == undefined ? "" : $("#altmsg").val();
    if (alrtmsg != "" ) {
        swal({
            title: 'Success',
            text: alrtmsg,
            type: "success",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        $("#altmsg").val("");
    }
   
});


function tabclick() {
    var header = document.getElementById("tab_ulid");
    var btns = header.getElementsByClassName("btnLi");
    for (var i = 0; i < btns.length; i++) {

        btns[i].addEventListener("click", function () {

            $(event.target).find('a').click();

            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
            var header = document.getElementById("tabs");
            var btns = header.getElementsByClassName("sectab");
            for (var i = 0; i < btns.length; i++) {

                var styleid = $("#" + btns[i].id).attr("style");
                styleid = styleid.split(':');
                if (styleid[1].trim() == "inline;") {
                    $("#" + btns[i].id).attr("style", "display: block;");
                }
            }



        });
    }
}



function RefreshPage() {
    var path = window.location.pathname;
    window.location = path;
}

function isNumber(evt) {
    var indno = evt.key == "." ? -1 : 0;
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode != 46 || indno != -1) && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}



function isString(e) {
    var regex = new RegExp("^[a-zA-Z\\-\\s]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    else {
        e.preventDefault();
        return false;
    }
}


//All Common Code Image Popup
function GetImageUpload(id, name,type) {
    debugger;
    $("#img_preview").html('');
    $("#typeid").val(type);

    if (name == "preview") {
        $("#uploadid").hide();
    }
    else {
        $("#uploadid").show();
    }

    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    $("#noid").val(no);
    $("#nameid").val(name);
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat[2] == "Edit") {
        $('#UploadPreviewModal').modal('toggle');
        if (type == "Style") {
            LoadImage("styleimg", "styleimage_Id", no);
        }
        if (type == "Item") {
            LoadImage("ItemAttrimages", "Attrimages", no);
        }
        if (type == "Raw") {
            LoadImage("rawimg", "", no);
        }
        if (type == "RawItem") {
            LoadImage("rawitemimg", "rawitemimg_Id", no);
        }
    }
    else {
        $('#UploadPreviewModal').modal('toggle');
        if (type == "Style") {
            LoadImage("styleimg", "styleimage_Id", no);
        }
        if (type == "Item") {
            LoadImage("ItemAttrimages", "Attrimages", no);
        }
        if (type == "Raw") {
            LoadImage("rawimg", "", no);
        }
        if (type == "RawItem") {
            LoadImage("rawitemimg", "rawitemimg_Id", no);
        }
    }
    $('#UploadPreviewModal').attr('style', 'z-index: 10001;');
    $(".modal-backdrop").attr('style', 'z-index: 10000;');
}

function LoadImage(id,pathid,no) {
    debugger;
    $("#img_preview").html("");

    var strd1 = '<div id="background"></div>';
    $("#img_preview").append(strd1);
    var file1 = document.querySelector('#'+id + no);
    var file2 = document.querySelector('#ItemAttrimages');
    file2.files = file1.files;

    var filesLength = file2.files.length;
    var wid = parseFloat(filesLength) * 80;
    var Rid = $("#img_preview").find("input:radio").attr("id");
    var m = 1;
    if (Rid == undefined) {
        m = 0;
    }

    if (pathid != "") {
        var path = $("#" + pathid + no).val();
        if (path != "" && path != undefined && filesLength == 0) {

            $.each(path.split(','), function (k, d) {
                var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + d + "' style='height:100%;width:100%' /> </label><img src='" + d + "' class='DefaultBigStyle1'>";
                $("#img_preview").append(imgstr);
                $('#id0').trigger('click');
            });
        }
    }
    for (var i = 0; i < filesLength; i++) {
        var f = file2.files[i];
        var fileReader = new FileReader();
        var k = 0;
        fileReader.onload = (function (e) {
            debugger;
            k = m + k;
            var file = e.target;
            var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /> </label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
            $("#img_preview").append(imgstr);
            $('#id0').trigger('click');
            k++;
        });

        fileReader.readAsDataURL(f);
    }
}


$("#ItemAttrimages").on("change", function (e) {
    debugger;
    $("#img_preview").html('');
    var strd = '<div id="background"></div>';
    $("#img_preview").append(strd);
    var files = e.target.files,
        filesLength = files.length;
    var wid = parseFloat(filesLength) * 80;

    for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        var k = 0;
        fileReader.onload = (function (e) {
            var file = e.target;
            var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /></label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
            $("#img_preview").append(imgstr);
            $('#id0').trigger('click');
            k++;
        });

        fileReader.readAsDataURL(f);
    }
    
});

function Savehide() {
    debugger;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    var file1 = document.querySelector('#ItemAttrimages');
    var emptyFile = document.createElement('input');
    emptyFile.type = 'file';

    var no = $("#noid").val();
    var name = $("#nameid").val();
    var typeid = $("#typeid").val();
    if (parseInt(no) > 0) {
        if (typeid == "Item") {
            var file2 = document.querySelector('#ItemAttrimages' + no);
        }
        if (typeid == "RawItem") {
            file2 = document.querySelector('#rawitemimg' + no);
        }
        var filesLength = file1.files.length;
        if (parseInt(filesLength) > 0) {
            file2.files = file1.files;
            $("#img_preview").html("");
        }
        file1.files = emptyFile.files;
        if (typeid == "Item") {
            $('#IAImg_count' + no).val(filesLength);
        }
        if (typeid == "RawItem") {
            $('#rawitemcount' + no).val(filesLength);
        }

        $('.modal').modal('hide');
    }
    else {
        if (typeid == "Style") {
            file2 = document.querySelector('#styleimg');
        }
        if (typeid == "Item") {
            file2 = document.querySelector('#ItemAttrimages' + no);
        }
        if (typeid == "Raw") {
            file2 = document.querySelector('#rawimg' + no);
        }
        if (typeid == "RawItem") {
            file2 = document.querySelector('#rawitemimg' + no);
        }
        
        filesLength = file1.files.length;
        if (parseInt(filesLength) > 0) {
            file2.files = file1.files;
            $("#img_preview").html("");
        }
        file1.files = emptyFile.files;
        if (typeid == "Item") {
            $('#IAImg_count' + no).val(filesLength);
        }
        if (typeid == "RawItem") {
            $('#rawitemcount' + no).val(filesLength);
        }
        $('.modal').modal('hide');
    }

}


function hidepop() {
    $('.modal').modal('hide');
}

