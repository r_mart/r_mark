﻿//using MyGSTSolution.Web.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;


namespace Garments_ERP.Helper
{
    public class Email
    {
        string UserName = ConfigurationManager.AppSettings["UserMailId"];
        string PassWord = ConfigurationManager.AppSettings["Password"];
        int smtp_port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp_Port"]);
        string smpt_Host = ConfigurationManager.AppSettings["smtp_Host"];
        bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["ssl"]);


        public bool SendMail(string MailBody, string MailId)
        {
            bool Result = false;
            try
            {
                using (MailMessage message = new MailMessage())
                {

                    message.To.Add(MailId);

                    message.From = new MailAddress(UserName);
                    message.Body = MailBody;
                    message.Subject = "GarmentERP OTP/Password";
                    message.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = smpt_Host; //Or Your SMTP Server Address
                    smtp.Port = smtp_port;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential(UserName, PassWord);
                    smtp.EnableSsl = ssl;

                    smtp.Send(message);
                    Result = true;

                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
            return Result;
        }
    }
}