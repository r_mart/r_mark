  [RBAC]
        public ActionResult BOM_PO_Edit(long id, long bomid)
        {
             M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            entity = billofmaterialservice.GetById(bomid);
            entity.CRM_POMaster = CustomerPoservice.getbyid(id);
            var dd = entity.CRM_POMaster.Quotrefno;
            var qut = Convert.ToInt32(dd);
            entity.porawitemlist = quotationrawitemservice.getbyquotationid(qut);
            //entity.M_BillOfMaterialDetails = billmaterialdetailservice.GetById(bomid);
            entity.M_BillOfMaterialDetail = billmaterialdetailservice.GetById(bomid);
            // entity.M_BOMSizeDetails = bomsizedetailservice.GetById(entity.M_BillOfMaterialDetail);
            var rawitemlist = itemservice.get().ToList();
            ViewBag.colorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", (long)entity.CRM_POMaster.styledetailentity.Stylecolorid);
            ViewBag.customerlist = DropDownList<M_CustomerMasterEntity>.LoadItems(customerservice.get(), "Id", "contactname", (int)entity.CRM_POMaster.customerid);
            ViewBag.addresslist = DropDownList<M_ContactAddressEntity>.LoadItems(customerservice.getAddresssbycustomerid((long)entity.CRM_POMaster.customerid), "Id", "Addess", (int)entity.CRM_POMaster.Addressid);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.CRM_POMaster.styledetailentity.Styleid);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.CRM_POMaster.styledetailentity.Segmenttypeid);
            if (entity.Prooce_Cycle_id != null) { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", (int)entity.Prooce_Cycle_id); }
            else { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", 0); }
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            entity.itemlist_ = rawitemlist;
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            entity.unitlist_ = unitservice.get();

            return View(entity);
        }


        [HttpPost]
        public ActionResult Edit(long id, FormCollection frm, M_BillOfMaterialMasterEntity entity)
        {
            var rawitemstrlist = frm["BOMitemList"].ToString();
            var styleid = Convert.ToInt64(frm["Styleid"]);
            entity.StyleId = styleid;
            var BOM_No = frm["BOM_No"];
            entity.BOM_No = BOM_No;
            entity.EnquiryId = id;
            entity.UpdatedDate = DateTime.Now;
            entity.IsActive = true;
            bool updatedid = billofmaterialservice.Update(entity);
            if (updatedid == true)
            {
                string[] rawitemarr = rawitemstrlist.Split('#');
                // "64~65~66~#12~12~12~#"
                var sizelist = frm["BOMSizeList"].ToString();
                var qtylist = frm["BOMQtyList"].ToString();
                var sizeidlist = frm["BOMSizeIdList"].ToString();

                string[] sizearr = sizelist.Split('#');
                string[] qtyarr = qtylist.Split('#');
                string[] sizeidarr = sizeidlist.Split('#');
                var j = 0;
                var k = 0;
                bool bomsizeid = false;
                foreach (var arritem in rawitemarr)
                {
                    string[] styleitem = arritem.Split('~');
                    M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
                    styleentity.BOM_ID = entity.BOM_ID;
                    styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
                    styleentity.ItemId = Convert.ToInt32(styleitem[1]);
                    styleentity.SupplierName = Convert.ToString(styleitem[2]);
                    styleentity.RequiredQty = Convert.ToDecimal(styleitem[3]);
                    styleentity.UnitId = Convert.ToInt32(styleitem[4]);
                    styleentity.ID = Convert.ToInt32(styleitem[5]);
                    // styleentity.Comment = frm["comment"].ToString();
                    styleentity.IsActive = true;
                    styleentity.CreatedDate = DateTime.Now;
                    styleentity.UpdatedDate = DateTime.Now;
                    bool updatetid = billmaterialdetailservice.Update(styleentity);
                    if (updatetid == true)
                    {
                        string[] qtysingle = { };
                        qtysingle = qtyarr[j].Split('~');


                        string[] sizeidsingle = { };
                        sizeidsingle = sizeidarr[k].Split('~');

                        int i = 0;

                        string[] sizerecord = sizearr[j].Split('~');


                        foreach (var sizeitem in sizerecord)
                        {
                            M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
                            bomentity.BOMD_Id = styleentity.ID;
                            bomentity.Id = Convert.ToInt32(sizeidsingle[k]);
                            bomentity.SizeId = Convert.ToInt32(sizeitem);
                            bomentity.Quantity = Convert.ToDecimal(qtysingle[i]);
                            bomentity.IsActive = true;
                            bomsizeid = bomsizedetailservice.Update(bomentity);
                            i++;
                        }
                    }
                    j++;
                    k++;
                }
                if (bomsizeid == true)
                {
                    TempData["alertbommsg"] = "Bill of Material Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            entity = billofmaterialservice.GetById(entity.BOM_ID);
            entity.CRM_EnquiryMaster = enquiryservice.getbyid(id);
            entity.style = enquirystyledetailservice.getbyenquiryid(id);
            entity.rawitemlist = enquiryrawitemservice.getbyenquiryid(id);
            entity.M_BillOfMaterialDetail = billmaterialdetailservice.GetById(entity.BOM_ID);
            var rawitemlist = itemservice.get().ToList();
            ViewBag.colorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", (long)entity.style.Stylecolorid);
            ViewBag.customerlist = DropDownList<M_CustomerMasterEntity>.LoadItems(customerservice.get(), "Id", "contactname", (int)entity.CRM_EnquiryMaster.customerid);
            ViewBag.addresslist = DropDownList<M_ContactAddressEntity>.LoadItems(customerservice.getAddresssbycustomerid((long)entity.CRM_EnquiryMaster.customerid), "Id", "Addess", (int)entity.CRM_EnquiryMaster.Addressid);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.style.Styleid);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.style.Segmenttypeid);
            if (entity.Prooce_Cycle_id != null) { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", (int)entity.Prooce_Cycle_id); }
            else { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", 0); }
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            entity.itemlist_ = rawitemlist;
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            entity.unitlist_ = unitservice.get();
            return View(entity);
        }
        [HttpPost]
        public ActionResult BOM_PO_Edit(long id, FormCollection frm, M_BillOfMaterialMasterEntity entity)
        {
            var rawitemstrlist = frm["BOMitemList"].ToString();
            var styleid = Convert.ToInt64(frm["Styleid"]);
            entity.StyleId = styleid;
            var BOM_No = frm["BOM_No"];
            entity.BOM_No = BOM_No;
            entity.Poid = id;
            entity.UpdatedDate = DateTime.Now;
            entity.IsActive = true;
            bool updatedid = billofmaterialservice.Update(entity);
            if (updatedid == true)
            {
                string[] rawitemarr = rawitemstrlist.Split('#');
                // "64~65~66~#12~12~12~#"
                var sizelist = frm["BOMSizeList"].ToString();
                var qtylist = frm["BOMQtyList"].ToString();
                var sizeidlist = frm["BOMSizeIdList"].ToString();

                string[] sizearr = sizelist.Split('#');
                string[] qtyarr = qtylist.Split('#');
                string[] sizeidarr = sizeidlist.Split('#');
                var j = 0;
                var k = 0;
                bool bomsizeid = false;
                foreach (var arritem in rawitemarr)
                {
                    string[] styleitem = arritem.Split('~');
                    M_BillOfMaterialDetailEntity styleentity = new M_BillOfMaterialDetailEntity();
                    styleentity.BOM_ID = entity.BOM_ID;
                    styleentity.ItemSubCategoryId = Convert.ToInt32(styleitem[0]);
                    styleentity.ItemId = Convert.ToInt32(styleitem[1]);
                    styleentity.SupplierName = Convert.ToString(styleitem[2]);
                    styleentity.RequiredQty = Convert.ToDecimal(styleitem[3]);
                    styleentity.UnitId = Convert.ToInt32(styleitem[4]);
                    styleentity.ID = Convert.ToInt32(styleitem[5]);
                    // styleentity.Comment = frm["comment"].ToString();
                    styleentity.IsActive = true;
                    styleentity.CreatedDate = DateTime.Now;
                    styleentity.UpdatedDate = DateTime.Now;
                    bool updatetid = billmaterialdetailservice.Update(styleentity);
                    if (updatetid == true)
                    {
                        string[] qtysingle = { };
                        qtysingle = qtyarr[j].Split('~');


                        string[] sizeidsingle = { };
                        sizeidsingle = sizeidarr[k].Split('~');

                        int i = 0;

                        string[] sizerecord = sizearr[j].Split('~');


                        foreach (var sizeitem in sizerecord)
                        {
                            M_BOMSizeDetailMasterEntity bomentity = new M_BOMSizeDetailMasterEntity();
                            bomentity.BOMD_Id = styleentity.ID;
                            bomentity.Id = Convert.ToInt32(sizeidsingle[k]);
                            bomentity.SizeId = Convert.ToInt32(sizeitem);
                            bomentity.Quantity = Convert.ToDecimal(qtysingle[i]);
                            bomentity.IsActive = true;
                            bomsizeid = bomsizedetailservice.Update(bomentity);
                            i++;
                        }
                    }
                    j++;
                    k++;
                }
                if (bomsizeid == true)
                {
                    TempData["alertbommsg"] = "Bill of Material Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            entity.CRM_POMaster = CustomerPoservice.getbyid(id);
            var dd = entity.CRM_POMaster.Quotrefno;
            var qut = Convert.ToInt32(dd);
            entity.porawitemlist = quotationrawitemservice.getbyquotationid(qut);
            //entity.M_BillOfMaterialDetails = billmaterialdetailservice.GetById(bomid);
            entity = billofmaterialservice.GetById(entity.BOM_ID);
            entity.M_BillOfMaterialDetail = billmaterialdetailservice.GetById(entity.BOM_ID);
            // entity.M_BOMSizeDetails = bomsizedetailservice.GetById(entity.M_BillOfMaterialDetail);
            var rawitemlist = itemservice.get().ToList();
            ViewBag.colorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", (long)entity.CRM_POMaster.styledetailentity.Stylecolorid);
            ViewBag.customerlist = DropDownList<M_CustomerMasterEntity>.LoadItems(customerservice.get(), "Id", "contactname", (int)entity.CRM_POMaster.customerid);
            ViewBag.addresslist = DropDownList<M_ContactAddressEntity>.LoadItems(customerservice.getAddresssbycustomerid((long)entity.CRM_EnquiryMaster.customerid), "Id", "Addess", (int)entity.CRM_POMaster.Addressid);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (long)entity.style.Styleid);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get(), "Id", "Segmenttype", (long)entity.CRM_POMaster.styledetailentity.Segmenttypeid);
            if (entity.Prooce_Cycle_id != null) { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", (int)entity.Prooce_Cycle_id); }
            else { ViewBag.processcyclelist = DropDownList<M_ProcessCycleEntity>.LoadItems(processcycleservice.get(), "Prooce_Cycle_id", "Process_cycle_Name", 0); }
            ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            entity.itemlist_ = rawitemlist;
            ViewBag.supplierlist = DropDownList<M_SupplierMasterEntity>.LoadItems(supplierservice.get(), "Id", "Suppliername", 0);
            entity.unitlist_ = unitservice.get();

            return View(entity);
        }