﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class FG_TransferMasterEntity
    {
        public long Id { get; set; }
        public string TransferNo { get; set; }
        public Nullable<System.DateTime> TransferDate { get; set; }
        public Nullable<long> ProductionPlanId { get; set; }
        public Nullable<System.DateTime> ProductionPlanDate { get; set; }
        public Nullable<long> RefWorkOrderId { get; set; }
        public Nullable<long> WarehouseId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comment { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<System.DateTime> RefWorkOrderDate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<int> Item_Attribute_Id { get; set; }
        public string HSNCode { get; set; }
        public Nullable<long> Unit { get; set; }
        public Nullable<decimal> WOQty { get; set; }
        public Nullable<decimal> TotBatchQty { get; set; }
        public Nullable<decimal> ProducedQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public string WarehouseName { get; set; }
        public string ItemName { get; set; }
        public string UnitName { get; set; }
        public string RackAllocation { get; set; }
        public string BatchList { get; set; }

        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual List<FG_TransferDetailEntity> FG_TransferDetail { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
        public virtual M_WarehouseEntity M_WAREHOUSE { get; set; }
        public virtual MFG_WORK_ORDEREntity MFG_WORK_ORDER { get; set; }
    }
}
