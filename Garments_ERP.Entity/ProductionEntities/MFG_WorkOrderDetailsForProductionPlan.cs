﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Garments_ERP.Entity
{
    public class MFG_ProductionPlanStyleDetails
    {
        public long ID { get; set; }
        public long WorkOrderID { get; set; }
        public long StyleID { get; set; }
        public string StyleNo { get; set; }
        public string StyleName { get; set; }
        public string SegmentName { get; set; }
        public string ItemName { get; set; }
        public Nullable<long> SizeID { get; set; }
        public string SizeType { get; set; }
        public Nullable<decimal> WO_Qty { get; set; }
        public Nullable<decimal> Target_Qty { get; set; }
        public Nullable<decimal> Actual_Qty { get; set; }
        public Nullable<int> UnitID { get; set; }
        public SelectList Units { get; set; }
    }

    public class MFG_ProductionPlanProcessDetails : M_ProcessMasterEntity
    {
        public decimal? ProcessID { get; set; }
        public string Operator { get; set; }
        public decimal? Wastage { get; set; }
        public string Incharge { get; set; }
        public long InchargeID { get; set; }
        public string Machine { get; set; }
        public string ProcessProductIN { get; set; }
        public string ProcessProduct { get; set; }
        public Nullable<long> ProcessProductid { get; set; }
        public Nullable<int> ProcessProductidAttr { get; set; }
        public int? Processsequence { get; set; }

    }

    public class MFG_WorkOrderDetailsForProductionPlan
    {
        private MFG_WORK_ORDEREntity workOrderEntity;
        private List<MFG_ProductionPlanStyleDetails> styleList;
        private List<MFG_ProductionPlanProcessDetails> processList;
        private List<M_BillOfMaterialDetailEntity> bomdetailList;
        public MFG_WorkOrderDetailsForProductionPlan()
        {
            workOrderEntity = new MFG_WORK_ORDEREntity();
            styleList = new List<MFG_ProductionPlanStyleDetails>();
            processList = new List<MFG_ProductionPlanProcessDetails>();
            bomdetailList = new List<M_BillOfMaterialDetailEntity>();
        }

        public MFG_WORK_ORDEREntity WorkOrderEntity
        {
            get { return workOrderEntity; }

            set { workOrderEntity = value; }
        }

        public List<MFG_ProductionPlanStyleDetails> StyleList
        {
            get { return styleList; }

            set { styleList = value; }
        }
        public List<MFG_ProductionPlanProcessDetails> ProcessList
        {
            get { return processList; }

            set { processList = value; }
        }
        public List<M_BillOfMaterialDetailEntity> BOMDetailList
        {
            get { return bomdetailList; }

            set { bomdetailList = value; }
        }

    }
}
