﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class FG_Transfer_To_Retail_DetailsEntity
    {
        public long FG_TR_Detail_ID { get; set; }
        public Nullable<long> FG_TR_ID { get; set; }
        public Nullable<decimal> TransferToRetail_Qty { get; set; }
        public Nullable<decimal> Balance_Qty { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<long> InwardId { get; set; }
        public Nullable<long> InwardQty { get; set; }

        public virtual FG_Transfer_To_RetailEntity FG_Transfer_To_Retail { get; set; }
    }
}
