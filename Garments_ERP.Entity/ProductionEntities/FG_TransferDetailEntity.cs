﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class FG_TransferDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> FG_TransferId { get; set; }
        public Nullable<decimal> ProducedQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string IsQC_Done { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> batchlotid { get; set; }
        public Nullable<decimal> BatchLotQty { get; set; }
        public string BtachLot { get; set; }

        public virtual FG_TransferMasterEntity FG_TransferMaster { get; set; }
        public virtual M_ItemSizeMasterEntity M_ItemSizeMaster { get; set; }
    }
}
