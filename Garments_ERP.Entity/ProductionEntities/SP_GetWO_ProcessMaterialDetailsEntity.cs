﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class SP_GetWO_ProcessMaterialDetailsEntity
    {
        public Nullable<long> WOProcessID { get; set; }
        public long WOProcessMaterial_ID { get; set; }
        public string Itemsubcategory { get; set; }
        public string ItemName { get; set; }
        public string ItemUnit { get; set; }
        public Nullable<decimal> IssuedQty { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> ItemCheckedby { get; set; }
        public Nullable<System.DateTime> ItemCheckedDate { get; set; }
        public Nullable<System.DateTime> WOEndDate { get; set; }
        public Nullable<int> WOEndedBy { get; set; }
        public Nullable<System.DateTime> WOStartDate { get; set; }
        public Nullable<int> WOStartedBY { get; set; }
    }
}
