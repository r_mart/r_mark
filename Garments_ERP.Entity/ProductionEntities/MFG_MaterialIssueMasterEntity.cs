﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_MaterialIssueMasterEntity
    {
        public long Id { get; set; }
        [Required]
        public long WarehouseID { get; set; }
        public string MaterialIssueNo { get; set; }
        [Required]
        public int EmpID { get; set; }
        public long StyleID { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<long> PPNo { get; set; }
        public Nullable<System.DateTime> PPDate { get; set; }
        public Nullable<long> RefWO { get; set; }
        public Nullable<System.DateTime> RefWODate { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> DeptId { get; set; }

        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<M_ItemMasterEntity> itemmaster { get; set; }
        public List<MFG_MaterialIssueItemDetailsEntity> issueitemlist { get; set; }
        public List<M_ItemOutwardMasterEntity> outwarditemlist { get; set; }
        public List<MFG_MaterialIssueStyleImageEntity> styleimglist { get; set; }
        public virtual EmployeeEntity Employee { get; set; }
        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }
        public virtual M_WarehouseEntity M_WAREHOUSE { get; set; }
        public virtual ICollection<MFG_MaterialIssueItemDetailsEntity> MFG_MaterialIssueItemDetails { get; set; }
        public virtual MFG_WORK_ORDEREntity MFG_WORK_ORDER { get; set; }
        public virtual ICollection<MFG_MaterialIssueStyleImageEntity> MFG_MaterialIssueStyleImage { get; set; }
        public virtual MFG_PRODUCTION_PLANEntity Productionplan { get; set; }
        public virtual M_DepartmentMasterEntity deptenty { get; set; }
    }
}
