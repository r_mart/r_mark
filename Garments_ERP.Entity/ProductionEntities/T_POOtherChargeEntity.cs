﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class T_POOtherChargeEntity
    {
        public long Id { get; set; }
        public Nullable<long> TXID { get; set; }
        public Nullable<int> OtherChargeId { get; set; }
        public Nullable<decimal> OtherChargeValue { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<int> InventoryModeId { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }

        public virtual M_POOtherChargesMasterEntity OCMaster { get; set; }
    }
}
