﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class MFG_WorkOrderProcessDetailsEntity
    {
        public long WOProcessID { get; set; }
        public Nullable<long> WO_ID { get; set; }
        public Nullable<decimal> ProcessID { get; set; }
        public Nullable<int> ProcessInchargeID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }

        public virtual EmployeeEntity Employee { get; set; }
        public virtual M_ProcessMasterEntity M_PROCESS_Master { get; set; }
        public virtual MFG_WORK_ORDEREntity MFG_WORK_ORDER { get; set; }
       // [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual List<MFG_WorkOrderProcessMaterialsEntity> MFG_WorkOrderProcessMaterials { get; set; }
    }
}
