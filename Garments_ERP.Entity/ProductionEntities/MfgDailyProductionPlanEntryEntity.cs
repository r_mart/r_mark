﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   
    public class MfgDailyProductionPlanEntryEntity
    {
        public long ID { get; set; }
        public DateTime ProductionDate { get; set; }
        public long MonthlyPlanID { get; set; }
        public decimal? MonthlyPlanQuantity { get; set; }
        public decimal? MonthlyPendingQuantity { get; set; }
        public decimal? MonthlyFinishedQuantity { get; set; }
        public string MonthlyFromDate { get; set; }
        public string MonthlyToDate { get; set; }

        public string MonthlyFromDateToValidate { get; set; }
        public string MonthlyToDateToValidate { get; set; }

        public long StyleID { get; set; }
        public string StyleNo { get; set; }
        public string StyleName { get; set; }
        public string StyleDescription { get; set; }
        public Nullable<long> MachineID { get; set; }
        public Nullable<decimal> DailyPlanned_Qty { get; set; }
        public Nullable<decimal> DailyProduced_Qty { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<int> WorkCenterID { get; set; }
        public Nullable<int> ToolID { get; set; }
        public Nullable<int> OperatorID { get; set; }
        public int CreatedBy { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }

        public DateTime CreatedDate { get; set; }
        public List<DailyProductionEntryStyleSize> StyleSizeDetails { get; set; }

    }

    public class DailyProductionEntryStyleSize
    {
        public long SizeID { get; set; }
        public string SizeType { get; set; }
        public decimal? PlannedQuantity { get; set; }
        public decimal? ProducedQuantity { get; set; }
        public bool QC { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    }
}
