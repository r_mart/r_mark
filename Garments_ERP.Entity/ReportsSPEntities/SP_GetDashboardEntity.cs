﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SP_GetDashboardEntity
    {
        public string aaction { get; set; }
        public string color { get; set; }
        public string title { get; set; }
        public Nullable<int> countid { get; set; }
        public Nullable<int> opens { get; set; }
        public Nullable<int> pending { get; set; }
        public Nullable<int> inprogress { get; set; }
        public Nullable<int> completed { get; set; }
        public Nullable<int> Rejected { get; set; }    
        
    }
}
