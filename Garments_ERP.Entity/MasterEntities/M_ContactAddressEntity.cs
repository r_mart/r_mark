﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class M_ContactAddressEntity
    {
        public long Id { get; set; }
        public Nullable<long> customerid { get; set; }
        public string Addess { get; set; }
        public Nullable<int> Cityid { get; set; }
        public Nullable<int> Stateid { get; set; }
        public Nullable<int> Countryid { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }


        public virtual M_CityMasterEntity M_Cityentity { get; set; }
        public virtual M_StateMasterEntity M_Stateentity { get; set; }
    }
}
