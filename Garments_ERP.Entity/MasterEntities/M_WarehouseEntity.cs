﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_WarehouseEntity
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Enter Warehouse Name")]
        [System.Web.Mvc.Remote("CheckwarehouseName", "Warehouse", ErrorMessage = "Warehouse name already in use!", AdditionalFields = "ID")]
        public string SHORT_NAME { get; set; }
        public Nullable<int> OWNER_ENTITY_ID { get; set; }
        public Nullable<int> OWNER_DEPT_ID { get; set; }
        public Nullable<long> ADDRESS_ID { get; set; }
        public Nullable<int> WAREHOUSE_TYPE_ID { get; set; }
        public string WAREHOUSE_CODE { get; set; }
        public Nullable<bool> IS_ENABLED { get; set; }
        public Nullable<int> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<int> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public Nullable<long> WAREHOUSE_ROLE { get; set; }
        public Nullable<byte> ORDINAL { get; set; }
        public string Comment { get; set; }

        public virtual EmployeeEntity Employee { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
        public virtual M_RoleEntity M_ROLE { get; set; }
        public virtual M_WarehouseTypeMasterEntity M_WarehouseTypeMaster { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }

    }
}
