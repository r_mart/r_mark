﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_companyEntity
    {
        public int Company_ID { get; set; }
        public string Company_Name { get; set; }
        public string Legal_Name { get; set; }
        public string Company_Address { get; set; }
        public string Country_Code { get; set; }
        public Nullable<int> State_Id { get; set; }
        public Nullable<int> City_Id { get; set; }
        public string Pincode { get; set; }
        [StringLength(10)]
        public string Mobile_No { get; set; }
        public string Pan_No { get; set; }
        public string Website { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public string Head_Office_Add { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string FaxNo { get; set; }
        public string ECCNo { get; set; }
        public string PLANo { get; set; }
        public string BstNo { get; set; }
        public string CstNo { get; set; }
        public string GSTNo { get; set; }
        public string Remark { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string UserName { get; set; }

        public virtual M_companyEntity companyent { get; set; }
    }
}
