﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.MasterEntities
{
    public class T_ITEM_TRANSFER_DETAILEntity
    {
        public long ID { get; set; }
        public Nullable<long> TRANSFER_ID { get; set; }
        public Nullable<long> ITEM_ID { get; set; }
        public Nullable<decimal> TRANSFER_QTY { get; set; }
        public string WAREHOUSE_STORAGE_TO { get; set; }
        public Nullable<long> BATCH_NO_ID { get; set; }
        public Nullable<long> UNIT_ID { get; set; }
        public Nullable<long> ITEM_ATTRIBUTE_ID { get; set; }
        public Nullable<bool> QC { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public string MODIFY_BY { get; set; }
        public Nullable<System.DateTime> MODIFY_ON { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }

        public Nullable<decimal> InspectedQty { get; set; }
        public Nullable<decimal> RejectedQty { get; set; }

        public virtual M_ItemMasterEntity itement { get; set; }
        public virtual M_Product_InfoEntity BatchInfo { get; set; }
        public virtual M_Product_InfoEntity BatchInfo1 { get; set; }
        public virtual M_UnitMasterEntity unit { get; set; }

    }
}
