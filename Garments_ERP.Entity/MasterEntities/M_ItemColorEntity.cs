﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
    public class M_ItemColorEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Item Color")]
        [System.Web.Mvc.Remote("CheckItemColor", "ItemColor", ErrorMessage = "Item Color already in use!", AdditionalFields = "Id")]
        public string ItemColor { get; set; }
        public string ItemColordesc { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ICollection<M_ItemMasterEntity> M_ItemMaster { get; set; }
    
    }
}
