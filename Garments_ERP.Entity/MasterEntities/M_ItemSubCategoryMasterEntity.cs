﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class M_ItemSubCategoryMasterEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter item sub-category name")]
        [System.Web.Mvc.Remote("CheckItemSubCategory", "ItemSubCategory", ErrorMessage = "Item sub-category is already exists.", AdditionalFields = "Id, itemcategoryid")]
        public string Itemsubcategory { get; set; }
        public Nullable<long> itemcategoryid { get; set; }
        public string itemsubcategorydesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }

        public List<M_ItemSubCategoryMasterEntity> itemsubcategorylist { get; set; }

        public virtual M_ItemCategoryMasterEntity M_ItemCategoryMasterentity { get; set; }

    }
}
