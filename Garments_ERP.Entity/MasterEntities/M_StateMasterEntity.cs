﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_StateMasterEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> CountryID { get; set; }
        public Nullable<int> StateCode { get; set; }

        public virtual List<M_CityMasterEntity> M_CityMaster { get; set; }
        public virtual M_CountryMasterEntity M_CountryMaster { get; set; }
    }
}
