﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
   public class M_DepartmentMasterEntity
    {
        public int Id { get; set; }
        public string DepartmentNo { get; set; }

        [Required(ErrorMessage = "Enter Department Name")]
        [System.Web.Mvc.Remote("CheckDepartmentName", "Department", ErrorMessage = "Department name already in use!", AdditionalFields = "Id")]
        public string Departmentname { get; set; }
        public string Shortname { get; set; }
        public string Departmentdesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<long> ParentId { get; set; }
        public virtual List<SRM_GRNInwardEntity> SRM_GRNInward { get; set; }
    }
}
