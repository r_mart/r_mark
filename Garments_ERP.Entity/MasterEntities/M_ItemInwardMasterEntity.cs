﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.MasterEntities
{
    public class M_ItemInwardMasterEntity
    {
        public long ID { get; set; }
        public Nullable<long> ItemInward_ID { get; set; }
        public Nullable<decimal> Jobwork_ID { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public Nullable<int> UOM { get; set; }
        public Nullable<decimal> ItemQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> InventoryModeId { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> Transaction_ID { get; set; }
        public string Item_Attribute_Value_ID { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual SRM_GRNItemEntity SRM_GRNItem { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
    }
}
