﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_LedgerBillingMasterEntity
    {
        public long Id { get; set; }
        public long LedgerId { get; set; }
        [Required(ErrorMessage = "Enter Billing Name")]
        [System.Web.Mvc.Remote("CheckCustomerName", "Customer", ErrorMessage = "Billing Name Already In Use!", AdditionalFields = "Id")]

        public string Billingname { get; set; }
        public string unit { get; set; }
        [StringLength(10)]
        public string Mobileno { get; set; }
        [StringLength(10)]
        public string Telephoneno { get; set; }
        [StringLength(10)]
        public string Faxno { get; set; }
        [RegularExpression(@"^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", ErrorMessage = "Enter Valid email")]
        public string Email { get; set; }
        public Nullable<int> Department { get; set; }
        [RegularExpression(@"^[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}\d{1}Z[(0-9)(a-zA-Z)]{1}", ErrorMessage = "Enter Valid GSTIN.")]
        public string GSTNo { get; set; }
        public string Address { get; set; }
        public Nullable<int> Cityid { get; set; }
        public Nullable<int> Stateid { get; set; }
        public List<M_CountryMasterEntity> countrylist { get; set; }
        public Nullable<int> Countryid { get; set; }
        public string Bankname { get; set; }
        public string Accountno { get; set; }
        public string Bankcode { get; set; }
        public Nullable<bool> sez { get; set; }


        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdelete { get; set; }

        public List<M_DepartmentMasterEntity> deptlist { get; set; }
        public virtual M_LedgerMasterEntity M_LedgerMaster { get; set; }
        public virtual M_CityMasterEntity M_Cityentity { get; set; }
        public virtual M_StateMasterEntity M_Stateentity { get; set; }
        public virtual M_CountryMasterEntity M_Countryentity { get; set; }

    }
}
