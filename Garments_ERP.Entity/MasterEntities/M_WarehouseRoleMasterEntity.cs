﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_WarehouseRoleMasterEntity
    {
        public long ID { get; set; }
        public string CODE { get; set; }
        public string SHORT_NAME { get; set; }
        public Nullable<bool> IS_ENABLED { get; set; }
        public Nullable<int> MEMBER_TYPE { get; set; }
        public Nullable<long> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<long> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public Nullable<long> LFT { get; set; }
        public Nullable<long> RGT { get; set; }

        public virtual List<M_WarehouseEntity> M_WAREHOUSE { get; set; }
    }
}
