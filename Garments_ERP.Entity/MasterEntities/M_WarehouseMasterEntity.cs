﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_WarehouseMasterEntity
    {
        public long Id { get; set; }
        public string Warehouse { get; set; }
        public string Warehousedesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public virtual List<SRM_GRNInwardEntity> SRM_GRNInward { get; set; }
        public virtual List<SRM_GRNItemEntity> SRM_GRNItem { get; set; }
    }
}
