﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_RoleEntity
    {
        public long roleId { get; set; }
        public string roleName { get; set; }
        public string Discription { get; set; }
        public Nullable<long> ParentId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<long> LFT { get; set; }
        public Nullable<long> RGT { get; set; }
        public virtual M_RoleEntity RoleEnt { get; set; }
    }
}
