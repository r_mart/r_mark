﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_PurchaseRequestItemDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> PR_ID { get; set; }
        public Nullable<int> ItemCategoryId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public string ItemDesc { get; set; }
        public Nullable<System.DateTime> RequiredDate { get; set; }
        public Nullable<int> UnitId { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> orderQty { get; set; }
        public Nullable<int> IsReadymade { get; set; }

        public List<string> rawitemimg_list { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_ItemSubCategoryMasterEntity M_ItemSubCategoryMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual SRM_PurchaseRequestMasterEntity SRM_PurchaseRequestMaster { get; set; }
        public List<M_ItemMasterEntity> ItemList { get; set; }
    }
}
