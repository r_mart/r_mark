﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_SupplierContactDetailsEntity
    {
        public long Id { get; set; }
        public Nullable<long> Supplierid { get; set; }
        public string Contactpersonname { get; set; }
        public string Mobileno { get; set; }
        public string Telephoneno { get; set; }
        public string Faxno { get; set; }
        public Nullable<int> Department { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        //[RegularExpression(@"^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", ErrorMessage = "Enter valid email")]
        public string Email { get; set; }

        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
        public virtual M_SupplierMasterEntity M_SupplierMaster { get; set; }
    }
}
