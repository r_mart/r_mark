﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class SRM_PurchaseRequestMasterEntity
    {
        public long Id { get; set; }
        public Nullable<long> BOM_Id { get; set; }
        public Nullable<long> POid { get; set; }
        public string PR_No { get; set; }
        public Nullable<System.DateTime> PR_Date { get; set; }
        public string Priority { get; set; }
        public string AdditionalInfo { get; set; }
        public Nullable<int> AccountHeadId { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<bool> ApprovalStatus { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> BOM_Date { get; set; }
        public Nullable<long> EnquiryId { get; set; }
        public Nullable<long> StyleId { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsReadymade { get; set; }


        public List<M_ItemMasterEntity> itemlist_ { get; set; }
        public List<M_UnitMasterEntity> unitlist_ { get; set; }
        public List<SRM_PurchaseRequestItemDetailEntity> rawitemlist { get; set; }
        public virtual EmployeeEntity Employee { get; set; }
        public virtual M_BillOfMaterialMasterEntity M_BillOfMaterialMaster { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
        public virtual CRM_EnquiryMasterEntity CRM_EnquiryMaster { get; set; }
        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }
        public virtual List<SRM_PurchaseRequestItemDetailEntity> SRM_PurchaseRequestItemDetail { get; set; }
    }
}
