﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.Accounts
{
    public class Acc_Voucher_Customer_Details_Entity
    {
        public long Voucher_Customer_Detail_ID { get; set; }
        public Nullable<int> Voucher_ID { get; set; }
        public string Customer_Mob_No { get; set; }
        public string Customer_Name { get; set; }
        public Nullable<int> Customer_ID { get; set; }
        public string Consignee_Mob_No { get; set; }
        public string Consignee_Name { get; set; }
        public Nullable<int> Consignee_ID { get; set; }
        public Nullable<int> Employee_ID { get; set; }
        public Nullable<int> Payment_Settlement_ID { get; set; }
        public Nullable<int> Payment_Mode_ID { get; set; }
        public Nullable<decimal> Cash_Amount { get; set; }
        public string Cheque_No { get; set; }
        public string Cheque_Date { get; set; }
        public string Bank_Name { get; set; }
        public Nullable<decimal> Cheque_Amount { get; set; }
        public string Credit_Card_No { get; set; }
        public Nullable<decimal> Credit_Card_Amount { get; set; }
        public string Net_Banking_Ref_No { get; set; }
        public Nullable<decimal> Net_Banking_Amount { get; set; }
        public Nullable<decimal> Total_Amount { get; set; }
        public Nullable<decimal> Received_Amount { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public string CustomerName { get; set; }
        public string NameContact { get; set; }
        public int Branch_Id { get; set; }
        public virtual Acc_Voucher_Entity Acc_Voucher { get; set; }
    }
}
