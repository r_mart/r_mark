﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.Accounts
{
  public class Acc_Voucher_Type_Entity
    {

        public int Voucher_Type_ID { get; set; }
        public string Voucher_Type { get; set; }
        public Nullable<int> Voucher_Type_Master_ID { get; set; }
        public string Start_With { get; set; }
        public string Voucher_No { get; set; }
        public Nullable<bool> Auto_Narration { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public int Created_By { get; set; }
        public System.DateTime Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public string Voucher_Narration { get; set; }
        public string Report_Name { get; set; }
        public Nullable<System.DateTime> From_Date { get; set; }
        public Nullable<System.DateTime> To_Date { get; set; }
        public string Reset_Counter_On { get; set; }
        public string Reset_Counter_Value { get; set; }
        public Nullable<int> cOUNTER { get; set; }
        public string Pre_Voucher_No { get; set; }
        public string Post_Voucher_No { get; set; }
        public Nullable<bool> Show_Comments { get; set; }
        public Nullable<bool> is_Accountable { get; set; }
        public string DR_Group_Type { get; set; }
        public string CR_Group_Type { get; set; }

    }
}
