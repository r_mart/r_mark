﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.CommonEntities
{
   public class MFG_PO_DeliveryDetail_Entity
    {
        public long PO_DelSchedule_ID { get; set; }
        public Nullable<System.DateTime> Delivery_Date { get; set; }
        public Nullable<decimal> Delivery_Qty { get; set; }
        public string Comments { get; set; }
        public Nullable<int> Ledger_Id { get; set; }
        public Nullable<long> CPO_ID { get; set; }
        public Nullable<long> SPO_ID { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public Nullable<int> Item_Attribute_Id { get; set; }
        public virtual CRM_CustomerPOMasterentity CRM_CustomerPOMaster { get; set; }
        public virtual M_LedgersEntity M_Ledgers { get; set; }
        public virtual SRM_PurchaseOrderMasterEntity SRM_PurchaseOrderMaster { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
    }
}
