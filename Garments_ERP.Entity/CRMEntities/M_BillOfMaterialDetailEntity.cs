﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class M_BillOfMaterialDetailEntity
    {
        public long ID { get; set; }
        public long BOM_ID { get; set; }
        public Nullable<int> ItemSubCategoryId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<long> SupplierId { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comment { get; set; }
        public string SupplierName { get; set; }
        public Nullable<decimal> PerQty { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<string> rawitemimg_list { get; set; }
        
        public List<M_BOMSizeDetailMasterEntity> sizelist_ { get; set; }
        public virtual M_BillOfMaterialMasterEntity M_BillOfMaterialMaster { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_ItemSubCategoryMasterEntity M_ItemSubCategoryMaster { get; set; }
        public List<M_ItemSubCategoryMasterEntity> ItemCategoryList { get; set; }
        public virtual M_SupplierMasterEntity M_SupplierMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public List<M_ItemMasterEntity> ItemList { get; set; }
        public virtual M_LedgersEntity LedgerEntity { get; set; }
        public virtual CRM_QuotationRawItemDetailEntity QuotnItementity { get; set; }
    }
}
