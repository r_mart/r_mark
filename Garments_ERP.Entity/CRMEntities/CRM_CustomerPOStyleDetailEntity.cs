﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class CRM_CustomerPOStyleDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> Poid { get; set; }
        public Nullable<int> Segmenttypeid { get; set; }
        public Nullable<long> Styleid { get; set; }
        public Nullable<long> Itemid { get; set; }
        public Nullable<int> Styleshadeid { get; set; }
        public Nullable<int> Stylecolorid { get; set; }
        public string Styleimage { get; set; }
        public Nullable<decimal> Totalqty { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Brand { get; set; }
        public string Styleno { get; set; }
        public string Styledesc { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<CRM_CustoemrPOStyleSizeDetailEntity> sizelist { get; set; }
        public List<CRM_CustomerPOStyleImageEntity> inagelist { get; set; }

        public virtual M_SegmentTypeMasterEntity segTMent { get; set; }
        public virtual M_StyleMasterEntity styleM { get; set; }
        public virtual M_ItemMasterEntity itemM { get; set; }
    }
}
