﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class CRM_QuotationStyleImageEntity
    {
        public long Id { get; set; }
        public Nullable<long> Quotationid { get; set; }
        public string Quotationimg { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }

        public Nullable<long> QuoStyleId { get; set; }

    }
}
