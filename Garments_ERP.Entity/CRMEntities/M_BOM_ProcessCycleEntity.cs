﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_BOM_ProcessCycleEntity
    {
        public long ID { get; set; }
        public long BOM_ID { get; set; }
        public Nullable<int> ProcessId { get; set; }
        public Nullable<int> Processsequence { get; set; }
        public Nullable<long> ItemOutId { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<decimal> PerQty { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        
        //Only For Job Work Calculation
        public Nullable<decimal> westage { get; set; }

        public virtual M_BillOfMaterialMasterEntity M_BillOfMaterialMaster { get; set; }
        public virtual M_ProcessMasterEntity processmasterEntity { get; set; }
        public virtual M_ItemMasterEntity ItemMasterentity { get; set; }
    }
}
