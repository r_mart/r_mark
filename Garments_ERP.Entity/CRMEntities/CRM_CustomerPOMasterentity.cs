﻿using Garments_ERP.Entity.CommonEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class CRM_CustomerPOMasterentity
    {
        public long Id { get; set; }
        public long QuotationID { get; set; }

        public string Quotrefno { get; set; }
        public Nullable<System.DateTime> Quotdate { get; set; }
        public Nullable<long> customerid { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public Nullable<System.DateTime> Tentivedate { get; set; }
        public Nullable<decimal> Costprice { get; set; }
        public Nullable<decimal> Saleprice { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Remark { get; set; }
        public Nullable<long> Addressid { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Craetedby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string POno { get; set; }
        public Nullable<System.DateTime> PODate { get; set; }
        public List<string> imglist { get; set; }
        public Nullable<bool> Sampling { get; set; }
        public string podatestr { get; set; }
        public string Enquiryrefno { get; set; }
        public Nullable<long> EnqId { get; set; }
        public string ProductName { get; set; }
        public Nullable<decimal> ProductQty { get; set; }
        public string StyleName { get; set; }
        public string ProductAttrib { get; set; }
        public string Comments { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public Nullable<int> IsReadmade { get; set; }

        public CRM_CustomerPOStyleDetailEntity styledetailentity { get; set; }
        public List<CRM_CustomerPOStyleImageEntity> styleimglist { get; set; }
        //public virtual M_CustomerMasterEntity M_CustomerEntity { get; set; }
        public virtual M_LedgersEntity MLedgersEntity { get; set; }
        public virtual M_Ledger_BillingDetailsEntity MLedgers_BillingDetailsEntity { get; set; }
        public virtual CRM_QuotationMasterEntity QuoEntity { get; set; }
        public virtual List<MFG_PO_DeliveryDetail_Entity> MFG_PO_DeliveryDetail { get; set; }

        public virtual List<T_POOtherChargeEntity> POOtherChargeEntity { get; set; }
    }
}
