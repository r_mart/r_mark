﻿CREATE TABLE [dbo].[M_ItemSubCategoryMaster] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [Itemsubcategory]     NVARCHAR (50) NULL,
    [itemcategoryid]      BIGINT        NULL,
    [itemsubcategorydesc] NVARCHAR (80) NULL,
    [Createddate]         DATETIME      NULL,
    [Createdby]           INT           NULL,
    [Updateddate]         DATETIME      NULL,
    [Updatedby]           INT           NULL,
    [IsActive]            BIT           NULL,
    CONSTRAINT [PK_M_ItemSubCategoryMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_ItemSubCategoryMaster_M_ItemSubCategoryMaster] FOREIGN KEY ([itemcategoryid]) REFERENCES [dbo].[M_ItemCategoryMaster] ([Id])
);

