﻿CREATE TABLE [dbo].[M_SupplierContactDetails] (
    [Id]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [Supplierid]        BIGINT        NULL,
    [Contactpersonname] NVARCHAR (50) NULL,
    [Mobileno]          NVARCHAR (12) NULL,
    [Telephoneno]       NVARCHAR (10) NULL,
    [Faxno]             NVARCHAR (20) NULL,
    [Department]        INT           NULL,
    [Remark]            NVARCHAR (80) NULL,
    [Createddate]       DATETIME      NULL,
    [Createdby]         INT           NULL,
    [Updateddate]       DATETIME      NULL,
    [Updatedby]         INT           NULL,
    [IsActive]          BIT           NULL,
    [Email]             NVARCHAR (50) NULL,
    CONSTRAINT [PK_M_SupplierContactDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_SupplierContactDetails_M_DepartmentMaster] FOREIGN KEY ([Department]) REFERENCES [dbo].[M_DepartmentMaster] ([Id]),
    CONSTRAINT [FK_M_SupplierContactDetails_M_SupplierMaster] FOREIGN KEY ([Supplierid]) REFERENCES [dbo].[M_SupplierMaster] ([Id])
);

