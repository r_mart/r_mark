﻿
CREATE procedure [dbo].[SP_GetItemInwardDetails]
as 
begin
select WI.SHORT_NAME,
 I.GRNNo,
 I.InwardDate,
 GI.ItemId,
 II.ItemName,
 GRN_Qty,
 PO_Qty,
 Challan_Qty,
 Balance_Qty,
 Accepted_Qty from SRM_GRNInward I
 INNER JOIN SRM_GRNItem GI ON I.ID=GI.ItemInwardId
 INNER JOIN M_ItemMaster II ON II.Id=GI.ItemId
 INNER JOIN M_WAREHOUSE WI ON WI.ID=I.WarehouseId
 END
