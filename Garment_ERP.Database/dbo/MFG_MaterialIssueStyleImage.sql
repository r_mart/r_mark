﻿CREATE TABLE [dbo].[MFG_MaterialIssueStyleImage] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [StyleImgName]    NVARCHAR (300) NULL,
    [ItemID]          BIGINT         NULL,
    [MaterialIssueId] BIGINT         NOT NULL,
    [IsActive]        BIT            NULL,
    CONSTRAINT [PK_MFG_MaterialIssueStyleImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

