﻿CREATE TABLE [dbo].[CRM_EnquiryRawItemImage] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Enquiryrawitemdetailid] BIGINT         NULL,
    [image]                  NVARCHAR (200) NULL,
    [Createddate]            DATETIME       CONSTRAINT [DF_CRM_EnquiryRawItemImage_Createddate] DEFAULT (getdate()) NULL,
    [Createdby]              INT            NULL,
    [Updateddate]            DATETIME       NULL,
    [Updatedby]              INT            NULL,
    [IsActive]               BIT            NULL,
    CONSTRAINT [PK_CRM_EnquiryRawItemImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

