﻿CREATE TABLE [dbo].[M_InspectionTypeMaster] (
    [Id]                        INT            IDENTITY (1, 1) NOT NULL,
    [Inspectiontype]            NVARCHAR (100) NULL,
    [Inspectiontypedescription] NVARCHAR (200) NULL,
    [Department]                INT            NULL,
    [Comment]                   NVARCHAR (100) NULL,
    [Createddate]               DATETIME       NULL,
    [Createdby]                 INT            NULL,
    [Updateddate]               DATETIME       NULL,
    [Updatedby]                 INT            NULL,
    [IsActive]                  BIT            NULL,
    CONSTRAINT [PK_M_InspectionTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

