﻿CREATE TABLE [dbo].[M_ROLE] (
    [ID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [CODE]        VARCHAR (50)  NOT NULL,
    [SHORT_NAME]  VARCHAR (255) NULL,
    [IS_ENABLED]  BIT           NULL,
    [MEMBER_TYPE] INT           NULL,
    [CREATED_BY]  BIGINT        NULL,
    [CREATED_ON]  DATETIME      NULL,
    [UPDATED_BY]  BIGINT        NULL,
    [UPDATED_ON]  DATETIME      NULL,
    [LFT]         BIGINT        NULL,
    [RGT]         BIGINT        NULL,
    CONSTRAINT [PK_M_ROLE] PRIMARY KEY CLUSTERED ([ID] ASC)
);

