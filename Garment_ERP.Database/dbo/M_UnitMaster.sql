﻿CREATE TABLE [dbo].[M_UnitMaster] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [ItemUnit]     NVARCHAR (100) NULL,
    [Unitdesc]     NVARCHAR (150) NULL,
    [Createddate]  DATETIME       NULL,
    [CreatedBy]    INT            NULL,
    [Updateddate]  DATETIME       NULL,
    [Updatedby]    INT            NULL,
    [IsActive]     BIT            NULL,
    [DepartmentId] INT            NULL,
    [Comment]      NVARCHAR (255) NULL,
    CONSTRAINT [PK_M_UnitMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_UnitMaster_M_DepartmentMaster] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[M_DepartmentMaster] ([Id])
);

