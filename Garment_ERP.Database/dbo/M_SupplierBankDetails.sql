﻿CREATE TABLE [dbo].[M_SupplierBankDetails] (
    [Id]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Supplierid]  BIGINT        NULL,
    [Bankname]    NVARCHAR (50) NULL,
    [Accountno]   NVARCHAR (40) NULL,
    [Createddate] DATETIME      NULL,
    [Createdby]   INT           NULL,
    [Updateddate] DATETIME      NULL,
    [Updatedby]   INT           NULL,
    [IsActive]    BIT           NULL,
    [Bankcode]    NVARCHAR (20) NULL,
    CONSTRAINT [PK_M_SupplierBankDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_SupplierBankDetails_M_SupplierMaster] FOREIGN KEY ([Supplierid]) REFERENCES [dbo].[M_SupplierMaster] ([Id])
);

