﻿CREATE TABLE [dbo].[M_ItemShapeMaster] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [Itemshape]     NVARCHAR (50) NULL,
    [Itemshapedesc] NVARCHAR (80) NULL,
    [Createdate]    DATETIME      NULL,
    [Createdby]     INT           NULL,
    [Updateddate]   DATETIME      NULL,
    [Updatedby]     INT           NULL,
    [IsActive]      BIT           NULL,
    CONSTRAINT [PK_M_ItemShapeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

