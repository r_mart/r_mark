﻿CREATE TABLE [dbo].[MFG_MaterialIssueItemDetails] (
    [Id]                BIGINT       IDENTITY (1, 1) NOT NULL,
    [MaterialIssueid]   BIGINT       NOT NULL,
    [Process_ID]        NUMERIC (18) NULL,
    [ItemSubCategoryID] INT          NULL,
    [ItemID]            BIGINT       NOT NULL,
    [UOM_ID]            INT          NULL,
    [ReqQty]            DECIMAL (18) NULL,
    [AvailableQty]      DECIMAL (18) NULL,
    [IssueQty]          DECIMAL (18) NULL,
    [BalanceQty]        DECIMAL (18) NULL,
    [IsActive]          BIT          NULL,
    CONSTRAINT [PK_MFG_MaterialIssueItemDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MFG_MaterialIssueItemDetails_M_ItemMaster] FOREIGN KEY ([ItemID]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_MFG_MaterialIssueItemDetails_M_ItemSubCategoryMaster] FOREIGN KEY ([ItemSubCategoryID]) REFERENCES [dbo].[M_ItemSubCategoryMaster] ([Id]),
    CONSTRAINT [FK_MFG_MaterialIssueItemDetails_M_PROCESS_Master] FOREIGN KEY ([Process_ID]) REFERENCES [dbo].[M_PROCESS_Master] ([Process_Id]),
    CONSTRAINT [FK_MFG_MaterialIssueItemDetails_MFG_MaterialIssueItemDetails] FOREIGN KEY ([UOM_ID]) REFERENCES [dbo].[M_UnitMaster] ([Id])
);

