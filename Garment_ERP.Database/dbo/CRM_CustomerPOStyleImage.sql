﻿CREATE TABLE [dbo].[CRM_CustomerPOStyleImage] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [styleimg]    NVARCHAR (200) NULL,
    [POid]        BIGINT         NULL,
    [Createddate] DATETIME       NULL,
    [Updateddate] DATETIME       NULL,
    [Createdby]   INT            NULL,
    [Updatedby]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_CustomerPOStyleImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

