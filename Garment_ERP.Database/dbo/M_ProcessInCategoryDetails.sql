﻿CREATE TABLE [dbo].[M_ProcessInCategoryDetails] (
    [Id]             BIGINT IDENTITY (1, 1) NOT NULL,
    [ProCycTran_ID]  BIGINT NULL,
    [ItemcategoryIn] BIGINT NULL,
    [IsActive]       BIT    NULL,
    CONSTRAINT [PK_M_ProcessInCategoryDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_ProcessInCategoryDetails_ProcessCycleTransaction] FOREIGN KEY ([ProCycTran_ID]) REFERENCES [dbo].[ProcessCycleTransaction] ([Id])
);

