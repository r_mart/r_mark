﻿CREATE TABLE [dbo].[T_INVENTORY_ITEM_TXN] (
    [ID]                   NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [INVENTORY_BALANCE_ID] NUMERIC (18)    NOT NULL,
    [TXN_DATE]             DATETIME        NULL,
    [TXN_TYPE]             NUMERIC (18)    NULL,
    [TXN_ID]               NUMERIC (18)    NULL,
    [TXN_QTY]              NUMERIC (18, 6) NULL,
    [DR_CR_FLAG]           CHAR (1)        NULL,
    [CREATED_BY]           NUMERIC (18)    NULL,
    [CREATED_ON]           SMALLDATETIME   NULL,
    [UPDATED_BY]           NUMERIC (18)    NULL,
    [UPDATED_ON]           SMALLDATETIME   NULL,
    [AVAILABLE_QTY]        NUMERIC (18, 6) NULL,
    CONSTRAINT [PK_T_INVENTORY_ITEM_TXN] PRIMARY KEY CLUSTERED ([ID] ASC)
);

