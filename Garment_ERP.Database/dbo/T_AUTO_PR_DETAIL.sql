﻿CREATE TABLE [dbo].[T_AUTO_PR_DETAIL] (
    [ID]                  NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [PURCHASE_REQUEST_ID] NUMERIC (18)    NOT NULL,
    [ITEM_ID]             NUMERIC (18)    NULL,
    [Style_Ref_ID]        NUMERIC (18)    NULL,
    [WAREHOUSE_ID]        NUMERIC (18)    NULL,
    [ITEM_BALANCE]        NUMERIC (18, 2) NULL,
    [CREATED_BY]          NUMERIC (18)    NULL,
    [CREATED_ON]          SMALLDATETIME   NULL,
    [UPDATED_BY]          NUMERIC (18)    NULL,
    [UPDATED_ON]          SMALLDATETIME   NULL
);

