﻿CREATE TABLE [dbo].[M_ItemSizeMaster] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Itemid]      BIGINT         NULL,
    [Styleid]     BIGINT         NULL,
    [Sizetype]    NVARCHAR (40)  NULL,
    [Sizedesc]    NVARCHAR (100) NULL,
    [Createddat]  DATETIME       NULL,
    [Createdby]   INT            NULL,
    [Updateddate] DATETIME       NULL,
    [Updatedby]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_M_ItemSizeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

