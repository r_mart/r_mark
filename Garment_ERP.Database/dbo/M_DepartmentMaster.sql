﻿CREATE TABLE [dbo].[M_DepartmentMaster] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [DepartmentNo]   NCHAR (10)     NULL,
    [Departmentname] NVARCHAR (80)  NULL,
    [Shortname]      NVARCHAR (50)  NULL,
    [Departmentdesc] NVARCHAR (100) NULL,
    [IsActive]       BIT            NULL,
    [Createddate]    DATETIME       NULL,
    [Createdby]      INT            NULL,
    [Updateddate]    DATETIME       NULL,
    [UpdatedBy]      INT            NULL,
    CONSTRAINT [PK_M_DepartmentMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

