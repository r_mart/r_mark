﻿CREATE TABLE [dbo].[M_WarehouseMaster] (
    [Id]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [Warehouse]     NVARCHAR (50) NULL,
    [Warehousedesc] NVARCHAR (80) NULL,
    [Createddate]   DATETIME      NULL,
    [Createdby]     INT           NULL,
    [Updateddate]   DATETIME      NULL,
    [Updatedby]     INT           NULL,
    [IsActive]      BIT           NULL,
    CONSTRAINT [PK_M_WarehouseMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

