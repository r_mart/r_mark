﻿CREATE TABLE [dbo].[M_SupplierTypeMaster] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [TypeName] NVARCHAR (20) NULL,
    [Typedesc] NVARCHAR (50) NULL,
    [IsActive] BIT           NULL,
    CONSTRAINT [PK_M_SupplierTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

