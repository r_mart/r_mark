﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_GRNInward
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        cl_role rlobj = new cl_role();

        //List Of GRN With Status and User Wise
        public List<SRM_GRNInwardEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_GRNInwardEntity> list = new List<SRM_GRNInwardEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_GRNInward.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x=>x.ID).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_GRNInward.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x => x.ID).ToList();
                }
                
                foreach (var item in records)
                {
                    SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
                    entity.ID = item.ID;
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WarehouseMaster = locentity;
                        }
                    }
                    entity.SupplierId = item.SupplierId;
                    if (entity.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == entity.SupplierId).FirstOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    entity.GRNNo = item.GRNNo;
                    entity.InwardDate = item.InwardDate;
                    entity.ChallanNO = item.ChallanNO;
                    entity.ChallanDate = item.ChallanDate;
                    entity.InvoiceNo = item.InvoiceNo;
                    entity.InvoiceDate = item.InvoiceDate;
                    entity.DepartmentId = item.DepartmentId;
                    entity.InwardBy = item.InwardBy;
                    entity.InvoiceCreated = item.InvoiceCreated;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.ProcessingStatusId = item.ProcessingStatusId;
                    entity.InvoiceAmount = item.InvoiceAmount;
                    entity.EwayBillNo = item.EwayBillNo;
                    entity.TruckNo = item.TruckNo;
                    entity.Transporter = item.Transporter;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.POno = db.SRM_PurchaseOrderMaster.Where(x => x.Id == item.POid).Select(x => x.PO_NO).FirstOrDefault();
                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == item.ID && x.InventoryMode==1).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    {
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id==4).ToList();
                                ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr != "")
                            {
                                BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + "," + rackname;
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid = 0;
                            long itemid = Convert.ToInt64(data.ItemId);
                            string ItemName = getItemName(itemid, IAid, ISid);
                            str = str + Convert.ToString(data.ItemId) + "~" + ItemName + "~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }
                    list.Add(entity);
                }

                if ( roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.ID).ToList();
                }
            }

            return list;

        }

        ////List Of GRN With Status and User and Form Date And To Date Wise
        public List<SRM_GRNInwardEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_GRNInwardEntity> list = new List<SRM_GRNInwardEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_GRNInward.Where(x => x.IsActive == true && x.InwardDate >= from && x.InwardDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x=>x.ID).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_GRNInward.Where(x => x.IsActive == true && x.InwardDate >= from && x.InwardDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.ID).ToList();      
                }
                foreach (var item in records)
                {
                    SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
                    entity.ID = item.ID;

                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WarehouseMaster = locentity;
                        }
                    }
                    
                    entity.SupplierId = item.SupplierId;
                    if (entity.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == entity.SupplierId).FirstOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    entity.GRNNo = item.GRNNo;
                    entity.InwardDate = item.InwardDate;
                    entity.ChallanNO = item.ChallanNO;
                    entity.ChallanDate = item.ChallanDate;
                    entity.InvoiceNo = item.InvoiceNo;
                    entity.InvoiceDate = item.InvoiceDate;
                    entity.DepartmentId = item.DepartmentId;
                    entity.InwardBy = item.InwardBy;
                    entity.InvoiceCreated = item.InvoiceCreated;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.ProcessingStatusId = item.ProcessingStatusId;
                    entity.InvoiceAmount = item.InvoiceAmount;
                    entity.EwayBillNo = item.EwayBillNo;
                    entity.TruckNo = item.TruckNo;
                    entity.Transporter = item.Transporter;
                    entity.IsActive = item.IsActive;
                    entity.POno = db.SRM_PurchaseOrderMaster.Where(x => x.Id == item.POid).Select(x => x.PO_NO).FirstOrDefault();
                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == item.ID && x.InventoryMode==1).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    {
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 4).ToList();
                                ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr != "")
                            {
                                BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + "," + rackname;
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid = 0;
                            long itemid =Convert.ToInt64(data.ItemId);
                            string ItemName = getItemName(itemid, IAid, ISid);
                            str = str + Convert.ToString(data.ItemId) + "~"+ ItemName+"~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }


                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.InwardDate >= from && x.InwardDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.ID).ToList();
                }

            }
            return list;
        }




        public List<M_Dynamic_ControlsEntity> GetControlList()
        {
            try
            {
                List<M_Dynamic_ControlsEntity> list = new List<M_Dynamic_ControlsEntity>();

                using (var db = new GarmentERPDBEntities())
                {
                    var records = db.M_Dynamic_Controls.ToList();
                    foreach (var item in records)
                    {
                        M_Dynamic_ControlsEntity entity = new M_Dynamic_ControlsEntity();
                        entity.Dynamic_Controls_Id = item.Dynamic_Controls_Id;
                        entity.Control_Name = item.Control_Name;
                        list.Add(entity);
                    }
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(SRM_GRNInwardEntity entity)
        {
            bool isupdate = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNInward.Find(entity.ID);
                record.ID = entity.ID;
                record.ChallanDate = entity.ChallanDate;
                record.InvoiceDate = entity.InvoiceDate;
                record.CreatedDate = entity.CreatedDate;
                record.UpdatedDate = entity.UpdatedDate;
                record.ProcessingStatusId = entity.ProcessingStatusId;
                record.InvoiceAmount = entity.InvoiceAmount;
                record.EwayBillNo = entity.EwayBillNo;
                record.TruckNo = entity.TruckNo;
                record.Transporter = entity.Transporter;
                //record.BatchNO = entity.BatchNO;
                //record.SerialNo = entity.SerialNo;
                record.IsActive = entity.IsActive;
                db.SaveChanges();
                isupdate = true;
            }

            return isupdate;
        }
        public SRM_GRNItemEntity getbyitemid(long id)
        {
            SRM_GRNItemEntity entity = new SRM_GRNItemEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNItem.Where(x => x.ItemId == id && x.IsActive == true).FirstOrDefault();
                if (record != null)
                {
                    entity.ID = record.ID;
                    entity.ItemId = record.ItemId;
                    entity.UnitId = record.UnitId;
                    entity.Balance_Qty = record.Balance_Qty;
                    entity.GRN_Qty = record.GRN_Qty;
                }
            }
            return entity;
        }
        public bool UpdateGRNItem(SRM_GRNItemEntity entity)
        {
            bool isupdate = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNItem.Find(entity.ID);
                record.ItemId = entity.ItemId;
                record.UnitId = entity.UnitId;
                // record.Accepted_Qty = entity.Accepted_Qty;
                record.Balance_Qty = entity.Balance_Qty;
                record.Challan_Qty = entity.Challan_Qty;
                record.GRN_Qty = entity.GRN_Qty;
                db.SaveChanges();
                var inwardrecord = db.M_ItemInwardMaster.Where(x => x.ItemInward_ID == entity.ID).FirstOrDefault();
                inwardrecord.Item_ID = entity.ItemId;
                inwardrecord.ItemQty = entity.GRN_Qty;
                inwardrecord.UOM = entity.UnitId;
                inwardrecord.BalanceQty = entity.GRN_Qty;
                db.SaveChanges();
                isupdate = true;
            }
            return isupdate;
        }

        public bool DeleteGRNItem(long id)
        {
            bool ISdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNItem.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                var inwardrecord = db.M_ItemInwardMaster.Where(x => x.ItemInward_ID == id).FirstOrDefault();
                inwardrecord.IsActive = false;
                db.SaveChanges();
                ISdelete = true;
            }

            return ISdelete;
        }

        public SRM_GRNInwardEntity getbyid(long id)
        {
            SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNInward.Find(id);
                if (record != null)
                {
                    entity.ID = id;
                    entity.InvoiceDate = record.InvoiceDate;

                    entity.IsStatus = record.IsStatus;
                    entity.Company_ID = record.Company_ID;
                    entity.BranchId = record.BranchId;
                    entity.IsReadymade = record.IsReadymade;
                    entity.InvoiceNo = record.InvoiceNo;
                    entity.InwardDate = record.InwardDate;
                    entity.WarehouseId = record.WarehouseId;
                    if (record.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == record.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WarehouseMaster = locentity;
                        }
                    }
                    
                    entity.ChallanDate = record.ChallanDate;
                    entity.ChallanNO = record.ChallanNO;
                    entity.DepartmentId = record.DepartmentId;
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    if (record.M_DepartmentMaster != null)
                    {
                        deptentity.Id = record.M_DepartmentMaster.Id;
                        deptentity.Departmentname = record.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.SupplierId = record.SupplierId;

                    if (entity.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id ==entity.SupplierId).FirstOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }

                    entity.POid = record.POid;
                    SRM_PurchaseOrderMasterEntity poentity = new SRM_PurchaseOrderMasterEntity();
                    if (record.SRM_PurchaseOrderMaster != null)
                    {
                        poentity.Id = record.SRM_PurchaseOrderMaster.Id;
                        poentity.PO_NO = record.SRM_PurchaseOrderMaster.PO_NO;
                        entity.SRM_PurchaseOrderMaster = poentity;
                    }

                    entity.GRNNo = record.GRNNo;
                    entity.InvoiceAmount = record.InvoiceAmount;
                    entity.EwayBillNo = record.EwayBillNo;
                    entity.TruckNo = record.TruckNo;
                    entity.Transporter = record.Transporter;
                    entity.IsStatus = record.IsStatus;
                    entity.Company_ID = record.Company_ID;
                    entity.BranchId = record.BranchId;
                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == record.ID && x.InventoryMode==1).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    {
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id==4).ToList();
                                ProInfo= ProInfo!=null && ProInfo.Count>0 ? ProInfo: db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr != "")
                            {
                                BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + rackname + ",";
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid = 0;
                            long itemid = Convert.ToInt64(data.ItemId);
                            string ItemName = getItemName(itemid, IAid, ISid);
                            str = str + Convert.ToString(data.ItemId) + "~" + ItemName + "~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }

                    entity.ProcessingStatusId = record.ProcessingStatusId;

                    var otherCharge = db.T_POOtherCharge.Where(x => x.TXID == record.POid && x.InventoryModeId == 10008).ToList();
                    if (otherCharge != null && otherCharge.Count>0)
                    {
                        List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                        foreach (var ocdata in otherCharge)
                        {
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.Id = ocdata.Id;
                            ent.TXID = ocdata.TXID;
                            ent.OtherChargeId = ocdata.OtherChargeId;
                            if (ocdata.OtherChargeId > 0)
                            {
                                M_POOtherChargesMasterEntity ocmaster = new M_POOtherChargesMasterEntity();
                                string OCname = db.M_POOtherChargesMaster.Where(x => x.id == ocdata.OtherChargeId).Select(x => x.POOtherChargeName).FirstOrDefault();
                                ocmaster.POOtherChargeName = OCname;
                                ent.OCMaster = ocmaster;
                            }

                            ent.OtherChargeValue = ocdata.OtherChargeValue;
                            ent.GST = ocdata.GST;
                            ent.InventoryModeId = ocdata.InventoryModeId;
                            ent.Company_ID = ocdata.Company_ID;
                            ent.BranchId = ocdata.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = ocdata.CreatedBy;
                            oclist.Add(ent);
                        }
                        entity.POOtherChargeEntity = oclist;
                    }


                    List<SRM_GRNItemEntity> grnitemlist = new List<SRM_GRNItemEntity>();
                    var itemlist = db.SRM_GRNItem.Where(x => x.ItemInwardId == id).ToList();
                    foreach (var item in itemlist)
                    {
                        SRM_GRNItemEntity grnitem = new SRM_GRNItemEntity();
                        grnitem.ID = item.ID;
                        grnitem.ItemId = item.ItemId;
                        int IAid = Convert.ToInt32(item.Item_Attribute_ID);
                        int ISid = 0;
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        if (item.M_ItemMaster != null)
                        {
                            itementity.Id = item.M_ItemMaster.Id;
                            itementity.ItemName = getItemName(item.M_ItemMaster.Id, IAid, ISid);
                            grnitem.M_ItemMaster = itementity;
                        }
                        grnitem.Item_Attribute_ID = item.Item_Attribute_ID > 0 ? item.Item_Attribute_ID : 0;
                        grnitem.HSNCode = item.HSNCode;
                        grnitem.gstRate = item.gstRate;
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        if (item.M_UnitMaster != null)
                        {
                            unitentity.Id = item.M_UnitMaster.Id;
                            unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                            grnitem.M_UnitMaster = unitentity;
                        }
                        grnitem.PO_Qty = item.PO_Qty;
                      
                        grnitem.Accepted_Qty = item.Accepted_Qty;
                        grnitem.Balance_Qty = item.Balance_Qty;
                        grnitem.Challan_Qty = item.Challan_Qty;
                        grnitem.GRN_Qty = item.GRN_Qty;
                        grnitem.UnitId = item.UnitId;

                        List<string> rawimgdata = new List<string>();
                        long Quoid = (long)db.SRM_PurchaseOrderMaster.Where(x => x.Id == record.POid).Select(x => x.QuotationId).FirstOrDefault();
                        long SPR = (long)db.SRM_QuotationMaster.Where(x => x.Id == Quoid).Select(x => x.PR_Id).FirstOrDefault();
                        long bomid = (long)db.SRM_PurchaseRequestMaster.Where(x => x.Id == SPR).Select(x => x.BOM_Id).FirstOrDefault();
                        grnitem.IsReadymade = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == bomid).Select(x => x.IsReadymate).FirstOrDefault();
                        long bomrawid = (long)db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == bomid && x.ItemId == item.ItemId && x.Item_Attribute_ID == item.Item_Attribute_ID).Select(x => x.ID).FirstOrDefault();
                        var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == bomrawid).ToList();
                        if (rawimg != null)
                        {
                            foreach (var img in rawimg)
                            {
                                if (img.image != "")
                                {
                                    rawimgdata.Add(img.image);
                                }
                            }
                        }
                        grnitem.rawitemimg_list = rawimgdata;
                        

                        string PIvalstr = "";
                        string PIidstr = "";
                        var ItemInId = db.M_ItemInwardMaster.Where(x => x.ItemInward_ID == item.ID && x.InventoryModeId==1).ToList();
                        foreach(var IinData in ItemInId)
                        {
                            var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == IinData.ID).ToList();
                            PIidstr = "";
                            foreach (var PIdata in ProInfo)
                            {
                                PIvalstr = PIvalstr + PIdata.Dynamic_Controls_value + "~";
                                PIidstr = PIidstr + PIdata.Dynamic_Controls_Id + ",";
                            }
                            if (PIvalstr != "")
                            {
                                PIvalstr = PIvalstr + IinData.ItemQty + "#";
                            }
                        }

                        if (PIvalstr != "")
                        {
                            grnitem.itemPIval = PIvalstr.Remove(PIvalstr.Length - 1, 1);
                        }
                        else
                        {
                            grnitem.itemPIval = "";
                        }

                        if (PIidstr != "")
                        {
                            grnitem.itemPIId = PIidstr.Remove(PIidstr.Length - 1, 1);
                        }
                        else
                        {
                            grnitem.itemPIId = "";
                        }
                        

                        string Batchvalstr = "";
                        foreach (var IinData in ItemInId)
                        {
                            var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == IinData.ID && x.Dynamic_Controls_Id == 4).ToList();
                            ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == IinData.ID && x.Dynamic_Controls_Id == 3).ToList();
                            foreach (var PIdata in ProInfo)
                            {
                                Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                            }
                            if (Batchvalstr != "")
                            {
                                Batchvalstr = Batchvalstr + IinData.ID + "#";
                            }
                        }

                        if (Batchvalstr != "")
                        {
                            grnitem.BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                        }
                        else
                        {
                            grnitem.BatchList = "";
                        }
                        
                        grnitemlist.Add(grnitem);

                    }
                    entity.SRM_GRNItemlist = grnitemlist;


                }

            }
            return entity;
        }

        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public List<SRM_GRNInwardEntity> get(DateTime from, DateTime to)
        {
            List<SRM_GRNInwardEntity> list = new List<SRM_GRNInwardEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_GRNInward.ToList().Where(x => x.IsActive == true && x.InwardDate >= from && x.InwardDate <= to);
                foreach (var item in records)
                {
                    SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
                    entity.ID = item.ID;
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WarehouseMaster = locentity;
                        }
                    }
                    
                    entity.SupplierId = item.SupplierId;
                    if (entity.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id ==entity.SupplierId).FirstOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    entity.GRNNo = item.GRNNo;
                    entity.InwardDate = item.InwardDate;
                    entity.ChallanNO = item.ChallanNO;
                    entity.ChallanDate = item.ChallanDate;
                    entity.InvoiceNo = item.InvoiceNo;
                    entity.InvoiceDate = item.InvoiceDate;
                    entity.DepartmentId = item.DepartmentId;
                    entity.InwardBy = item.InwardBy;
                    entity.InvoiceCreated = item.InvoiceCreated;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.ProcessingStatusId = item.ProcessingStatusId;
                    entity.InvoiceAmount = item.InvoiceAmount;
                    entity.EwayBillNo = item.EwayBillNo;
                    entity.TruckNo = item.TruckNo;
                    entity.Transporter = item.Transporter;
                    entity.IsActive = item.IsActive;

                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    list.Add(entity);
                }
            }

            return list;

        }
        public List<SRM_GRNInwardEntity> get()
        {
            List<SRM_GRNInwardEntity> list = new List<SRM_GRNInwardEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_GRNInward.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
                    entity.ID = item.ID;
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WarehouseMaster = locentity;
                        }
                    }
                    
                    entity.SupplierId = item.SupplierId;
                    if (entity.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == entity.SupplierId).FirstOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    entity.GRNNo = item.GRNNo;
                    entity.InwardDate = item.InwardDate;
                    entity.ChallanNO = item.ChallanNO;
                    entity.ChallanDate = item.ChallanDate;
                    entity.InvoiceNo = item.InvoiceNo;
                    entity.InvoiceDate = item.InvoiceDate;
                    entity.DepartmentId = item.DepartmentId;
                    entity.InwardBy = item.InwardBy;
                    entity.InvoiceCreated = item.InvoiceCreated;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.ProcessingStatusId = item.ProcessingStatusId;
                    entity.InvoiceAmount = item.InvoiceAmount;
                    entity.EwayBillNo = item.EwayBillNo;
                    entity.TruckNo = item.TruckNo;
                    entity.Transporter = item.Transporter;
                    entity.IsActive = item.IsActive;

                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.POno = db.SRM_PurchaseOrderMaster.Where(x => x.Id == item.POid).Select(x => x.PO_NO).FirstOrDefault();
                    list.Add(entity);
                }
            }

            return list;

        }

        //insert itemcord
        public long Insert(SRM_GRNInwardEntity entity)
        {


            long id = 0;
            long inwarditemid = 0;
            using (var db = new GarmentERPDBEntities())
            {

                var GRNdat = db.SRM_GRNInward.Where(x => x.POid == entity.POid).Select(x => x.ID).ToList();
                foreach (var grnid in GRNdat)
                {
                    MFG_WORK_ORDER_DETAIL detail1 = new MFG_WORK_ORDER_DETAIL();
                    var grn = db.SRM_GRNItem.Where(x => x.ItemInwardId == grnid).ToList();
                    foreach (var wd in grn)
                    {
                        wd.Balance_Qty = 0;
                        db.SaveChanges();
                    }
                }

                SRM_GRNInward item = new SRM_GRNInward();
                item.ID = entity.ID;
                item.WarehouseId = entity.WarehouseId;
                item.SupplierId = entity.SupplierId;
                item.InwardDate = entity.InwardDate;
                item.ChallanDate = entity.ChallanDate;
                item.InvoiceDate = entity.InvoiceDate;
                item.DepartmentId = entity.DepartmentId;
                item.InwardBy = entity.InwardBy;
                item.InvoiceCreated = entity.InvoiceCreated;
                item.CreatedBy = entity.CreatedBy;
                item.CreatedDate = DateTime.Now;
                item.ProcessingStatusId = entity.ProcessingStatusId;
                item.POid = entity.POid;
                item.InvoiceAmount = entity.InvoiceAmount;
                item.EwayBillNo = entity.EwayBillNo;
                item.TruckNo = entity.TruckNo;
                item.Transporter = entity.Transporter;
                item.IsActive = entity.IsActive;
                item.IsStatus = entity.IsStatus;
                item.Company_ID = entity.Company_ID;
                item.BranchId = entity.BranchId;
                item.IsReadymade = entity.IsReadymade;
                if (entity.IsReadymade == 0)
                {
                    item.GRNNo = "RM-" + Convert.ToString(getnextgrnno());

                    item.InvoiceNo = "RM-" + Convert.ToString(getnextinvno());
                    item.ChallanNO = "RM-" + Convert.ToString(getnextchnno());
                }
                else
                {
                    item.GRNNo = "RF-" + Convert.ToString(getnextgrnno());

                    item.InvoiceNo = "RF-" + Convert.ToString(getnextinvno());
                    item.ChallanNO = "RF-" + Convert.ToString(getnextchnno());
                }

                db.SRM_GRNInward.Add(item);
                db.SaveChanges();
                id = item.ID;
                if (id > 0)
                {
                    foreach (var subitem in entity.SRM_GRNItemlist)
                    {
                        SRM_GRNItem itemrecord = new SRM_GRNItem();
                        itemrecord.ItemInwardId = id;
                        itemrecord.ItemId = subitem.ItemId;
                        itemrecord.Item_Attribute_ID = subitem.Item_Attribute_ID;
                        itemrecord.HSNCode = subitem.HSNCode;
                        itemrecord.gstRate = subitem.gstRate;
                        itemrecord.PO_Qty = subitem.PO_Qty;
                        itemrecord.UnitId = subitem.UnitId;
                        itemrecord.Accepted_Qty = subitem.Accepted_Qty;
                        itemrecord.Balance_Qty = subitem.Balance_Qty;
                        itemrecord.Challan_Qty = subitem.Challan_Qty;
                        itemrecord.GRN_Qty = subitem.GRN_Qty;
                        itemrecord.IsActive = true;
                        itemrecord.Createddate = DateTime.Now;
                        itemrecord.CreatedBy = subitem.CreatedBy;
                        itemrecord.Company_ID = entity.Company_ID;
                        itemrecord.BranchId = entity.BranchId;

                        db.SRM_GRNItem.Add(itemrecord);
                        db.SaveChanges();
                        inwarditemid = itemrecord.ID;

                        string[] itemPI = subitem.itemPIval.Split('#');
                        string PIids = subitem.itemPIId;
                        
                        foreach (var myPIdata in itemPI)
                        {
                            if (inwarditemid > 0)
                            {
                                string[] PIsplt = myPIdata.Split('~');
                                string[] PIidsplt = PIids.Split(',');
                                int len = PIsplt.Length;
                                M_ItemInwardMaster inwardrecord = new M_ItemInwardMaster();
                                inwardrecord.ItemInward_ID = inwarditemid;
                                inwardrecord.Item_ID = subitem.ItemId;
                                inwardrecord.Item_Attribute_ID = subitem.Item_Attribute_ID;
                                inwardrecord.ItemQty = Convert.ToDecimal(PIsplt[len-1]);
                                inwardrecord.UOM = subitem.UnitId;
                                inwardrecord.BalanceQty = subitem.Balance_Qty;
                                inwardrecord.IsActive = true;
                                inwardrecord.InventoryModeId = 1;
                                inwardrecord.CreatedDate = DateTime.Now;
                                inwardrecord.CreatedBy = subitem.CreatedBy;
                                inwardrecord.Company_ID = entity.Company_ID;
                                inwardrecord.BranchId = entity.BranchId;


                                db.M_ItemInwardMaster.Add(inwardrecord);
                                db.SaveChanges();
                                long IIid = inwardrecord.ID;
                                int k = 0;
                                foreach (var pidata in PIidsplt)
                                {
                                    M_Product_Info prodinfo = new M_Product_Info();
                                    prodinfo.ItemInward_ID = IIid;
                                    prodinfo.Dynamic_Controls_Id = Convert.ToInt64(pidata);
                                    prodinfo.Dynamic_Controls_value = Convert.ToString(PIsplt[k]);
                                    prodinfo.Created_Date = DateTime.Now;
                                    prodinfo.Created_By = subitem.CreatedBy;
                                    prodinfo.Company_ID = entity.Company_ID;
                                    prodinfo.BranchId = entity.BranchId;
                                    db.M_Product_Info.Add(prodinfo);
                                    db.SaveChanges();
                                    k++;
                                }
                            }
                        }
                    }
                }

                if(id>0)
                {
                    List<bool> POIdTF = new List<bool>();
                    var rec1 = db.SRM_GRNInward.Where(x => x.POid == entity.POid).Select(x => x.ID).ToList();
                    POIdTF.Clear();
                    foreach (var dat2 in rec1)
                    {
                        var recd1 = db.SRM_GRNItem.Where(x => x.ItemInwardId == dat2).ToList();

                        foreach (var dat3 in recd1)
                        {
                            if (dat3.Balance_Qty == 0)
                            {
                                POIdTF.Add(true);
                            }
                            else
                            {
                                POIdTF.Add(false);
                            }
                        }
                    }
                    int chki = POIdTF.Count(ai => ai == false);
                    if (chki == 0)
                    {
                        var data = db.SRM_PurchaseOrderMaster.Find(entity.POid);
                        data.IsStatus = 4;
                        data.Updateddate = DateTime.Now;
                        data.Updatedby = entity.CreatedBy;
                        db.SaveChanges();

                        var indata = db.SRM_GRNInward.Where(x => x.POid == entity.POid).Select(x=>x.ID).ToList();
                        foreach(var did in indata)
                        {
                            var GRNdata = db.SRM_GRNInward.Find(did);
                            GRNdata.IsStatus = 4;
                            GRNdata.UpdatedDate = DateTime.Now;
                            GRNdata.UpdatedBy = entity.CreatedBy;
                            db.SaveChanges();
                        }
                        
                    }
                }
            }
            return id;
        }

        public string getnextpono()
        {
            string no;
            no = "PO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                //need to write code to get from supplier PO table.
                var record = db.CRM_EnquiryMaster.Max(x => x.Id) + 1;
                no = no + "-" + record.ToString();
            }
            return no;
        }

        public bool Delete(long id,int Userid)
        {
            bool ISdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNInward.Find(id);
                record.IsActive = false;
                record.UpdatedBy = Userid;
                record.UpdatedDate = DateTime.Now;
                db.SaveChanges();
                ISdelete = true;

                var poid = db.SRM_GRNInward.Where(x => x.ID == id).Select(x => x.POid).FirstOrDefault();
                List<bool> POIdTF = new List<bool>();
                var rec1 = db.SRM_GRNInward.Where(x => x.POid == poid).Select(x => x.ID).ToList();
                POIdTF.Clear();
                foreach (var dat2 in rec1)
                {
                    var recd1 = db.SRM_GRNItem.Where(x => x.ItemInwardId == dat2).ToList();

                    foreach (var dat3 in recd1)
                    {
                        if (dat3.Balance_Qty == 0)
                        {
                            POIdTF.Add(true);
                        }
                        else
                        {
                            POIdTF.Add(false);
                        }
                    }
                }
                int chki = POIdTF.Count(ai => ai == false);
                if (chki == 0)
                {
                    var data = db.SRM_PurchaseOrderMaster.Find(poid);
                    data.IsStatus = 3;
                    data.Updateddate = DateTime.Now;
                    data.Updatedby = Userid;
                    db.SaveChanges();

                    var indata = db.SRM_GRNInward.Where(x => x.POid == poid).Select(x => x.ID).ToList();
                    foreach (var did in indata)
                    {
                        var GRNdata = db.SRM_GRNInward.Find(did);
                        GRNdata.IsStatus = 3;
                        GRNdata.UpdatedDate = DateTime.Now;
                        GRNdata.UpdatedBy = Userid;
                        db.SaveChanges();
                    }

                }
                
            }
            return ISdelete;
        }

        public string getnextgrnno()
        {
            string no;
            no = "GRN-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNInward.Count() > 0 ? db.SRM_GRNInward.Max(x => x.ID) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public string getnextchnno()
        {
            string no;
            no = "CHN-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNInward.Count() > 0 ? db.SRM_GRNInward.Max(x => x.ID) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public string getnextinvno()
        {
            string no;
            no = "INV-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_GRNInward.Count() > 0 ? db.SRM_GRNInward.Max(x => x.ID) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public string GetbysupplierAdd(long suppid)
        {
            string records;
            using (var db = new GarmentERPDBEntities())
            {
                records = db.M_Ledger_BillingDetails.Where(x => x.Id == suppid && x.Is_Active == true).Select(x => x.Address).SingleOrDefault();

            }
            return records;
        }
        public object GRNReportGet(long id)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_SupplierGRN_Report";
                cmd.Parameters.Add("ID", SqlDbType.BigInt).Value = id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }
        public List<SRM_GRNInwardEntity> getItemBal(long poid, long itemid,int IAttr)
        {
            List<SRM_GRNInwardEntity> entitydata = new List<SRM_GRNInwardEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                //cl_CustomerPO poobj = new cl_CustomerPO();
                var itemdata = db.SRM_GRNInward.Where(x => x.POid == poid && x.IsActive == true).ToList();

                if (itemdata != null)
                {
                    decimal total_ = 0;
                    foreach (var item in itemdata)
                    {
                        SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
                        var detailrecordlist = db.SRM_GRNItem.Where(x => x.ItemInwardId == item.ID && x.ItemId==itemid && x.Item_Attribute_ID== IAttr).ToList();
                        List<SRM_GRNItemEntity> detaillist = new List<SRM_GRNItemEntity>();
                        foreach (var size in detailrecordlist)
                        {
                            SRM_GRNItemEntity detail = new SRM_GRNItemEntity();
                            detail.ItemInwardId = size.ItemInwardId;
                            total_ = total_ +(decimal)size.Accepted_Qty;
                        }
                        entity.TotProduceQty = total_;
                        entitydata.Add(entity);
                    }
                }

            }

            return entitydata;
        }
    }
}
