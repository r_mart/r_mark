﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Supplier
    {
        public List<M_SupplierMasterEntity> get()
        {
            List<M_SupplierMasterEntity> list = new List<M_SupplierMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SupplierMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_SupplierMasterEntity entity = new M_SupplierMasterEntity();
                    entity.Id = item.Id;
                    entity.Suppliercode = item.Suppliercode;
                    entity.Suppliertype = item.Suppliertype;
                    M_SupplierTypeMasterEntity suppliertypeentity = new M_SupplierTypeMasterEntity();
                    if (item.M_SupplierTypeMaster != null)
                    {
                        suppliertypeentity.Id = item.M_SupplierTypeMaster.Id;
                        suppliertypeentity.TypeName = item.M_SupplierTypeMaster.TypeName;
                        entity.M_SupplierTypeMaster = suppliertypeentity;
                    }
                    entity.Suppliername = item.Suppliername;
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    entity.Countryid = item.Countryid;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.CSTRegno = item.CSTRegno;
                    entity.Email = item.Email;
                    entity.GSTNo = item.GSTNo;
                    entity.IsActive = item.IsActive;
                    entity.Phone = item.Phone;
                    entity.Remark = item.Remark;
                    entity.Stateid = item.Stateid;
                    entity.Tinno = item.Tinno;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Updateddate = item.Updateddate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_SupplierMasterEntity GetById(long id)
        {
            //M_SupplierMasterEntity list = new M_SupplierMasterEntity();
            M_SupplierMasterEntity entity = new M_SupplierMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SupplierMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {

                    entity.Id = item.Id;
                    entity.Suppliercode = item.Suppliercode;
                    entity.Suppliertype = item.Suppliertype;
                    M_SupplierTypeMasterEntity suppliertypeentity = new M_SupplierTypeMasterEntity();
                    if (item.M_SupplierTypeMaster != null)
                    {
                        suppliertypeentity.Id = item.M_SupplierTypeMaster.Id;
                        suppliertypeentity.TypeName = item.M_SupplierTypeMaster.TypeName;
                        entity.M_SupplierTypeMaster = suppliertypeentity;
                    }
                    entity.Suppliername = item.Suppliername;
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    entity.Countryid = item.Countryid;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.CSTRegno = item.CSTRegno;
                    entity.Email = item.Email;
                    entity.GSTNo = item.GSTNo;
                    entity.IsActive = item.IsActive;
                    entity.Phone = item.Phone;
                    entity.Remark = item.Remark;
                    entity.Stateid = item.Stateid;
                    entity.Tinno = item.Tinno;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Updateddate = item.Updateddate;
                    //list.Add(entity);
                }
            }
            return entity;
        }
        public long Insert(M_SupplierMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_SupplierMaster item = new M_SupplierMaster();
                item.Id = entity.Id;
                item.Suppliercode = entity.Suppliercode;
                item.Suppliername = entity.Suppliername;
                item.Suppliertype = entity.Suppliertype;
                item.Address = entity.Address;
                item.Cityid = entity.Cityid;
                item.Countryid = entity.Countryid;
                item.CreatedBy = entity.CreatedBy;
                item.Createddate = entity.Createddate;
                item.CSTRegno = entity.CSTRegno;
                item.Email = entity.Email;
                item.GSTNo = entity.GSTNo;
                item.IsActive = entity.IsActive;
                item.Phone = entity.Phone;
                item.Remark = entity.Remark;
                item.Stateid = entity.Stateid;
                item.Tinno = entity.Tinno;
                item.UpdatedBy = entity.UpdatedBy;
                item.Updateddate = entity.Updateddate;
                db.M_SupplierMaster.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        public bool Update(M_SupplierMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SupplierMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.Address = entity.Address;
                record.Cityid = entity.Cityid;
                record.Suppliername = entity.Suppliername;
                record.Countryid = entity.Countryid;
                record.CSTRegno = entity.CSTRegno;
                record.Suppliercode = entity.Suppliercode;
                record.Suppliertype = entity.Suppliertype;
                record.Email = entity.Email;
                record.GSTNo = entity.GSTNo;
                record.IsActive = entity.IsActive;
                record.Phone = entity.Phone;
                record.Remark = entity.Remark;
                record.Stateid = entity.Stateid;
                record.Tinno = entity.Tinno;
                record.UpdatedBy = entity.UpdatedBy;
                record.Updateddate = entity.Updateddate;
                
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_SupplierMaster.Where(x => x.Id == id).FirstOrDefault();
                contact.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }

       //Code For Supplier Auto Search
          public M_SupplierMasterEntity GetSuppliername(long Suppliername)
        {
            //M_SupplierMasterEntity list = new M_SupplierMasterEntity();
            M_SupplierMasterEntity entity = new M_SupplierMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SupplierMaster.Where(x => x.Id == Suppliername && x.IsActive == true);
                foreach (var item in records)
                {
                    //entity.Id = item.Id;                
                    entity.Suppliername = item.Suppliername;                    
                }
            }
            return entity;
        }

    }
}
