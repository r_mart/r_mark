﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Data.Classes.Helper;

namespace Garments_ERP.Data.Admin
{
    public class cl_DailyProductionPlan
    {
        public long Insert(MfgDailyProductionPlanEntity entity)
        {
            long PlanID = 0;
            using (var db = new GarmentERPDBEntities())
            {
                MFG_PRODUCTION_DAILY_PLAN record = new MFG_PRODUCTION_DAILY_PLAN();
                record.MonthlyPlanID = entity.MonthlyPlanID;
                record.ProductionDate = entity.ProductionDate;
                record.StyleID = entity.StyleID;
                record.Daily_Target_Qty = entity.DailyPlanned_Qty;
                record.ToolID = entity.ToolID;
                record.WorkCenterID = entity.WorkCenterID;
                record.OperatorID = entity.OperatorID;
                record.MachineID = entity.MachineID;
                record.UnitID = entity.UnitID;
                record.CreatedBy = entity.CreatedBy;
                record.CreatedDate = entity.CreatedDate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                db.MFG_PRODUCTION_DAILY_PLAN.Add(record);
                db.SaveChanges();
                PlanID = record.ID;

                if (PlanID > 0)
                {
                    foreach (var item in entity.StyleSizeDetails)
                    {
                        MFG_PRODUCTION_DAILY_PLAN_DETAILS detailentity = new MFG_PRODUCTION_DAILY_PLAN_DETAILS();
                        try
                        {
                            detailentity.DailyPlanID = PlanID;
                            detailentity.SizeID = item.SizeID;
                            detailentity.Attribute_ID = item.Attribute_ID;
                            detailentity.Attribute_Value_ID = item.Attribute_Value_ID;
                            detailentity.Target_Qty = item.PlannedQuantity;
                            detailentity.Produced_Qty = item.ProducedQuantity;
                            detailentity.CreatedBy = entity.CreatedBy;
                            detailentity.CreatedDate = entity.CreatedDate;
                            detailentity.Company_ID = entity.Company_ID;
                            detailentity.BranchId = entity.BranchId;
                            db.MFG_PRODUCTION_DAILY_PLAN_DETAILS.Add(detailentity);
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                        }

                    }
                }
            }

            return PlanID;
        }
        public List<MfgDailyProductionPlanEntity> Get()
        {
            List<MfgDailyProductionPlanEntity> list = new List<MfgDailyProductionPlanEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_PRODUCTION_DAILY_PLAN.ToList();

                foreach (var item in records)
                {
                    MfgDailyProductionPlanEntity entity = new MfgDailyProductionPlanEntity();
                    entity.ID = item.ID;
                    //entity.PlanNo = item.PlanNo;
                    //entity.WorkOrderID = item.WorkOrderID;
                    //entity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                    //entity.StyleID = item.StyleID;
                    //entity.StyleName = item.M_StyleMaster.Stylename;
                    //entity.PlanDateString = Convert.ToDateTime(item.PlanDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    //entity.StartDateString = Convert.ToDateTime(item.StartDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    //entity.TargetDateString = Convert.ToDateTime(item.TargetDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    //entity.Target_Qty = item.Target_Qty;
                    //entity.ApprovedStatus = (item.ApprovedStatus == null || item.ApprovedStatus == 0) ? "No" : "Yes";
                    //entity.CreatedDate = item.CreatedDate;
                    list.Add(entity);
                }

            }

            return list;
        }
        public MfgDailyProductionPlanEntity Getbyid(long id)
        {
            MfgDailyProductionPlanEntity entity = new MfgDailyProductionPlanEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_PRODUCTION_DAILY_PLAN.Find(id);
                if (record != null)
                {
                    entity.ID = record.ID;
                    //entity.PlanNo = record.PlanNo;
                    //entity.POID = record.POID;
                    //entity.WorkOrderID = record.WorkOrderID;
                    //entity.CustomerID = record.CustomerID;

                    //entity.DepartmentID = record.DepartmentID;
                    //entity.StyleID = record.StyleID;
                    //entity.PlanDate = record.PlanDate;
                    //entity.PlanDateString = Convert.ToDateTime(record.PlanDate).ToString(Helper.DATE_FORMAT_FOR_EDIT);
                    //entity.StartDateString = Convert.ToDateTime(record.StartDate).ToString(Helper.DATE_FORMAT_FOR_EDIT);
                    //entity.TargetDateString = Convert.ToDateTime(record.TargetDate).ToString(Helper.DATE_FORMAT_FOR_EDIT);

                    //entity.SampleReferenceNo = record.SampleReferenceNo;
                    //entity.StartDate = record.StartDate;
                    //entity.ProcessCycleID = record.ProcessCycleID;
                    //entity.Target_Qty = record.Target_Qty;
                    //entity.UnitID = record.UnitID;
                    //entity.TargetDate = record.TargetDate;
                    //entity.WorkCenterID = record.WorkCenterID;
                    //entity.ToolID = record.ToolID;
                    //entity.OperatorID = record.OperatorID;
                    //entity.Comment = record.Comment;
                    //entity.Approved = record.ApprovedStatus;

                }
            }
            return entity;
        }

    }
}
