﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes
{
    public class cl_ReturnOrder
    {
        cl_MaterialIssue Mobj = new cl_MaterialIssue();
        public string getnextROno()
        {
            string no;
            no = "RO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.T_ISSUE_RETURNS.Count() > 0 ? db.T_ISSUE_RETURNS.Max(x => x.ID) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public T_ISSUE_RETURNSEntity GetById(long id)
        {
            try
            {
                T_ISSUE_RETURNSEntity item = new T_ISSUE_RETURNSEntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var entity = db.T_ISSUE_RETURNS.Where(x => x.ID == id).FirstOrDefault();
                    if (entity != null)
                    {
                        item.ID = entity.ID;
                        item.RETURNS_DATE = entity.RETURNS_DATE;
                        item.RETURNS_DESC = entity.RETURNS_DESC;
                        item.CREATED_BY = entity.CREATED_BY;
                        item.CREATED_ON = DateTime.Now;
                        item.Company_ID = entity.Company_ID;
                        item.BranchId = entity.BranchId;
                        item.STYPE = entity.STYPE;
                        item.BUYER_SELLER = entity.BUYER_SELLER;
                        if (entity.STYPE == "customer" || entity.STYPE == "supplier")
                        {
                            string type = entity.STYPE == "customer" ? "1" : "2";
                            var custname = db.M_Ledgers.Where(x => x.Ledger_Id == entity.BUYER_SELLER && x.LedgerType_Id.Contains(type) && x.Is_Active == true).Select(x => x.Ledger_Name).FirstOrDefault();
                            M_LedgersEntity lent = new M_LedgersEntity();
                            lent.Ledger_Name = custname;
                            item.MLedgerEntity = lent;
                        }
                        else
                        {
                            var Empname = db.Employees.Where(x => x.UserId == entity.BUYER_SELLER && x.IsActive == true).Select(x => x.Name).FirstOrDefault();
                            EmployeeEntity Empent = new EmployeeEntity();
                            Empent.Name = Empname;
                            item.empentity = Empent;
                        }

                        item.PO_ID = entity.PO_ID;
                        if (entity.MODE_ID == 10009 || entity.MODE_ID == 2)
                        {
                            item.POno = db.CRM_CustomerPOMaster.Where(x => x.Id == entity.PO_ID && x.IsActive == true).Select(x => x.POno).FirstOrDefault();
                        }
                        else if (entity.MODE_ID == 10008)
                        {
                            item.POno = db.SRM_PurchaseOrderMaster.Where(x => x.Id == entity.PO_ID && x.IsActive == true).Select(x => x.PO_NO).FirstOrDefault();
                        }
                        item.PO_DATE = entity.PO_DATE;
                        item.WO_ID = entity.WO_ID;
                        if (entity.WO_ID > 0)
                        {
                            item.wono = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == entity.WO_ID && x.IsActive == true).Select(x => x.WorkOrderNo).FirstOrDefault();
                            item.WO_DATE = entity.WO_DATE;
                        }
                        else
                        {
                            item.wono = "";
                        }

                        item.INVOICE_ID = entity.INVOICE_ID;
                        if (entity.INVOICE_ID > 0)
                        {
                            item.invno = db.T_Invoice_Challan.Where(x => x.InvoiceId == entity.INVOICE_ID).Select(x => x.InvoiceNo).FirstOrDefault();
                            item.INV_DATE = entity.INV_DATE;
                        }
                        else
                        {
                            item.invno = "";
                        }
                        item.MATERIAL_ISSUE_ID = entity.MATERIAL_ISSUE_ID;
                        if (entity.MATERIAL_ISSUE_ID > 0)
                        {
                            item.MI_NO = db.MFG_MaterialIssueMaster.Where(x => x.Id == entity.MATERIAL_ISSUE_ID && x.IsActive == true).Select(x => x.MaterialIssueNo).FirstOrDefault();
                        }
                        else
                        {
                            item.MI_NO = "";
                        }

                        item.TRANSFER_ID = entity.TRANSFER_ID;
                        if (entity.TRANSFER_ID > 0)
                        {
                            item.TRANSFER_DATE = entity.TRANSFER_DATE;
                            item.Tno = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == entity.TRANSFER_ID).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                        }
                        else
                        {
                            item.Tno = "";
                        }
                        item.MODE_ID = entity.MODE_ID;
                        item.RETURN_NO = entity.RETURN_NO;

                        List<T_ISSUE_RETURNS_DETAILEntity> list = new List<T_ISSUE_RETURNS_DETAILEntity>();
                        var listdata = db.T_ISSUE_RETURNS_DETAIL.Where(x => x.RETURNS_ID == entity.ID).ToList();
                        foreach (var items in listdata)
                        {
                            T_ISSUE_RETURNS_DETAILEntity entitys = new T_ISSUE_RETURNS_DETAILEntity();
                            entitys.ITEM_ID = items.ITEM_ID;
                            entitys.ITEM_ATTRIBUTE_ID = items.ITEM_ATTRIBUTE_ID;
                            int ISid = 0;
                            int iaid = Convert.ToInt32(items.ITEM_ATTRIBUTE_ID);
                            long itemid = Convert.ToInt64(items.ITEM_ID);
                            string itemname = Mobj.getItemName(itemid, iaid, ISid);
                            M_ItemMasterEntity itement = new M_ItemMasterEntity();
                            itement.ItemName = itemname;
                            entitys.ItemMaster = itement;
                            entitys.HSN_CODE = items.HSN_CODE;
                            entitys.UNIT = Convert.ToString(items.UNIT);
                            long unitid = Convert.ToInt64(items.UNIT);
                            M_UnitMasterEntity uent = new M_UnitMasterEntity();
                            if (unitid > 0)
                            {
                                string uniname = db.M_UnitMaster.Where(x => x.Id == unitid && x.IsActive == true).Select(x => x.ItemUnit).FirstOrDefault();
                                uent.ItemUnit = uniname;
                            }
                            else
                            {
                                uent.ItemUnit = "";
                            }
                            entitys.unitent = uent;


                            entitys.PO_TRANSFER_QTY = items.PO_TRANSFER_QTY;
                            entitys.ISSUED_QTY = items.ISSUED_QTY;
                            long batchno =Convert.ToInt64(items.BATCH_LOT_ID);
                            List<M_Product_InfoEntity> plist = new List<M_Product_InfoEntity>();
                            if (batchno > 0)
                            {
                                M_Product_InfoEntity pent = new M_Product_InfoEntity();
                                long inwid = Convert.ToInt64(batchno);
                                var pdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwid && x.Dynamic_Controls_Id == 4).FirstOrDefault();
                                if (pdata != null)
                                {
                                    pent.Dynamic_Controls_value = pdata.Dynamic_Controls_value;
                                    pent.ItemInward_ID = inwid;
                                }
                                else
                                {
                                    pdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwid && x.Dynamic_Controls_Id == 3).FirstOrDefault();
                                    if (pdata != null)
                                    {
                                        pent.Dynamic_Controls_value = pdata.Dynamic_Controls_value;
                                        pent.ItemInward_ID = inwid;
                                    }
                                }
                                plist.Add(pent);
                            }

                            entitys.BatchList = plist;
                            entitys.RETURNED_QTY = items.RETURNED_QTY;
                            entitys.BALANCED_QTY = items.BALANCED_QTY;
                            list.Add(entitys);
                        }
                        item.T_ISSUE_RTRN_DETAILEntity = list;
                    }
                }

                return item;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<T_ISSUE_RETURNSEntity> get()
        {
            try
            {
                List<T_ISSUE_RETURNSEntity> list = new List<T_ISSUE_RETURNSEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var rodata = db.T_ISSUE_RETURNS.ToList();
                    if(rodata!=null)
                    {
                        foreach(var entity in rodata)
                        {
                            T_ISSUE_RETURNSEntity item = new T_ISSUE_RETURNSEntity();
                            item.ID = entity.ID;
                            item.RETURNS_DATE = entity.RETURNS_DATE;
                            item.RETURNS_DESC = entity.RETURNS_DESC;
                            item.CREATED_BY = entity.CREATED_BY;
                            item.CREATED_ON = DateTime.Now;
                            item.Company_ID = entity.Company_ID;
                            item.BranchId = entity.BranchId;
                            item.STYPE = entity.STYPE;
                            item.BUYER_SELLER = entity.BUYER_SELLER;
                            if(entity.STYPE=="customer" || entity.STYPE == "supplier")
                            {
                                string type = entity.STYPE == "customer" ? "1" : "2";
                                var custname = db.M_Ledgers.Where(x => x.Ledger_Id == entity.BUYER_SELLER && x.LedgerType_Id.Contains(type) && x.Is_Active==true).Select(x => x.Ledger_Name).FirstOrDefault();
                                M_LedgersEntity lent = new M_LedgersEntity();
                                lent.Ledger_Name = custname;
                                item.MLedgerEntity = lent;
                            }
                            else
                            {
                                var Empname = db.Employees.Where(x => x.UserId == entity.BUYER_SELLER && x.IsActive == true).Select(x => x.Name).FirstOrDefault();
                                EmployeeEntity Empent = new EmployeeEntity();
                                Empent.Name = Empname;
                                item.empentity = Empent;
                            }

                            item.PO_ID = entity.PO_ID;
                            if(entity.MODE_ID== 10009 || entity.MODE_ID == 2)
                            {
                                item.POno = db.CRM_CustomerPOMaster.Where(x => x.Id == entity.PO_ID && x.IsActive == true).Select(x => x.POno).FirstOrDefault();
                            }
                            else if (entity.MODE_ID == 10008)
                            {
                                item.POno = db.SRM_PurchaseOrderMaster.Where(x => x.Id == entity.PO_ID && x.IsActive == true).Select(x => x.PO_NO).FirstOrDefault();
                            }
                            item.PO_DATE = entity.PO_DATE;
                            item.WO_ID = entity.WO_ID;
                            if(entity.WO_ID>0)
                            {
                                item.wono = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == entity.WO_ID && x.IsActive == true).Select(x => x.WorkOrderNo).FirstOrDefault();
                                item.WO_DATE = entity.WO_DATE;
                            }
                            else
                            {
                                item.wono = "";
                            }
                            
                            item.INVOICE_ID = entity.INVOICE_ID;
                            if (entity.INVOICE_ID > 0)
                            {
                                item.invno = db.T_Invoice_Challan.Where(x => x.InvoiceId == entity.INVOICE_ID).Select(x => x.InvoiceNo).FirstOrDefault();
                                item.INV_DATE = entity.INV_DATE;
                            }
                            else
                            {
                                item.invno = "";
                            }
                            item.MATERIAL_ISSUE_ID = entity.MATERIAL_ISSUE_ID;
                            if(entity.MATERIAL_ISSUE_ID>0)
                            {
                                item.MI_NO = db.MFG_MaterialIssueMaster.Where(x => x.Id == entity.MATERIAL_ISSUE_ID && x.IsActive == true).Select(x=>x.MaterialIssueNo).FirstOrDefault();
                            }
                            else
                            {
                                item.MI_NO = "";
                            }

                            item.TRANSFER_ID = entity.TRANSFER_ID;
                            if (entity.TRANSFER_ID > 0)
                            {
                                item.TRANSFER_DATE = entity.TRANSFER_DATE;
                                item.Tno = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == entity.TRANSFER_ID).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                            }
                            else
                            {
                                item.Tno = "";
                            }
                            item.MODE_ID = entity.MODE_ID;
                            item.RETURN_NO = entity.RETURN_NO;
                            list.Add(item);

                        }
                    }
                }

                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNS_DETAILEntity> GetInvoiceItem(long Id)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    List<T_ISSUE_RETURNS_DETAILEntity> list = new List<T_ISSUE_RETURNS_DETAILEntity>();
                    var listdata = db.T_InvoiceChallan_ProductDetails.Where(x => x.InvoiceId == Id).ToList();
                    foreach(var item in listdata)
                    {
                        T_ISSUE_RETURNS_DETAILEntity entity = new T_ISSUE_RETURNS_DETAILEntity();
                        entity.ITEM_ID = item.ItemId;
                        entity.ITEM_ATTRIBUTE_ID = item.Item_Attribute_ID;
                        int ISid = 0;
                        int iaid = Convert.ToInt32(item.Item_Attribute_ID);
                        long itemid = Convert.ToInt64(item.ItemId);
                        string itemname = Mobj.getItemName(itemid, iaid, ISid);
                        M_ItemMasterEntity itement = new M_ItemMasterEntity();
                        itement.ItemName = itemname;
                        entity.ItemMaster = itement;
                        entity.HSN_CODE = item.HSNNO;
                        entity.UNIT =Convert.ToString(item.UOM);
                        long unitid = Convert.ToInt64(item.UOM);
                        M_UnitMasterEntity uent = new M_UnitMasterEntity();
                        if (unitid>0)
                        {
                            string uniname = db.M_UnitMaster.Where(x => x.Id == unitid && x.IsActive == true).Select(x=>x.ItemUnit).FirstOrDefault();
                            uent.ItemUnit = uniname;
                        }
                        else
                        {
                            uent.ItemUnit = "";
                        }
                        entity.unitent = uent;


                        entity.PO_TRANSFER_QTY = item.POQty;
                        entity.ISSUED_QTY = item.Qty;
                        string batchList = item.Batchid;
                        string[] batch = batchList.Split(',');
                        List<M_Product_InfoEntity> plist = new List<M_Product_InfoEntity>();
                        foreach(var data in batch)
                        {
                            if (data != "")
                            {
                                M_Product_InfoEntity pent = new M_Product_InfoEntity();
                                long inwid = Convert.ToInt64(data);
                                var pdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwid && x.Dynamic_Controls_Id == 4).FirstOrDefault();
                                if (pdata != null)
                                {
                                    pent.Dynamic_Controls_value = pdata.Dynamic_Controls_value;
                                    pent.ItemInward_ID = inwid;
                                }
                                else
                                {
                                    pdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwid && x.Dynamic_Controls_Id == 3).FirstOrDefault();
                                    if (pdata != null)
                                    {
                                        pent.Dynamic_Controls_value = pdata.Dynamic_Controls_value;
                                        pent.ItemInward_ID = inwid;
                                    }
                                }
                                plist.Add(pent);
                            }
                        }
                        entity.BatchList = plist;

                        list.Add(entity);
                    }


                    return list;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNS_DETAILEntity> GetSPOitem(long Id)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    List<T_ISSUE_RETURNS_DETAILEntity> list = new List<T_ISSUE_RETURNS_DETAILEntity>();
                    var listdata = db.SRM_PurchaseOrderItemDetail.Where(x => x.PO_ID == Id).ToList();
                    foreach (var item in listdata)
                    {
                        T_ISSUE_RETURNS_DETAILEntity entity = new T_ISSUE_RETURNS_DETAILEntity();
                        entity.ITEM_ID = item.ItemId;
                        entity.ITEM_ATTRIBUTE_ID = item.Item_Attribute_ID;
                        int ISid = 0;
                        int iaid = Convert.ToInt32(item.Item_Attribute_ID);
                        long itemid = Convert.ToInt64(item.ItemId);
                        string itemname = Mobj.getItemName(itemid, iaid, ISid);
                        M_ItemMasterEntity itement = new M_ItemMasterEntity();
                        itement.ItemName = itemname;
                        entity.ItemMaster = itement;
                        entity.HSN_CODE = db.M_ItemMaster.Where(x=>x.Id== itemid).Select(x=>x.HScode).FirstOrDefault();
                        entity.UNIT = Convert.ToString(item.UnitId);
                        long unitid = Convert.ToInt64(item.UnitId);
                        M_UnitMasterEntity uent = new M_UnitMasterEntity();
                        if (unitid > 0)
                        {
                            string uniname = db.M_UnitMaster.Where(x => x.Id == unitid && x.IsActive == true).Select(x => x.ItemUnit).FirstOrDefault();
                            uent.ItemUnit = uniname;
                        }
                        else
                        {
                            uent.ItemUnit = "";
                        }
                        entity.unitent = uent;
                        entity.PO_TRANSFER_QTY = item.POQty;
                        entity.ISSUED_QTY = item.POQty;
                        list.Add(entity);
                    }


                    return list;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<T_ISSUE_RETURNS_DETAILEntity> GetTransferitem(long Id)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    List<T_ISSUE_RETURNS_DETAILEntity> list = new List<T_ISSUE_RETURNS_DETAILEntity>();
                    var listdata = db.T_ITEM_TRANSFER_DETAIL.Where(x => x.TRANSFER_ID == Id).ToList();
                    foreach (var item in listdata)
                    {
                        T_ISSUE_RETURNS_DETAILEntity entity = new T_ISSUE_RETURNS_DETAILEntity();
                        entity.ITEM_ID = item.ITEM_ID;
                        entity.ITEM_ATTRIBUTE_ID =Convert.ToInt32(item.ITEM_ATTRIBUTE_ID);
                        int ISid = 0;
                        int iaid = Convert.ToInt32(item.ITEM_ATTRIBUTE_ID);
                        long itemid = Convert.ToInt64(item.ITEM_ID);
                        string itemname = Mobj.getItemName(itemid, iaid, ISid);
                        M_ItemMasterEntity itement = new M_ItemMasterEntity();
                        itement.ItemName = itemname;
                        entity.ItemMaster = itement;
                        entity.HSN_CODE = db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.HScode).FirstOrDefault();
                        entity.UNIT = Convert.ToString(item.UNIT_ID);
                        long unitid = Convert.ToInt64(item.UNIT_ID);
                        M_UnitMasterEntity uent = new M_UnitMasterEntity();
                        if (unitid > 0)
                        {
                            string uniname = db.M_UnitMaster.Where(x => x.Id == unitid && x.IsActive == true).Select(x => x.ItemUnit).FirstOrDefault();
                            uent.ItemUnit = uniname;
                        }
                        else
                        {
                            uent.ItemUnit = "";
                        }
                        entity.unitent = uent;
                        entity.PO_TRANSFER_QTY = item.TRANSFER_QTY;
                        entity.ISSUED_QTY = item.TRANSFER_QTY;
                        long batchno =Convert.ToInt64(item.BATCH_NO_ID);
                        List<M_Product_InfoEntity> plist = new List<M_Product_InfoEntity>();
                        if (batchno > 0)
                        {
                            M_Product_InfoEntity pent = new M_Product_InfoEntity();
                            long inwid = Convert.ToInt64(batchno);
                            var pdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwid && x.Dynamic_Controls_Id == 4).FirstOrDefault();
                            if (pdata != null)
                            {
                                pent.Dynamic_Controls_value = pdata.Dynamic_Controls_value;
                                pent.ItemInward_ID = inwid;
                            }
                            else
                            {
                                pdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwid && x.Dynamic_Controls_Id == 3).FirstOrDefault();
                                if (pdata != null)
                                {
                                    pent.Dynamic_Controls_value = pdata.Dynamic_Controls_value;
                                    pent.ItemInward_ID = inwid;
                                }
                            }
                            plist.Add(pent);
                        }
                        entity.BatchList = plist;
                        list.Add(entity);
                    }


                    return list;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long insert(T_ISSUE_RETURNSEntity entity)
        {
            try
            {
                long id = 0;
                using (var db = new GarmentERPDBEntities())
                {
                    T_ISSUE_RETURNS item = new T_ISSUE_RETURNS();
                    item.RETURNS_DATE = entity.RETURNS_DATE;
                    item.RETURNS_DESC = entity.RETURNS_DESC;
                    item.CREATED_BY = entity.CREATED_BY;
                    item.CREATED_ON = DateTime.Now;
                    item.Company_ID = entity.Company_ID;
                    item.BranchId = entity.BranchId;
                    item.STYPE = entity.STYPE;
                    item.BUYER_SELLER = entity.BUYER_SELLER;
                    item.PO_ID = entity.PO_ID;
                    item.PO_DATE = entity.PO_DATE;
                    item.WO_ID = entity.WO_ID;
                    if (entity.WO_ID > 0)
                    {
                        item.WO_DATE = entity.WO_DATE;
                    }
                    item.INVOICE_ID = entity.INVOICE_ID;
                    if (entity.INVOICE_ID > 0)
                    {
                        item.INV_DATE = entity.INV_DATE;
                    }
                    item.MATERIAL_ISSUE_ID = entity.MATERIAL_ISSUE_ID;
                    item.TRANSFER_ID = entity.TRANSFER_ID;
                    if (entity.TRANSFER_ID > 0)
                    {
                        item.TRANSFER_DATE = entity.TRANSFER_DATE;
                    }
                    item.MODE_ID = entity.MODE_ID;
                    item.RETURN_NO = getnextROno();
                    db.T_ISSUE_RETURNS.Add(item);
                    db.SaveChanges();

                    id =Convert.ToInt64(item.ID);
                    if(id>0)
                    {
                        if(entity.T_ISSUE_RTRN_DETAILEntity!=null)
                        {
                            foreach( var ritem in entity.T_ISSUE_RTRN_DETAILEntity)
                            {
                                T_ISSUE_RETURNS_DETAIL rt = new T_ISSUE_RETURNS_DETAIL();
                                rt.RETURNS_ID = Convert.ToDecimal(id);
                                rt.ISSUED_QTY = ritem.ISSUED_QTY;
                                rt.RETURNED_QTY = ritem.RETURNED_QTY;
                                rt.UNIT = ritem.UNIT;
                                rt.ITEM_ID = ritem.ITEM_ID;
                                rt.Company_ID = ritem.Company_ID;
                                rt.BranchId = ritem.BranchId;
                                rt.ITEM_ATTRIBUTE_ID = ritem.ITEM_ATTRIBUTE_ID;
                                rt.HSN_CODE = ritem.HSN_CODE;
                                rt.PO_TRANSFER_QTY = ritem.PO_TRANSFER_QTY;
                                rt.BATCH_LOT_ID = ritem.BATCH_LOT_ID;
                                rt.BALANCED_QTY = ritem.BALANCED_QTY;
                                db.T_ISSUE_RETURNS_DETAIL.Add(rt);
                                db.SaveChanges();
                                id =Convert.ToInt64(rt.ID);
                            }
                        }
                    }


                }
                return id;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
