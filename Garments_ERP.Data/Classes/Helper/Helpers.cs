﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Garments_ERP.Data.Classes.Helper
{
    public static class Helper
    {
        private static string _dateFormat = "dd-MMM-yyyy";
        public static string DATE_FORMAT_FOR_DISPLAY
        {
            get
            {
                return _dateFormat;
            }
        }


        private static string _dateFormatForEdit = "MM-dd-yyyy";
        public static string DATE_FORMAT_FOR_EDIT
        {
            get
            {
                return _dateFormatForEdit;
            }
        }

    }
}

