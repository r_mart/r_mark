﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_ItemColor
    {
    public List<M_ItemColorEntity> get()
    {
        List<M_ItemColorEntity> list = new List<M_ItemColorEntity>();
        using (var db = new GarmentERPDBEntities())
        {
            var records = db.M_ItemColorMaster.ToList().Where(x => x.IsActive == true);
            foreach (var item in records)
            {
                M_ItemColorEntity entity = new M_ItemColorEntity();

                entity.Id = item.Id;
                entity.ItemColor = item.ItemColor;
                entity.ItemColordesc = item.ItemColordesc;
                entity.Createdate = item.Createdate;
                entity.Createdby = item.Createdby;
                entity.Updateddate = item.Updateddate;
                entity.Updatedby = item.Updatedby;
                entity.IsActive = item.IsActive;
                list.Add(entity);
            }

        }
        return list;
    }
    public int Insert(M_ItemColorEntity entity)
    {
        int id = 0;

        M_ItemColorMaster record = new M_ItemColorMaster();
        record.Id = entity.Id;
        record.ItemColor = entity.ItemColor;
        record.ItemColordesc = entity.ItemColordesc;
        record.Createdate = DateTime.Now;
        record.Createdby = entity.Createdby;
        record.Updateddate = entity.Updateddate;
        record.Updatedby = entity.Updatedby;
        record.IsActive = true;
        using (var db = new GarmentERPDBEntities())
        {
            db.M_ItemColorMaster.Add(record);
            db.SaveChanges();
            id = record.Id;
        }

        return id;

    }

    public bool CheckItemColorExistance(string ItemColor, int? Id)
    {
        bool result = true;

        using (var db = new GarmentERPDBEntities())
        {
            if (Id == null)
            {
                var itemcolor = db.M_ItemColorMaster.Where(x => x.ItemColor == ItemColor && x.IsActive == true).FirstOrDefault();
                if (itemcolor != null)
                    result = false;
            }
            else
            {
                var itemcolor = db.M_ItemColorMaster.Where(x => x.ItemColor == ItemColor && x.Id != Id && x.IsActive == true).FirstOrDefault();
                if (itemcolor != null)
                    result = false;
            }


        }
        return result;
    }

    public bool Update(M_ItemColorEntity entity)
    {
        bool isupdated = false;

        using (var db = new GarmentERPDBEntities())
        {
            var record = db.M_ItemColorMaster.Find(entity.Id);
            record.ItemColor = entity.ItemColor;
            record.ItemColordesc = entity.ItemColordesc;
            db.SaveChanges();
            isupdated = true;
        }

        return isupdated;

    }


    public bool Delete(int id)
    {
        bool isdelete = false;
        using (var db = new GarmentERPDBEntities())
        {
            var record = db.M_ItemColorMaster.Find(id);
            record.IsActive = false;
            db.SaveChanges();
            isdelete = true;
        }
        return isdelete;
    }

    }
}
