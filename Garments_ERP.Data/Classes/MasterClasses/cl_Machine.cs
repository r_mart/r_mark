﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Machine
    {
        public long Insert(MachineEntity entity)
        {
            long id = 0;
            MachineMaster record = new MachineMaster();
            record.Id = entity.Id;
            record.Machinename = entity.Machinename;

            record.Machinedesc = entity.Machinedesc;

            record.Machinetypeid = entity.Machinetypeid;
            record.Vendortypeid = entity.Vendortypeid;
            record.MachineSn = entity.MachineSn;
            record.Purchasedate = entity.Purchasedate;
            record.Purchaseprice = entity.Purchaseprice;
            record.Pricecurrency = entity.Pricecurrency;
            record.Inspectiontypeid = entity.Inspectiontypeid;
            record.Inspectionduration = entity.Inspectionduration;
            record.Durationnos = entity.Durationnos;
            record.Inspectiondate = entity.Inspectiondate;
            record.Inspectionnextdate = entity.Inspectionnextdate;
            record.Productioncapacity = entity.Productioncapacity;
            record.CapacityUOMid = entity.CapacityUOMid;
            record.Capacityduration = entity.Capacityduration;
            record.Runwidth = entity.Runwidth;
            record.Runheight = entity.Runheight;
            record.Runlength = entity.Runlength;
            record.LwhUOMid = entity.LwhUOMid;
            record.Remark = entity.Remark;
            record.Createddate = entity.Createddate;
            record.Createdby = entity.Createdby;

            //New Code Added by Rahul on 09-01-2020
            //record.Updateddate = entity.Updateddate;
            //record.Updatedby = entity.Updatedby;
            record.Company_ID = entity.Company_ID;
            record.BranchId = entity.Branch_ID;
            record.IsActive = entity.IsActive;
            using (var db = new GarmentERPDBEntities())
            {
                db.MachineMasters.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;
        }

        public bool Update(MachineEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MachineMasters.Find(entity.Id);
                record.Id = entity.Id;
                record.Machinename = entity.Machinename;
                record.Machinedesc = entity.Machinedesc;
                record.Machinetypeid = entity.Machinetypeid;
                record.Vendortypeid = entity.Vendortypeid;
                record.MachineSn = entity.MachineSn;
                record.Purchasedate = entity.Purchasedate;
                record.Purchaseprice = entity.Purchaseprice;
                record.Pricecurrency = entity.Pricecurrency;
                record.Inspectiontypeid = entity.Inspectiontypeid;
                record.Inspectionduration = entity.Inspectionduration;
                record.Durationnos = entity.Durationnos;
                record.Inspectiondate = entity.Inspectiondate;
                record.Inspectionnextdate = entity.Inspectionnextdate;
                record.Productioncapacity = entity.Productioncapacity;
                record.CapacityUOMid = entity.CapacityUOMid;
                record.Capacityduration = entity.Capacityduration;
                record.Runwidth = entity.Runwidth;
                record.Runheight = entity.Runheight;
                record.Runlength = entity.Runlength;
                record.LwhUOMid = entity.LwhUOMid;
                record.Remark = entity.Remark;

                //New Code Added by Rahul on 09-01-2020
                // record.Createddate = item.Createddate;
                // record.Createdby = item.Createdby;
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                updated = true;

            }
            return updated;

        }

        public bool Delete(long id)
        {
            var deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MachineMasters.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                deleted = true;

            }
            return deleted;
        }

        public MachineEntity GetById(long id)
        {
            MachineEntity record = new MachineEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.MachineMasters.Find(id);
                record.Id = item.Id;
                record.Machinename = item.Machinename;

                record.Machinedesc = item.Machinedesc;

                record.Machinetypeid = item.Machinetypeid;
                record.Vendortypeid = item.Vendortypeid;
                record.MachineSn = item.MachineSn;
                record.Purchasedate = item.Purchasedate;
                record.Purchaseprice = item.Purchaseprice;
                record.Pricecurrency = item.Pricecurrency;
                record.Inspectiontypeid = item.Inspectiontypeid;
                record.Inspectionduration = item.Inspectionduration;
                record.Durationnos = item.Durationnos;
                record.Inspectiondate = item.Inspectiondate;
                record.Inspectionnextdate = item.Inspectionnextdate;
                record.Productioncapacity = item.Productioncapacity;
                record.CapacityUOMid = item.CapacityUOMid;
                record.Capacityduration = item.Capacityduration;
                record.Runwidth = item.Runwidth;
                record.Runheight = item.Runheight;
                record.Runlength = item.Runlength;
                record.LwhUOMid = item.LwhUOMid;
                record.Remark = item.Remark;
                record.Createddate = item.Createddate;
                record.Createdby = item.Createdby;
                record.Updateddate = item.Updateddate;
                record.Updatedby = item.Updatedby;
                record.Company_ID = item.Company_ID;
                record.Branch_ID = item.BranchId;
                M_MachineTypeMasterEntity machinetypeentity = new M_MachineTypeMasterEntity();
                if (item.M_MachineTypeMaster != null)
                {
                    machinetypeentity.Machine_TypeId = item.M_MachineTypeMaster.Machine_TypeId;
                    machinetypeentity.Machine_TypeName = item.M_MachineTypeMaster.Machine_TypeName;
                    record.M_MachineTypeentity = machinetypeentity;
                }

            }
            return record;
        }



        public List<MachineEntity> Get()
        {
            List<MachineEntity> list = new List<MachineEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MachineMasters.ToList().Where(x => x.IsActive == true);
                foreach (var entity in records)
                {
                    MachineEntity record = new MachineEntity();
                    record.Id = entity.Id;
                    record.Machinename = entity.Machinename;

                    record.Machinedesc = entity.Machinedesc;

                    record.Machinetypeid = entity.Machinetypeid;
                    record.Vendortypeid = entity.Vendortypeid;
                    record.MachineSn = entity.MachineSn;
                    record.Purchasedate = entity.Purchasedate;
                    record.Purchaseprice = entity.Purchaseprice;
                    record.Pricecurrency = entity.Pricecurrency;
                    record.Inspectiontypeid = entity.Inspectiontypeid;
                    record.Inspectionduration = entity.Inspectionduration;
                    record.Durationnos = entity.Durationnos;
                    record.Inspectiondate = entity.Inspectiondate;
                    record.Inspectionnextdate = entity.Inspectionnextdate;
                    record.Productioncapacity = entity.Productioncapacity;
                    record.CapacityUOMid = entity.CapacityUOMid;
                    record.Capacityduration = entity.Capacityduration;
                    record.Runwidth = entity.Runwidth;
                    record.Runheight = entity.Runheight;
                    record.Runlength = entity.Runlength;
                    record.LwhUOMid = entity.LwhUOMid;
                    record.Remark = entity.Remark;
                    record.Createddate = entity.Createddate;
                    record.Createdby = entity.Createdby;
                    record.Updateddate = entity.Updateddate;
                    record.Updatedby = entity.Updatedby;
                    record.Company_ID = entity.Company_ID;
                    record.Branch_ID = entity.BranchId;
                    M_MachineTypeMasterEntity machinetypeentity = new M_MachineTypeMasterEntity(); ;
                    if (entity.M_MachineTypeMaster != null)
                    {
                        machinetypeentity.Machine_TypeId = entity.M_MachineTypeMaster.Machine_TypeId;
                        machinetypeentity.Machine_TypeName = entity.M_MachineTypeMaster.Machine_TypeName;
                        record.M_MachineTypeentity = machinetypeentity;
                    }
                    list.Add(record);
                }

            }
            return list;
        }
    }
}
