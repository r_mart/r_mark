﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Data.Admin
{
    public class cl_Item
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public List<M_ProductInfoEntity> GetRackData(long ItemId, int ItemAttr)
        {
            try
            {
                List<M_ProductInfoEntity> list = new List<M_ProductInfoEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.M_ItemInwardMaster.Where(x => x.Item_ID == ItemId && x.Item_Attribute_ID == ItemAttr).ToList();
                    foreach(var item in record)
                    {

                        M_ProductInfoEntity RAEntity = new M_ProductInfoEntity();
                        long inwardid = 0;
                        var Rackdata = db.M_RackAllocation.Where(x => x.ItemId == ItemId && x.Item_Attribute_Id == ItemAttr).ToList();
                        foreach(var Batval in Rackdata)
                        {
                            string[] batsplyt = Batval.BatchLot.Split(',');
                            long iid = 0;
                            foreach (var inval in batsplyt)
                            {
                                iid = Convert.ToInt64(inval);
                                if(item.ID== iid)
                                {
                                    inwardid=Batval.Id;
                                    break;
                                }
                            }
                            if(inwardid>0)
                            {
                                break;
                            }
                        }

                        var PRackdata = db.M_RackAllocation.Where(x => x.Id==inwardid).FirstOrDefault();
                        if (PRackdata != null)
                        {
                            if (PRackdata.warehouseid > 0)
                            {
                                M_WarehouseEntity wareHent = new M_WarehouseEntity();
                                string WHName = db.M_WAREHOUSE.Where(x => x.ID == PRackdata.warehouseid).Select(x => x.SHORT_NAME).FirstOrDefault();
                                RAEntity.Warehouse = WHName;
                            }
                            else
                            {
                                RAEntity.Warehouse = "";
                            }
                            string[] RackstrId = PRackdata.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + rackname + ",";
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }
                            RAEntity.Rack = racknameStr;
                        }
                        else
                        {
                            RAEntity.Warehouse = "";
                            RAEntity.Rack = "";
                        }

                        var productdata = db.M_Product_Info.Where(x => x.ItemInward_ID == item.ID).ToList();
                        string expdat = "", lotno = "", batchno = "", serialno = "";
                        foreach(var PId in productdata)
                        {
                            //Expiry Date
                            if(PId.Dynamic_Controls_Id==1)
                            {
                                expdat = PId.Dynamic_Controls_value;
                            }
                            else if (PId.Dynamic_Controls_Id == 3)//Lot No
                            {
                                lotno = PId.Dynamic_Controls_value;
                            }
                            else if (PId.Dynamic_Controls_Id == 4)
                            {
                                batchno = PId.Dynamic_Controls_value;
                            }
                            else if (PId.Dynamic_Controls_Id == 6)//Serial No
                            {
                                serialno = PId.Dynamic_Controls_value;
                            }
                        }
                        RAEntity.ExpiryDate = expdat == "" ? "" : expdat;
                        RAEntity.LotNo = lotno == "" ? "" : lotno;
                        RAEntity.BatchNo = batchno == "" ? "" : batchno;
                        RAEntity.SerialNo = serialno == "" ? "" : serialno;

                        RAEntity.TotalQty = item.ItemQty;
                        var itemout = db.M_ItemOutwardMaster.Where(x => x.Inward_ID == item.ID).ToList();
                        decimal totoutQty = 0;
                        foreach(var IO in itemout)
                        {
                            totoutQty =Convert.ToDecimal(totoutQty) +Convert.ToDecimal(IO.ItemQty);
                        }
                        RAEntity.ConsumedQty = totoutQty;
                        decimal BalQty = 0;
                        if (item.ItemQty> totoutQty)
                        {
                            BalQty =Convert.ToDecimal(item.ItemQty) -Convert.ToDecimal(totoutQty);
                        }
                        else
                        {
                            BalQty = 0;
                        }
                        RAEntity.BalancedQty = BalQty;
                        if (BalQty > 0)
                        {
                            list.Add(RAEntity);
                        }
                    }

                }
                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<M_InventoryListEntity> GetInventory(string MyJson, int type, XmlDocument doc)
        {
            try
            {
                List<M_InventoryListEntity> list = new List<M_InventoryListEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SP_Inventory(type, MyJson, doc.InnerXml).ToList();
                    foreach (var data in record)
                    {
                        M_InventoryListEntity entity = new M_InventoryListEntity();
                        entity.Id = data.Id;
                        entity.Item_Attribute_ID = data.Item_Attribute_ID;
                        entity.ItemName = data.ItemName;
                        entity.ItemCreateddate = data.ItemCreateddate;
                        entity.ItemUpdateddate = data.ItemUpdateddate;
                        entity.Unit =data.Unit;
                        entity.UnitName = data.UnitName;
                        entity.ItemCategoryid = data.ItemCategoryid;
                        entity.Itemcategory = data.Itemcategory;
                        entity.Itemsubcategoryid = data.Itemsubcategoryid;
                        entity.Itemsubcategory = data.Itemsubcategory;
                        entity.Itemgroupid = data.Itemgroupid;
                        entity.Itemgroup = data.Itemgroup;
                        entity.ItemStyleid = data.ItemStyleid;
                        entity.ItemStyleNo = data.ItemStyleNo;
                        entity.ItemStyledescription = data.ItemStyledescription;
                        entity.Taxrate = data.Taxrate;
                        entity.OpeningStock = data.OpeningStock;
                        entity.PurchaseINQty = data.PurchaseINQty;
                        entity.ProcessQty = data.ProcessQty;
                        entity.BatchInfo = data.BatchInfo;
                        entity.TotalStock = data.TotalStock;
                        entity.OutQty = data.OutQty;
                        entity.BalStock = data.BalStock;
                        entity.HSNCode = data.HSNCode;
                        entity.RACount = data.RACount;
                        list.Add(entity);
                    }
                }
                return list;
                
            }
            catch (Exception)
            {

                throw;
            }

        }


        public long Insert(M_ItemMasterEntity entity,string AttributeValList)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_ItemMaster record = new M_ItemMaster();
                record.Id = entity.Id;
                record.ItemCode = entity.ItemCode;
                record.HScode = entity.HScode;
                record.HSchapter = entity.HSchapter;
                record.ItemName = entity.ItemName;
                record.Specification = entity.Specification;
                record.Unit = entity.Unit;
                record.CurrencyId = entity.CurrencyId;
                record.ItemRate = entity.ItemRate;
                record.Taxrate = entity.Taxrate;
                record.OpeningStock = entity.OpeningStock;
                record.Length = entity.Length;
                record.Width = entity.Width;
                record.Height = entity.Height;
                record.ItemCategoryid = entity.ItemCategoryid;
                record.Itemcolorid = entity.Itemcolorid;
                record.Companyname = entity.Companyname;
                record.Itemsubcategoryid = entity.Itemsubcategoryid;
                record.Brand = entity.Brand;
                record.Shortname = entity.Shortname;
                record.Batchperserial = entity.Batchperserial;
                record.Warehouseid = entity.Warehouseid;
                record.Rackid = entity.Rackid;
                record.Shapeid = entity.Shapeid;
                record.ShadeId = entity.ShadeId;
                record.EOQ = entity.EOQ;
                record.QCrequired = entity.QCrequired;
                record.Packingtypeid = entity.Packingtypeid;
                record.ItemImage = entity.ItemImage;
                record.Description = entity.Description;
                record.Itemgroupid = entity.Itemgroupid;
                record.ItemStyleid = entity.ItemStyleid;
                record.ItemStyleNo = entity.ItemStyleNo;
                record.ItemStyledescription = entity.ItemStyledescription;
                record.CreatedBy = entity.CreatedBy;
                record.Createddate = entity.Createddate;
                //New Code Added by Rahul on 09-01-2020
                //record.Updateddate = entity.Updateddate;
                //record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                db.M_ItemMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
                
            }


            return insertedid;
        }

        public List<M_ItemMasterEntity> get()
        {
            List<M_ItemMasterEntity> list = new List<M_ItemMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemMasterEntity entity = new M_ItemMasterEntity();
                    entity.Id = item.Id;
                    entity.ItemCode = item.ItemCode;
                    entity.ItemName = item.ItemName;
                    entity.HSchapter = item.HSchapter;
                    entity.HScode = item.HScode;
                    entity.Specification = item.Specification;
                    entity.Unit = item.Unit;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.CurrencyId = item.CurrencyId;
                    entity.ItemRate = item.ItemRate;
                    entity.Taxrate = item.Taxrate;
                    entity.OpeningStock = item.OpeningStock;
                    entity.Length = item.Length;
                    entity.Width = item.Width;
                    entity.Height = item.Height;
                    entity.ItemCategoryid = item.ItemCategoryid;
                    if (item.M_ItemCategoryMaster != null)
                    {
                        M_ItemCategoryMasterEntity categoryentity = new M_ItemCategoryMasterEntity();
                        categoryentity.Id = item.M_ItemCategoryMaster.Id;
                        categoryentity.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMaster = categoryentity;
                    }
                    entity.Itemcolorid = item.Itemcolorid;
                    if (item.M_ItemColorMaster != null)
                    {
                        M_ItemColorEntity colorentity = new M_ItemColorEntity();
                        colorentity.Id = item.M_ItemColorMaster.Id;
                        colorentity.ItemColor = item.M_ItemColorMaster.ItemColor;
                        colorentity.ItemColordesc = item.M_ItemColorMaster.ItemColordesc;
                        entity.M_ItemColorMaster = colorentity;
                    }
                    entity.Companyname = item.Companyname;
                    entity.Itemsubcategoryid = item.Itemsubcategoryid;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity subcateentity = new M_ItemSubCategoryMasterEntity();
                        subcateentity.Id = item.M_ItemSubCategoryMaster.Id;
                        subcateentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        subcateentity.itemcategoryid = item.M_ItemSubCategoryMaster.itemcategoryid;
                        entity.M_ItemSubCategoryMaster = subcateentity;
                    }
                    entity.Brand = item.Brand;
                    entity.Shortname = item.Shortname;
                    entity.Batchperserial = item.Batchperserial;
                    entity.Warehouseid = item.Warehouseid;
                    entity.Rackid = item.Rackid;
                    if (item.M_RackMaster != null)
                    {
                        M_RackMasterEntity rackentity = new M_RackMasterEntity();
                        rackentity.Id = item.M_RackMaster.Id;
                        rackentity.Rack = item.M_RackMaster.Rack;
                        rackentity.Rackdesc = item.M_RackMaster.Rackdesc;
                        entity.M_RackMaster = rackentity;
                    }
                    entity.Shapeid = item.Shapeid;
                    entity.EOQ = item.EOQ;
                    entity.QCrequired = item.QCrequired;
                    entity.Packingtypeid = item.Packingtypeid;
                    entity.ItemImage = item.ItemImage;
                    entity.Description = item.Description;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }


        public List<M_ItemMasterEntity> getFinishedItem()
        {
            List<M_ItemMasterEntity> list = new List<M_ItemMasterEntity>();
            DataSet dt = new DataSet();
            using (var db = new GarmentERPDBEntities())
            {
                using (SqlConnection cs = new SqlConnection(con))
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cs;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Attribute";
                    cmd.Parameters.Add("type", SqlDbType.Int).Value = 19; //Get List Of Finished Item
                    cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = "";
                    cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    foreach (DataTable table in dt.Tables)
                    {
                        foreach (DataRow dr in table.Rows)
                        {
                            M_ItemMasterEntity entity = new M_ItemMasterEntity();
                            entity.Id = Convert.ToInt64(dr["Id"]);
                            entity.ItemCode = Convert.ToString(dr["ItemCode"]);
                            entity.ItemName = Convert.ToString(dr["ItemName"]);
                            entity.HSchapter = Convert.ToString(dr["HSchapter"]);
                            entity.HScode = Convert.ToString(dr["HScode"]);
                            entity.Specification = Convert.ToString(dr["Specification"]);
                            entity.Unit = Convert.ToInt32(dr["Unit"]);
                            int unitid = Convert.ToInt32(dr["Unit"]);
                            if (unitid > 0)
                            {
                                M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                                var unitdata = db.M_UnitMaster.Where(x => x.Id == unitid).FirstOrDefault();
                                if (unitdata != null)
                                {
                                    unitentity.Id = unitdata.Id;
                                    unitentity.ItemUnit = unitdata.ItemUnit;
                                    entity.M_UnitMaster = unitentity;
                                }
                            }
                            entity.Taxrate = Convert.ToDecimal(dr["Taxrate"]);
                            entity.OpeningStock = Convert.ToDecimal(dr["OpeningStock"]);
                            entity.Length = Convert.ToDecimal(dr["Length"]);
                            entity.Width = Convert.ToDecimal(dr["Width"]);
                            entity.Height = Convert.ToDecimal(dr["Height"]);
                            entity.ItemCategoryid = Convert.ToInt64(dr["ItemCategoryid"]);
                            long itemcatid = Convert.ToInt64(dr["ItemCategoryid"]);
                            if (itemcatid >0)
                            {
                                M_ItemCategoryMasterEntity categoryentity = new M_ItemCategoryMasterEntity();
                                var ItemCatdata = db.M_ItemCategoryMaster.Where(x => x.Id == itemcatid).FirstOrDefault();
                                if (ItemCatdata != null)
                                {
                                    categoryentity.Id = ItemCatdata.Id;
                                    categoryentity.Itemcategory = ItemCatdata.Itemcategory;
                                    entity.M_ItemCategoryMaster = categoryentity;
                                }
                            }
                            entity.Companyname = Convert.ToString(dr["Companyname"]);
                            entity.Itemsubcategoryid = Convert.ToInt32(dr["Itemsubcategoryid"]);
                            int itemsubcat = Convert.ToInt32(dr["Itemsubcategoryid"]);
                            if (itemsubcat > 0)
                            {
                                M_ItemSubCategoryMasterEntity subcateentity = new M_ItemSubCategoryMasterEntity();
                                var itemsubcatdata = db.M_ItemSubCategoryMaster.Where(x => x.Id == itemsubcat).FirstOrDefault();
                                subcateentity.Id = itemsubcatdata.Id;
                                subcateentity.Itemsubcategory = itemsubcatdata.Itemsubcategory;
                                subcateentity.itemcategoryid = itemsubcatdata.itemcategoryid;
                                entity.M_ItemSubCategoryMaster = subcateentity;
                            }
                            entity.Brand = Convert.ToString(dr["Brand"]);
                            entity.Shortname = Convert.ToString(dr["Shortname"]);
                            entity.EOQ = Convert.ToString(dr["EOQ"])==""?"0": Convert.ToString(dr["EOQ"]);
                            entity.QCrequired = Convert.ToBoolean(dr["QCrequired"]);
                            entity.Packingtypeid = Convert.ToInt32(dr["Packingtypeid"]);
                            entity.Description = Convert.ToString(dr["Description"]);
                            entity.IsActive = Convert.ToBoolean(dr["IsActive"]);
                            entity.Company_ID = Convert.ToInt32(dr["Company_ID"]);
                            entity.Branch_ID = Convert.ToInt32(dr["BranchId"]);
                            list.Add(entity);
                        }
                    }

                }
            }

            
            return list;
        }

        public M_ItemMasterEntity GetById(long id)
        {
            M_ItemMasterEntity entity = new M_ItemMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    
                    entity.Id = item.Id;
                    entity.ItemCode = item.ItemCode;
                    entity.ItemName = item.ItemName;
                    entity.HScode = item.HScode;
                    entity.HSchapter = item.HSchapter;
                    entity.Specification = item.Specification;
                    entity.Unit = item.Unit;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.CurrencyId = item.CurrencyId;
                    entity.ItemRate = item.ItemRate;
                    entity.Taxrate = item.Taxrate;
                    entity.OpeningStock = item.OpeningStock;
                    entity.Length = item.Length;
                    entity.Width = item.Width;
                    entity.Height = item.Height;
                    entity.Itemgroupid = item.Itemgroupid;
                    entity.ItemStyleid=item.ItemStyleid;
                    M_StyleMasterEntity style = new M_StyleMasterEntity();
                    var stylename = db.M_StyleMaster.Where(x => x.Id == item.ItemStyleid).Select(x => x.Stylename).FirstOrDefault();
                    style.Stylename = stylename;
                    entity.StyleName = style;
                    entity.ItemStyleNo = item.ItemStyleNo;
                    entity.ItemStyledescription = item.ItemStyledescription;
                    entity.ItemCategoryid = item.ItemCategoryid;
                   
                    if (item.M_ItemCategoryMaster != null)
                    {
                        M_ItemCategoryMasterEntity categoryentity = new M_ItemCategoryMasterEntity();
                        categoryentity.Id = item.M_ItemCategoryMaster.Id;
                        categoryentity.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMaster = categoryentity;
                    }
                    entity.Itemcolorid = item.Itemcolorid;
                    if (item.M_ItemColorMaster != null)
                    {
                        M_ItemColorEntity colorentity = new M_ItemColorEntity();
                        colorentity.Id = item.M_ItemColorMaster.Id;
                        colorentity.ItemColor = item.M_ItemColorMaster.ItemColor;
                        colorentity.ItemColordesc = item.M_ItemColorMaster.ItemColordesc;
                        entity.M_ItemColorMaster = colorentity;
                    }
                    entity.Companyname = item.Companyname;
                    entity.Itemsubcategoryid = item.Itemsubcategoryid;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity subcateentity = new M_ItemSubCategoryMasterEntity();
                        subcateentity.Id = item.M_ItemSubCategoryMaster.Id;
                        subcateentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        subcateentity.itemcategoryid = item.M_ItemSubCategoryMaster.itemcategoryid;
                        entity.M_ItemSubCategoryMaster = subcateentity;
                    }
                    entity.Brand = item.Brand;
                    entity.Shortname = item.Shortname;
                    entity.Batchperserial = item.Batchperserial;
                    entity.Warehouseid = item.Warehouseid;

                    entity.Rackid = item.Rackid;
                    if (item.M_RackMaster != null)
                    {
                        M_RackMasterEntity rackentity = new M_RackMasterEntity();
                        rackentity.Id = item.M_RackMaster.Id;
                        rackentity.Rack = item.M_RackMaster.Rack;
                        rackentity.Rackdesc = item.M_RackMaster.Rackdesc;
                        entity.M_RackMaster = rackentity;
                    }
                    entity.Shapeid = item.Shapeid;
                    entity.ShadeId = item.ShadeId;
                    entity.EOQ = item.EOQ;
                    entity.QCrequired = item.QCrequired;
                    entity.Packingtypeid = item.Packingtypeid;
                    entity.ItemImage = item.ItemImage;
                    entity.Description = item.Description;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    List<M_ItemMasterImageEntity> imgentitylist = new List<M_ItemMasterImageEntity>();
                    var itemimages = db.M_ItemMasterImage.Where(x => x.ItemId == id).ToList();
                    if (itemimages !=null)
                    { 
                        foreach (var img in itemimages)
                        {
                            M_ItemMasterImageEntity imgentity = new M_ItemMasterImageEntity();
                            imgentity.Id = img.Id;
                            imgentity.image = img.image;
                            imgentitylist.Add(imgentity);
                        }
                        entity.M_ItemImages = imgentitylist;
                    }

                    List<M_Item_Attribute_MasterEntity> ItemAttrMaster = new List<M_Item_Attribute_MasterEntity>();
                    var Itematrr = db.M_Item_Attribute_Master.Where(x => x.Item_ID == entity.Id && x.Is_Active==true && x.Company_ID==item.Company_ID && x.BranchId==item.BranchId ).ToList();
                    if(Itematrr.Count!=0)
                    {
                        foreach(var attdata in Itematrr)
                        {
                            M_Item_Attribute_MasterEntity itemattr_Master = new M_Item_Attribute_MasterEntity();
                            itemattr_Master.Item_Attribute_ID = attdata.Item_Attribute_ID;
                            itemattr_Master.Item_ID = attdata.Item_ID;
                            itemattr_Master.Attribute_ID = attdata.Attribute_ID;
                            itemattr_Master.Attribute_Value_ID = attdata.Attribute_Value_ID;
                            itemattr_Master.openingstock = attdata.openingstock;
                            itemattr_Master.rate = attdata.rate;
                            itemattr_Master.ROL = attdata.ROL;
                            itemattr_Master.Company_ID = attdata.Company_ID;
                            itemattr_Master.BranchId = attdata.BranchId;

                            

                            var itemid = Convert.ToInt64(item.Id);
                            long data = db.CRM_EnquiryStyleDetail.Where(x => x.Itemid==itemid).Select(x => x.Id).FirstOrDefault();
                            if (data > 0)
                            {
                                itemattr_Master.Status = true;
                                entity.Status = true;
                            }
                            else
                            {
                                long bomiid=db.M_BillOfMaterialDetail.Where(x=>x.ItemId==itemid && x.Item_Attribute_ID== attdata.Item_Attribute_ID).Select(x => x.BOM_ID).FirstOrDefault();
                                if (bomiid > 0)
                                {
                                    itemattr_Master.Status = true;
                                    entity.Status = true;
                                }
                                else
                                {
                                    itemattr_Master.Status = false;
                                    entity.Status = false;
                                }
                            }
                            ItemAttrMaster.Add(itemattr_Master);

                        }
                        entity.M_Item_Attribute_Master = ItemAttrMaster;
                    }
                    

                }
            }
            return entity;
        }

        public M_ItemMasterEntity GetOutstandingById(long id)
        {
            M_ItemMasterEntity entity = new M_ItemMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    entity.Id = item.Id;
                    entity.ItemCode = item.ItemCode;
                    entity.ItemName = item.ItemName;
                    entity.HScode = item.HScode;    
                    entity.Taxrate = item.Taxrate;
                    entity.OpeningStock = item.OpeningStock;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;

                }
            }
            return entity;
        }
        public List<M_ItemMasterEntity> GetByCategoryId(long id)
        {
            List<M_ItemMasterEntity> entitylist = new List<M_ItemMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMaster.Where(x => x.Itemsubcategoryid == id && x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemMasterEntity entity = new M_ItemMasterEntity();
                    entity.Id = item.Id;
                    entity.ItemName = item.ItemName;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    entitylist.Add(entity);
                }
            }
            return entitylist;
        }


        public bool Update(M_ItemMasterEntity entity,string AttributeValList)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.ItemCode = entity.ItemCode;
                record.ItemName = entity.ItemName;
                record.HSchapter = entity.HSchapter;
                record.HScode = entity.HScode;
                record.Specification = entity.Specification;
                record.Unit = entity.Unit;
                record.CurrencyId = entity.CurrencyId;
                record.ItemRate = entity.ItemRate;
                record.Taxrate = entity.Taxrate;
                record.OpeningStock = entity.OpeningStock;
                record.Length = entity.Length;
                record.Width = entity.Width;
                record.Height = entity.Height;
                record.ItemCategoryid = entity.ItemCategoryid;
                record.Itemcolorid = entity.Itemcolorid;
                record.Companyname = entity.Companyname;
                record.Itemsubcategoryid = entity.Itemsubcategoryid;
                record.Brand = entity.Brand;
                record.Shortname = entity.Shortname;
                record.Batchperserial = entity.Batchperserial;
                record.Warehouseid = entity.Warehouseid;
                record.Rackid = entity.Rackid;
                record.Shapeid = entity.Shapeid;
                record.ShadeId = entity.ShadeId;
                record.EOQ = entity.EOQ;
                record.QCrequired = entity.QCrequired;
                record.Packingtypeid = entity.Packingtypeid;
                record.ItemImage = entity.ItemImage;
                record.Description = entity.Description;
                record.Itemgroupid = entity.Itemgroupid;
                record.ItemStyleid=entity.ItemStyleid;
                record.ItemStyleNo = entity.ItemStyleNo;
                record.ItemStyledescription = entity.ItemStyledescription;
                record.Updateddate = entity.Updateddate;
                record.UpdatedBy = entity.UpdatedBy;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                    
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_ItemMaster.Where(x => x.Id == id).FirstOrDefault();
                item.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }

        public long InsertImage(M_ItemMasterImageEntity entity)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_ItemMasterImage record = new M_ItemMasterImage();
                record.Id = entity.Id;
                record.ItemId = entity.ItemId;
                record.image = entity.image;
                record.Createddate = entity.Createddate;
                record.Createdby = entity.Createdby;
                //New Code Added by Rahul on 09-01-2020
                //record.Updatedby = entity.Updatedby;
                //record.Updateddate = entity.Updateddate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                try
                {
                    db.M_ItemMasterImage.Add(record);
                    db.SaveChanges();
                    insertedid = record.Id;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return insertedid;
        }




        //Insert Image Data
        public long InsertImageData(M_ImageMasterEntity entity)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_ImageMaster record = new M_ImageMaster();
                record.ItemId = entity.ItemId;
                record.Item_Attribute_ID = entity.Item_Attribute_ID;
                record.SubCategoryId = entity.SubCategoryId;
                record.StyleId = entity.StyleId;
                record.image = entity.image;
                record.IsCustomize = entity.IsCustomize;
                record.Createddate = entity.Createddate;
                record.Createdby = entity.Createdby;

                //New Code Added by Rahul on 09-01-2020
                //record.Updatedby = entity.Updatedby;
                //record.Updateddate = entity.Updateddate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.IsActive = entity.IsActive;
                try
                {
                    db.M_ImageMaster.Add(record);
                    db.SaveChanges();
                    insertedid = record.Id;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return insertedid;
        }


        public bool UpdateImage(M_ItemMasterImageEntity entity)
        {
            bool updatedstatus = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemMasterImage.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.image = entity.image;
                record.Updatedby = entity.Updatedby;
                record.Updateddate = entity.Updateddate;
                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    updatedstatus = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return updatedstatus;
        }
        public M_ItemMasterImageEntity GetImageById(long id)
        {
            M_ItemMasterImageEntity entity = new M_ItemMasterImageEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMasterImage.Where(x => x.Id == id && x.IsActive == true);
                foreach (var record in records)
                {
                    entity.Id = record.Id;
                    entity.ItemId = record.ItemId;
                    entity.image = record.image;
                    entity.Createddate = record.Createddate;
                    entity.Updateddate = record.Updateddate;
                    entity.IsActive = record.IsActive;
                }
            }
            return entity;
        }
        public bool DeleteImage(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemMasterImage.Where(x => x.Id == id).FirstOrDefault();
                record.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {
                    throw;
                }
                return IsDeleted;
            }
        }

        public long InsertItemAttrib(XmlDocument entity)
        {
            long insertedid = 0;
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 2; //Insert
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = "";
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = entity.InnerXml;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach(DataTable table in dt.Tables)
                {
                    foreach(DataRow dr in table.Rows)
                    {
                        insertedid = Convert.ToInt64(dr["save"]);
                    }
                }

            }
            return insertedid;
        }

    }
}
