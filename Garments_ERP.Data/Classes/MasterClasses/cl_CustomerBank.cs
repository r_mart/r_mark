﻿using Garments_ERP.Entity;
using Garments_ERP.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_CustomerBank
    {
        public long Insert(M_CustomerBankDetailsEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_CustomerBankDetails item = new M_CustomerBankDetails();
                item.Id = entity.Id;
                item.Customerid = entity.Customerid;
                item.Bankcode = entity.Bankcode;
                item.Bankname = entity.Bankname;
                item.Accountno = entity.Accountno;
                item.IsActive = entity.IsActive;
                item.Createdby = entity.Createdby;
                item.Createddate = entity.Createddate;
                item.Updatedby = entity.Updatedby;
                item.Updateddate = entity.Updateddate;
                db.M_CustomerBankDetails.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        public M_CustomerBankDetailsEntity GetById(long id)
        {
            M_CustomerBankDetailsEntity entity = new M_CustomerBankDetailsEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerBankDetails.Where(x => x.Customerid == id && x.IsActive == true);
                foreach (var item in records)
                {
                    
                    entity.Id = item.Id;
                    entity.Accountno = item.Accountno;
                    entity.Bankcode = item.Bankcode;
                    entity.Bankname = item.Bankname;
                    entity.Customerid = item.Customerid;
                    entity.Createdby = item.Createdby;
                    entity.Createddate = item.Createddate;
                    if (item.M_CustomerMaster != null)
                    {
                        M_CustomerMasterEntity customerentity = new M_CustomerMasterEntity();
                        customerentity.Id = item.M_CustomerMaster.Id;
                        customerentity.contactname = item.M_CustomerMaster.contactname;
                        entity.M_CustomerMaster = customerentity;
                    }
                   // list.Add(entity);
                }
            }
            return entity;
        }
        public bool Update(M_CustomerBankDetailsEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_CustomerBankDetails.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.Accountno = entity.Accountno;
                record.Bankcode = entity.Bankcode;
                record.Bankname = entity.Bankname;
                record.Customerid = entity.Customerid;
                record.IsActive = entity.IsActive;
                record.Updatedby = entity.Updatedby;
                record.Updateddate = entity.Updateddate;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
    }
}
