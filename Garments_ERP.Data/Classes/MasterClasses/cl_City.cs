﻿using System;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace Garments_ERP.Data.Admin
{
    public class cl_City
    {
        public List<M_CityMasterEntity> get()
        {
           
            List<M_CityMasterEntity> list = new List<M_CityMasterEntity>();
            using (var db=new GarmentERPDBEntities())
            {
                var records = db.M_CityMaster.ToList();
                foreach (var item in records)
                {
                    M_CityMasterEntity entity = new M_CityMasterEntity();
                    entity.ID = item.ID;
                    entity.Name = item.Name;
                    entity.StateID = item.StateID;
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<M_CityMasterEntity> getByid(long id)
        {
         
            List<M_CityMasterEntity> list = new List<M_CityMasterEntity>();
            using (var db=new GarmentERPDBEntities())
            {
                var records = db.M_CityMaster.ToList().Where(x => x.StateID == id);
                foreach (var item in records)
                {
                    M_CityMasterEntity entity = new M_CityMasterEntity();
                    entity.ID = item.ID;
                    entity.Name = item.Name;
                    entity.StateID = item.StateID;
                    list.Add(entity);
                }
            }
            return list;
        }
    }
}
