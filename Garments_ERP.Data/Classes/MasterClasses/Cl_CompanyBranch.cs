﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.MasterClasses
{
     public class Cl_CompanyBranch
    {
          public List<M_BranchEntity> getComdetail()
        {
            List<M_BranchEntity> list = new List<M_BranchEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Branch.ToList().Where(x => x.Is_Active == true);
                foreach (var item in records)
                {
                    M_BranchEntity entity = new M_BranchEntity();
                    entity.Branch_ID = item.Branch_ID;
                    entity.Company_ID = item.Company_ID;
                    if(item.Company_ID!=null)
                    {
                        M_companyEntity mcom = new M_companyEntity();
                        mcom.Company_Name = db.M_company.Where(x => x.Company_ID == item.Company_ID).Select(x => x.Company_Name).FirstOrDefault();
                        entity.companyEntity = mcom;
                    }

                    entity.Branch_Name = item.Branch_Name;
                    entity.Branch_Address = item.Branch_Address;          
                    entity.Country_Code = item.Country_Code;
                    entity.GSTIN_No = item.GSTIN_No;
                    entity.State_ID = item.State_ID;
                    entity.Tel_No = item.Tel_No;
                    entity.Pincode = item.Pincode;
                    entity.City_ID = item.City_ID;
                    entity.Email = item.Email;
                    entity.mob_No = item.Mobile_No;

                    list.Add(entity);
                }
            }
            return list;
        }
        public long Insert(M_BranchEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_Branch record = new M_Branch();
                record.Branch_ID = entity.Branch_ID;
                record.Company_ID = entity.Company_ID;
                record.Branch_Name = entity.Branch_Name;
                record.Branch_Address = entity.Branch_Address;
                record.Country_Code = entity.Country_Code;
                record.Tel_No = entity.Tel_No;
                record.Mobile_No = entity.mob_No;
                record.GSTIN_No = entity.GSTIN_No;
                record.State_ID = entity.State_ID;
                record.City_ID = entity.City_ID;
                record.Pincode = entity.Pincode;
                record.Email = entity.Email;
                record.Created_Date = entity.Created_Date;
                record.Created_By = entity.Created_By;
                record.Is_Active = entity.Is_Active;


                db.M_Branch.Add(record);
                db.SaveChanges();
                id = record.Branch_ID;
            }

            return id;
        }
        public M_BranchEntity GetById(long id)
        {
            M_BranchEntity record = new M_BranchEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_Branch.Find(id);
                record.Branch_ID = item.Branch_ID;
                record.Company_ID = item.Company_ID;
                record.Branch_Name = item.Branch_Name;
                record.Branch_Address = item.Branch_Address;
                record.Country_Code = item.Country_Code;
                record.Tel_No = item.Tel_No;
                record.mob_No = item.Mobile_No;
                record.GSTIN_No = item.GSTIN_No;
                record.State_ID = item.State_ID;
                record.City_ID = item.City_ID;
                record.Pincode = item.Pincode;
                record.Email = item.Email;
                record.Modified_Date = item.Modified_Date;
                record.Modified_By = item.Modified_By;
                record.Is_Active = item.Is_Active;
            }

            return record;
        }
        public bool Update(M_BranchEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Branch.Find(entity.Branch_ID);
                record.Branch_ID = entity.Branch_ID;
                record.Company_ID = entity.Company_ID;
                record.Branch_Name = entity.Branch_Name;
                record.Branch_Address = entity.Branch_Address;
                record.Country_Code = entity.Country_Code;
                record.Tel_No = entity.Tel_No;
                record.Mobile_No = entity.mob_No;
                record.GSTIN_No = entity.GSTIN_No;
                record.State_ID = entity.State_ID;
                record.City_ID = entity.City_ID;
                record.Pincode = entity.Pincode;
                record.Email = entity.Email;
                record.Modified_Date = entity.Modified_Date;
                record.Modified_By = entity.Modified_By;
                db.SaveChanges();
                updated = true;

            }
            return updated;

        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
               
                var contact1 = db.M_Branch.Where(x => x.Branch_ID == id).FirstOrDefault();
                contact1.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }             

              
                return IsDeleted;

            }
        }

        public string GetPanNo(int CompanyId)
        {
            string panno = "27";
            using (var db = new GarmentERPDBEntities())
            {
                panno = panno + db.M_company.Where(x => x.Company_ID == CompanyId).Select(x => x.Pan_No).FirstOrDefault();
            }
            return panno;
        }
    }
}
