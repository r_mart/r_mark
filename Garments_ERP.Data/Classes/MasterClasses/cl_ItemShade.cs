﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_ItemShade
    {

        public List<M_ItemShadeMasterEntity> get()
        {
            List<M_ItemShadeMasterEntity> list = new List<M_ItemShadeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemShadeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemShadeMasterEntity entity = new M_ItemShadeMasterEntity();

                    entity.Id = item.Id;
                    entity.Itemshade = item.Itemshade;
                    entity.Itemshadedesc = item.Itemshadedesc;
                    entity.Createdate = item.Createdate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public int Insert(M_ItemShadeMasterEntity entity)
        {
            int id = 0;

            M_ItemShadeMaster record = new M_ItemShadeMaster();
            record.Id = entity.Id;
            record.Itemshade = entity.Itemshade;
            record.Itemshadedesc = entity.Itemshadedesc;
            record.Createdate = DateTime.Now;
            record.Createdby = entity.Createdby;

            //New Code Added by Rahul on 09-01-2020
            record.Company_ID = entity.Company_ID;
            record.BranchId = entity.Branch_ID;

            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_ItemShadeMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }


            return id;

        }
        public bool CheckItemShadeExistance(string Itemshade, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var itemshade = db.M_ItemShadeMaster.Where(x => x.Itemshade == Itemshade && x.IsActive == true).FirstOrDefault();
                    if (itemshade != null)
                        result = false;
                }
                else
                {
                    var itemshade = db.M_ItemShadeMaster.Where(x => x.Itemshade == Itemshade && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (itemshade != null)
                        result = false;
                }


            }
            return result;
        }
        public bool Update(M_ItemShadeMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemShadeMaster.Find(entity.Id);
                record.Itemshade = entity.Itemshade;
                record.Itemshadedesc = entity.Itemshadedesc;

                //New Code Added by Rahul on 09-01-2020
                record.Updatedby = entity.Updatedby;
                record.Updateddate = entity.Updateddate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemShadeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        //Item shape master method's
        public List<M_ItemShapeMasterEntity> getShape()
        {
            List<M_ItemShapeMasterEntity> list = new List<M_ItemShapeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemShapeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemShapeMasterEntity entity = new M_ItemShapeMasterEntity();
                    entity.Id = item.Id;
                    entity.Itemshape = item.Itemshape;
                    entity.Itemshapedesc = item.Itemshapedesc;
                    entity.Createdate = item.Createdate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public int InsertShape(M_ItemShapeMasterEntity entity)
        {
            int id = 0;
            M_ItemShapeMaster record = new M_ItemShapeMaster();
            record.Id = entity.Id;
            record.Itemshape = entity.Itemshape;
            record.Itemshapedesc = entity.Itemshapedesc;
            record.Createdate = DateTime.Now;
            record.Createdby = entity.Createdby;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_ItemShapeMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }
            return id;
        }
        public bool CheckItemShapeExistance(string Itemshape, int? Id)
        {
            bool result = true;
            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var itemshape = db.M_ItemShapeMaster.Where(x => x.Itemshape == Itemshape && x.IsActive == true).FirstOrDefault();
                    if (itemshape != null)
                        result = false;
                }
                else
                {
                    var itemshape = db.M_ItemShapeMaster.Where(x => x.Itemshape == Itemshape && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (itemshape != null)
                        result = false;
                }
            }
            return result;
        }
        public bool UpdateShape(M_ItemShapeMasterEntity entity)
        {
            bool isupdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemShapeMaster.Find(entity.Id);
                record.Itemshape = entity.Itemshape;
                record.Itemshapedesc = entity.Itemshapedesc;
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }
        public bool DeleteShape(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemShapeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
    }
}
