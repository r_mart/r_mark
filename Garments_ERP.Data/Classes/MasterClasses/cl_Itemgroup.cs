﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Itemgroup
    {

        public List<M_ItemGroupEntity> Get()
        {
            List<M_ItemGroupEntity> list = new List<M_ItemGroupEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemGroupMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemGroupEntity entity = new M_ItemGroupEntity();
                    entity.Id = item.Id;
                    entity.Itemgroup = item.Itemgroup;
                    entity.Itemgroupdesc = item.Itemgroupdesc;
                    list.Add(entity);
                }

            }
            return list;

        }

        public int Insert(M_ItemGroupEntity entity)
        {
            int id = 0;

            M_ItemGroupMaster record = new M_ItemGroupMaster();
            record.Id = entity.Id;
            record.Itemgroup = entity.Itemgroup;
            record.Itemgroupdesc = entity.Itemgroupdesc;
            record.Createddate = DateTime.Now;
            record.Createdby = entity.Createdby;

            //New Code Added by Rahul on 09-01-2020
            record.Company_ID = entity.Company_ID;
            record.BranchId = entity.Branch_ID;

            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_ItemGroupMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }


            return id;

        }

        public bool Update(M_ItemGroupEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemGroupMaster.Find(entity.Id);
                record.Itemgroup = entity.Itemgroup;
                record.Itemgroupdesc = entity.Itemgroupdesc;
                //New Code Added by Rahul on 09-01-2020
                record.Updatedby = entity.Updatedby;
                record.Updateddate = entity.Updateddate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;

        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemGroupMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public bool CheckExistance(string Itemgroup, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var itemcatestatus = db.M_ItemGroupMaster.Where(x => x.Itemgroup == Itemgroup && x.IsActive == true).FirstOrDefault();
                    if (itemcatestatus != null)
                        result = false;
                }
                else
                {
                    var itemcatestatus = db.M_ItemGroupMaster.Where(x => x.Itemgroup == Itemgroup && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (itemcatestatus != null)
                        result = false;
                }
            }
            return result;
        }

    }
}
