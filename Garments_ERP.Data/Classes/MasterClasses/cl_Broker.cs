﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.MasterClasses
{
    public class cl_Broker
    {
        public List<M_BrokerMasterEntity> GetAll()
        {
            List<M_BrokerMasterEntity> list = new List<M_BrokerMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BrokerMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_BrokerMasterEntity entity = new M_BrokerMasterEntity();
                    entity.Id = item.Id;
                    entity.BrokerName = item.BrokerName;
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    M_CityMasterEntity cityeentity = new M_CityMasterEntity();
                    if (item.M_CityMaster != null)
                    {
                        cityeentity.ID = item.M_CityMaster.ID;
                        cityeentity.Name = item.M_CityMaster.Name;
                        entity.M_CityMaster = cityeentity;
                    }
                    entity.Countryid = item.Countryid;
                    M_CountryMasterEntity countryeentity = new M_CountryMasterEntity();
                    if (item.M_CountryMaster != null)
                    {
                        countryeentity.ID = item.M_CountryMaster.ID;
                        countryeentity.Name = item.M_CountryMaster.Name;
                        entity.M_CountryMaster = countryeentity;
                    }
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Email = item.Email;
                    entity.PANNo = item.PANNo;
                    entity.GSTNo = item.GSTNo;
                    entity.IsActive = item.IsActive;
                    entity.Phone = item.Phone;
                    entity.Remark = item.Remark;
                    entity.Stateid = item.Stateid;
                    M_StateMasterEntity stateentity = new M_StateMasterEntity();
                    if (item.M_StateMaster != null)
                    {
                        stateentity.ID = item.M_StateMaster.ID;
                        stateentity.Name = item.M_StateMaster.Name;
                        entity.M_StateMaster = stateentity;
                    }
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Updateddate = item.Updateddate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_BrokerMasterEntity GetById(long id)
        {
            //M_BrokerMasterEntity list = new M_BrokerMasterEntity();
            M_BrokerMasterEntity entity = new M_BrokerMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BrokerMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {

                    entity.Id = item.Id;
                    entity.BrokerName = item.BrokerName;                   
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    M_CityMasterEntity cityeentity = new M_CityMasterEntity();
                    if (item.M_CityMaster != null)
                    {
                        cityeentity.ID = item.M_CityMaster.ID;
                        cityeentity.Name = item.M_CityMaster.Name;
                        entity.M_CityMaster = cityeentity;
                    }
                    entity.Countryid = item.Countryid;
                    M_CountryMasterEntity countryeentity = new M_CountryMasterEntity();
                    if (item.M_CountryMaster != null)
                    {
                        countryeentity.ID = item.M_CountryMaster.ID;
                        countryeentity.Name = item.M_CountryMaster.Name;
                        entity.M_CountryMaster = countryeentity;
                    }
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;                
                    entity.Email = item.Email;
                    entity.PANNo = item.PANNo;
                    entity.GSTNo = item.GSTNo;
                    entity.IsActive = item.IsActive;
                    entity.Phone = item.Phone;
                    entity.Remark = item.Remark;
                    entity.Stateid = item.Stateid;
                    M_StateMasterEntity stateentity = new M_StateMasterEntity();
                    if (item.M_StateMaster != null)
                    {
                        stateentity.ID = item.M_StateMaster.ID;
                        stateentity.Name = item.M_StateMaster.Name;
                        entity.M_StateMaster = stateentity;
                    }
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Updateddate = item.Updateddate;
                    //list.Add(entity);
                }
            }
            return entity;
        }
        public long Insert(M_BrokerMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_BrokerMaster item = new M_BrokerMaster();
                item.Id = entity.Id;
                item.BrokerName = entity.BrokerName;
                item.Address = entity.Address;
                item.Cityid = entity.Cityid;
                item.Countryid = entity.Countryid;
                item.CreatedBy = entity.CreatedBy;
                item.Createddate = entity.Createddate;
                item.Email = entity.Email;
                item.PANNo = entity.PANNo;
                item.GSTNo = entity.GSTNo;
                item.IsActive = entity.IsActive;
                item.Phone = entity.Phone;
                item.Remark = entity.Remark;
                item.Stateid = entity.Stateid;
                item.UpdatedBy = entity.UpdatedBy;
                item.Updateddate = entity.Updateddate;
                db.M_BrokerMaster.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        public bool Update(M_BrokerMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BrokerMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.Address = entity.Address;
                record.BrokerName = entity.BrokerName;
                record.Email = entity.Email;
                record.PANNo = entity.PANNo;
                record.GSTNo = entity.GSTNo;
                record.IsActive = entity.IsActive;
                record.Phone = entity.Phone;
                record.Remark = entity.Remark;
                record.Countryid = entity.Countryid;
                record.Stateid = entity.Stateid;
                record.Cityid = entity.Cityid;
                record.UpdatedBy = entity.UpdatedBy;
                record.Updateddate = entity.Updateddate;

                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_BrokerMaster.Where(x => x.Id == id).FirstOrDefault();
                contact.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
