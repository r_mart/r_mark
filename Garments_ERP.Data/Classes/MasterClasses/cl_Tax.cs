﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Tax
    {
        public List<M_TaxMasterEntity> get()
        {
            List<M_TaxMasterEntity> list = new List<M_TaxMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_TaxMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_TaxMasterEntity entity = new M_TaxMasterEntity();

                    entity.Id = item.Id;
                    entity.Taxname = item.Taxname;
                    entity.Taxdescription = item.Taxdescription;
                    entity.Percentage = item.Percentage;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public M_TaxMasterEntity getbyid(long id)
        {
            M_TaxMasterEntity entity = new M_TaxMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_TaxMaster.Find(id);
                entity.Id = item.Id;
                entity.Taxname = item.Taxname;
                entity.Taxdescription = item.Taxdescription;
                entity.Percentage = item.Percentage;
                entity.Createddate = item.Createddate;
                entity.Createdby = item.Createdby;
                entity.Updateddate = item.Updateddate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.IsActive = item.IsActive;
            }
            return entity;
        }
        public int Insert(M_TaxMasterEntity entity)
        {
            int id = 0;

            M_TaxMaster record = new M_TaxMaster();
            record.Id = entity.Id;
            record.Taxname = entity.Taxname;
            record.Percentage = entity.Percentage;
            record.Taxdescription = entity.Taxdescription;
            record.Createddate = DateTime.Now;
            record.Createdby = entity.Createdby;
            record.Updateddate = entity.Updateddate;
            record.UpdatedBy = entity.UpdatedBy;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_TaxMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;

        }

        public bool CheckTaxNameExistance(string TaxName, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var taxname = db.M_TaxMaster.Where(x => x.Taxname == TaxName && x.IsActive == true).FirstOrDefault();
                    if (taxname != null)
                        result = false;
                }
                else
                {
                    var taxname = db.M_TaxMaster.Where(x => x.Taxname == TaxName && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (taxname != null)
                        result = false;
                }


            }
            return result;
        }

        public bool Update(M_TaxMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_TaxMaster.Find(entity.Id);
                record.Taxname = entity.Taxname;
                record.Percentage = entity.Percentage;
                record.Taxdescription = entity.Taxdescription;
                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }


        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_TaxMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
    }
}
