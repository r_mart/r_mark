﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Department
    {
        public List<M_DepartmentMasterEntity> get()
        {
            List<M_DepartmentMasterEntity> list = new List<M_DepartmentMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_DepartmentMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_DepartmentMasterEntity entity = new M_DepartmentMasterEntity();
                    entity.Id = item.Id;
                    entity.DepartmentNo = item.DepartmentNo;
                    entity.Departmentname = item.Departmentname;
                    entity.Departmentdesc = item.Departmentdesc;
                    entity.Shortname = item.Shortname;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.ParentId = item.ParentId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public long Insert(M_DepartmentMasterEntity entity)
        {
            long id = 0;

            M_DepartmentMaster record = new M_DepartmentMaster();
            record.Id = entity.Id;
            record.Departmentname = entity.Departmentname;
            record.Shortname = entity.Shortname;
            record.Departmentdesc = entity.Departmentdesc;
            record.Createddate = DateTime.Now;
            record.Createdby = entity.Createdby;
            record.ParentId = entity.ParentId;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_DepartmentMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;

        }

        public bool CheckDepartmentNameExistance(string DepartmentName, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var departmentname = db.M_DepartmentMaster.Where(x => x.Departmentname == DepartmentName && x.IsActive == true).FirstOrDefault();
                    if (departmentname != null)
                        result = false;
                }
                else
                {
                    var departmentname = db.M_BrandMaster.Where(x => x.Brandname == DepartmentName && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (departmentname != null)
                        result = false;
                }


            }
            return result;
        }

        public bool Update(M_DepartmentMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_DepartmentMaster.Find(entity.Id);
                record.Departmentname = entity.Departmentname;
                record.Shortname = entity.Shortname;
                record.Departmentdesc = entity.Departmentdesc;
                record.UpdatedBy = entity.UpdatedBy;
                record.Updateddate = entity.Updateddate;
                record.ParentId = entity.ParentId;
                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }


        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_DepartmentMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
    }
}
