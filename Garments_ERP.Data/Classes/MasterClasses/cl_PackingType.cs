﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_PackingType
    {
        public List<M_PackingTypeMasterEntity> get()
        {
            List<M_PackingTypeMasterEntity> list = new List<M_PackingTypeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_PackingTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_PackingTypeMasterEntity entity = new M_PackingTypeMasterEntity();
                    entity.Id = item.Id;
                    entity.Packingtype = item.Packingtype;
                    entity.Packingtypedesc = item.Packingtypedesc;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;
                    list.Add(entity);
                }
            }
            return list;
        }
    }
}
