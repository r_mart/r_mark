﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_ProcessingStatus
    {
        public List<M_ProcessingStatusEntity> get()
        {
            List<M_ProcessingStatusEntity> list = new List<M_ProcessingStatusEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ProcessingStatus.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ProcessingStatusEntity entity = new M_ProcessingStatusEntity();

                    entity.Id = item.Id;
                    entity.ProcessingStatusName = item.ProcessingStatusName;
                    entity.ProcessingStatusdesc = item.ProcessingStatusdesc;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public long Insert(M_ProcessingStatusEntity entity)
        {
            long id = 0;

            M_ProcessingStatus record = new M_ProcessingStatus();
            record.Id = entity.Id;
            record.ProcessingStatusName = entity.ProcessingStatusName;
            record.ProcessingStatusdesc = entity.ProcessingStatusdesc;
            record.Createddate = DateTime.Now;
            record.Createdby = entity.Createdby;

            //New Code Added by Rahul on 09-01-2020
            //record.Updateddate = entity.Updateddate;
            //record.UpdatedBy = entity.UpdatedBy;
            record.Company_ID = entity.Company_ID;
            record.BranchId = entity.Branch_ID;

            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_ProcessingStatus.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;

        }

        public bool CheckProcessingStatusExistance(string ProcessingStatus, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var prostatus = db.M_ProcessingStatus.Where(x => x.ProcessingStatusName == ProcessingStatus && x.IsActive == true).FirstOrDefault();
                    if (prostatus != null)
                        result = false;
                }
                else
                {
                    var prostatus = db.M_ProcessingStatus.Where(x => x.ProcessingStatusName == ProcessingStatus && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (prostatus != null)
                        result = false;
                }


            }
            return result;
        }

        public bool Update(M_ProcessingStatusEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ProcessingStatus.Find(entity.Id);
                record.ProcessingStatusName = entity.ProcessingStatusName;
                record.ProcessingStatusdesc = entity.ProcessingStatusdesc;
                //New Code Added by Rahul on 09-01-2020
                record.Updateddate = entity.Updateddate;
                record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }


        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ProcessingStatus.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
    }
}
