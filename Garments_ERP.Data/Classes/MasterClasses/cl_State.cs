﻿using Garments_ERP.Entity;
using Garments_ERP.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
   public class cl_State
    {
       public List<M_StateMasterEntity> get()
       {
           List<M_StateMasterEntity> list = new List<M_StateMasterEntity>();
           using (var db=new GarmentERPDBEntities())
           {
               var records = db.M_StateMaster.ToList();
               foreach (var item in records)
               {
                   M_StateMasterEntity entity = new M_StateMasterEntity();
                   entity.ID = item.ID;
                   entity.Name = item.Name;
                   entity.StateCode = item.StateCode;
                   entity.CountryID = item.CountryID;
                   list.Add(entity);
               }
           }
           return list;
       }
        public List<M_StateMasterEntity> getBycountryid(long id)
        {

            List<M_StateMasterEntity> list = new List<M_StateMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_StateMaster.ToList().Where(x => x.CountryID == id);
                foreach (var item in records)
                {
                    M_StateMasterEntity entity = new M_StateMasterEntity();
                    entity.ID = item.ID;
                    entity.Name = item.Name;
                    entity.CountryID = item.CountryID;
                    list.Add(entity);
                }
            }
            return list;
        }
    }
}
