﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Data.Admin
{
    public class cl_Enquiry
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        cl_role rlobj = new cl_role();

        public long AddUpdate(List<string> Style_Img, List<string> RawItemImg, CRM_EnquiryMasterEntity entity, List<CRM_EnquiryRawitemDetailEntity> RawItem)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    if(entity.EnquiryId>0)
                    {
                        var item = db.CRM_EnquiryMaster.Find(entity.EnquiryId);
                        item.Enquirydate = entity.Enquirydate;
                        item.customerid = entity.customerid;
                        item.BrokerId = entity.BrokerId;
                        item.narration = entity.narration;
                        item.Approvalstatus = entity.Approvalstatus;
                        item.Tentivedate = entity.Tentivedate;
                        item.Sampling = entity.Sampling;
                        item.Addressid = entity.Addressid;
                        item.Approvaldate = entity.Approvaldate;
                        item.Comment = entity.Comment;
                        item.Updateddate = entity.Createddate;
                        item.UpdatedBy = entity.CreatedBy;
                        item.IsActive = entity.IsActive;
                        item.IsStatus = entity.IsStatus;
                        item.Company_ID = entity.Company_ID;
                        item.BranchId = entity.BranchId;
                        item.EnquiryTypeId = entity.Enquirytype;
                        item.IsReadymade = entity.IsReadmade;
                        db.SaveChanges();
                        id = entity.EnquiryId;
                    }
                    else
                    {
                        CRM_EnquiryMaster item = new CRM_EnquiryMaster();
                        item.Enquirydate = entity.Enquirydate;
                        item.customerid = entity.customerid;
                        item.BrokerId = entity.BrokerId;
                        item.narration = entity.narration;
                        item.Approvalstatus = entity.Approvalstatus;
                        item.Tentivedate = entity.Tentivedate;
                        item.Sampling = entity.Sampling;
                        item.Addressid = entity.Addressid;
                        item.Approvaldate = entity.Approvaldate;
                        item.Comment = entity.Comment;
                        item.Createddate = entity.Createddate;
                        item.CreatedBy = entity.CreatedBy;
                        item.IsActive = entity.IsActive;
                        item.IsStatus = entity.IsStatus;
                        item.Company_ID = entity.Company_ID;
                        item.BranchId = entity.BranchId;
                        item.EnquiryTypeId = entity.Enquirytype;
                        item.IsReadymade = entity.IsReadmade;
                        if (entity.IsReadmade == 0)
                        {
                            item.Enquiryno = "RM-" + Convert.ToString(getnextenqno());
                        }
                        else
                        {
                            item.Enquiryno = "RF-" + Convert.ToString(getnextenqno());
                        }
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.CRM_EnquiryMaster.Add(item);
                        db.SaveChanges();
                        db.Configuration.ValidateOnSaveEnabled = true;

                        id = item.Id;
                    }
                    
                    if(entity.StyleEnt!=null)
                    {
                        if(entity.StyleEnt.Count>0)
                        {
                            foreach(var StyleData in entity.StyleEnt)
                            {
                                long styleid = 0;
                                if(StyleData.Id>0)
                                {
                                    var styleEnt = db.CRM_EnquiryStyleDetail.Find(StyleData.Id);
                                    styleEnt.Enquiryid = id;
                                    styleEnt.Segmenttypeid = StyleData.Segmenttypeid;
                                    styleEnt.Styleid = StyleData.Styleid;
                                    styleEnt.Itemid = StyleData.Itemid;
                                    styleEnt.Reqqty = StyleData.Reqqty;
                                    styleEnt.Updateddate = entity.Createddate;
                                    styleEnt.Updatedby = entity.CreatedBy;
                                    styleEnt.IsActive = true;
                                    styleEnt.Styleno = StyleData.Styleno;
                                    styleEnt.Styledesc = StyleData.Styledesc;
                                    styleEnt.Brand = StyleData.Brand;
                                    styleEnt.Totalqty = StyleData.Totalqty;
                                    styleEnt.SizeId = StyleData.Sizeid;
                                    styleEnt.Attribute_ID = StyleData.Attribute_ID;
                                    styleEnt.Attribute_Value_ID = StyleData.Attribute_Value_ID;
                                    styleEnt.rate = StyleData.rate;
                                    styleEnt.Company_ID = entity.Company_ID;
                                    styleEnt.BranchId = entity.BranchId;
                                    db.SaveChanges();
                                    styleid = StyleData.Id;
                                }
                                else
                                {
                                    CRM_EnquiryStyleDetail styleEnt = new CRM_EnquiryStyleDetail();
                                    styleEnt.Enquiryid = id;
                                    styleEnt.Segmenttypeid = StyleData.Segmenttypeid;
                                    styleEnt.Styleid = StyleData.Styleid;
                                    styleEnt.Itemid = StyleData.Itemid;
                                    styleEnt.Reqqty = StyleData.Reqqty;
                                    styleEnt.Createdate = entity.Createddate;
                                    styleEnt.Createdby = entity.CreatedBy;
                                    styleEnt.IsActive = true;
                                    styleEnt.Styleno = StyleData.Styleno;
                                    styleEnt.Styledesc = StyleData.Styledesc;
                                    styleEnt.Brand = StyleData.Brand;
                                    styleEnt.Totalqty = StyleData.Totalqty;
                                    styleEnt.SizeId = StyleData.Sizeid;
                                    styleEnt.Attribute_ID = StyleData.Attribute_ID;
                                    styleEnt.Attribute_Value_ID = StyleData.Attribute_Value_ID;
                                    styleEnt.rate = StyleData.rate;
                                    styleEnt.Company_ID = entity.Company_ID;
                                    styleEnt.BranchId = entity.BranchId;
                                    db.CRM_EnquiryStyleDetail.Add(styleEnt);
                                    db.SaveChanges();
                                    styleid = styleEnt.Id;
                                    if (styleid > 0 && StyleData.StyleImgCount>0)
                                    {
                                        var Style_Imgarray = Style_Img.ToList();
                                        for (int k=0;k< StyleData.StyleImgCount;k++)
                                        {
                                            CRM_EnquiryStyleImage ESImg_Ent = new CRM_EnquiryStyleImage();
                                            ESImg_Ent.Enquiryid = id;
                                            ESImg_Ent.styleimg = Convert.ToString(Style_Imgarray[k]);
                                            ESImg_Ent.Createdby = entity.CreatedBy;
                                            ESImg_Ent.IsActive = true;
                                            ESImg_Ent.Company_ID = entity.Company_ID;
                                            ESImg_Ent.BranchId = entity.BranchId;
                                            ESImg_Ent.EnqStyleId = styleid;
                                            db.CRM_EnquiryStyleImage.Add(ESImg_Ent);
                                            db.SaveChanges();
                                            Style_Imgarray.RemoveAt(0);
                                        }
                                    }
                                }
                                
                                
                            }
                        }

                        if(RawItem.Count>0)
                        {
                            foreach(var RIEnt in RawItem)
                            {
                                long EnqRI_Id = 0;
                                if(RIEnt.Id>0)
                                {
                                    var RI = db.CRM_EnquiryRawitemDetail.Find(RIEnt.Id);
                                    RI.Enquiryid = id;
                                    RI.Itemcategoryid = RIEnt.Itemcategoryid;
                                    RI.Itemsubcategoryid = RIEnt.Itemsubcategoryid;
                                    RI.Itemid = RIEnt.Itemid;
                                    RI.Itemdesc = RIEnt.Itemdesc;
                                    RI.Supplier = RIEnt.Supplier;
                                    RI.Createdate = entity.Createddate;
                                    RI.Createdby = entity.CreatedBy;
                                    RI.IsActive = true;
                                    RI.Company_ID = entity.Company_ID;
                                    RI.BranchId = entity.BranchId;
                                    RI.EnqStyleId = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == id && x.Itemid == RIEnt.Itemid).Select(x => x.Id).FirstOrDefault();
                                    db.SaveChanges();
                                }
                                else
                                {
                                    CRM_EnquiryRawitemDetail RI = new CRM_EnquiryRawitemDetail();
                                    RI.Enquiryid = id;
                                    RI.Itemcategoryid = RIEnt.Itemcategoryid;
                                    RI.Itemsubcategoryid = RIEnt.Itemsubcategoryid;
                                    RI.Itemid = RIEnt.Itemid;
                                    RI.Itemdesc = RIEnt.Itemdesc;
                                    RI.Supplier = RIEnt.Supplier;
                                    RI.Createdate = entity.Createddate;
                                    RI.Createdby = entity.CreatedBy;
                                    RI.IsActive = true;
                                    RI.Company_ID = entity.Company_ID;
                                    RI.BranchId = entity.BranchId;
                                    RI.EnqStyleId = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == id && x.Itemid == RIEnt.Itemid).Select(x => x.Id).FirstOrDefault();
                                    db.CRM_EnquiryRawitemDetail.Add(RI);
                                    db.SaveChanges();
                                    EnqRI_Id = RI.Id;
                                    if (EnqRI_Id > 0 && RIEnt.RawImgCount > 0)
                                    {
                                        var RawItemImgarray = RawItemImg.ToList();
                                        for (int k = 0; k < RIEnt.RawImgCount; k++)
                                        {
                                            CRM_EnquiryRawItemImage ESImg_Ent = new CRM_EnquiryRawItemImage();
                                            ESImg_Ent.Enquiryrawitemdetailid = EnqRI_Id;
                                            ESImg_Ent.image = Convert.ToString(RawItemImgarray[k]);
                                            ESImg_Ent.Createdby = entity.CreatedBy;

                                            ESImg_Ent.IsActive = true;
                                            ESImg_Ent.Company_ID = entity.Company_ID;
                                            ESImg_Ent.BranchId = entity.BranchId;
                                            db.CRM_EnquiryRawItemImage.Add(ESImg_Ent);
                                            db.SaveChanges();
                                            RawItemImgarray.RemoveAt(0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                        

                
                }
            }
            catch (Exception)
            {
                throw;
            }
            return id;
        }

        public CRM_EnquiryMasterEntity getbyid(long id)
        {
            CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.CRM_EnquiryMaster.Find(id);
                // CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
                entity.EnquiryId = item.Id;
                entity.Enquiryno = item.Enquiryno;
                entity.Enquirydate = item.Enquirydate;
                entity.customerid = item.customerid;
                entity.Sampling = item.Sampling;
                entity.IsReadmade = item.IsReadymade;
                entity.Enquirytype = item.EnquiryTypeId;
                if (item.customerid != null)
                {
                    M_LedgersEntity legderentity = new M_LedgersEntity();
                    var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).FirstOrDefault();
                    if (custdata != null)
                    {

                        legderentity.Ledger_Id = custdata.Ledger_Id;
                        legderentity.Ledger_Name = custdata.Ledger_Name;
                        entity.M_Ledgersentity = legderentity;
                    }
                    else
                    {
                        legderentity.Ledger_Id = Convert.ToInt32(item.customerid);
                        legderentity.Ledger_Name = "--";
                        entity.M_Ledgersentity = legderentity;

                    }
                }
                entity.BrokerId = item.BrokerId;

                if (item.BrokerId != null)
                {
                    M_LedgersEntity legderentityBro = new M_LedgersEntity();
                    var brokdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.BrokerId).SingleOrDefault();
                    if (brokdata != null)
                    {
                        legderentityBro.Ledger_Id = brokdata.Ledger_Id;
                        legderentityBro.Ledger_Name = brokdata.Ledger_Name;
                        entity.M_Ledgersentitybrok = legderentityBro;
                    }
                    else
                    {
                        legderentityBro.Ledger_Id = Convert.ToInt32(item.BrokerId);
                        legderentityBro.Ledger_Name = "--";
                        entity.M_Ledgersentitybrok = legderentityBro;
                    }
                }
                entity.narration = item.narration;
                entity.Approvalstatus = item.Approvalstatus;
                entity.Approvaldate = item.Approvaldate;
                entity.Currencyid = item.Currencyid;
                entity.Exchangerate = item.Exchangerate;
                entity.Costprice = item.Costprice;
                entity.SalePrice = item.SalePrice;
                entity.Dateofcosting = item.Dateofcosting;
                entity.Comment = item.Comment;
                entity.Createddate = item.Createddate;
                entity.CreatedBy = item.CreatedBy;
                entity.Updateddate = item.Updateddate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.IsActive = item.IsActive;
                entity.Addressid = item.Addressid;
                entity.Tentivedate = item.Tentivedate;
                entity.IsStatus = item.IsStatus;
                entity.Enquirytype = item.EnquiryTypeId;
                M_EnquiryTypeMasterEntity Typeent = new M_EnquiryTypeMasterEntity();
                if (item.EnquiryTypeId != null)
                {
                    Typeent.EnquiryType = db.M_EnquiryTypeMaster.Where(x => x.id == item.EnquiryTypeId).Select(x => x.EnquiryType).FirstOrDefault();
                }
                else
                {
                    Typeent.EnquiryType = "";
                }
                entity.EnqType = Typeent;

            }

            return entity;
        }

        public List<CRM_EnquiryStyleDetailsEntity> GetEnqryStyleData(long EnquiryId)
        {
            List<CRM_EnquiryStyleDetailsEntity> list = new List<CRM_EnquiryStyleDetailsEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == EnquiryId).ToList();
                    foreach(var rec in record)
                    {
                        CRM_EnquiryStyleDetailsEntity ent = new CRM_EnquiryStyleDetailsEntity();
                        ent.Id = rec.Id;
                        ent.NopOrderItemId = 0;
                        ent.Enquiryid = rec.Enquiryid;
                        ent.Segmenttypeid = rec.Segmenttypeid;
                        var SegRec = db.M_SegmentTypeMaster.Where(x => x.Id == rec.Segmenttypeid).FirstOrDefault();
                        if(SegRec!=null)
                        {
                            M_SegmentTypeMasterEntity segTypeE = new M_SegmentTypeMasterEntity();
                            segTypeE.Segmenttype = SegRec.Segmenttype;
                            ent.SegTypeEnt = segTypeE;
                        }
                        ent.Styleid = rec.Styleid;
                        var styleRec = db.M_StyleMaster.Where(x => x.Id == rec.Styleid).FirstOrDefault();
                        if(styleRec!=null)
                        {
                            M_StyleMasterEntity styent = new M_StyleMasterEntity();
                            styent.Stylename = styleRec.Stylename;
                            ent.styleEnt = styent;
                        }
                        ent.Styleno = rec.Styleno;
                        ent.Styledesc = rec.Styledesc;
                        ent.Brand = rec.Brand;
                        ent.Itemid = rec.Itemid;
                        var itemEnt = db.M_ItemMaster.Where(x => x.Id == rec.Itemid).FirstOrDefault();
                        if(itemEnt!=null)
                        {
                            M_ItemMasterEntity iteme = new M_ItemMasterEntity();
                            long itemid = Convert.ToInt64(rec.Itemid);
                            int Attiid = Convert.ToInt32(rec.SizeId);
                            iteme.ItemName = getItemName(itemid, Attiid, 0);
                            ent.ItemEnt = iteme;
                            ent.ItemUnit = db.M_UnitMaster.Where(x => x.Id == itemEnt.Unit).Select(x => x.ItemUnit).FirstOrDefault();
                        }
                        ent.Reqqty = rec.Reqqty;
                        ent.Createdate = rec.Createdate;
                        ent.Createdby = rec.Createdby;
                        ent.Totalqty = rec.Totalqty;
                        ent.rate = rec.rate;
                        ent.Company_ID = rec.Company_ID;
                        ent.BranchId = rec.BranchId;
                        ent.IsActive = rec.IsActive;
                        ent.Sizeid = rec.SizeId;
                        ent.Attribute_ID = rec.Attribute_ID;
                        var attid = Convert.ToInt32(rec.Attribute_ID);
                        var Attrec = db.M_Attribute_Master.Where(x => x.Attribute_ID == attid).FirstOrDefault();
                        if(Attrec!=null)
                        {
                            M_Attribute_MasterEntity Attre = new M_Attribute_MasterEntity();
                            Attre.Attribute_Name = Attrec.Attribute_Name;
                            ent.AttrEnt = Attre;
                        }
                        ent.Attribute_Value_ID = rec.Attribute_Value_ID;
                        var attValid = Convert.ToInt32(rec.Attribute_Value_ID);
                        var AttValrec = db.M_Attribute_Value_Master.Where(x => x.Attribute_Value_ID == attValid).FirstOrDefault();
                        if (AttValrec != null)
                        {
                            M_Attribute_Value_MasterEntity Attrvale = new M_Attribute_Value_MasterEntity();
                            Attrvale.Attribute_Value = AttValrec.Attribute_Value;
                            ent.AttrValEnt = Attrvale;
                        }
                        List<CRM_EnquiryStyleImageEntity> listimg = new List<CRM_EnquiryStyleImageEntity>();
                        var StyleRecImage = db.CRM_EnquiryStyleImage.Where(x => x.Enquiryid == EnquiryId && x.EnqStyleId == rec.Id).ToList();
                        if (StyleRecImage!=null && StyleRecImage.Count > 0)
                        {
                            
                            foreach (var img in StyleRecImage)
                            {
                                CRM_EnquiryStyleImageEntity ESImg_Ent = new CRM_EnquiryStyleImageEntity();
                                ESImg_Ent.Enquiryid = EnquiryId;
                                ESImg_Ent.styleimg = img.styleimg;
                                ESImg_Ent.EnqStyleId = img.EnqStyleId;
                                listimg.Add(ESImg_Ent);
                            }
                        }
                        ent.StyleImage = listimg;
                        list.Add(ent);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return list;
        }

        public List<CRM_EnquiryRawitemDetailEntity> GetEnqryStyleRawItemData(long EnquiryId,long styleid)
        {
            List<CRM_EnquiryRawitemDetailEntity> list = new List<CRM_EnquiryRawitemDetailEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_EnquiryRawitemDetail.Where(x => x.Enquiryid == EnquiryId && x.EnqStyleId == styleid ).ToList();
                    foreach(var rec in record)
                    {
                        CRM_EnquiryRawitemDetailEntity ent = new CRM_EnquiryRawitemDetailEntity();
                        ent.Id = rec.Id;
                        ent.Enquiryid = rec.Enquiryid;
                        ent.Itemcategoryid = rec.Itemcategoryid;
                        var catname = db.M_ItemCategoryMaster.Where(x => x.Id == rec.Itemcategoryid).FirstOrDefault();
                        ent.ItemCategoryName = catname != null ? catname.Itemcategory : "";
                        ent.Itemsubcategoryid = rec.Itemsubcategoryid;
                        var catsubname=db.M_ItemSubCategoryMaster.Where(x => x.Id == rec.Itemsubcategoryid).FirstOrDefault();
                        ent.SubCategoryName = catsubname != null ? catsubname.Itemsubcategory : "";
                        ent.Itemid = rec.Itemid;
                        ent.Itemdesc = rec.Itemdesc;
                        ent.Supplier = rec.Supplier;
                        ent.Createdate = rec.Createdate;
                        ent.Createdby = rec.Createdby;
                        ent.IsActive = rec.IsActive;
                        ent.Company_ID = rec.Company_ID;
                        ent.BranchId = rec.BranchId;
                        ent.EnqStyleId = rec.EnqStyleId;
                        List<CRM_EnquiryRawItemImageEntity> imglist = new List<CRM_EnquiryRawItemImageEntity>();
                        var imgrec = db.CRM_EnquiryRawItemImage.Where(x => x.Enquiryrawitemdetailid == rec.Id).ToList();
                        foreach(var img in imgrec)
                        {
                            CRM_EnquiryRawItemImageEntity imgent = new CRM_EnquiryRawItemImageEntity();
                            imgent.Id = img.Id;
                            imgent.Enquiryrawitemdetailid = img.Enquiryrawitemdetailid;
                            imgent.image = img.image;
                            imgent.Createddate = img.Createddate;
                            imgent.Createdby = img.Createdby;
                            imgent.IsActive = img.IsActive;
                            imglist.Add(imgent);
                        }
                        ent.RawItemImg = imglist;

                        list.Add(ent);
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return list;
        }


        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }


        public List<M_EnquiryTypeMasterEntity> getEnqiryType()
        {
            try
            {
                List<M_EnquiryTypeMasterEntity> list = new List<M_EnquiryTypeMasterEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var EnqTypeList = db.M_EnquiryTypeMaster.Where(x => x.IsActive == true).ToList();
                    foreach(var data in EnqTypeList)
                    {
                        M_EnquiryTypeMasterEntity ent = new M_EnquiryTypeMasterEntity();
                        ent.id = data.id;
                        ent.EnquiryType = data.EnquiryType;
                        ent.discription = data.discription;
                        ent.IsActive = data.IsActive;
                        list.Add(ent);

                    }
                    return list;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        //get
        public object StatusCount(int Type,string attribute,XmlDocument doc)
        {
            try
            {
                DataSet dt = new DataSet();
                using (SqlConnection cs = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cs;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Common_StatusCount";
                    cmd.Parameters.Add("type", SqlDbType.Int).Value = Type;
                    cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = attribute;
                    cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = doc.InnerXml;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
                return dt;

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        //Commom Code For Auto Extender List Display With Procedure
        public object SearchList(XmlDocument doc, int type, string id,long screenId)
        {
            try
            {
                DataSet dt = new DataSet();
                using (SqlConnection cs = new SqlConnection(con))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cs;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_SearchList";
                    cmd.Parameters.Add("type", SqlDbType.Int).Value = type;
                    cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value =Convert.ToString(id);
                    cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = doc.InnerXml;
                    cmd.Parameters.Add("screenId", SqlDbType.BigInt).Value = screenId;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    

                }
                return dt;

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public string getnextenqno()
        {
            string no;
            no = "ENQ-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.CRM_EnquiryMaster.Count() >0 ? db.CRM_EnquiryMaster.Max(x => x.Id) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public bool Update(CRM_EnquiryMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.CRM_EnquiryMaster.Find(entity.EnquiryId);
                record.Id = entity.EnquiryId;
                record.BrokerId = entity.BrokerId;
               
                record.narration = entity.narration;
                record.Approvalstatus = entity.Approvalstatus;
                record.Approvaldate = entity.Approvaldate;
                record.Comment = entity.Comment;
                record.Sampling = entity.Sampling;
                record.Updateddate = DateTime.Now;
                record.UpdatedBy = entity.UpdatedBy;
                record.IsStatus = entity.IsStatus;
                record.EnquiryTypeId = entity.Enquirytype;
                if (entity.imgelist != null)
                {
                    if (entity.imgelist.Count > 0)
                    {
                        var oldimagerecordlist = db.CRM_EnquiryStyleImage.Where(x => x.Enquiryid == entity.EnquiryId).ToList();
                        foreach (var oldimg in oldimagerecordlist)
                        {
                            var delerecord = db.CRM_EnquiryStyleImage.Find(oldimg.Id);
                            delerecord.IsActive = false;
                            db.SaveChanges();
                        }
                    }
                }


                foreach (var itemimg in entity.imgelist)
                {

                    CRM_EnquiryStyleImage imgrecord = new CRM_EnquiryStyleImage();
                    imgrecord.Enquiryid = entity.EnquiryId;
                    imgrecord.styleimg = itemimg.styleimg;
                    imgrecord.IsActive = true;
                    imgrecord.Createdby = entity.UpdatedBy;
                    imgrecord.Company_ID = entity.Company_ID;
                    imgrecord.BranchId = entity.BranchId;

                    db.CRM_EnquiryStyleImage.Add(imgrecord);
                    db.SaveChanges();

                }
               
                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }

        public List<CRM_EnquiryMasterEntity> getEnquirynobycustomerid(long customerid,int Company_ID,int BranchId)
        {
            List<CRM_EnquiryMasterEntity> enquirylist = new List<CRM_EnquiryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_EnquiryMaster.Where(x => x.customerid == customerid && x.IsStatus==3 && x.Company_ID==Company_ID && x.BranchId==BranchId).OrderByDescending(x=>x.Id).ToList();
                foreach (var item in records)
                {
                    CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
                    entity.EnquiryId = item.Id;
                    entity.Enquiryno = item.Enquiryno;
                    enquirylist.Add(entity);
                }
            }

            return enquirylist;
        }
        


        public List<CRM_EnquiryMasterEntity> get(DateTime from, DateTime to)
        {
            List<CRM_EnquiryMasterEntity> list = new List<CRM_EnquiryMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_EnquiryMaster.OrderByDescending(x => x.Id).ToList().Where(x => x.IsActive == true && x.Enquirydate >= from && x.Enquirydate <= to);
               
                foreach (var item in records)
                {
                    CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
                    entity.EnquiryId = item.Id;
                    entity.Enquiryno = item.Enquiryno;
                    entity.Enquirydate = item.Enquirydate;
                    entity.Sampling = item.Sampling;
                    entity.customerid = item.customerid;
                    entity.BrokerId = item.BrokerId;
                    entity.narration = item.narration;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Currencyid = item.Currencyid;
                    entity.Addressid = item.Addressid;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Comment = item.Comment;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    M_LedgersEntity ledentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (custdata != null)
                        {
                            ledentity.Ledger_Id = custdata.Ledger_Id;
                            ledentity.Ledger_Name = custdata.Ledger_Name;
                            entity.M_Ledgersentity = ledentity;
                        }
                        else
                        {
                            ledentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            ledentity.Ledger_Name = "--";
                            entity.M_Ledgersentity = ledentity;
                        }
                    }
                    M_LedgersEntity ledentitybrok = new M_LedgersEntity();
                    if (item.BrokerId != null)
                    {
                        var brokdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.BrokerId).SingleOrDefault();
                        if (brokdata != null)
                        {
                            ledentitybrok.Ledger_Id = brokdata.Ledger_Id;
                            ledentitybrok.Ledger_Name = brokdata.Ledger_Name;
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                        else
                        {
                            ledentitybrok.Ledger_Id = Convert.ToInt32(item.BrokerId);
                            ledentitybrok.Ledger_Name = "--";
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                    }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.Id);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == item.Id).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                    entity.Enquirytype = item.EnquiryTypeId;
                    M_EnquiryTypeMasterEntity Typeent = new M_EnquiryTypeMasterEntity();
                    if (item.EnquiryTypeId != null)
                    {
                        Typeent.EnquiryType = db.M_EnquiryTypeMaster.Where(x => x.id == item.EnquiryTypeId).Select(x => x.EnquiryType).FirstOrDefault();
                    }
                    else
                    {
                        Typeent.EnquiryType = "";
                    }
                    entity.EnqType = Typeent;
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<CRM_EnquiryMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<CRM_EnquiryMasterEntity> list = new List<CRM_EnquiryMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_EnquiryMaster.Where(x => x.IsActive == true && x.Enquirydate >= from && x.Enquirydate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x => x.Id).ToList();
                if (records.Count>=0 && roleid != 4)
                {
                    records = db.CRM_EnquiryMaster.Where(x => x.IsActive == true && x.Enquirydate >= from && x.Enquirydate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }

                foreach (var item in records)
                {
                    CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
                    entity.EnquiryId = item.Id;
                    entity.Enquiryno = item.Enquiryno;
                    entity.IsReadmade = item.IsReadymade;
                    entity.Enquirydate = item.Enquirydate;
                    entity.Sampling = item.Sampling;
                    entity.customerid = item.customerid;
                    entity.BrokerId = item.BrokerId;
                    entity.narration = item.narration;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Currencyid = item.Currencyid;
                    entity.Addressid = item.Addressid;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Comment = item.Comment;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    M_LedgersEntity ledentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (custdata != null)
                        {
                            ledentity.Ledger_Id = custdata.Ledger_Id;
                            ledentity.Ledger_Name = custdata.Ledger_Name;
                            entity.M_Ledgersentity = ledentity;
                        }
                        else
                        {
                            ledentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            ledentity.Ledger_Name = "--";
                            entity.M_Ledgersentity = ledentity;
                        }
                    }
                    M_LedgersEntity ledentitybrok = new M_LedgersEntity();
                    if (item.BrokerId != null)
                    {
                        var brokdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.BrokerId).SingleOrDefault();
                        if (brokdata != null)
                        {
                            ledentitybrok.Ledger_Id = brokdata.Ledger_Id;
                            ledentitybrok.Ledger_Name = brokdata.Ledger_Name;
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                        else
                        {
                            ledentitybrok.Ledger_Id = Convert.ToInt32(item.BrokerId);
                            ledentitybrok.Ledger_Name = "--";
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                    }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.Id);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == item.Id).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                    entity.Enquirytype = item.EnquiryTypeId;
                    M_EnquiryTypeMasterEntity Typeent = new M_EnquiryTypeMasterEntity();
                    if (item.EnquiryTypeId != null)
                    {
                        Typeent.EnquiryType = db.M_EnquiryTypeMaster.Where(x => x.id == item.EnquiryTypeId).Select(x => x.EnquiryType).FirstOrDefault();
                    }
                    else
                    {
                        Typeent.EnquiryType = "";
                    }
                    entity.EnqType = Typeent;
                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.EnquiryId).ToList();
                }
            }
            return list;
        }


        public List<CRM_EnquiryMasterEntity> get()
        {
            List<CRM_EnquiryMasterEntity> list = new List<CRM_EnquiryMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_EnquiryMaster.OrderByDescending(x => x.Id).ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
                    entity.EnquiryId = item.Id;
                    entity.Enquiryno = item.Enquiryno;
                    entity.Enquirydate = item.Enquirydate;
                    entity.Sampling = item.Sampling;
                    entity.customerid = item.customerid;
                    entity.BrokerId = item.BrokerId;
                    entity.narration = item.narration;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Currencyid = item.Currencyid;
                    entity.Addressid = item.Addressid;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Comment = item.Comment;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    M_LedgersEntity ledentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (custdata != null)
                        {
                            ledentity.Ledger_Id = custdata.Ledger_Id;
                            ledentity.Ledger_Name = custdata.Ledger_Name;
                            entity.M_Ledgersentity = ledentity;
                        }
                        else
                        {
                            ledentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            ledentity.Ledger_Name = "--";
                            entity.M_Ledgersentity = ledentity;
                        }
                    }
                    M_LedgersEntity ledentitybrok = new M_LedgersEntity();
                    if (item.BrokerId != null)
                    {
                        var brokdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.BrokerId).SingleOrDefault();
                        if (brokdata != null)
                        {
                            ledentitybrok.Ledger_Id = brokdata.Ledger_Id;
                            ledentitybrok.Ledger_Name = brokdata.Ledger_Name;
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                        else
                        {
                            ledentitybrok.Ledger_Id = Convert.ToInt32(item.BrokerId);
                            ledentitybrok.Ledger_Name = "--";
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                    }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.Id);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == item.Id).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                    entity.Enquirytype = item.EnquiryTypeId;
                    M_EnquiryTypeMasterEntity Typeent = new M_EnquiryTypeMasterEntity();
                    if (item.EnquiryTypeId != null)
                    {
                        Typeent.EnquiryType = db.M_EnquiryTypeMaster.Where(x => x.id == item.EnquiryTypeId).Select(x => x.EnquiryType).FirstOrDefault();
                    }
                    else
                    {
                        Typeent.EnquiryType = "";
                    }
                    entity.EnqType = Typeent;
                    list.Add(entity);
                }
            }

            return list;

        }

        public List<CRM_EnquiryMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId,int Userid,int roleid)
        {
            List<CRM_EnquiryMasterEntity> list = new List<CRM_EnquiryMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_EnquiryMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy==Userid).OrderByDescending(x => x.Id).ToList();
                if (records.Count>=0 && roleid != 4)
                {
                    records = db.CRM_EnquiryMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                foreach (var item in records)
                {
                    CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
                    entity.EnquiryId = item.Id;
                    entity.Enquiryno = item.Enquiryno;
                    entity.IsReadmade = item.IsReadymade;
                    entity.Enquirydate = item.Enquirydate;
                    entity.Sampling = item.Sampling;
                    entity.customerid = item.customerid;
                    entity.BrokerId = item.BrokerId;
                    entity.narration = item.narration;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Currencyid = item.Currencyid;
                    entity.Addressid = item.Addressid;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Comment = item.Comment;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    M_LedgersEntity ledentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (custdata != null)
                        {
                            ledentity.Ledger_Id = custdata.Ledger_Id;
                            ledentity.Ledger_Name = custdata.Ledger_Name;
                            entity.M_Ledgersentity = ledentity;
                        }
                        else
                        {
                            ledentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            ledentity.Ledger_Name = "--";
                            entity.M_Ledgersentity = ledentity;
                        }
                    }
                    M_LedgersEntity ledentitybrok = new M_LedgersEntity();
                    if (item.BrokerId != null)
                    {
                        var brokdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.BrokerId).SingleOrDefault();
                        if (brokdata != null)
                        {
                            ledentitybrok.Ledger_Id = brokdata.Ledger_Id;
                            ledentitybrok.Ledger_Name = brokdata.Ledger_Name;
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                        else
                        {
                            ledentitybrok.Ledger_Id = Convert.ToInt32(item.BrokerId);
                            ledentitybrok.Ledger_Name = "--";
                            entity.M_Ledgersentitybrok = ledentitybrok;
                        }
                    }
                    //entity.ProductName = GetProductName(item.Id);
                    //entity.StyleName = GetStyleName(item.Id);
                    //entity.ProductAttrib = GetProductAttribName(item.Id);
                    //entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == item.Id).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                    entity.Enquirytype = item.EnquiryTypeId;
                    M_EnquiryTypeMasterEntity Typeent = new M_EnquiryTypeMasterEntity();
                    if (item.EnquiryTypeId != null)
                    {
                        Typeent.EnquiryType = db.M_EnquiryTypeMaster.Where(x => x.id == item.EnquiryTypeId).Select(x => x.EnquiryType).FirstOrDefault();
                    }
                    else
                    {
                        Typeent.EnquiryType = "";
                    }
                    entity.EnqType = Typeent;
                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.EnquiryId).ToList();
                }
            }

            return list;

        }

        public string GetProductName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var itemid = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == Eid).Select(x => x.Itemid).Distinct().FirstOrDefault();
                itemname = db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.ItemName).FirstOrDefault();
            }
            return itemname;
        }

        public string GetStyleName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var styleid = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == Eid).Select(x => x.Styleid).Distinct().FirstOrDefault();
                itemname = db.M_StyleMaster.Where(x => x.Id == styleid).Select(x => x.Stylename).FirstOrDefault();
            }
            return itemname;
        }

        public string GetProductAttribName(long Eid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 34; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(Eid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                int i = 0;
                foreach (DataTable table in dt.Tables)
                {
                    
                    foreach (DataRow dr in table.Rows)
                    {
                        if (i == 0)
                        {
                            itemname = Convert.ToString(dr["AttributeValue"]);
                        }
                        else
                        {
                            itemname = itemname+","+Convert.ToString(dr["AttributeValue"]);
                        }
                    }
                    i++;
                }

            }
            return itemname;
        }

        public bool Delete(long id,int Userid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_EnquiryMaster.Find(id);
                    record.IsActive = false;
                    record.UpdatedBy = Userid;
                    record.Updateddate = DateTime.Now;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }


        public bool Rejectedenquiry(long id,int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_EnquiryMaster.Find(id);
                    
                    record.IsStatus =5;
                    record.Updateddate = DateTime.Now;
                    record.UpdatedBy = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }
        //insert itemcord
        public long Insert(CRM_EnquiryMasterEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    CRM_EnquiryMaster item = new CRM_EnquiryMaster();
                    item.Id = entity.EnquiryId;
                    item.Enquirydate = entity.Enquirydate;
                    item.customerid = entity.customerid;
                    item.BrokerId = entity.BrokerId;
                    item.narration = entity.narration;
                    item.Approvalstatus = entity.Approvalstatus;
                    item.Tentivedate = entity.Tentivedate;
                    item.Sampling = entity.Sampling;
                    item.Addressid = entity.Addressid;
                    item.Approvaldate = entity.Approvaldate;
                    item.Currencyid = entity.Currencyid;
                    item.Exchangerate = entity.Exchangerate;
                    item.Costprice = entity.Costprice;
                    item.SalePrice = entity.SalePrice;
                    item.Dateofcosting = entity.Dateofcosting;
                    item.Comment = entity.Comment;
                    item.Createddate = entity.Createddate;
                    item.CreatedBy = entity.CreatedBy;
                    item.IsActive = entity.IsActive;
                    item.IsStatus = entity.IsStatus;
                    item.Company_ID = entity.Company_ID;
                    item.BranchId = entity.BranchId;
                    item.EnquiryTypeId = entity.Enquirytype;
                    item.IsReadymade = entity.IsReadmade;
                    if (entity.IsReadmade == 0)
                    {
                        item.Enquiryno = "RM-" + Convert.ToString(getnextenqno());
                    }
                    else
                    {
                        item.Enquiryno = "RF-" + Convert.ToString(getnextenqno());
                    }
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.CRM_EnquiryMaster.Add(item);
                    db.SaveChanges();
                    db.Configuration.ValidateOnSaveEnabled = true;

                    id = item.Id;

                    //code for save enquiry images
                    foreach (var img in entity.styleimglist)
                    {
                        if (img != "")
                        {
                            CRM_EnquiryStyleImage imgrecord = new CRM_EnquiryStyleImage();
                            imgrecord.Enquiryid = id;
                            imgrecord.styleimg = img;
                            imgrecord.Createdby = entity.CreatedBy;
                            imgrecord.Company_ID = entity.Company_ID;
                            imgrecord.BranchId = entity.BranchId;
                            imgrecord.IsActive = true;
                            // imgrecord.Createddate = DateTime.Now;
                            db.CRM_EnquiryStyleImage.Add(imgrecord);
                            db.SaveChanges();
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }
    }
}
