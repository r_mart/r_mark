﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.CommonEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;
namespace Garments_ERP.Data.Admin
{
    public class cl_CustomerPO
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        cl_role rlobj = new cl_role();
        cl_GRNInward grnobj = new cl_GRNInward();


        public List<M_POOtherChargesMasterEntity> getOtherCharge()
        {
            try
            {
                List<M_POOtherChargesMasterEntity> list = new List<M_POOtherChargesMasterEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.M_POOtherChargesMaster.Where(x => x.IsActive == true).ToList();
                    if(record!=null)
                    {
                        foreach(var data in record)
                        {
                            M_POOtherChargesMasterEntity ent = new M_POOtherChargesMasterEntity();
                            ent.id = data.id;
                            ent.POOtherChargeName = data.POOtherChargeName;
                            ent.Discription = data.Discription;
                            ent.IsActive = data.IsActive;
                            ent.CreatedOn = data.CreatedOn;
                            ent.CreatedBy = data.CreatedBy;
                            ent.UpdateOn = data.UpdateOn;
                            ent.UpdateBy = data.UpdateBy;
                            list.Add(ent);
                        }
                    }
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }


        //List Of Quotation With Status and User Wise
        public List<CRM_CustomerPOMasterentity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_CustomerPOMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.Craetedby == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.CRM_CustomerPOMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                foreach (var item in records)
                {

                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.Id = item.Id;
                    var QuotationNo = db.CRM_CustomerPOMaster.Where(x => x.customerid == item.customerid && x.IsActive == true && x.QuotID == item.QuotID).Select(u => u.CRM_QuotationMaster.QuotationNo).ToArray();
                    entity.POno = item.POno;
                    entity.IsReadmade = item.IsReadymade;
                    entity.PODate = item.PODate;
                    entity.Quotrefno = QuotationNo[0];
                    entity.Quotdate = item.Quotdate;
                    entity.Costprice = item.Costprice;
                    entity.Saleprice = item.Saleprice;
                    entity.customerid = item.customerid;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Costprice = item.Costprice;
                    entity.Addressid = item.Addressid;
                    entity.Remark = item.Remark;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;

                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdat = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).FirstOrDefault();
                        if (custdat != null)
                        {
                            custentity.Ledger_Id = custdat.Ledger_Id;
                            custentity.Ledger_Name = custdat.Ledger_Name;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                        }

                        entity.MLedgersEntity = custentity;
                    }
                    entity.Enquiryrefno = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.Enquiryrefno).FirstOrDefault();
                    long EnquiryID = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.EnquiryID).FirstOrDefault();
                    entity.ProductName = GetProductName(item.QuotID);
                    entity.StyleName = GetStyleName(item.QuotID);
                    entity.ProductAttrib = GetProductAttribName(EnquiryID);
                    entity.ProductQty = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == item.Id).Select(x => x.Totalqty).Distinct().FirstOrDefault();

                    list.Add(entity);


                }

                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.Craetedby)).OrderByDescending(x => x.Id).ToList();
                }
            }

            return list;

        }

        ////List Of Quotation With Status and User and Form Date And To Date Wise
        public List<CRM_CustomerPOMasterentity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_CustomerPOMaster.Where(x => x.IsActive == true && x.PODate >= from && x.PODate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.Craetedby == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.CRM_CustomerPOMaster.Where(x => x.IsActive == true && x.PODate >= from && x.PODate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }

                foreach (var item in records)
                {

                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.Id = item.Id;
                    var QuotationNo = db.CRM_CustomerPOMaster.Where(x => x.customerid == item.customerid && x.IsActive == true && x.QuotID == item.QuotID).Select(u => u.CRM_QuotationMaster.QuotationNo).ToArray();
                    entity.POno = item.POno;
                    entity.PODate = item.PODate;
                    entity.IsReadmade = item.IsReadymade;
                    entity.Quotrefno = QuotationNo[0];
                    entity.Quotdate = item.Quotdate;
                    entity.Costprice = item.Costprice;
                    entity.Saleprice = item.Saleprice;
                    entity.customerid = item.customerid;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Costprice = item.Costprice;
                    entity.Addressid = item.Addressid;
                    entity.Remark = item.Remark;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdat = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).FirstOrDefault();
                        if (custdat != null)
                        {
                            custentity.Ledger_Id = custdat.Ledger_Id;
                            custentity.Ledger_Name = custdat.Ledger_Name;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                        }

                        entity.MLedgersEntity = custentity;
                    }
                    entity.Enquiryrefno = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.Enquiryrefno).FirstOrDefault();
                    long EnquiryID = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.EnquiryID).FirstOrDefault();
                    entity.ProductName = GetProductName(item.QuotID);
                    entity.StyleName = GetStyleName(item.QuotID);
                    entity.ProductAttrib = GetProductAttribName(EnquiryID);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == EnquiryID).Select(x => x.Totalqty).Distinct().FirstOrDefault();

                    list.Add(entity);


                }

                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.PODate >= from && x.PODate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.Craetedby)).OrderByDescending(x => x.Id).ToList();
                }
            }
            return list;
        }

        //Reject Quotation to update Status
        public bool RejectedCPO(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_CustomerPOMaster.Find(id);

                    record.IsStatus = 5;
                    record.Updateddate = DateTime.Now;
                    record.Updatedby = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return isdeleted;
        }


        public object EditAttributeValue(string MyJson, int type, XmlDocument doc)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = type;
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = MyJson;
                cmd.Parameters.Add("attributexml", SqlDbType.VarChar).Value = doc.InnerXml;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            return dt;
        }

        public string getnextPONo()
        {
            string no;
            no = "CPO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.CRM_CustomerPOMaster.Count() > 0 ? db.CRM_CustomerPOMaster.Max(x => x.Id) + 1 : 1;
                //Int64? record = db.CRM_QuotationMaster.Max(x => x.Id);
                no = no + "-" + rec.ToString();
            }

            return no;
        }

        public List<CRM_CustomerPOMasterentity> Get(DateTime from, DateTime to)
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {

                var records = db.CRM_CustomerPOMaster.ToList().Where(x => x.IsActive == true && x.PODate >= from && x.PODate <= to).ToList();

                foreach (var item in records)
                {

                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.Id = item.Id;
                    entity.POno = item.POno;
                    entity.IsReadmade = item.IsReadymade;
                    entity.PODate = item.PODate;
                    var QuotationNo = db.CRM_CustomerPOMaster.Where(x => x.customerid == item.customerid && x.IsActive == true && x.QuotID == item.QuotID).Select(u => u.CRM_QuotationMaster.QuotationNo).ToArray();
                    entity.Quotrefno = QuotationNo[0];
                    entity.Quotdate = item.Quotdate;
                    entity.Costprice = item.Costprice;
                    entity.Saleprice = item.Saleprice;
                    entity.customerid = item.customerid;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Costprice = item.Costprice;
                    entity.Addressid = item.Addressid;
                    entity.Remark = item.Remark;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Comments = item.comments;
                    entity.IsStatus = item.IsStatus;
                    entity.BranchId = item.BranchId;
                    entity.Company_ID = item.Company_ID;

                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdat = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).FirstOrDefault();
                        if (custdat != null)
                        {
                            custentity.Ledger_Id = custdat.Ledger_Id;
                            custentity.Ledger_Name = custdat.Ledger_Name;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                        }

                        entity.MLedgersEntity = custentity;
                    }
                    entity.Enquiryrefno = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.Enquiryrefno).FirstOrDefault();
                    long EnquiryID = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.EnquiryID).FirstOrDefault();
                    entity.ProductName = GetProductName(item.QuotID);
                    entity.StyleName = GetStyleName(item.QuotID);
                    entity.ProductAttrib = GetProductAttribName(EnquiryID);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == EnquiryID).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<CRM_CustomerPOMasterentity> Get()
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_CustomerPOMaster.ToList().Where(x => x.IsActive == true);

                foreach (var item in records)
                {

                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.Id = item.Id;
                    var QuotationNo = db.CRM_CustomerPOMaster.Where(x => x.customerid == item.customerid && x.IsActive == true && x.QuotID == item.QuotID).Select(u => u.CRM_QuotationMaster.QuotationNo).ToArray();
                    entity.POno = item.POno;
                    entity.PODate = item.PODate;
                    entity.Quotrefno = QuotationNo[0];
                    entity.Quotdate = item.Quotdate;
                    entity.IsReadmade = item.IsReadymade;

                    entity.Costprice = item.Costprice;
                    entity.Saleprice = item.Saleprice;

                    entity.customerid = item.customerid;

                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;

                    entity.Costprice = item.Costprice;

                    entity.Addressid = item.Addressid;
                    entity.Remark = item.Remark;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;

                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdat = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).FirstOrDefault();
                        if (custdat != null)
                        {
                            custentity.Ledger_Id = custdat.Ledger_Id;
                            custentity.Ledger_Name = custdat.Ledger_Name;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                        }

                        entity.MLedgersEntity = custentity;
                    }
                    entity.Enquiryrefno = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.Enquiryrefno).FirstOrDefault();
                    long EnquiryID = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID && x.IsActive == true).Select(x => x.EnquiryID).FirstOrDefault();
                    entity.ProductName = GetProductName(item.QuotID);
                    entity.StyleName = GetStyleName(item.QuotID);
                    entity.ProductAttrib = GetProductAttribName(EnquiryID);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == EnquiryID).Select(x => x.Totalqty).Distinct().FirstOrDefault();

                    list.Add(entity);


                }
            }
            return list;
        }

        public string GetProductName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var itemid = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == Eid).Select(x => x.Itemid).Distinct().FirstOrDefault();
                itemname = db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.ItemName).FirstOrDefault();
            }
            return itemname;
        }

        public string GetStyleName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var styleid = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == Eid).Select(x => x.Styleid).Distinct().FirstOrDefault();
                itemname = db.M_StyleMaster.Where(x => x.Id == styleid).Select(x => x.Stylename).FirstOrDefault();
            }
            return itemname;
        }


        public string GetProductAttribName(long Eid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 34; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(Eid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                int i = 0;
                foreach (DataTable table in dt.Tables)
                {

                    foreach (DataRow dr in table.Rows)
                    {
                        if (i == 0)
                        {
                            itemname = Convert.ToString(dr["AttributeValue"]);
                        }
                        else
                        {
                            itemname = itemname + "," + Convert.ToString(dr["AttributeValue"]);
                        }
                    }
                    i++;
                }

            }
            return itemname;
        }


        public List<CRM_CustomerPOMasterentity> GetPO()
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 11; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = "";
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {

                        CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                        poentity.Id = Convert.ToInt64(dr["Id"]);
                        poentity.QuotationID = Convert.ToInt64(dr["QuotID"]);
                        poentity.Quotrefno = Convert.ToString(dr["Quotrefno"]);
                        poentity.Quotdate = Convert.ToDateTime(dr["Quotdate"]);
                        poentity.customerid = Convert.ToInt32(dr["customerid"]);
                        poentity.Approvalstatus = (Convert.ToString(dr["Approvalstatus"]) == "" || Convert.ToString(dr["Approvalstatus"]) == "1") ? false : true;
                        poentity.Approvaldate = Convert.ToDateTime(dr["Approvaldate"]);
                        poentity.Costprice = Convert.ToDecimal(dr["Costprice"]);
                        poentity.Saleprice = Convert.ToDecimal(dr["Saleprice"]);
                        poentity.Subtotal = Convert.ToDecimal(dr["Subtotal"]);
                        poentity.Tax = Convert.ToDecimal(dr["Tax"]);
                        poentity.Total = Convert.ToDecimal(dr["Total"]);
                        poentity.Remark = Convert.ToString(dr["Remark"]);
                        poentity.Addressid = Convert.ToInt32(dr["Addressid"]);
                        poentity.IsActive = (Convert.ToString(dr["IsActive"]) == "" || Convert.ToString(dr["IsActive"]) == "1") ? false : true;
                        poentity.POno = Convert.ToString(dr["POno"]);
                        poentity.PODate = Convert.ToDateTime(dr["PODate"]);
                        poentity.Sampling = (Convert.ToString(dr["Sampling"]) == "" || Convert.ToString(dr["Sampling"]) == "1") ? false : true;
                        poentity.Tentivedate = Convert.ToDateTime(dr["Tentivedate"]);
                        poentity.Company_ID = Convert.ToInt32(dr["Company_ID"]);
                        poentity.BranchId = Convert.ToInt32(dr["BranchId"]);
                        poentity.IsStatus = Convert.ToInt32(dr["IsStatus"]);
                        poentity.Comments = Convert.ToString(dr["comments"]);
                        long quoid = Convert.ToInt64(dr["QuotID"]);
                        int custid = Convert.ToInt32(dr["customerid"]);
                        using (var db = new GarmentERPDBEntities())
                        {
                            M_LedgersEntity ledentity = new M_LedgersEntity();
                            var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == custid).SingleOrDefault();
                            if (custdata != null)
                            {
                                ledentity.Ledger_Id = custdata.Ledger_Id;
                                ledentity.Ledger_Name = custdata.Ledger_Name;
                                poentity.MLedgersEntity = ledentity;
                            }
                            else
                            {
                                ledentity.Ledger_Id = custid;
                                ledentity.Ledger_Name = "--";
                                poentity.MLedgersEntity = ledentity;
                            }
                            long poid = Convert.ToInt64(dr["Id"]);
                            var POstyle = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == poid).FirstOrDefault();
                            CRM_CustomerPOStyleDetailEntity poent = new CRM_CustomerPOStyleDetailEntity();
                            poent.Id = POstyle.Id;
                            poentity.styledetailentity = poent;
                            poentity.Enquiryrefno = db.CRM_QuotationMaster.Where(x => x.Id == quoid && x.IsActive == true).Select(x => x.Enquiryrefno).FirstOrDefault();
                            long EnquiryID = db.CRM_QuotationMaster.Where(x => x.Id == quoid && x.IsActive == true).Select(x => x.EnquiryID).FirstOrDefault();
                            poentity.EnqId = EnquiryID;
                            poentity.ProductName = GetProductName(quoid);
                            poentity.StyleName = GetStyleName(quoid);
                            poentity.ProductAttrib = GetProductAttribName(EnquiryID);
                            poentity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == EnquiryID).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                        }
                        list.Add(poentity);

                    }
                }

            }
            return list;
        }

        public List<CRM_CustomerPOMasterentity> Getbycustomerid(long custid)
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_CustomerPOMaster.ToList().Where(x => x.customerid == custid && x.IsActive == true).ToList().OrderByDescending(x => x.Id);
                foreach (var item in records)
                {
                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.POno = item.POno;
                    entity.Id = item.Id;
                    list.Add(entity);
                }

            }
            return list;
        }

        public List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid)
        {
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_CustomerPOMaster.ToList().Where(x => x.customerid == custid && x.IsActive == true).ToList().OrderByDescending(x => x.Id);
                foreach (var item in records)
                {
                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.POno = item.POno;
                    entity.Id = item.Id;
                    list.Add(entity);
                }

            }
            return list;
        }

        
        public CRM_CustomerPOMasterentity getbyid(long id)
        {
            CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.CRM_CustomerPOMaster.Find(id);
                if (item != null)
                {
                    entity.Id = item.Id;
                    entity.POno = item.POno;
                    entity.PODate = item.PODate;
                    entity.Quotrefno = item.Quotrefno;
                    entity.IsReadmade = item.IsReadymade;
                    entity.QuotationID = item.QuotID;
                    var QuoNo = db.CRM_QuotationMaster.Where(x => x.Id == item.QuotID).Select(x => x.QuotationNo).FirstOrDefault();
                    CRM_QuotationMasterEntity qutentity = new CRM_QuotationMasterEntity();
                    qutentity.QuotationNo = QuoNo;
                    entity.QuoEntity = qutentity;
                    entity.Quotdate = item.Quotdate;
                    entity.Costprice = item.Costprice;
                    entity.Saleprice = item.Saleprice;
                    entity.customerid = item.customerid;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Costprice = item.Costprice;
                    entity.Tax = item.Tax;
                    entity.Subtotal = item.Subtotal;
                    entity.Total = item.Total;
                    entity.GrandTotal = item.GrandTotal;
                    entity.Addressid = item.Addressid;
                    entity.Remark = item.Remark;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Tentivedate = item.Tentivedate;
                    entity.IsStatus = item.IsStatus;
                    entity.Comments = item.comments;
                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.customerid != null)
                    {
                        var custdat = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).FirstOrDefault();
                        if (custdat != null)
                        {
                            custentity.Ledger_Id = custdat.Ledger_Id;
                            custentity.Ledger_Name = custdat.Ledger_Name;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                        }

                        entity.MLedgersEntity = custentity;
                    }


                    var otherCharge = db.T_POOtherCharge.Where(x => x.TXID == item.Id && x.InventoryModeId == 10007).ToList();
                    if (otherCharge != null)
                    {
                        List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                        foreach(var ocdata in otherCharge)
                        {
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.Id = ocdata.Id;
                            ent.TXID = ocdata.TXID;
                            ent.OtherChargeId = ocdata.OtherChargeId;
                            if(ocdata.OtherChargeId>0)
                            {
                                M_POOtherChargesMasterEntity ocmaster = new M_POOtherChargesMasterEntity();
                                string OCname = db.M_POOtherChargesMaster.Where(x => x.id == ocdata.OtherChargeId).Select(x => x.POOtherChargeName).FirstOrDefault();
                                ocmaster.POOtherChargeName = OCname;
                                ent.OCMaster = ocmaster;
                            }

                            ent.OtherChargeValue = ocdata.OtherChargeValue;
                            ent.GST = ocdata.GST;
                            ent.InventoryModeId = ocdata.InventoryModeId;
                            ent.Company_ID = ocdata.Company_ID;
                            ent.BranchId = ocdata.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = ocdata.CreatedBy;
                            oclist.Add(ent);
                        }
                        entity.POOtherChargeEntity = oclist;
                    }
                    
                    var stylerecord = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == item.Id).FirstOrDefault();

                    if (stylerecord != null)
                    {
                        CRM_CustomerPOStyleDetailEntity styleentty = new CRM_CustomerPOStyleDetailEntity();
                        styleentty.Id = stylerecord.Id;
                        styleentty.Poid = stylerecord.Poid;
                        styleentty.Segmenttypeid = stylerecord.Segmenttypeid;
                        var segName = db.M_SegmentTypeMaster.Where(x => x.Id == stylerecord.Segmenttypeid).Select(x => x.Segmenttype).FirstOrDefault();
                        M_SegmentTypeMasterEntity segent = new M_SegmentTypeMasterEntity();
                        segent.Segmenttype = segName;
                        styleentty.segTMent = segent;

                        styleentty.Styleid = stylerecord.Styleid;
                        var stylename = db.M_StyleMaster.Where(x => x.Id == stylerecord.Styleid).Select(x => x.Stylename).FirstOrDefault();
                        M_StyleMasterEntity styent = new M_StyleMasterEntity();
                        styent.Stylename = stylename;
                        styleentty.styleM = styent;

                        styleentty.Itemid = stylerecord.Itemid;
                        var itemname = db.M_ItemMaster.Where(x => x.Id == stylerecord.Itemid).Select(x => x.ItemName).FirstOrDefault();
                        M_ItemMasterEntity itement = new M_ItemMasterEntity();
                        var postyle = db.CRM_CustomerPOStyleSizeDetail.Where(x => x.POstyleid == stylerecord.Id).Select(x => x.Sizeid).FirstOrDefault();
                        int IAid = Convert.ToInt32(postyle);
                        int ISid = 0;
                        long itemid = Convert.ToInt64(stylerecord.Itemid);
                        itement.ItemName = grnobj.getItemName(itemid, IAid, ISid);
                        styleentty.itemM = itement;
                        
                        styleentty.Styleshadeid = stylerecord.Styleshadeid;
                        styleentty.Stylecolorid = stylerecord.Stylecolorid;
                        styleentty.Styledesc = stylerecord.Styledesc;
                        styleentty.Totalqty = stylerecord.Totalqty;
                        styleentty.Brand = stylerecord.Brand;
                        styleentty.Styleno = stylerecord.Styleno;
                        entity.styledetailentity = styleentty;

                        var sizerecords = db.CRM_CustomerPOStyleSizeDetail.Where(x => x.POstyleid == stylerecord.Id & x.IsActive == true).ToList();
                        List<CRM_CustoemrPOStyleSizeDetailEntity> sizelist = new List<CRM_CustoemrPOStyleSizeDetailEntity>();
                        decimal total_ = 0;
                        foreach (var itemsize in sizerecords)
                        {
                            CRM_CustoemrPOStyleSizeDetailEntity sizeentity = new CRM_CustoemrPOStyleSizeDetailEntity();
                            sizeentity.POstyleid = itemsize.Id;
                            sizeentity.SIzepcs = itemsize.SIzepcs;
                            total_ = total_ + (decimal)itemsize.SIzepcs;
                            sizeentity.Sizeper = itemsize.Sizeper;
                            sizeentity.Sizetype = itemsize.Sizetype;
                            sizeentity.Sizeid = itemsize.Sizeid;
                            sizeentity.Id = itemsize.Id;
                            sizeentity.Attribute_ID = itemsize.Attribute_ID;
                            sizeentity.Attribute_Value_ID = itemsize.Attribute_Value_ID;
                            sizeentity.rate = itemsize.rate;
                            sizeentity.SellRate = itemsize.SellRate;
                            sizeentity.GST = itemsize.GST;
                            sizeentity.Subtotal = itemsize.Subtotal;
                            sizeentity.IsActive = true;
                            sizelist.Add(sizeentity);
                        }

                        entity.styledetailentity.sizelist = sizelist;
                        entity.styledetailentity.Totalqty = total_;

                    }

                    var imgrecords = db.CRM_CustomerPOStyleImage.Where(x => x.POid == id && x.IsActive == true).ToList();
                    List<CRM_CustomerPOStyleImageEntity> listimg = new List<CRM_CustomerPOStyleImageEntity>();
                    foreach (var img in imgrecords)
                    {
                        CRM_CustomerPOStyleImageEntity imgentity = new CRM_CustomerPOStyleImageEntity();
                        imgentity.styleimg = img.styleimg;
                        imgentity.Id = img.Id;
                        imgentity.POid = img.POid;
                        listimg.Add(imgentity);

                    } 
                    entity.styleimglist = listimg;

                    var po_deliveryDetail = db.MFG_PO_DeliveryDetail.Where(x => x.CPO_ID == id && x.IsActive == true).ToList();
                    List<MFG_PO_DeliveryDetail_Entity> po_deliveryschedules = new List<MFG_PO_DeliveryDetail_Entity>();
                    foreach (var sche_ in po_deliveryDetail)
                    {
                        MFG_PO_DeliveryDetail_Entity po_delentity = new MFG_PO_DeliveryDetail_Entity();
                        po_delentity.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                        po_delentity.CPO_ID = sche_.CPO_ID;
                        po_delentity.Delivery_Date = sche_.Delivery_Date;
                        po_delentity.Delivery_Qty = sche_.Delivery_Qty;
                        po_delentity.Comments = sche_.Comments;
                        po_delentity.Item_ID = sche_.Item_ID;
                        po_delentity.Item_Attribute_Id = sche_.Item_Attribute_Id;
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        if (sche_.Item_ID != null)
                        {
                            var itemrecord = db.M_ItemMaster.Where(x => x.Id == sche_.Item_ID).SingleOrDefault();
                            itementity.Id = itemrecord.Id;
                            int ISid = Convert.ToInt32(itemrecord.ItemCategoryid);
                            cl_BillOfMaterialDetail Bobj = new cl_BillOfMaterialDetail();
                            long itemid =(long)sche_.Item_ID;
                            int Attribid = (int)sche_.Item_Attribute_Id;
                            itementity.ItemName = Bobj.getItemName(itemid, Attribid, ISid);
                        }
                        po_delentity.M_ItemMaster = itementity;
                        po_delentity.Ledger_Id = sche_.Ledger_Id;
                        po_deliveryschedules.Add(po_delentity);
                    }
                    entity.MFG_PO_DeliveryDetail = po_deliveryschedules;

                }

            }



            return entity;
        }
        

        public long Insert(CRM_CustomerPOMasterentity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                CRM_CustomerPOMaster record = new CRM_CustomerPOMaster();
                record.Id = entity.Id;
                record.PODate = entity.PODate;
                record.QuotID = entity.QuotationID;
                record.Quotrefno = entity.Quotrefno;
                record.Quotdate = entity.Quotdate;
                record.customerid = entity.customerid;
                record.Costprice = entity.Costprice;
                record.Saleprice = entity.Saleprice;
                record.customerid = entity.customerid;
                record.Approvalstatus = entity.Approvalstatus;
                record.Approvaldate = entity.Approvaldate;
                record.Costprice = entity.Costprice;
                record.Addressid = entity.Addressid;
                record.Remark = entity.Remark;
                record.Subtotal = entity.Subtotal;
                record.Tax = entity.Tax;
                record.Total = entity.Total;
                record.GrandTotal = entity.GrandTotal;
                record.Createddate = DateTime.Now;
                record.Craetedby = entity.Craetedby;
                record.IsActive = entity.IsActive;
                record.Tentivedate = entity.Tentivedate;
                record.comments = entity.Comments;
                record.IsStatus = entity.IsStatus;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.IsReadymade = entity.IsReadmade;
                if (entity.IsReadmade == 0)
                {
                    record.POno = "RM-" + Convert.ToString(getnextPONo());
                }
                else
                {
                    record.POno = "RF-" + Convert.ToString(getnextPONo());
                }
                db.CRM_CustomerPOMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
                if (id > 0)
                {

                    if(entity.POOtherChargeEntity!=null)
                    {
                        foreach(var data in entity.POOtherChargeEntity)
                        {
                            T_POOtherCharge ent = new T_POOtherCharge();
                            ent.TXID = id;
                            ent.OtherChargeId = data.OtherChargeId;
                            ent.OtherChargeValue = data.OtherChargeValue;
                            ent.GST = data.GST;
                            ent.InventoryModeId = data.InventoryModeId;
                            ent.Company_ID = data.Company_ID;
                            ent.BranchId = data.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = data.CreatedBy;
                            db.T_POOtherCharge.Add(ent);
                            db.SaveChanges();
                        }
                    }

                    if (entity.styledetailentity != null)
                    {
                        CRM_CustomerPOStyleDetail stylerecord = new CRM_CustomerPOStyleDetail();
                        stylerecord.Poid = id;
                        stylerecord.Styleid = entity.styledetailentity.Styleid;
                        stylerecord.Stylecolorid = entity.styledetailentity.Stylecolorid;
                        stylerecord.Styledesc = entity.styledetailentity.Styledesc;
                        stylerecord.Styleno = entity.styledetailentity.Styleno;
                        stylerecord.Styleshadeid = entity.styledetailentity.Styleshadeid;
                        stylerecord.Brand = entity.styledetailentity.Brand;
                        stylerecord.Totalqty = entity.styledetailentity.Totalqty;
                        stylerecord.IsActive = true;
                        stylerecord.Itemid = entity.styledetailentity.Itemid;
                        stylerecord.Segmenttypeid = entity.styledetailentity.Segmenttypeid;
                        stylerecord.Createdate = DateTime.Now;
                        stylerecord.Createdby = entity.Craetedby;
                        stylerecord.Company_ID = entity.Company_ID;
                        stylerecord.BranchId = entity.BranchId;
                        db.CRM_CustomerPOStyleDetail.Add(stylerecord);
                        db.SaveChanges();
                        long style_recordid = stylerecord.Id;

                        if (stylerecord.Id > 0)
                        {
                            foreach (var item in entity.styledetailentity.sizelist)
                            {
                                CRM_CustomerPOStyleSizeDetail sizerecord = new CRM_CustomerPOStyleSizeDetail();
                                sizerecord.POstyleid = style_recordid;
                                sizerecord.SIzepcs = item.SIzepcs;
                                sizerecord.Sizeper = 0;
                                sizerecord.Sizetype = "";
                                sizerecord.Sizeid = item.Sizeid;
                                sizerecord.Attribute_ID = item.Attribute_ID;
                                sizerecord.Attribute_Value_ID = item.Attribute_Value_ID;
                                sizerecord.rate = item.rate;
                                sizerecord.IsActive = true;
                                sizerecord.Createdby = entity.Craetedby;
                                sizerecord.Createddate = DateTime.Now;
                                sizerecord.Company_ID = entity.Company_ID;
                                sizerecord.BranchId = entity.BranchId;
                                sizerecord.SellRate = item.SellRate;
                                sizerecord.GST = item.GST;
                                sizerecord.Subtotal = item.Subtotal;
                                db.CRM_CustomerPOStyleSizeDetail.Add(sizerecord);
                                db.SaveChanges();

                                if (item.itemimg_list.Count > 0)
                                {
                                    foreach (var img in item.itemimg_list)
                                    {
                                        if (img != "")
                                        {
                                            M_ImageMaster record1 = new M_ImageMaster();
                                            record1.ItemId = entity.styledetailentity.Itemid;
                                            record1.Item_Attribute_ID = (int)item.Sizeid;
                                            record1.StyleId = (int)entity.styledetailentity.Styleid;
                                            record1.image = img;
                                            record1.IsCustomize = (int)id;
                                            record1.Createddate = entity.Createddate;
                                            record1.Createdby = entity.Craetedby;

                                            //New Code Added by Rahul on 09-01-2020
                                            //record.Updatedby = entity.Updatedby;
                                            //record.Updateddate = entity.Updateddate;
                                            record1.Company_ID = entity.Company_ID;
                                            record1.BranchId = entity.BranchId;
                                            record1.IsActive = entity.IsActive;
                                            db.M_ImageMaster.Add(record1);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (entity.imglist != null)
                    {
                        foreach (var img in entity.imglist)
                        {
                            CRM_CustomerPOStyleImage imgentity = new CRM_CustomerPOStyleImage();
                            imgentity.POid = id;
                            imgentity.styleimg = img;
                            imgentity.Createddate = DateTime.Now;
                            imgentity.IsActive = true;
                            imgentity.Createdby = entity.Craetedby;
                            imgentity.Company_ID = entity.Company_ID;
                            imgentity.BranchId = entity.BranchId;
                            db.CRM_CustomerPOStyleImage.Add(imgentity);
                            db.SaveChanges();

                        }
                    }

                    if (entity.MFG_PO_DeliveryDetail != null)
                    {
                        foreach (var sche_ in entity.MFG_PO_DeliveryDetail)
                        {
                            MFG_PO_DeliveryDetail po_delentity = new MFG_PO_DeliveryDetail();
                            po_delentity.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                            po_delentity.Ledger_Id = sche_.Ledger_Id;
                            po_delentity.CPO_ID = id;
                            po_delentity.Delivery_Qty = sche_.Delivery_Qty;
                            po_delentity.Delivery_Date = sche_.Delivery_Date;
                            po_delentity.Comments = sche_.Comments;
                            po_delentity.Item_ID = sche_.Item_ID;
                            po_delentity.Created_Date = sche_.Created_Date;
                            po_delentity.IsActive = sche_.IsActive;
                            po_delentity.Created_By = sche_.Created_By;
                            po_delentity.Item_Attribute_Id = sche_.Item_Attribute_Id;
                            //po_delentity.Company_ID = sche_.Company_ID;
                            //po_delentity.BranchId = sche_.BranchId;
                            db.MFG_PO_DeliveryDetail.Add(po_delentity);
                            db.SaveChanges();
                        }
                    }
                }

                if (id > 0)
                {
                    var data = db.CRM_QuotationMaster.Find(entity.QuotationID);
                    data.IsStatus = 4;
                    data.Createddate = DateTime.Now;
                    data.CreatedBy = entity.Craetedby;
                    db.SaveChanges();
                }
            }



            return id;
        }

        public bool Update(CRM_CustomerPOMasterentity entity)
        {
            bool isupdatd = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    CRM_CustomerPOMaster record = db.CRM_CustomerPOMaster.Find(entity.Id);

                    record.Costprice = entity.Costprice;
                    record.Saleprice = entity.Saleprice;
                    record.Costprice = entity.Costprice;
                    record.Tentivedate = entity.Tentivedate;
                    record.Remark = entity.Remark;
                    record.Subtotal = entity.Subtotal;
                    record.Total = entity.Total;
                    record.GrandTotal = entity.GrandTotal;
                    record.Updateddate = DateTime.Now;
                    record.Updatedby = entity.Updatedby;
                    record.IsStatus = entity.IsStatus;
                    record.Approvaldate = entity.Approvaldate;
                    record.Approvalstatus = entity.Approvalstatus;
                    record.comments = entity.Comments;

                    db.SaveChanges();
                    isupdatd = true;

                    if(entity.POOtherChargeEntity!=null)
                    {
                        var singleRec = db.T_POOtherCharge.Where(x => x.TXID == entity.Id && x.InventoryModeId == 10007).ToList();
                        foreach (var rec in singleRec)
                        {
                            var OCrecord = db.T_POOtherCharge.FirstOrDefault(x => x.Id == rec.Id);
                            db.T_POOtherCharge.Remove(OCrecord);
                            db.SaveChanges();
                        }

                        foreach (var data in entity.POOtherChargeEntity)
                        {
                            T_POOtherCharge ent = new T_POOtherCharge();
                            ent.TXID = entity.Id;
                            ent.OtherChargeId = data.OtherChargeId;
                            ent.OtherChargeValue = data.OtherChargeValue;
                            ent.GST = data.GST;
                            ent.InventoryModeId = data.InventoryModeId;
                            ent.Company_ID = data.Company_ID;
                            ent.BranchId = data.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = data.CreatedBy;
                            db.T_POOtherCharge.Add(ent);
                            db.SaveChanges();
                        }
                    }



                    var stylerecord = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == entity.Id);
                    stylerecord.FirstOrDefault().Totalqty = entity.styledetailentity.Totalqty;
                    db.SaveChanges();

                    foreach (var size in entity.styledetailentity.sizelist)
                    {

                        CRM_CustomerPOStyleSizeDetail sizerecord = db.CRM_CustomerPOStyleSizeDetail.Find(size.Id);
                        // sizerecord.POstyleid = stylerecord.Id;
                        sizerecord.SIzepcs = size.SIzepcs;
                        sizerecord.Sizeper = 0;
                        sizerecord.Sizetype = "";
                        sizerecord.Sizeid = size.Sizeid;
                        sizerecord.Attribute_ID = size.Attribute_ID;
                        sizerecord.Attribute_Value_ID = size.Attribute_Value_ID;
                        sizerecord.rate = size.rate;
                        sizerecord.IsActive = true;
                        sizerecord.Updatedby = entity.Updatedby;
                        sizerecord.Updateddate = DateTime.Now;
                        db.SaveChanges();

                        if (size.itemimg_list.Count > 0)
                        {
                            var imgrec = db.M_ImageMaster.Where(x => x.ItemId == entity.styledetailentity.Itemid && x.Item_Attribute_ID == size.Sizeid && x.IsCustomize == entity.Id).ToList();
                            if (imgrec != null)
                            {
                                foreach (var img in imgrec)
                                {
                                    var imgrecord = db.M_ImageMaster.Find(img.Id);
                                    imgrecord.IsActive = false;
                                    imgrecord.Updateddate = DateTime.Now;
                                    db.SaveChanges();
                                }
                            }

                            foreach (var img in size.itemimg_list)
                            {
                                if (img != "")
                                {
                                    M_ImageMaster record1 = new M_ImageMaster();
                                    record1.ItemId = entity.styledetailentity.Itemid;
                                    record1.Item_Attribute_ID = (int)size.Sizeid;
                                    record1.StyleId = (int)entity.styledetailentity.Styleid;
                                    record1.image = img;
                                    record1.IsCustomize = (int)entity.Id;
                                    record1.Createddate = entity.Createddate;
                                    record1.Createdby = entity.Craetedby;

                                    //New Code Added by Rahul on 09-01-2020
                                    //record.Updatedby = entity.Updatedby;
                                    //record.Updateddate = entity.Updateddate;
                                    record1.Company_ID = entity.Company_ID;
                                    record1.BranchId = entity.BranchId;
                                    record1.IsActive = entity.IsActive;
                                    db.M_ImageMaster.Add(record1);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }

                    //for old image
                    if (entity.styleimglist != null)
                    {
                        if (entity.styleimglist.Count > 0)
                        {
                            var oldimagerecordlist = db.CRM_CustomerPOStyleImage.Where(x => x.POid == entity.Id).ToList();
                            foreach (var oldimg in oldimagerecordlist)
                            {
                                var delerecord = db.CRM_CustomerPOStyleImage.Find(oldimg.Id);
                                delerecord.IsActive = false;
                                delerecord.Updatedby = entity.Updatedby;
                                delerecord.Updateddate = DateTime.Now;
                                db.SaveChanges();
                            }
                        }
                    }

                    //for new style image save
                    foreach (var img in entity.styleimglist)
                    {
                        CRM_CustomerPOStyleImage imgrecord = new CRM_CustomerPOStyleImage();
                        imgrecord.POid = entity.Id;
                        imgrecord.styleimg = img.styleimg;
                        imgrecord.IsActive = true;
                        imgrecord.Createddate = DateTime.Now;
                        imgrecord.Createdby = entity.Updatedby;
                        db.CRM_CustomerPOStyleImage.Add(imgrecord);
                        db.SaveChanges();

                    }

                    //Code to insert and update po delivery schedules....
                    if (entity.MFG_PO_DeliveryDetail != null)
                    {

                        foreach (var sche_ in entity.MFG_PO_DeliveryDetail)
                        {
                            if (sche_.PO_DelSchedule_ID > 0)
                            {
                                MFG_PO_DeliveryDetail po_delup = db.MFG_PO_DeliveryDetail.Find(sche_.PO_DelSchedule_ID);
                                po_delup.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                                //po_delup.Ledger_Id = sche_.Ledger_Id;
                                po_delup.CPO_ID = entity.Id;
                                po_delup.Delivery_Qty = sche_.Delivery_Qty;
                                po_delup.Delivery_Date = sche_.Delivery_Date;
                                po_delup.Comments = sche_.Comments;
                                po_delup.Item_ID = sche_.Item_ID;
                                po_delup.Item_Attribute_Id = sche_.Item_Attribute_Id;
                                po_delup.Modified_Date = sche_.Modified_Date;
                                po_delup.Modified_By = sche_.Modified_By;
                                db.SaveChanges();
                            }
                            else
                            {
                                MFG_PO_DeliveryDetail po_delentity = new MFG_PO_DeliveryDetail();
                                po_delentity.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                                po_delentity.Ledger_Id = sche_.Ledger_Id;
                                po_delentity.CPO_ID = entity.Id;
                                po_delentity.Delivery_Qty = sche_.Delivery_Qty;
                                po_delentity.Delivery_Date = sche_.Delivery_Date;
                                po_delentity.Comments = sche_.Comments;
                                po_delentity.Item_ID = sche_.Item_ID;
                                po_delentity.Item_Attribute_Id = sche_.Item_Attribute_Id;
                                po_delentity.Created_Date = DateTime.Now;
                                po_delentity.IsActive = sche_.IsActive;
                                po_delentity.Created_By =sche_.Created_By;
                                db.MFG_PO_DeliveryDetail.Add(po_delentity);
                                db.SaveChanges();
                            }

                        }
                    }

                }
                isupdatd = true;
            }
            catch (Exception)
            {

            }

            return isupdatd;
        }

        public bool Delete(long id, int Userid)
        {
            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.CRM_CustomerPOMaster.Find(id);
                record.IsActive = false;
                record.Approvalstatus = false;
                record.Updatedby = Userid;
                record.Updateddate = DateTime.Now;
                db.SaveChanges();
                deleted = true;

                long quoid = db.CRM_CustomerPOMaster.Where(x => x.Id == id).Select(x => x.QuotID).FirstOrDefault();
                var Quorecord = db.CRM_QuotationMaster.Find(quoid);
                Quorecord.IsStatus = 3;
                Quorecord.UpdatedBy = Userid;
                Quorecord.Updateddate = DateTime.Now;
                db.SaveChanges();

            }
            return deleted;
        }

        public object getbyPONO(string PONO)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_CustomerPO_report";
                cmd.Parameters.Add("POno", SqlDbType.NVarChar, 100).Value = PONO;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }

    }
}
