﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Data.Admin
{
   public class cl_Quotation
    {
        cl_Enquiry enqobj = new cl_Enquiry();
        cl_GRNInward grnobj = new cl_GRNInward();
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        cl_role rlobj = new cl_role();

        public long AddUpdate(List<string> Style_Img, List<string> RawItemImg, CRM_QuotationMasterEntity entity, List<CRM_QuotationRawItemDetailEntity> RawItem)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    if (entity.Id > 0)
                    {
                        var item = db.CRM_QuotationMaster.Find(entity.Id);
                        item.Enquiryrefno = entity.Enquiryrefno;
                        item.Quotationdate = entity.Quotationdate;
                        item.customerid = entity.customerid;
                        item.EnquiryID = entity.EnquiryID;
                        item.narration = entity.narration;
                        item.Approvalstatus = entity.Approvalstatus;
                        item.Tentivedate = entity.Tentivedate;
                        item.Sampling = entity.Sampling;
                        item.Addressid = entity.Addressid;
                        item.Approvaldate = entity.Approvaldate;
                        item.Comment = entity.Comment;
                        item.Createddate = entity.Createddate;
                        item.CreatedBy = entity.CreatedBy;
                        item.IsActive = entity.IsActive;
                        item.IsStatus = entity.IsStatus;
                        item.Company_ID = entity.Company_ID;
                        item.BranchId = entity.BranchId;
                        item.IsReadymade = entity.IsReadmade;
                        item.Costprice = entity.Costprice;
                        item.SalePrice = entity.SalePrice;
                        item.Tax = entity.Tax;
                        item.Total = entity.Total;
                        item.GrandTotal = entity.GrandTotal;
                        db.SaveChanges();
                        id = entity.Id;
                    }
                    else
                    {
                        CRM_QuotationMaster item = new CRM_QuotationMaster();
                        item.Enquiryrefno = entity.Enquiryrefno;
                        item.Quotationdate = entity.Quotationdate;
                        item.customerid = entity.customerid;
                        item.EnquiryID = entity.EnquiryID;
                        item.narration = entity.narration;
                        item.Approvalstatus = entity.Approvalstatus;
                        item.Tentivedate = entity.Tentivedate;
                        item.Sampling = entity.Sampling;
                        item.Addressid = entity.Addressid;
                        item.Approvaldate = entity.Approvaldate;
                        item.Comment = entity.Comment;
                        item.Createddate = entity.Createddate;
                        item.CreatedBy = entity.CreatedBy;
                        item.IsActive = entity.IsActive;
                        item.IsStatus = entity.IsStatus;
                        item.Company_ID = entity.Company_ID;
                        item.BranchId = entity.BranchId;
                        item.IsReadymade = entity.IsReadmade;
                        item.Costprice = entity.Costprice;
                        item.SalePrice = entity.SalePrice;
                        item.Tax = entity.Tax;
                        item.Total = entity.Total;
                        item.GrandTotal = entity.GrandTotal;
                        if (entity.IsReadmade == 0)
                        {
                            item.QuotationNo = "RM-" + Convert.ToString(getnextQuotatioNo());
                        }
                        else
                        {
                            item.QuotationNo = "RF-" + Convert.ToString(getnextQuotatioNo());
                        }
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.CRM_QuotationMaster.Add(item);
                        db.SaveChanges();
                        db.Configuration.ValidateOnSaveEnabled = true;

                        id = item.Id;
                    }

                    

                    if (id > 0)
                    {
                        if (entity.StyleEnt != null)
                        {
                            if (entity.StyleEnt.Count > 0)
                            {
                                foreach (var StyleData in entity.StyleEnt)
                                {
                                    long styleid = 0;
                                    if (StyleData.Id > 0)
                                    {
                                        var styleEnt = db.CRM_QuotationStyleDetail.Find(StyleData.Id);
                                        styleEnt.Quotationid = id;
                                        styleEnt.Segmenttypeid = StyleData.Segmenttypeid;
                                        styleEnt.Styleid = StyleData.Styleid;
                                        styleEnt.Itemid = StyleData.Itemid;
                                        styleEnt.Reqqty = StyleData.Reqqty;
                                        styleEnt.Createdate = entity.Createddate;
                                        styleEnt.Createdby = entity.CreatedBy;
                                        styleEnt.IsActive = true;
                                        styleEnt.Styleno = StyleData.Styleno;
                                        styleEnt.Styledesc = StyleData.Styledesc;
                                        styleEnt.Brand = StyleData.Brand;
                                        styleEnt.Totalqty = StyleData.Totalqty;
                                        styleEnt.Sizeid = StyleData.Sizeid;
                                        styleEnt.Attribute_ID = StyleData.Attribute_ID;
                                        styleEnt.Attribute_Value_ID = StyleData.Attribute_Value_ID;
                                        styleEnt.rate = StyleData.rate;
                                        styleEnt.Company_ID = entity.Company_ID;
                                        styleEnt.BranchId = entity.BranchId;
                                        styleEnt.SellRate = StyleData.SellRate;
                                        styleEnt.GST = StyleData.GST;
                                        styleEnt.Subtotal = StyleData.Subtotal;
                                        db.SaveChanges();
                                        styleid = StyleData.Id;
                                    }
                                    else
                                    {
                                        CRM_QuotationStyleDetail styleEnt = new CRM_QuotationStyleDetail();
                                        styleEnt.Quotationid = id;
                                        styleEnt.Segmenttypeid = StyleData.Segmenttypeid;
                                        styleEnt.Styleid = StyleData.Styleid;
                                        styleEnt.Itemid = StyleData.Itemid;
                                        styleEnt.Reqqty = StyleData.Reqqty;
                                        styleEnt.Createdate = entity.Createddate;
                                        styleEnt.Createdby = entity.CreatedBy;
                                        styleEnt.IsActive = true;
                                        styleEnt.Styleno = StyleData.Styleno;
                                        styleEnt.Styledesc = StyleData.Styledesc;
                                        styleEnt.Brand = StyleData.Brand;
                                        styleEnt.Totalqty = StyleData.Totalqty;
                                        styleEnt.Sizeid = StyleData.Sizeid;
                                        styleEnt.Attribute_ID = StyleData.Attribute_ID;
                                        styleEnt.Attribute_Value_ID = StyleData.Attribute_Value_ID;
                                        styleEnt.rate = StyleData.rate;
                                        styleEnt.Company_ID = entity.Company_ID;
                                        styleEnt.BranchId = entity.BranchId;
                                        styleEnt.SellRate = StyleData.SellRate;
                                        styleEnt.GST = StyleData.GST;
                                        styleEnt.Subtotal = StyleData.Subtotal;
                                        db.CRM_QuotationStyleDetail.Add(styleEnt);
                                        db.SaveChanges();
                                        styleid = styleEnt.Id;
                                        if (styleid > 0 && StyleData.StyleImgCount > 0)
                                        {
                                            var Style_Imgarray = Style_Img.ToList();
                                            for (int k = 0; k < StyleData.StyleImgCount; k++)
                                            {

                                                CRM_QuotationStyleImage ESImg_Ent = new CRM_QuotationStyleImage();
                                                ESImg_Ent.Quotationid = id;
                                                ESImg_Ent.Quotationimg = Convert.ToString(Style_Imgarray[k]);
                                                ESImg_Ent.Createdby = entity.CreatedBy;
                                                ESImg_Ent.IsActive = true;
                                                ESImg_Ent.Company_ID = entity.Company_ID;
                                                ESImg_Ent.BranchId = entity.BranchId;
                                                ESImg_Ent.QuoStyleId = styleid;
                                                db.CRM_QuotationStyleImage.Add(ESImg_Ent);
                                                db.SaveChanges();
                                                Style_Imgarray.RemoveAt(0);
                                            }
                                        }
                                    }


                                }
                            }

                            if (RawItem.Count > 0)
                            {
                                foreach (var RIEnt in RawItem)
                                {
                                    long EnqRI_Id = 0;
                                    if (RIEnt.Id > 0)
                                    {
                                        var RI = db.CRM_QuotationRawItemDetail.Find(RIEnt.Id);
                                        RI.Quotationid = id;
                                        RI.Itemcategoryid = RIEnt.Itemcategoryid;
                                        RI.Itemsubcategoryid = RIEnt.Itemsubcategoryid;
                                        RI.Itemdesc = RIEnt.Itemdesc;
                                        RI.Itemid = RIEnt.Itemid;
                                        RI.Supplier = RIEnt.Supplier;
                                        RI.Createdate = entity.Createddate;
                                        RI.Createdby = entity.CreatedBy;
                                        RI.IsActive = true;
                                        RI.Company_ID = entity.Company_ID;
                                        RI.BranchId = entity.BranchId;
                                        RI.QuoStyleId = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == id && x.Itemid == RIEnt.Itemid).Select(x => x.Id).FirstOrDefault();
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        CRM_QuotationRawItemDetail RI = new CRM_QuotationRawItemDetail();
                                        RI.Quotationid = id;
                                        RI.Itemcategoryid = RIEnt.Itemcategoryid;
                                        RI.Itemsubcategoryid = RIEnt.Itemsubcategoryid;
                                        RI.Itemdesc = RIEnt.Itemdesc;
                                        RI.Itemid = RIEnt.Itemid;
                                        RI.Supplier = RIEnt.Supplier;
                                        RI.Createdate = entity.Createddate;
                                        RI.Createdby = entity.CreatedBy;
                                        RI.IsActive = true;
                                        RI.Company_ID = entity.Company_ID;
                                        RI.BranchId = entity.BranchId;
                                        RI.QuoStyleId = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == id && x.Itemid == RIEnt.Itemid).Select(x => x.Id).FirstOrDefault();
                                        db.CRM_QuotationRawItemDetail.Add(RI);
                                        db.SaveChanges();
                                        EnqRI_Id = RI.Id;
                                        if (EnqRI_Id > 0 && RIEnt.RawImgCount > 0 && RawItemImg.Count>0)
                                        {
                                            var RawItemImgarray = RawItemImg.ToList();
                                            for (int k = 0; k < RIEnt.RawImgCount; k++)
                                            {
                                                CRM_QuotationRawItemImage ESImg_Ent = new CRM_QuotationRawItemImage();
                                                ESImg_Ent.Quotationrawitemdetailid = EnqRI_Id;
                                                ESImg_Ent.image = Convert.ToString(RawItemImgarray[k]);
                                                ESImg_Ent.Createdby = entity.CreatedBy;
                                                ESImg_Ent.IsActive = true;
                                                ESImg_Ent.Company_ID = entity.Company_ID;
                                                ESImg_Ent.BranchId = entity.BranchId;
                                                db.CRM_QuotationRawItemImage.Add(ESImg_Ent);
                                                db.SaveChanges();
                                                RawItemImgarray.RemoveAt(0);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (entity.POOtherChargeEntity != null)
                        {
                            foreach (var data in entity.POOtherChargeEntity)
                            {
                                T_POOtherCharge ent = new T_POOtherCharge();
                                ent.TXID = id;
                                ent.OtherChargeId = data.OtherChargeId;
                                ent.OtherChargeValue = data.OtherChargeValue;
                                ent.GST = data.GST;
                                ent.InventoryModeId = 10011;
                                ent.Company_ID = entity.Company_ID;
                                ent.BranchId = entity.BranchId;
                                ent.CreatedOn = DateTime.Now;
                                ent.CreatedBy = entity.CreatedBy;
                                db.T_POOtherCharge.Add(ent);
                                db.SaveChanges();
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return id;
        }

        public List<CRM_QuotationStyleDetailEntity> GetQuoStyleData(long QuoId)
        {
            List<CRM_QuotationStyleDetailEntity> list = new List<CRM_QuotationStyleDetailEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == QuoId).ToList();
                    foreach (var rec in record)
                    {
                        CRM_QuotationStyleDetailEntity ent = new CRM_QuotationStyleDetailEntity();
                        ent.Id = rec.Id;
                        ent.NopOrderItemId = 0;
                        ent.Quotationid = rec.Quotationid;
                        ent.Segmenttypeid = rec.Segmenttypeid;
                        var SegRec = db.M_SegmentTypeMaster.Where(x => x.Id == rec.Segmenttypeid).FirstOrDefault();
                        if (SegRec != null)
                        {
                            M_SegmentTypeMasterEntity segTypeE = new M_SegmentTypeMasterEntity();
                            segTypeE.Segmenttype = SegRec.Segmenttype;
                            ent.SegTypeEnt = segTypeE;
                        }
                        ent.Styleid = rec.Styleid;
                        var styleRec = db.M_StyleMaster.Where(x => x.Id == rec.Styleid).FirstOrDefault();
                        if (styleRec != null)
                        {
                            M_StyleMasterEntity styent = new M_StyleMasterEntity();
                            styent.Stylename = styleRec.Stylename;
                            ent.styleEnt = styent;
                        }
                        ent.Styleno = rec.Styleno;
                        ent.Styledesc = rec.Styledesc;
                        ent.Brand = rec.Brand;
                        ent.Itemid = rec.Itemid;
                        var itemEnt = db.M_ItemMaster.Where(x => x.Id == rec.Itemid).FirstOrDefault();
                        if (itemEnt != null)
                        {
                            M_ItemMasterEntity iteme = new M_ItemMasterEntity();
                            long itemid = Convert.ToInt64(rec.Itemid);
                            int Attiid = Convert.ToInt32(rec.Sizeid);
                            iteme.ItemName = enqobj.getItemName(itemid, Attiid, 0);
                            ent.ItemEnt = iteme;
                            ent.ItemUnit = db.M_UnitMaster.Where(x => x.Id == itemEnt.Unit).Select(x => x.ItemUnit).FirstOrDefault();
                        }
                        ent.Reqqty = rec.Reqqty;
                        ent.Createdate = rec.Createdate;
                        ent.Createdby = rec.Createdby;
                        ent.Totalqty = rec.Totalqty;
                        ent.rate = rec.rate;
                        ent.Company_ID = rec.Company_ID;
                        ent.BranchId = rec.BranchId;
                        ent.IsActive = rec.IsActive;
                        ent.Sizeid = rec.Sizeid;
                        ent.Attribute_ID = rec.Attribute_ID;
                        var attid = Convert.ToInt32(rec.Attribute_ID);
                        var Attrec = db.M_Attribute_Master.Where(x => x.Attribute_ID == attid).FirstOrDefault();
                        if (Attrec != null)
                        {
                            M_Attribute_MasterEntity Attre = new M_Attribute_MasterEntity();
                            Attre.Attribute_Name = Attrec.Attribute_Name;
                            ent.AttrEnt = Attre;
                        }
                        ent.Attribute_Value_ID = rec.Attribute_Value_ID;
                        var attValid = Convert.ToInt32(rec.Attribute_Value_ID);
                        var AttValrec = db.M_Attribute_Value_Master.Where(x => x.Attribute_Value_ID == attValid).FirstOrDefault();
                        if (AttValrec != null)
                        {
                            M_Attribute_Value_MasterEntity Attrvale = new M_Attribute_Value_MasterEntity();
                            Attrvale.Attribute_Value = AttValrec.Attribute_Value;
                            ent.AttrValEnt = Attrvale;
                        }
                        List<CRM_QuotationStyleImageEntity> listimg = new List<CRM_QuotationStyleImageEntity>();
                        var QuoStyleRecImage = db.CRM_QuotationStyleImage.Where(x => x.Quotationid == QuoId && x.QuoStyleId == rec.Id).ToList();
                        if (QuoStyleRecImage != null && QuoStyleRecImage.Count > 0)
                        {
                            foreach (var img in QuoStyleRecImage)
                            {
                                CRM_QuotationStyleImageEntity ESImg_Ent = new CRM_QuotationStyleImageEntity();
                                ESImg_Ent.Quotationid = QuoId;
                                ESImg_Ent.Quotationimg = img.Quotationimg;
                                ESImg_Ent.QuoStyleId = img.QuoStyleId;
                                listimg.Add(ESImg_Ent);
                            }
                        }
                        ent.StyleImage = listimg;
                        ent.SellRate = rec.SellRate;
                        ent.GST = rec.GST;
                        ent.Subtotal = rec.Subtotal;
                        list.Add(ent);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return list;
        }


        public List<CRM_QuotationRawItemDetailEntity> GetQuoStyleRawItemData(long QuoId, long styleid)
        {
            List<CRM_QuotationRawItemDetailEntity> list = new List<CRM_QuotationRawItemDetailEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_QuotationRawItemDetail.Where(x => x.Quotationid == QuoId && x.QuoStyleId == styleid).ToList();
                    foreach (var rec in record)
                    {
                        CRM_QuotationRawItemDetailEntity ent = new CRM_QuotationRawItemDetailEntity();
                        ent.Id = rec.Id;
                        ent.Quotationid = rec.Quotationid;
                        ent.Itemcategoryid = rec.Itemcategoryid;
                        var catname = db.M_ItemCategoryMaster.Where(x => x.Id == rec.Itemcategoryid).FirstOrDefault();
                        ent.ItemCategoryName = catname != null ? catname.Itemcategory : "";
                        ent.Itemsubcategoryid = rec.Itemsubcategoryid;
                        var catsubname = db.M_ItemSubCategoryMaster.Where(x => x.Id == rec.Itemsubcategoryid).FirstOrDefault();
                        ent.SubCategoryName = catsubname != null ? catsubname.Itemsubcategory : "";
                        ent.Itemid = rec.Itemid;
                        ent.Itemdesc = rec.Itemdesc;
                        ent.Supplier = rec.Supplier;
                        ent.Createdate = rec.Createdate;
                        ent.Createdby = rec.Createdby;
                        ent.IsActive = rec.IsActive;
                        ent.Company_ID = rec.Company_ID;
                        ent.BranchId = rec.BranchId;
                        ent.QuoStyleId = rec.QuoStyleId;
                        //List<CRM_EnquiryRawItemImageEntity> imglist = new List<CRM_EnquiryRawItemImageEntity>();
                        //var imgrec = db.CRM_EnquiryRawItemImage.Where(x => x.Enquiryrawitemdetailid == rec.Id).ToList();
                        //foreach (var img in imgrec)
                        //{
                        //    CRM_EnquiryRawItemImageEntity imgent = new CRM_EnquiryRawItemImageEntity();
                        //    imgent.Id = img.Id;
                        //    imgent.Enquiryrawitemdetailid = img.Enquiryrawitemdetailid;
                        //    imgent.image = img.image;
                        //    imgent.Createddate = img.Createddate;
                        //    imgent.Createdby = img.Createdby;
                        //    imgent.IsActive = img.IsActive;
                        //    imglist.Add(imgent);
                        //}
                        //ent.RawItemImg = imglist;

                        list.Add(ent);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return list;
        }

        //Reject Quotation to update Status
        public bool RejectedQuo(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.CRM_QuotationMaster.Find(id);
                    record.IsStatus = 5;
                    record.Updateddate = DateTime.Now;
                    record.UpdatedBy = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }

        //List Of Quotation With Status and User Wise
        public List<CRM_QuotationMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<CRM_QuotationMasterEntity> list = new List<CRM_QuotationMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_QuotationMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x => x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.CRM_QuotationMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                foreach (var item in records)
                {
                    CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
                    entity.Id = item.Id;
                    entity.Enquiryrefno = item.Enquiryrefno;
                    entity.QuotationNo = item.QuotationNo;
                    entity.Quotationdate = item.Quotationdate;
                    entity.Enquiryrefno = item.Enquiryrefno;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.customerid = item.customerid;
                    entity.narration = item.narration;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Currencyid = item.Currencyid;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Comment = item.Comment;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.Total = item.Total;
                    entity.IsStatus = item.IsStatus;
                    entity.IsReadmade = item.IsReadymade;
                    entity.GrandTotal = item.GrandTotal;
                    entity.POStatus = CheckPO(item.Id);

                    M_LedgersEntity custentity = new M_LedgersEntity();

                    if (item.customerid != null)
                    {
                        var custData = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (custData != null)
                        {
                            custentity.Ledger_Id = custData.Ledger_Id;
                            custentity.Ledger_Name = custData.Ledger_Name;
                            entity.mLedgers = custentity;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                            entity.mLedgers = custentity;
                        }
                    }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.EnquiryID);
                    entity.ProductQty = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == item.Id).Select(x => x.Reqqty).Distinct().FirstOrDefault();
                    list.Add(entity);
                }

                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();
                }
            }

            return list;

        }

        ////List Of Quotation With Status and User and Form Date And To Date Wise
        public List<CRM_QuotationMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<CRM_QuotationMasterEntity> list = new List<CRM_QuotationMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.CRM_QuotationMaster.Where(x => x.IsActive == true && x.Quotationdate >= from && x.Quotationdate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x => x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.CRM_QuotationMaster.Where(x => x.IsActive == true && x.Quotationdate >= from && x.Quotationdate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                
                foreach (var item in records)
                {
                    CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
                    entity.Id = item.Id;
                    entity.IsReadmade = item.IsReadymade;
                    entity.Enquiryrefno = item.Enquiryrefno;
                    entity.QuotationNo = item.QuotationNo;
                    entity.Quotationdate = item.Quotationdate;
                    entity.Enquiryrefno = item.Enquiryrefno;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.customerid = item.customerid;
                    entity.narration = item.narration;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Currencyid = item.Currencyid;
                    entity.Exchangerate = item.Exchangerate;
                    entity.Costprice = item.Costprice;
                    entity.SalePrice = item.SalePrice;
                    entity.Dateofcosting = item.Dateofcosting;
                    entity.Comment = item.Comment;
                    entity.Subtotal = item.Subtotal;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.Total = item.Total;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.GrandTotal = item.GrandTotal;
                    entity.POStatus = CheckPO(item.Id);

                    M_LedgersEntity custentity = new M_LedgersEntity();

                    if (item.customerid != null)
                    {
                        var custData = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (custData != null)
                        {
                            custentity.Ledger_Id = custData.Ledger_Id;
                            custentity.Ledger_Name = custData.Ledger_Name;
                            entity.mLedgers = custentity;
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                            entity.mLedgers = custentity;
                        }
                    }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.EnquiryID);
                    entity.ProductQty = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == item.Id).Select(x => x.Reqqty).Distinct().FirstOrDefault();
                    list.Add(entity);
                }
                if ( roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.Quotationdate >= from && x.Quotationdate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).ToList();
                }
            }
            return list;
        }


        public object _getGSTTaxlist()
        {
            
            using (SqlConnection cs = new SqlConnection(con))
            {
                string sqlquery = "select distinct g1.Tax_Rate,g2.Category_Name from [dbo].[Acc_GST_Charge_Category_Tax_Rate] g1 inner join [dbo].[Acc_GST_Charge_Category] g2 on(g1.GST_Charge_Category_Id = g2.GST_Charge_Category_Id)";
                SqlCommand cmd = new SqlCommand(sqlquery, cs);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }

        }

       public string getnextQuotatioNo()
       {
           string no;
           no = "CQUO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
           using (var db = new GarmentERPDBEntities())
           {
               var rec=db.CRM_QuotationMaster.Count()>0?db.CRM_QuotationMaster.Max(x => x.Id)+1:1;
              //Int64? record = db.CRM_QuotationMaster.Max(x => x.Id);
               no = no + "-" + rec.ToString();
           }

           return no;
       }

        
       public CRM_QuotationMasterEntity getbyid(long id)
       {
           CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
           using (var db = new GarmentERPDBEntities())
           {
               var item = db.CRM_QuotationMaster.Find(id);
               // CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
               entity.Id = item.Id;
                entity.IsReadmade = item.IsReadymade;
                entity.Enquiryrefno = item.Enquiryrefno;
                entity.QuotationNo = item.QuotationNo;
               entity.Quotationdate = item.Quotationdate ;
               entity.customerid = item.customerid;
                var ledname = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).Select(x => x.Ledger_Name).FirstOrDefault();
                M_LedgersEntity ledent = new M_LedgersEntity();
                ledent.Ledger_Name = ledname;
                entity.mLedgers = ledent;

               entity.narration = item.narration;
               entity.Approvalstatus = item.Approvalstatus;
               entity.Approvaldate = item.Approvaldate;
               entity.Currencyid = item.Currencyid;
               entity.Exchangerate = item.Exchangerate;
               entity.Subtotal = item.Subtotal;
               entity.Total = item.Total;
               entity.Tax = item.Tax;
                entity.Sampling = item.Sampling;
               entity.Costprice = item.Costprice;
               entity.SalePrice = item.SalePrice;
               entity.Dateofcosting = item.Dateofcosting;
               entity.Comment = item.Comment;
               entity.Createddate = item.Createddate;
               entity.CreatedBy = item.CreatedBy;
               entity.Updateddate = item.Updateddate;
               entity.UpdatedBy = item.UpdatedBy;
               entity.IsActive = item.IsActive;
               entity.Addressid = item.Addressid;
               entity.Tentivedate = item.Tentivedate;
               entity.Enquiryrefno = item.Enquiryrefno;
                entity.EnquiryID = item.EnquiryID;
                entity.IsStatus = item.IsStatus;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;

                var otherCharge = db.T_POOtherCharge.Where(x => x.TXID == item.Id && x.InventoryModeId == 10011).ToList();
                if (otherCharge != null)
                {
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in otherCharge)
                    {
                        T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                        ent.Id = ocdata.Id;
                        ent.TXID = ocdata.TXID;
                        ent.OtherChargeId = ocdata.OtherChargeId;
                        if (ocdata.OtherChargeId > 0)
                        {
                            M_POOtherChargesMasterEntity ocmaster = new M_POOtherChargesMasterEntity();
                            string OCname = db.M_POOtherChargesMaster.Where(x => x.id == ocdata.OtherChargeId).Select(x => x.POOtherChargeName).FirstOrDefault();
                            ocmaster.POOtherChargeName = OCname;
                            ent.OCMaster = ocmaster;
                        }

                        ent.OtherChargeValue = ocdata.OtherChargeValue;
                        ent.GST = ocdata.GST;
                        ent.InventoryModeId = ocdata.InventoryModeId;
                        ent.Company_ID = ocdata.Company_ID;
                        ent.BranchId = ocdata.BranchId;
                        ent.CreatedOn = DateTime.Now;
                        ent.CreatedBy = ocdata.CreatedBy;
                        oclist.Add(ent);
                    }
                    entity.POOtherChargeEntity = oclist;
                }


                var styleimglist = db.CRM_QuotationStyleImage.Where(x => x.Quotationid == id && x.IsActive == true).ToList();
               List<CRM_QuotationStyleImageEntity> listimg = new List<CRM_QuotationStyleImageEntity>();
                if (styleimglist.Count != 0)
                {
                    foreach (var img in styleimglist)
                    {
                        CRM_QuotationStyleImageEntity imgentity = new CRM_QuotationStyleImageEntity();
                        imgentity.Id = img.Id;
                        imgentity.Quotationimg = img.Quotationimg;
                        listimg.Add(imgentity);
                    }
                }
                else
                {
                    var Simglist = db.CRM_EnquiryStyleImage.Where(x => x.Enquiryid == item.EnquiryID && x.IsActive == true).ToList();
                    if (Simglist.Count != 0)
                    {
                        foreach (var img in Simglist)
                        {
                            CRM_QuotationStyleImageEntity imgentity1 = new CRM_QuotationStyleImageEntity();
                            imgentity1.Id = img.Id;
                            imgentity1.Quotationimg = img.styleimg;
                            listimg.Add(imgentity1);
                        }
                    }
                }

               entity.imgelist = listimg;

               var stylerecord = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == id && x.IsActive==true).FirstOrDefault();
               CRM_QuotationStyleDetailEntity stylenetity = new CRM_QuotationStyleDetailEntity();
               if(stylerecord!=null)
               {
                   stylenetity.Id = stylerecord.Id;
                   stylenetity.Segmenttypeid = stylerecord.Segmenttypeid;

                    var segName = db.M_SegmentTypeMaster.Where(x => x.Id == stylerecord.Segmenttypeid).Select(x => x.Segmenttype).FirstOrDefault();
                    M_SegmentTypeMasterEntity segent = new M_SegmentTypeMasterEntity();
                    segent.Segmenttype = segName;
                    stylenetity.SegTypeEnt = segent;

                    stylenetity.Styleid = stylerecord.Styleid;
                    var stylename = db.M_StyleMaster.Where(x => x.Id == stylerecord.Styleid).Select(x => x.Stylename).FirstOrDefault();
                    M_StyleMasterEntity styent = new M_StyleMasterEntity();
                    styent.Stylename = stylename;
                    stylenetity.styleEnt = styent;

                    stylenetity.Itemid = stylerecord.Itemid;
                    var itemname = db.M_ItemMaster.Where(x => x.Id == stylerecord.Itemid).Select(x => x.ItemName).FirstOrDefault();
                    M_ItemMasterEntity itement = new M_ItemMasterEntity();
                    int IAid = Convert.ToInt32(stylerecord.Sizeid);
                    int ISid = 0;
                    long itemid = Convert.ToInt64(stylerecord.Itemid);
                    itement.ItemName = grnobj.getItemName(itemid, IAid, ISid);
                    stylenetity.ItemEnt = itement;

                    stylenetity.Stylecolorid = stylerecord.Stylecolorid;
                   stylenetity.Styledesc = stylerecord.Styledesc;
                   stylenetity.Styleno = stylerecord.Styleno;
                   stylenetity.Styleshadeid = stylerecord.Styleshadeid;
                   stylenetity.Totalqty = stylerecord.Totalqty;
                   stylenetity.Brand = stylerecord.Brand;
                   stylenetity.Styleimage = stylerecord.Styleimage;
                    stylenetity.SellRate = stylerecord.SellRate;
                    stylenetity.GST = stylerecord.GST;
                    stylenetity.Subtotal = stylerecord.Subtotal;

                    entity.style = stylenetity;

                  // var stylesizerecord = db.CRM_QuotationStyleSizeDetail.Where(x => x.Quotationstyleid == stylerecord.Id).ToList();
                   var stylesizerecord = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == id).ToList();
                   List<StyleSizeEntity> liststylesize = new List<StyleSizeEntity>();
                   int total_ = 0;
                   foreach(var size in stylesizerecord)
                   {
                       StyleSizeEntity sizeentity = new StyleSizeEntity();
                       sizeentity.Id = size.Id;
                       sizeentity.Sizeper = size.Sizeper;
                       sizeentity.Sizetype = size.Sizetype;
                       sizeentity.Sizepcs = size.Reqqty;
                       total_ = total_ + (int)size.Reqqty;
                       sizeentity.SizeId = size.Sizeid;
                       liststylesize.Add(sizeentity);
                       //sizeentity.Reqqty = size.Sizepcs;
                       //sizeentity.si=size.Sizepcs
                   }
                   entity.style.Totalqty = total_;
                   entity.style.stylesizelist = liststylesize;
             }

             

             
          }

           return entity;
       }

       public List<CRM_QuotationMasterEntity> get(DateTime from,DateTime to)
       {
           List<CRM_QuotationMasterEntity> list = new List<CRM_QuotationMasterEntity>();

           using (var db = new GarmentERPDBEntities())
           {
                
                var records = db.CRM_QuotationMaster.ToList().Where(x => x.IsActive == true && x.Quotationdate>=from && x.Quotationdate<=to).ToList();
               foreach (var item in records)
               {
                   CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
                   entity.Id = item.Id;
                    entity.IsReadmade = item.IsReadymade;
                    entity.Enquiryrefno = item.Enquiryrefno;
                   entity.QuotationNo = item.QuotationNo;
                   entity.Quotationdate = item.Quotationdate;
                   entity.Enquiryrefno = item.Enquiryrefno;
                   entity.Dateofcosting = item.Dateofcosting;
                   entity.Exchangerate = item.Exchangerate;
                   entity.Costprice = item.Costprice;
                   entity.SalePrice = item.SalePrice;

                   entity.customerid = item.customerid;
                   entity.narration = item.narration;
                   entity.Approvalstatus = item.Approvalstatus;
                   entity.Approvaldate = item.Approvaldate;
                   entity.Currencyid = item.Currencyid;
                   entity.Exchangerate = item.Exchangerate;
                   entity.Costprice = item.Costprice;
                   entity.SalePrice = item.SalePrice;
                   entity.Dateofcosting = item.Dateofcosting;
                   entity.Comment = item.Comment;
                   entity.Subtotal = item.Subtotal;
                   entity.Createddate = item.Createddate;
                   entity.CreatedBy = item.CreatedBy;
                   entity.Updateddate = item.Updateddate;
                   entity.UpdatedBy = item.UpdatedBy;
                   entity.IsActive = item.IsActive;
                    entity.Total = item.Total;
                    entity.POStatus = CheckPO(item.Id);
                    M_LedgersEntity custentity = new M_LedgersEntity();

                   if (item.customerid != null)
                   {
                        var qtcust = db.M_Ledgers.Where(x => x.Ledger_Id == item.customerid).SingleOrDefault();
                        if (qtcust != null)
                        {
                            custentity.Ledger_Id = qtcust.Ledger_Id;
                            custentity.Ledger_Name = qtcust.Ledger_Name;
                            entity.mLedgers = custentity;
                        }
                        else
                        {
                            custentity.Ledger_Id =Convert.ToInt32(item.customerid);
                            custentity.Ledger_Name = "--";
                            entity.mLedgers = custentity;
                        }
                   }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.EnquiryID);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == item.EnquiryID).Select(x => x.Totalqty).Distinct().FirstOrDefault();

                    list.Add(entity);
               }
           }

           return list;

       }

        public string GetProductName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var itemid = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == Eid).Select(x => x.Itemid).Distinct().FirstOrDefault();
                itemname = db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.ItemName).FirstOrDefault();
            }
            return itemname;
        }

        public string GetStyleName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var styleid = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == Eid).Select(x => x.Styleid).Distinct().FirstOrDefault();
                itemname = db.M_StyleMaster.Where(x => x.Id == styleid).Select(x => x.Stylename).FirstOrDefault();
            }
            return itemname;
        }

        public string GetProductAttribName(long Eid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 34; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(Eid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                int i = 0;
                foreach (DataTable table in dt.Tables)
                {

                    foreach (DataRow dr in table.Rows)
                    {
                        if (i == 0)
                        {
                            itemname = Convert.ToString(dr["AttributeValue"]);
                        }
                        else
                        {
                            itemname = itemname + "," + Convert.ToString(dr["AttributeValue"]);
                        }
                    }
                    i++;
                }

            }
            return itemname;
        }


        public List<CRM_QuotationMasterEntity> get()
         {
           List<CRM_QuotationMasterEntity> list = new List<CRM_QuotationMasterEntity>();

           using(var db=new GarmentERPDBEntities())
           {
                var records=db.CRM_QuotationMaster.ToList().Where(x=>x.IsActive==true);
               
                foreach (var item in records)
	           {    
		             CRM_QuotationMasterEntity entity=new CRM_QuotationMasterEntity();
                      entity.Id =item.Id;
                    entity.IsReadmade = item.IsReadymade;
                    entity.Enquiryrefno = item.Enquiryrefno;
                      entity.QuotationNo =item.QuotationNo;
                      entity.Quotationdate =item.Quotationdate;
                      entity.Enquiryrefno=item.Enquiryrefno;
                      entity.Dateofcosting = item.Dateofcosting;
                      entity.Exchangerate = item.Exchangerate;
                      entity.Costprice = item.Costprice;
                      entity.SalePrice = item.SalePrice;

                      entity.customerid =item.customerid;
                      entity.narration =item.narration;
                      entity.Approvalstatus =item.Approvalstatus;
                      entity.Approvaldate =item.Approvaldate;
                      entity.Currencyid =item.Currencyid;
                      entity.Exchangerate =item.Exchangerate;
                      entity.Costprice =item.Costprice;
                      entity.SalePrice =item.SalePrice;
                      entity.Dateofcosting =item.Dateofcosting;
                      entity.Comment =item.Comment;
                      entity.Subtotal = item.Subtotal;
                      entity.Createddate =item.Createddate;
                      entity.CreatedBy =item.CreatedBy;
                      entity.Updateddate =item.Updateddate;
                      entity.UpdatedBy =item.UpdatedBy;
                      entity.IsActive = item.IsActive;
                      entity.Total = item.Total;
                      entity.POStatus = CheckPO(item.Id);
                   
                      M_LedgersEntity custentity = new M_LedgersEntity();

                      if (item.customerid != null)
                      {
                            var custData = db.M_Ledgers.Where(x => x.Ledger_Id ==item.customerid).SingleOrDefault();
                            if (custData != null)
                            {
                                custentity.Ledger_Id = custData.Ledger_Id;
                                custentity.Ledger_Name = custData.Ledger_Name;
                                entity.mLedgers = custentity;
                            }
                            else
                            {
                                custentity.Ledger_Id =Convert.ToInt32(item.customerid);
                                custentity.Ledger_Name ="--";
                                entity.mLedgers = custentity;
                            }
                      }
                    entity.ProductName = GetProductName(item.Id);
                    entity.StyleName = GetStyleName(item.Id);
                    entity.ProductAttrib = GetProductAttribName(item.EnquiryID);
                    entity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == item.EnquiryID).Select(x => x.Totalqty).Distinct().FirstOrDefault();
                    var otherCharge = db.T_POOtherCharge.Where(x => x.TXID == item.Id && x.InventoryModeId == 10011).ToList();
                    if (otherCharge != null)
                    {
                        List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                        foreach (var ocdata in otherCharge)
                        {
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.Id = ocdata.Id;
                            ent.TXID = ocdata.TXID;
                            ent.OtherChargeId = ocdata.OtherChargeId;
                            if (ocdata.OtherChargeId > 0)
                            {
                                M_POOtherChargesMasterEntity ocmaster = new M_POOtherChargesMasterEntity();
                                string OCname = db.M_POOtherChargesMaster.Where(x => x.id == ocdata.OtherChargeId).Select(x => x.POOtherChargeName).FirstOrDefault();
                                ocmaster.POOtherChargeName = OCname;
                                ent.OCMaster = ocmaster;
                            }

                            ent.OtherChargeValue = ocdata.OtherChargeValue;
                            ent.GST = ocdata.GST;
                            ent.InventoryModeId = ocdata.InventoryModeId;
                            ent.Company_ID = ocdata.Company_ID;
                            ent.BranchId = ocdata.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = ocdata.CreatedBy;
                            oclist.Add(ent);
                        }
                        entity.POOtherChargeEntity = oclist;
                    }
                    list.Add(entity);
                }
           }

           return list;

         }

        public bool CheckPO(long Quoid)
        {
            bool status = false;
            using (var db = new GarmentERPDBEntities())
            {
                var Quotationid = db.CRM_CustomerPOMaster.Where(x => x.IsActive == true && x.Approvalstatus == true).Select(u => u.QuotID).ToArray();
                foreach(var data in Quotationid)
                {
                    if(Convert.ToInt64(data)==Quoid)
                    {
                        status = true;
                    }
                }
            }
            return status;
        }

         //insert itemcord
        public long Insert(CRM_QuotationMasterEntity entity)
         {
             long id = 0;
             using (var db = new GarmentERPDBEntities())
             {
                CRM_QuotationMaster item = new CRM_QuotationMaster();
                 item.Id = entity.Id;
                 item.EnquiryID = entity.EnquiryID;
                 item.Addressid = entity.Addressid;
                 item.Quotationdate = entity.Quotationdate;
                 item.Enquiryrefno = entity.Enquiryrefno;
                 item.Dateofcosting = entity.Dateofcosting;
                 item.Exchangerate = entity.Exchangerate;
                 item.Costprice = entity.Costprice;
                 item.SalePrice = entity.SalePrice;
                 item.customerid = entity.customerid;
                 item.narration =Convert.ToString(entity.narration);
                 item.Approvalstatus = entity.Approvalstatus;
                 item.Tentivedate = entity.Tentivedate;
                 item.Sampling = entity.Sampling;
                 item.Subtotal = entity.Subtotal;
                 item.Approvaldate = entity.Approvaldate;
                 item.Currencyid = entity.Currencyid;
                 item.Tax = entity.Tax;
                 item.Total = entity.Total;
                 item.Comment =Convert.ToString(entity.Comment);
                 item.IsActive = entity.IsActive;
                 item.CreatedBy = entity.CreatedBy;
                 item.Createddate = entity.Createddate;
                 item.Company_ID = entity.Company_ID;
                 item.BranchId = entity.BranchId;
                 item.IsStatus = entity.IsStatus;
                item.IsReadymade = entity.IsReadmade;
                item.GrandTotal = entity.GrandTotal;
                if (entity.IsReadmade == 0)
                {
                    item.QuotationNo = "RM-" + Convert.ToString(getnextQuotatioNo());
                }
                else
                {
                    item.QuotationNo = "RF-" + Convert.ToString(getnextQuotatioNo());
                }
                
                 db.CRM_QuotationMaster.Add(item);
                 db.SaveChanges();
                 id = item.Id;

                if (id > 0)
                {
                    if (entity.POOtherChargeEntity != null)
                    {
                        foreach (var data in entity.POOtherChargeEntity)
                        {
                            T_POOtherCharge ent = new T_POOtherCharge();
                            ent.TXID = id;
                            ent.OtherChargeId = data.OtherChargeId;
                            ent.OtherChargeValue = data.OtherChargeValue;
                            ent.GST = data.GST;
                            ent.InventoryModeId = data.InventoryModeId;
                            ent.Company_ID = data.Company_ID;
                            ent.BranchId = data.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = data.CreatedBy;
                            db.T_POOtherCharge.Add(ent);
                            db.SaveChanges();
                        }
                    }
                }

                if (id>0)
                {
                    var data = db.CRM_EnquiryMaster.Find(entity.EnquiryID);
                    data.IsStatus = 4;
                    data.Updateddate = DateTime.Now;
                    data.UpdatedBy = entity.CreatedBy;
                    db.SaveChanges();
                }
             }
             return id;
         }
         public bool Update(CRM_QuotationMasterEntity entity)
         {
             bool isupdated = false;

             using (var db = new GarmentERPDBEntities())
             {
                 var record = db.CRM_QuotationMaster.Find(entity.Id);
                 record.Id = entity.Id;
                 record.narration = entity.narration;
                 record.Approvalstatus = entity.Approvalstatus;
                 record.Approvaldate = entity.Approvaldate;
                 record.Comment = entity.Comment;
                 record.Sampling = entity.Sampling;
                record.Updateddate = DateTime.Now;
                record.UpdatedBy = entity.UpdatedBy;
                record.Exchangerate = entity.Exchangerate;
                record.Costprice = entity.Costprice;
                record.SalePrice = entity.SalePrice;
                record.Total = entity.Total;
                record.Subtotal = entity.Subtotal;
                record.GrandTotal = entity.GrandTotal;
                record.Tax = entity.Tax;
                record.IsStatus = entity.IsStatus;

                if (entity.imgelist != null)
                 {
                     if (entity.imgelist.Count > 0)
                     {
                         //  if(entity.imgelist.Contains())

                         var oldimagerecordlist = db.CRM_QuotationStyleImage.Where(x => x.Quotationid == entity.Id).ToList();
                         foreach (var oldimg in oldimagerecordlist)
                         {
                             var delerecord = db.CRM_EnquiryStyleImage.Find(oldimg.Id);
                             delerecord.IsActive = false;
                             db.SaveChanges();
                         }
                     }

                    foreach (var itemimg in entity.imgelist)
                    {

                        CRM_QuotationStyleImage imgrecord = new CRM_QuotationStyleImage();
                        imgrecord.Quotationid = entity.Id;
                        imgrecord.Quotationimg = itemimg.Quotationimg;
                        imgrecord.IsActive = true;
                        imgrecord.Createdby = entity.UpdatedBy;
                        imgrecord.Company_ID = entity.Company_ID;
                        imgrecord.BranchId = entity.BranchId;
                        imgrecord.IsActive = true;
                        db.CRM_QuotationStyleImage.Add(imgrecord);
                        db.SaveChanges();

                    }
                }
                
                 db.SaveChanges();
                 isupdated = true;
                if (entity.POOtherChargeEntity != null)
                {
                    var singleRec = db.T_POOtherCharge.Where(x => x.TXID == entity.Id && x.InventoryModeId == 10010).ToList();
                    foreach (var rec in singleRec)
                    {
                        var OCrecord = db.T_POOtherCharge.FirstOrDefault(x => x.Id == rec.Id);
                        db.T_POOtherCharge.Remove(OCrecord);
                        db.SaveChanges();
                    }

                    foreach (var data in entity.POOtherChargeEntity)
                    {
                        T_POOtherCharge ent = new T_POOtherCharge();
                        ent.TXID = entity.Id;
                        ent.OtherChargeId = data.OtherChargeId;
                        ent.OtherChargeValue = data.OtherChargeValue;
                        ent.GST = data.GST;
                        ent.InventoryModeId = data.InventoryModeId;
                        ent.Company_ID = data.Company_ID;
                        ent.BranchId = data.BranchId;
                        ent.CreatedOn = DateTime.Now;
                        ent.CreatedBy = data.CreatedBy;
                        db.T_POOtherCharge.Add(ent);
                        db.SaveChanges();
                    }
                }
            }
             return isupdated;
         }

       public List<CRM_QuotationMasterEntity> getquotationnobycustomer(long customerid,int Company_ID,int BranchId)
         {
             List<CRM_QuotationMasterEntity> quotnolist = new List<CRM_QuotationMasterEntity>();
             using(var db=new GarmentERPDBEntities())
             {
                var list = db.CRM_QuotationMaster.Where(x => x.customerid == customerid && x.IsStatus == 3 && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x=>x.Id).ToList();
                 foreach (var item in list)
                 {
                     CRM_QuotationMasterEntity entity = new CRM_QuotationMasterEntity();
                     entity.QuotationNo = item.QuotationNo;
                     entity.Id = item.Id;
                     quotnolist.Add(entity);

                 }

             }

             return quotnolist;

         }
        public bool Delete(long id,int Userid)
        {
            bool isdeleted=false;


            using(var db=new GarmentERPDBEntities())
            {
                var record=db.CRM_QuotationMaster.Find(id);
                record.IsActive=false;
                record.UpdatedBy = Userid;
                record.Updateddate = DateTime.Now;
                db.SaveChanges();
                isdeleted =true;


                long enquid = db.CRM_QuotationMaster.Where(x => x.Id == id).Select(x => x.EnquiryID).FirstOrDefault();
                var Enqrecord = db.CRM_EnquiryMaster.Find(enquid);
                Enqrecord.IsStatus = 3;
                Enqrecord.UpdatedBy = Userid;
                Enqrecord.Updateddate = DateTime.Now;
                db.SaveChanges();

            }
            return isdeleted;
        }

        public object QuoteGet(long id)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Quotation_report";
                cmd.Parameters.Add("Id", SqlDbType.BigInt).Value = id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }
    }
}
