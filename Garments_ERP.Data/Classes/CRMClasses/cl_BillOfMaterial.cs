﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace Garments_ERP.Data.Admin
{
    public class cl_BillOfMaterial
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public List<CRM_CustomerPOMasterentity> EnqPOList(int Company_ID, int BranchId, long EnqId)
        {
            try
            {
                List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var EnqRecord = db.CRM_EnquiryMaster.Where(x => x.Id == EnqId && x.Id != 9999).FirstOrDefault();
                    long quoid = db.CRM_QuotationMaster.Where(x => x.EnquiryID == EnqRecord.Id).Select(x => x.Id).FirstOrDefault();
                    if (quoid > 0)
                    {
                        long poid = db.CRM_CustomerPOMaster.Where(x => x.QuotID == quoid).Select(x => x.Id).FirstOrDefault();
                        if (poid > 0)
                        {
                            var item2 = db.CRM_CustomerPOMaster.Find(poid);
                            CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                            poentity.Id = item2.Id;
                            poentity.POno = item2.POno;
                            poentity.PODate = item2.PODate;
                            var PostyleId = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == item2.Id).FirstOrDefault();
                            CRM_CustomerPOStyleDetailEntity postyent = new CRM_CustomerPOStyleDetailEntity();
                            postyent.Id = PostyleId.Id;
                            poentity.styledetailentity = postyent;
                            list.Add(poentity);
                        }
                    }
                }
                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CRM_CustomerPOMasterentity> POList(int Company_ID, int BranchId, long custid)
        {
            try
            {
                List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
                using (var db = new GarmentERPDBEntities())
                {
                    cl_CustomerPO CPO_obj = new cl_CustomerPO();
                    List<CRM_CustomerPOMasterentity> Cpolist = new List<CRM_CustomerPOMasterentity>();
                    var records = db.M_BillOfMaterialMaster.ToList().Where(x => x.IsActive == true && x.Company_ID == Company_ID && x.BranchId == x.BranchId && x.CustomerId== custid);
                    foreach (var item in records)
                    {
                        if (item.EnquiryId != null)
                        {
                            var EnqRecord = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId && x.Id != 9999 && x.customerid == custid).FirstOrDefault();
                            long quoid = db.CRM_QuotationMaster.Where(x => x.EnquiryID == EnqRecord.Id && x.customerid == custid).Select(x => x.Id).FirstOrDefault();
                            if (quoid > 0)
                            {
                                long poid = db.CRM_CustomerPOMaster.Where(x => x.QuotID == quoid && x.customerid == custid).Select(x => x.Id).FirstOrDefault();
                                if (poid > 0)
                                {
                                    var item2 = db.CRM_CustomerPOMaster.Find(poid);
                                    CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                                    poentity.Id = item2.Id;
                                    poentity.POno = item2.POno;
                                    poentity.PODate = item2.PODate;
                                    Cpolist.Add(poentity);
                                }
                            }
                        }
                        if (item.Poid != null)
                        {
                            long poid = db.CRM_CustomerPOMaster.Where(x => x.Id == item.Poid && x.customerid == custid).Select(x => x.Id).FirstOrDefault();
                            var item1 = db.CRM_CustomerPOMaster.Find(poid);
                            if (item1 != null)
                            {
                                CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                                poentity.Id = item1.Id;
                                poentity.POno = item1.POno;
                                poentity.PODate = item1.PODate;
                                Cpolist.Add(poentity);
                            }
                        }
                    }

                    var allpolist = CPO_obj.GetPO().Where(x => x.Approvalstatus == true && x.IsActive == true && x.Company_ID == Company_ID && x.BranchId == BranchId && x.IsStatus == 3);
                    list = allpolist != null ? allpolist.Where(x => !Cpolist.Any(y => y.Id == x.Id) && x.customerid == custid).ToList() : list;
                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<CRM_EnquiryMasterEntity> getenquiryList(int Company_ID,int BranchId, long custid)
        {
            try
            {
                cl_Enquiry enqobj = new cl_Enquiry();
                List<CRM_EnquiryMasterEntity> list = new List<CRM_EnquiryMasterEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    List<CRM_EnquiryMasterEntity> enqlist = new List<CRM_EnquiryMasterEntity>();
                    var records = db.M_BillOfMaterialMaster.ToList().Where(x => x.IsActive == true && x.Company_ID == Company_ID && x.BranchId == x.BranchId && x.CustomerId==custid);
                    foreach (var item in records)
                    {
                        if (item.EnquiryId != null)
                        {
                            var EnqRecord = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId && x.Id != 9999 && x.customerid == custid).FirstOrDefault();
                            CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                            enquiryentity.EnquiryId = EnqRecord.Id;
                            enquiryentity.Enquiryno = EnqRecord.Enquiryno;
                            enquiryentity.Enquirydate = EnqRecord.Enquirydate;
                            enqlist.Add(enquiryentity);
                            
                        }
                    }
                    var allenqlist = enqobj.get().Where(x =>x.Company_ID == Company_ID && x.BranchId == BranchId && x.IsStatus == 4 && x.customerid == custid && x.EnquiryId != 9999);
                    list = allenqlist.Where(x => !enqlist.Any(y => y.EnquiryId == x.EnquiryId)).ToList();

                }
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }




        public object InsertAutoPR(XmlDocument doc,int type, string AttibId)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_BOMAutoPR";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = type;//Save Auto PR
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value =Convert.ToString(AttibId);
                cmd.Parameters.Add("attributexml", SqlDbType.VarChar).Value = doc.InnerXml;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            return dt;
        }

        public string getnextbomno()
        {
            string no;
            no = "BOM-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BillOfMaterialMaster.FirstOrDefault() != null ? db.M_BillOfMaterialMaster.Max(x => x.BOM_ID) + 1 : 1;
                no = no + "-" + record.ToString();
            }
            return no;
        }

        public long getnextbomID()
        {
            long no;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BillOfMaterialMaster.FirstOrDefault() != null ? db.M_BillOfMaterialMaster.Max(x => x.BOM_ID) + 1 : 1;
                no =Convert.ToInt64(record);
            }
            return no;
        }

        public Base_BillOfMaterialMasterEntity get(DateTime from, DateTime to,int Company_ID,int BranchId)
        {
            Base_BillOfMaterialMasterEntity record = new Base_BillOfMaterialMasterEntity();

            cl_Enquiry enqobj = new cl_Enquiry();
            cl_CustomerPO cpo_obj = new cl_CustomerPO();
            List<M_BillOfMaterialMasterEntity> list = new List<M_BillOfMaterialMasterEntity>();
            List<CRM_EnquiryMasterEntity> enqlist = new List<CRM_EnquiryMasterEntity>();
            List<CRM_EnquiryMasterEntity> finalenqlist = new List<CRM_EnquiryMasterEntity>();
            List<CRM_CustomerPOMasterentity> Cpolist = new List<CRM_CustomerPOMasterentity>();
            List<CRM_CustomerPOMasterentity> finalpolist = new List<CRM_CustomerPOMasterentity>();


            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BillOfMaterialMaster.ToList().Where(x => x.IsActive == true && x.CreatedDate >= from && x.CreatedDate <= to);
                foreach (var item in records)
                {
                    M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
                    entity.BOM_ID = item.BOM_ID;
                    entity.BOM_No = item.BOM_No;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CustomerAddressId = item.CustomerAddressId;
                    if (item.CustomerAddressId != null)
                    {
                        M_Ledger_BillingDetailsEntity addressentity = new M_Ledger_BillingDetailsEntity();
                        var adddata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.CustomerAddressId).SingleOrDefault();
                        if(adddata!=null)
                        {
                            addressentity.Id = adddata.Id;
                            addressentity.Address = adddata.Address;
                            addressentity.City_Id = adddata.City_Id;
                            addressentity.Country_Id = adddata.Country_Id;
                            addressentity.State_Id = addressentity.State_Id;
                            addressentity.Email = addressentity.Email;
                            addressentity.GSTIN = addressentity.GSTIN;
                        }
                        else
                        {
                            addressentity.Id =Convert.ToInt32(item.CustomerAddressId);
                            addressentity.Address = "";
                            addressentity.City_Id = 0;
                            addressentity.Country_Id = 101;
                            addressentity.State_Id = 1646;
                            addressentity.Email ="";
                            addressentity.GSTIN = "27";
                        }
                        entity.MLedgerBillEntity= addressentity;
                    }
                    entity.CustomerId = item.CustomerId;
                    //if (item.M_BillOfMaterialDetail !=null)
                    //{
                    //    M_BillOfMaterialDetailEntity bomdentity = new M_BillOfMaterialDetailEntity();
                    //    bomdentity.ID = item.M_BillOfMaterialDetail.ID;
                    //}
                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.CustomerId != null)
                    {
                        var qtcust = db.M_Ledgers.Where(x => x.Ledger_Id == item.CustomerId).SingleOrDefault();
                        if (qtcust != null)
                        {
                            custentity.Ledger_Id = qtcust.Ledger_Id;
                            custentity.Ledger_Name = qtcust.Ledger_Name;
                            
                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.CustomerId);
                            custentity.Ledger_Name = "--";
                           
                        }
                        entity.MLedgerEntity = custentity;
                    }


                    entity.EnquiryId = item.EnquiryId;

                    if (item.EnquiryId!=null && item.EnquiryId >0)
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                            enquiryentity.EnquiryId = EnqData.Id;
                            enquiryentity.Enquiryno = EnqData.Enquiryno;
                            enquiryentity.Enquirydate = EnqData.Enquirydate;
                            enquiryentity.customerid = EnqData.customerid;
                            enquiryentity.Sampling = EnqData.Sampling;
                            enquiryentity.Tentivedate = EnqData.Tentivedate;
                            entity.CRM_EnquiryMaster = enquiryentity;
                        }
                    }
                    entity.Poid = item.Poid;
                    var item1 = db.CRM_CustomerPOMaster.Find(item.Poid);
                    if (item1 != null)
                    {
                        CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                        poentity.Id = item1.Id;
                        poentity.POno = item1.POno;
                        poentity.PODate = item1.PODate;
                        poentity.customerid = item1.customerid;
                        poentity.Sampling = item1.Sampling;
                        poentity.Tentivedate = item1.Tentivedate;
                        entity.CRM_POMaster = poentity;

                    }
                    entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                    if (item.M_ProcessCycleMaster != null)
                    {
                        M_ProcessCycleEntity procycleentity = new M_ProcessCycleEntity();
                        procycleentity.Prooce_Cycle_id = item.M_ProcessCycleMaster.Prooce_Cycle_id;
                        procycleentity.Process_cycle_Name = item.M_ProcessCycleMaster.Process_cycle_Name;
                        entity.M_ProcessCycleMaster = procycleentity;
                    }
                    entity.IsActive = item.IsActive;
                    entity.IsPRDone = item.IsPRDone;
                    entity.RevisionNo = item.RevisionNo;
                    entity.SizeId = item.SizeId;
                    if (item.M_ItemSizeMaster != null)
                    {
                        M_ItemSizeMasterEntity sizeentity = new M_ItemSizeMasterEntity();
                        sizeentity.Id = item.M_ItemSizeMaster.Id;
                        sizeentity.Sizetype = item.M_ItemSizeMaster.Sizetype;
                        sizeentity.Sizedesc = item.M_ItemSizeMaster.Sizedesc;
                        sizeentity.Itemid = item.M_ItemSizeMaster.Itemid;
                        entity.M_ItemSizeMaster = sizeentity;
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        styleentity.Createddate = item.M_StyleMaster.Createddate;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    list.Add(entity);
                }
                var allenqlist = enqobj.get(from, to).Where(x=>x.IsStatus==3 && x.Company_ID==Company_ID && x.BranchId==BranchId);
                finalenqlist = allenqlist.Where(x => !enqlist.Any(y => y.EnquiryId == x.EnquiryId)).ToList();
                var allpolist = cpo_obj.Get(from, to).Where(x => x.Approvalstatus == true && x.IsActive == true && x.IsStatus == 3 && x.Company_ID == Company_ID && x.BranchId == BranchId);
                finalpolist = allpolist.Where(x => !Cpolist.Any(y => y.Id == x.Id)).ToList();
            }
            record.bomlist = list;
            record.enqlist = finalenqlist;
            record.Cpolist = finalpolist;
            return record;
        }
        public Base_BillOfMaterialMasterEntity getBase(int Company_ID,int BranchId)
        {
            Base_BillOfMaterialMasterEntity baselist = new Base_BillOfMaterialMasterEntity();

            cl_Enquiry enqobj = new cl_Enquiry();
            cl_CustomerPO CPO_obj = new cl_CustomerPO();
            List<M_BillOfMaterialMasterEntity> list = new List<M_BillOfMaterialMasterEntity>();
            List<CRM_EnquiryMasterEntity> enqlist = new List<CRM_EnquiryMasterEntity>();
            List<CRM_EnquiryMasterEntity> finalenqlist = new List<CRM_EnquiryMasterEntity>();
            List<CRM_CustomerPOMasterentity> Cpolist = new List<CRM_CustomerPOMasterentity>();
            List<CRM_CustomerPOMasterentity> polist = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BillOfMaterialMaster.ToList().Where(x => x.IsActive == true && x.Company_ID == Company_ID && x.BranchId == x.BranchId);
                foreach (var item in records)
                {
                    M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
                    entity.BOM_ID = item.BOM_ID;
                    entity.BOM_No = item.BOM_No;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.IsReadymate = item.IsReadymate;
                    entity.CustomerAddressId = item.CustomerAddressId;
                    if (item.CustomerAddressId != null)
                    {
                        M_Ledger_BillingDetailsEntity addressentity = new M_Ledger_BillingDetailsEntity();
                        var custadd = db.M_Ledger_BillingDetails.Where(x => x.Id == item.CustomerAddressId).SingleOrDefault();
                        if (custadd != null)
                        {
                            addressentity.Id = custadd.Id;
                            addressentity.Address = custadd.Address;
                            addressentity.City_Id = custadd.City_Id;
                            addressentity.Country_Id = custadd.Country_Id;
                            addressentity.State_Id = custadd.State_Id;
                            addressentity.Email = custadd.Email;
                            addressentity.GSTIN = custadd.GSTIN;
                        }
                        else
                        {
                            addressentity.Id =Convert.ToInt32(item.CustomerAddressId);
                            addressentity.Address = "--";
                            addressentity.City_Id = 0;
                            addressentity.Country_Id = 101;
                            addressentity.State_Id = 1646;
                            addressentity.Email = "";
                            addressentity.GSTIN = "27";
                        }
                        entity.MLedgerBillEntity = addressentity;
                    }
                    entity.CustomerId = item.CustomerId;
                    //if (item.M_BillOfMaterialDetail !=null)
                    //{
                    //    M_BillOfMaterialDetailEntity bomdentity = new M_BillOfMaterialDetailEntity();
                    //    bomdentity.ID = item.M_BillOfMaterialDetail.ID;
                    //}
                    if (item.CustomerId != null)
                    {
                        M_LedgersEntity customerentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.CustomerId).SingleOrDefault();
                        if (custdata != null)
                        {
                            customerentity.Ledger_Id = custdata.Ledger_Id;
                            customerentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            customerentity.Ledger_Id =Convert.ToInt32(item.CustomerId);
                            customerentity.Ledger_Name = "--";
                        }
                        entity.MLedgerEntity = customerentity;
                    }
                    entity.EnquiryId = item.EnquiryId;


                    if (item.EnquiryId != null)
                    {
                        var EnqRecord = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                        enquiryentity.EnquiryId = EnqRecord.Id;
                        enquiryentity.Enquiryno = EnqRecord.Enquiryno;
                        enquiryentity.Enquirydate = EnqRecord.Enquirydate;
                        enquiryentity.customerid = EnqRecord.customerid;
                        enquiryentity.Sampling = EnqRecord.Sampling;
                        enquiryentity.Tentivedate = EnqRecord.Tentivedate;
                        M_LedgersEntity ledentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == EnqRecord.customerid).SingleOrDefault();
                        if (custdata != null)
                        {
                            ledentity.Ledger_Id = custdata.Ledger_Id;
                            ledentity.Ledger_Name = custdata.Ledger_Name;
                            enquiryentity.M_Ledgersentity = ledentity;
                        }
                        else
                        {
                            ledentity.Ledger_Id = Convert.ToInt32(EnqRecord.customerid);
                            ledentity.Ledger_Name = "--";
                            enquiryentity.M_Ledgersentity = ledentity;
                        }
                        enquiryentity.ProductName = GetProductName(EnqRecord.Id);
                        enquiryentity.StyleName = GetStyleName(EnqRecord.Id);
                        enquiryentity.ProductAttrib = GetProductAttribName(EnqRecord.Id);
                        enquiryentity.ProductQty = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == EnqRecord.Id).Select(x => x.Totalqty).Distinct().FirstOrDefault();


                        entity.CRM_EnquiryMaster = enquiryentity;
                        enqlist.Add(enquiryentity);

                        long quoid = db.CRM_QuotationMaster.Where(x => x.EnquiryID == EnqRecord.Id).Select(x => x.Id).FirstOrDefault();
                        if(quoid>0)
                        {
                            long poid = db.CRM_CustomerPOMaster.Where(x => x.QuotID == quoid).Select(x => x.Id).FirstOrDefault();
                            if(poid>0)
                            {
                                var item2 = db.CRM_CustomerPOMaster.Find(poid);
                                CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                                poentity.Id = item2.Id;
                                poentity.POno = item2.POno;
                                poentity.PODate = item2.PODate;
                                poentity.customerid = item2.customerid;
                                poentity.Sampling = item2.Sampling;
                                poentity.Tentivedate = item2.Tentivedate;
                                entity.CRM_POMaster = poentity;
                                Cpolist.Add(poentity);
                            }
                        }

                        // }
                    }
                    entity.Poid = item.Poid;
                    var item1 = db.CRM_CustomerPOMaster.Find(item.Poid);
                    if (item1 != null)
                    {
                        CRM_CustomerPOMasterentity poentity = new CRM_CustomerPOMasterentity();
                        poentity.Id = item1.Id;
                        poentity.POno = item1.POno;
                        poentity.PODate = item1.PODate;
                        poentity.customerid = item1.customerid;
                        poentity.Sampling = item1.Sampling;
                        poentity.Tentivedate = item1.Tentivedate;
                        entity.CRM_POMaster = poentity;
                        Cpolist.Add(poentity);
                    }
                    
                    entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                    M_ProcessCycleEntity procycleentity = new M_ProcessCycleEntity();
                    if (item.M_ProcessCycleMaster != null)
                    {
                        procycleentity.Prooce_Cycle_id = item.M_ProcessCycleMaster.Prooce_Cycle_id;
                        procycleentity.Process_cycle_Name = item.M_ProcessCycleMaster.Process_cycle_Name;
                    }
                    else
                    {
                        if(item.Prooce_Cycle_id>0)
                        {
                            var ProcessData = db.M_PROCESS_Master.Where(x => x.Process_Id == item.Prooce_Cycle_id).FirstOrDefault();
                            procycleentity.Prooce_Cycle_id =(int)ProcessData.Process_Id;
                            procycleentity.Process_cycle_Name = ProcessData.SHORT_NAME;
                        }
                    }
                    entity.M_ProcessCycleMaster = procycleentity;
                    entity.IsActive = item.IsActive;
                    entity.IsPRDone = item.IsPRDone;
                    entity.RevisionNo = item.RevisionNo;
                    entity.SizeId = item.SizeId;
                    if (item.M_ItemSizeMaster != null)
                    {
                        M_ItemSizeMasterEntity sizeentity = new M_ItemSizeMasterEntity();
                        sizeentity.Id = item.M_ItemSizeMaster.Id;
                        sizeentity.Sizetype = item.M_ItemSizeMaster.Sizetype;
                        sizeentity.Sizedesc = item.M_ItemSizeMaster.Sizedesc;
                        sizeentity.Itemid = item.M_ItemSizeMaster.Itemid;
                        entity.M_ItemSizeMaster = sizeentity;
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        styleentity.Createddate = item.M_StyleMaster.Createddate;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    list.Add(entity);
                }

                var allenqlist = enqobj.get().Where(x => x.Sampling == true && x.Company_ID==Company_ID && x.BranchId==BranchId && x.IsStatus==3);
                finalenqlist = allenqlist.Where(x => !enqlist.Any(y => y.EnquiryId == x.EnquiryId)).ToList();
                var allpolist = CPO_obj.GetPO().Where(x => x.Approvalstatus == true && x.IsActive==true && x.Company_ID == Company_ID && x.BranchId == BranchId && x.IsStatus == 3);
                polist = allpolist != null ? allpolist.Where(x => !Cpolist.Any(y => y.Id == x.Id)).ToList() : polist;
            }
            baselist.bomlist = list;
            baselist.enqlist = finalenqlist;
            baselist.Cpolist = polist;
            return baselist;
        }
        public string GetProductAttribName(long Eid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 34; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(Eid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                int i = 0;
                foreach (DataTable table in dt.Tables)
                {

                    foreach (DataRow dr in table.Rows)
                    {
                        if (i == 0)
                        {
                            itemname = Convert.ToString(dr["AttributeValue"]);
                        }
                        else
                        {
                            itemname = itemname + "," + Convert.ToString(dr["AttributeValue"]);
                        }
                    }
                    i++;
                }

            }
            return itemname;
        }

        public string GetProductName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var itemid = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == Eid).Select(x => x.Itemid).Distinct().FirstOrDefault();
                itemname = db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.ItemName).FirstOrDefault();
            }
            return itemname;
        }

        public string GetStyleName(long Eid)
        {
            string itemname = "";
            using (var db = new GarmentERPDBEntities())
            {
                var styleid = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == Eid).Select(x => x.Styleid).Distinct().FirstOrDefault();
                itemname = db.M_StyleMaster.Where(x => x.Id == styleid).Select(x => x.Stylename).FirstOrDefault();
            }
            return itemname;
        }


        public List<M_BillOfMaterialMasterEntity> get()
        {
            cl_Enquiry enqobj = new cl_Enquiry();
            List<M_BillOfMaterialMasterEntity> list = new List<M_BillOfMaterialMasterEntity>();
            List<CRM_EnquiryMasterEntity> enqlist = new List<CRM_EnquiryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BillOfMaterialMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
                    entity.BOM_ID = item.BOM_ID;
                    entity.BOM_No = item.BOM_No;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CustomerAddressId = item.CustomerAddressId;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    if (item.CustomerAddressId != null)
                    {
                        M_Ledger_BillingDetailsEntity addressentity = new M_Ledger_BillingDetailsEntity();
                        var adddata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.CustomerAddressId).SingleOrDefault();
                        if (adddata != null)
                        {
                            addressentity.Id = adddata.Id;
                            addressentity.Address = adddata.Address;
                            addressentity.City_Id = adddata.City_Id;
                            addressentity.Country_Id = adddata.Country_Id;
                            addressentity.State_Id = addressentity.State_Id;
                            addressentity.Email = addressentity.Email;
                            addressentity.GSTIN = addressentity.GSTIN;
                        }
                        else
                        {
                            addressentity.Id = Convert.ToInt32(item.CustomerAddressId);
                            addressentity.Address = "";
                            addressentity.City_Id = 0;
                            addressentity.Country_Id = 101;
                            addressentity.State_Id = 1646;
                            addressentity.Email = "";
                            addressentity.GSTIN = "27";
                        }
                        entity.MLedgerBillEntity = addressentity;
                    }
                    entity.CustomerId = item.CustomerId;
                    //if (item.M_BillOfMaterialDetail !=null)
                    //{
                    //    M_BillOfMaterialDetailEntity bomdentity = new M_BillOfMaterialDetailEntity();
                    //    bomdentity.ID = item.M_BillOfMaterialDetail.ID;
                    //}
                    M_LedgersEntity custentity = new M_LedgersEntity();
                    if (item.CustomerId != null)
                    {
                        var qtcust = db.M_Ledgers.Where(x => x.Ledger_Id == item.CustomerId).SingleOrDefault();
                        if (qtcust != null)
                        {
                            custentity.Ledger_Id = qtcust.Ledger_Id;
                            custentity.Ledger_Name = qtcust.Ledger_Name;

                        }
                        else
                        {
                            custentity.Ledger_Id = Convert.ToInt32(item.CustomerId);
                            custentity.Ledger_Name = "--";

                        }
                        entity.MLedgerEntity = custentity;
                    }
                    entity.EnquiryId = item.EnquiryId;
                    if (item.EnquiryId != null && item.EnquiryId>0) 
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                            enquiryentity.EnquiryId = EnqData.Id;
                            enquiryentity.Enquiryno = EnqData.Enquiryno;
                            enquiryentity.Enquirydate = EnqData.Enquirydate;
                            enquiryentity.customerid = EnqData.customerid;
                            enquiryentity.Sampling = EnqData.Sampling;
                            enquiryentity.Tentivedate = EnqData.Tentivedate;
                            entity.CRM_EnquiryMaster = enquiryentity;
                            enqlist.Add(enquiryentity);
                        }
                    }
                    entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                    if (item.M_ProcessCycleMaster != null)
                    {
                        M_ProcessCycleEntity procycleentity = new M_ProcessCycleEntity();
                        procycleentity.Prooce_Cycle_id = item.M_ProcessCycleMaster.Prooce_Cycle_id;
                        procycleentity.Process_cycle_Name = item.M_ProcessCycleMaster.Process_cycle_Name;
                        entity.M_ProcessCycleMaster = procycleentity;
                    }
                    entity.IsActive = item.IsActive;
                    entity.IsPRDone = item.IsPRDone;
                    entity.RevisionNo = item.RevisionNo;
                    entity.SizeId = item.SizeId;
                    if (item.M_ItemSizeMaster != null)
                    {
                        M_ItemSizeMasterEntity sizeentity = new M_ItemSizeMasterEntity();
                        sizeentity.Id = item.M_ItemSizeMaster.Id;
                        sizeentity.Sizetype = item.M_ItemSizeMaster.Sizetype;
                        sizeentity.Sizedesc = item.M_ItemSizeMaster.Sizedesc;
                        sizeentity.Itemid = item.M_ItemSizeMaster.Itemid;
                        entity.M_ItemSizeMaster = sizeentity;
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        styleentity.Createddate = item.M_StyleMaster.Createddate;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    list.Add(entity);
                }

                var allenqlist = enqobj.get();
                var finalenqlist = allenqlist.Where(x => !enqlist.Any(y => y.EnquiryId == x.EnquiryId)).ToList();


            }
            return list;
        }

        public List<M_BillOfMaterialMasterEntity> getbyBOM(int companyid,int branchid)
        {
            List<M_BillOfMaterialMasterEntity> quotnolist = new List<M_BillOfMaterialMasterEntity>();
            List<M_BillOfMaterialMasterEntity> BOMlist = new List<M_BillOfMaterialMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var BomID = db.SRM_PurchaseRequestMaster.Where(x => x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).Select(u => u.BOM_Id).ToArray();

                var list = db.M_BillOfMaterialMaster.Where(x => !BomID.Contains(x.BOM_ID) && x.IsActive==true && x.Company_ID == companyid && x.BranchId == branchid).OrderByDescending(x => x.BOM_ID).ToList();
                foreach (var item in list)
                {
                    M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
                    entity.BOM_ID = item.BOM_ID;
                    entity.BOM_No = item.BOM_No;
                    quotnolist.Add(entity);
                }
            }

            return quotnolist;
        }
        public M_BillOfMaterialMasterEntity GetByEnquiry(long id)
        {
            try
            {
                M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var Enqueryid = db.CRM_QuotationMaster.Where(x => x.Id == id).Select(u => u.EnquiryID).SingleOrDefault();
                    var item = db.M_BillOfMaterialMaster.Where(x => x.EnquiryId == Enqueryid && x.IsActive == true).FirstOrDefault();
                    if (item != null)
                    {

                        entity.BOM_ID = item.BOM_ID;
                        entity.BOM_No = item.BOM_No;
                        entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                    }
                    else
                    {
                        var poid = db.CRM_CustomerPOMaster.Where(x => x.QuotID == id && x.IsActive == true).Select(u => u.Id).SingleOrDefault();
                        var item1 = db.M_BillOfMaterialMaster.Where(x => x.Poid == poid && x.IsActive == true).FirstOrDefault();
                        entity.BOM_ID = item1.BOM_ID;
                        entity.BOM_No = item1.BOM_No;
                        entity.Prooce_Cycle_id = item1.Prooce_Cycle_id;
                    }
                }

                return entity;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_BillOfMaterialMasterEntity GetById(long id)
        {
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_BillOfMaterialMaster.Find(id);
                entity.IsReadymate = item.IsReadymate;
                entity.StyleId = item.StyleId;
                entity.BOM_No = item.BOM_No;
                entity.RevisionNo = item.RevisionNo;
                entity.EnquiryId = item.EnquiryId;
                entity.StyleId = item.StyleId;
                entity.SizeId = item.SizeId;
                entity.CustomerId = item.CustomerId;
                entity.CustomerAddressId = item.CustomerAddressId;
                entity.CreatedBy = item.CreatedBy;
                entity.CreatedDate = item.CreatedDate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.UpdatedDate = item.UpdatedDate;
                entity.IsActive = item.IsActive;
                entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                entity.IsPRDone = item.IsPRDone;
                entity.Poid = item.Poid != null ? item.Poid : 0;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;
                entity.IsStatus = item.IsStatus;
                entity.BOM_ID = item.BOM_ID;

                if(item.Poid!=null && item.Poid>0)
                {
                    CRM_CustomerPOMasterentity poent = new CRM_CustomerPOMasterentity();
                    var podata = db.CRM_CustomerPOMaster.Where(x => x.Id == item.Poid).FirstOrDefault();
                    poent.Id = podata.Id;
                    poent.PODate = podata.PODate;
                    poent.POno = podata.POno;
                    entity.CRM_POMaster = poent;
                }
                
                if (item.CustomerAddressId != null)
                {
                    M_Ledger_BillingDetailsEntity addressentity = new M_Ledger_BillingDetailsEntity();
                    var adddata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.CustomerAddressId).SingleOrDefault();
                    if (adddata != null)
                    {
                        addressentity.Id = adddata.Id;
                        addressentity.Address = adddata.Address;
                        addressentity.City_Id = adddata.City_Id;
                        addressentity.Country_Id = adddata.Country_Id;
                        addressentity.State_Id = addressentity.State_Id;
                        addressentity.Email = addressentity.Email;
                        addressentity.GSTIN = addressentity.GSTIN;
                    }
                    else
                    {
                        addressentity.Id = Convert.ToInt32(item.CustomerAddressId);
                        addressentity.Address = "";
                        addressentity.City_Id = 0;
                        addressentity.Country_Id = 101;
                        addressentity.State_Id = 1646;
                        addressentity.Email = "";
                        addressentity.GSTIN = "27";
                    }
                    entity.MLedgerBillEntity = addressentity;
                }

                M_LedgersEntity custentity = new M_LedgersEntity();
                if (item.CustomerId != null)
                {
                    var qtcust = db.M_Ledgers.Where(x => x.Ledger_Id == item.CustomerId).SingleOrDefault();
                    if (qtcust != null)
                    {
                        custentity.Ledger_Id = qtcust.Ledger_Id;
                        custentity.Ledger_Name = qtcust.Ledger_Name;

                    }
                    else
                    {
                        custentity.Ledger_Id = Convert.ToInt32(item.CustomerId);
                        custentity.Ledger_Name = "--";

                    }
                    entity.MLedgerEntity = custentity;
                }
                
                if (item.EnquiryId != null && item.EnquiryId>0)
                {
                    var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                    if (EnqData != null)
                    {
                        CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                        enquiryentity.EnquiryId = EnqData.Id;
                        enquiryentity.Enquiryno = EnqData.Enquiryno;
                        enquiryentity.Enquirydate = EnqData.Enquirydate;
                        enquiryentity.customerid = EnqData.customerid;
                        enquiryentity.Addressid = EnqData.Addressid;
                        enquiryentity.Sampling = EnqData.Sampling;
                        enquiryentity.Tentivedate = EnqData.Tentivedate == null ? DateTime.Now : EnqData.Tentivedate;
                        entity.CRM_EnquiryMaster = enquiryentity;
                    }
                }
                
                if (item.M_ProcessCycleMaster != null)
                {
                    M_ProcessCycleEntity procycleentity = new M_ProcessCycleEntity();
                    procycleentity.Prooce_Cycle_id = item.M_ProcessCycleMaster.Prooce_Cycle_id;
                    procycleentity.Process_cycle_Name = item.M_ProcessCycleMaster.Process_cycle_Name;
                    entity.M_ProcessCycleMaster = procycleentity;
                }
                if (item.M_StyleMaster != null)
                {
                    M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                    styleentity.Id = item.M_StyleMaster.Id;
                    styleentity.Stylename = item.M_StyleMaster.Stylename;
                    styleentity.Createddate = item.M_StyleMaster.Createddate;
                    entity.M_StyleMaster = styleentity;
                }
                M_BOMSizeDetailMasterEntity sizeent = new M_BOMSizeDetailMasterEntity();
                sizeent.Segmenttypeid = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Segmenttypeid).Distinct().FirstOrDefault();
                sizeent.Styleid = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Styleid).Distinct().FirstOrDefault();
                long styleid=(long)db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Styleid).Distinct().FirstOrDefault();
                M_StyleMasterEntity styleent = new M_StyleMasterEntity();
                if (styleid>0)
                {   
                    var styleentdata = db.M_StyleMaster.Where(x => x.Id == styleid).FirstOrDefault();
                    styleent.Stylename = styleentdata.Stylename;
                    styleent.StyleNo = styleentdata.StyleNo;
                    styleent.Styledescription = styleentdata.Styledescription;
                }
                else
                {
                    styleent.Stylename = "";
                    styleent.StyleNo = "";
                    styleent.Styledescription ="";
                }
                sizeent.stylemaster=styleent;


                sizeent.Itemid = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Itemid).Distinct().FirstOrDefault();
                M_ItemMasterEntity itement = new M_ItemMasterEntity();
                long itemid =(long)db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Itemid).Distinct().FirstOrDefault();
                if (itemid > 0)
                {
                    itement.ItemName = db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.ItemName).FirstOrDefault();
                }
                else
                {
                    itement.ItemName = "";
                }
                sizeent.itemmaster = itement;


                sizeent.Styleno = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Styleno).Distinct().FirstOrDefault();
                sizeent.Styledesc = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Styledesc).Distinct().FirstOrDefault();
                sizeent.Brand = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Brand).Distinct().FirstOrDefault();
                sizeent.Totalqty = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.BOM_ID).Select(x => x.Totalqty).Distinct().FirstOrDefault();

                List<string> imgdata = new List<string>();
                var listdata = db.M_BOMStyleImage.Where(x => x.BOM_ID == item.BOM_ID && x.IsActive==true).ToList();
                foreach(var img in listdata)
                {
                    if(img.BOMimg!="")
                    {
                        imgdata.Add(img.BOMimg);
                    }
                }
                entity.styleimglist = imgdata;
                entity.M_BOMSizeDetails = sizeent;

                List<M_BillOfMaterialDetailEntity> RawMaterialEntity = new List<M_BillOfMaterialDetailEntity>();
                var Rawmaterialitem = db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == item.BOM_ID).ToList();
                if(Rawmaterialitem.Count>0)
                {
                    foreach(var rawdata in Rawmaterialitem)
                    {
                        M_BillOfMaterialDetailEntity rawentity = new M_BillOfMaterialDetailEntity();
                        rawentity.ID = rawdata.ID;
                        rawentity.BOM_ID = rawdata.BOM_ID;
                        rawentity.ItemSubCategoryId = rawdata.ItemSubCategoryId;
                        rawentity.ItemId = rawdata.ItemId;
                        rawentity.ItemDescription = rawdata.ItemDescription;
                        rawentity.SupplierId = rawdata.SupplierId;
                        rawentity.UnitId = rawdata.UnitId;
                        rawentity.RequiredQty = rawdata.RequiredQty;
                        rawentity.CreatedBy = rawdata.CreatedBy;
                        rawentity.CreatedDate = rawdata.CreatedDate;
                        rawentity.UpdatedBy = rawdata.UpdatedBy;
                        rawentity.UpdatedDate = rawdata.UpdatedDate;
                        rawentity.IsActive = rawdata.IsActive;
                        rawentity.Comment = rawdata.Comment;
                        rawentity.PerQty = rawdata.PerQty;
                        rawentity.Item_Attribute_ID = rawdata.Item_Attribute_ID;
                        rawentity.Company_ID = rawdata.Company_ID;
                        rawentity.BranchId = rawdata.BranchId;
                        List<string> rawimgdata = new List<string>();
                        var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == rawdata.ID).ToList();
                        if(rawimg!=null)
                        {
                            foreach(var img in rawimg)
                            {
                                if(img.image!="")
                                {
                                    rawimgdata.Add(img.image);
                                }
                            }
                        }
                        rawentity.rawitemimg_list = rawimgdata;
                        RawMaterialEntity.Add(rawentity);
                    }
                }
                entity.M_BillOfMaterialDetail = RawMaterialEntity;
                
                var BOMProcCycle = db.M_BOM_ProcessCycle.Where(x => x.BOM_ID == item.BOM_ID).ToList();
                List<M_BOM_ProcessCycleEntity> BOMPCent = new List<M_BOM_ProcessCycleEntity>();
                if(BOMProcCycle.Count>0)
                {
                    foreach(var PCdata in BOMProcCycle)
                    {
                        M_BOM_ProcessCycleEntity PCent = new M_BOM_ProcessCycleEntity();
                        PCent.ID = PCdata.ID;
                        PCent.ProcessId = PCdata.ProcessId;
                        string processname = db.M_PROCESS_Master.Where(x => x.Process_Id == PCdata.ProcessId).Select(x => x.SHORT_NAME).FirstOrDefault();
                        M_ProcessMasterEntity PMent = new M_ProcessMasterEntity();
                        PMent.SHORT_NAME = processname;
                        PCent.processmasterEntity = PMent;
                        PCent.Processsequence = PCdata.Processsequence;
                        PCent.ItemOutId = PCdata.ItemOutId;
                        string outname = db.M_ItemMaster.Where(x => x.Id == PCdata.ItemOutId).Select(x => x.ItemName).FirstOrDefault();
                        M_ItemMasterEntity IMent = new M_ItemMasterEntity();
                        IMent.ItemName = outname;
                        PCent.ItemMasterentity = IMent;
                        PCent.Item_Attribute_ID = PCdata.Item_Attribute_ID;
                        PCent.PerQty = PCdata.PerQty;
                        PCent.RequiredQty = PCdata.RequiredQty;
                        BOMPCent.Add(PCent);

                    }
                }
                entity.BOM_ProcessCycle = BOMPCent;


            }
            return entity;
        }

        public M_BillOfMaterialMasterEntity GetDetailsForPRByBOM(long bomid)
        {
            M_BillOfMaterialMasterEntity entity = new M_BillOfMaterialMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_BillOfMaterialMaster.Find(bomid);
                entity.IsReadymate = item.IsReadymate;
                entity.BOM_ID = item.BOM_ID;
                entity.BOM_No = item.BOM_No;
                entity.CreatedDate =  item.CreatedDate;
                entity.CustomerId = item.CustomerId;
                entity.EnquiryId = item.EnquiryId;
                List<CRM_QuotationRawItemDetailEntity> rwalist = new List<CRM_QuotationRawItemDetailEntity>();
                if (item.EnquiryId != null && item.EnquiryId>0)
                {
                    var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                    if (EnqData != null)
                    {
                        CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                        enquiryentity.EnquiryId = EnqData.Id;
                        enquiryentity.Enquiryno = EnqData.Enquiryno;
                        enquiryentity.Enquirydate = EnqData.Enquirydate;
                        entity.CRM_EnquiryMaster = enquiryentity;

                        var rwitD = db.CRM_EnquiryRawitemDetail.Where(x => x.Enquiryid == EnqData.Id).Select(x => x.Id).ToArray();
                        foreach (var dd in rwitD)
                        {
                            var imgpath = db.CRM_EnquiryRawItemImage.Where(x => x.Enquiryrawitemdetailid == dd).Select(x => x.image).SingleOrDefault();
                            CRM_QuotationRawItemDetailEntity rwalist1 = new CRM_QuotationRawItemDetailEntity();
                            rwalist1.imagepath = imgpath;
                            rwalist.Add(rwalist1);
                        }
                        entity.porawitemlist = rwalist;
                    }
                }
                else
                {
                    entity.Poid = item.Poid;
                   
                    var qtid = db.CRM_CustomerPOMaster.Where(x => x.Id == item.Poid && x.IsActive == true).Select(x => x.QuotID).SingleOrDefault();

                    var Enqno = db.CRM_QuotationMaster.Where(x => x.Id == qtid).Select(x => x.Enquiryrefno).SingleOrDefault();
                    CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                    enquiryentity.Enquiryno = Enqno;
                    entity.CRM_EnquiryMaster = enquiryentity;


                    var CRM_RawItemD = db.CRM_QuotationRawItemDetail.Where(x => x.Quotationid == qtid && x.IsActive == true).ToList();
                    foreach(var dd in CRM_RawItemD)
                    {
                        CRM_QuotationRawItemDetailEntity rwalist1 =new CRM_QuotationRawItemDetailEntity();
                        rwalist1.imagepath = dd.imagepath;
                        rwalist.Add(rwalist1);
                    }
                    entity.porawitemlist = rwalist;
                }
                entity.StyleId = item.StyleId;
                if (item.M_StyleMaster != null)
                {
                    M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                    styleentity.Id = item.M_StyleMaster.Id;
                    styleentity.Stylename = item.M_StyleMaster.Stylename;
                    entity.M_StyleMaster = styleentity;
                }
                List<M_BillOfMaterialDetailEntity> BOMMateriallist = new List<M_BillOfMaterialDetailEntity>();
                var rawrecords = db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == bomid && x.IsActive == true).ToList();
                foreach (var record in rawrecords)
                {
                    M_BillOfMaterialDetailEntity bomdetailentity = new M_BillOfMaterialDetailEntity();
                    bomdetailentity.ID = record.ID;
                    bomdetailentity.ItemId = record.ItemId;
                    var itemname = db.M_ItemMaster.Where(x => x.Id == record.ItemId).Select(x=>x.ItemName).FirstOrDefault();
                    var HSNCode = db.M_ItemMaster.Where(x => x.Id == record.ItemId).Select(x => x.HScode).FirstOrDefault();
                    M_ItemMasterEntity IMent = new M_ItemMasterEntity();
                    cl_SupplierPRItemDetail sobj = new cl_SupplierPRItemDetail();
                    int IAid = Convert.ToInt32(record.Item_Attribute_ID);
                    int ISid = Convert.ToInt32(record.ItemSubCategoryId);
                    long itmid = Convert.ToInt64(record.ItemId);
                    IMent.ItemName = sobj.getItemName(itmid, IAid, ISid);
                    IMent.HScode = HSNCode;
                    bomdetailentity.M_ItemMaster = IMent;

                    bomdetailentity.UnitId = record.UnitId;
                    var unitname = db.M_UnitMaster.Where(x => x.Id == record.UnitId).Select(x => x.ItemUnit).FirstOrDefault();
                    M_UnitMasterEntity Uent = new M_UnitMasterEntity();
                    Uent.ItemUnit = unitname;
                    bomdetailentity.M_UnitMaster = Uent;
                    bomdetailentity.Item_Attribute_ID = record.Item_Attribute_ID;
                    bomdetailentity.RequiredQty = record.RequiredQty;
                    bomdetailentity.ItemSubCategoryId = record.ItemSubCategoryId;
                    if (record.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity cateentity = new M_ItemSubCategoryMasterEntity();
                        cateentity.Id = record.M_ItemSubCategoryMaster.Id;
                        cateentity.Itemsubcategory = record.M_ItemSubCategoryMaster.Itemsubcategory;
                        bomdetailentity.M_ItemSubCategoryMaster = cateentity;
                    }
                    List<M_ItemMasterEntity> itemslist = new List<M_ItemMasterEntity>();
                    var itemlist = db.M_ItemMaster.Where(x => x.Id == bomdetailentity.ItemId).ToList();
                    foreach (var itemrec_ in itemlist)
                    {
                        M_ItemMasterEntity cateentity = new M_ItemMasterEntity();
                        cateentity.Id = itemrec_.Id;
                        cateentity.ItemName = itemrec_.ItemName;
                        itemslist.Add(cateentity);
                    }
                    bomdetailentity.ItemList = itemslist;
                    bomdetailentity.SupplierId = record.SupplierId;
                    M_LedgersEntity supEnt = new M_LedgersEntity();
                    if(record.SupplierId!=null)
                    {
                        int ledid = Convert.ToInt32(record.SupplierId);
                        bomdetailentity.SupplierName = db.M_Ledgers.Where(x => x.Ledger_Id == ledid).Select(x=>x.Ledger_Name).FirstOrDefault();
                    }
                    else
                    {
                        bomdetailentity.SupplierName = "";
                    }
                    entity.itemlist_ = itemslist;

                    List<string> rawimgdata = new List<string>();
                    var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == record.ID).ToList();
                    if (rawimg != null)
                    {
                        foreach (var img in rawimg)
                        {
                            if (img.image != "")
                            {
                                rawimgdata.Add(img.image);
                            }
                        }
                    }
                    bomdetailentity.rawitemimg_list = rawimgdata;

                    BOMMateriallist.Add(bomdetailentity);
                    //entity.ItemList = itemslist;
                }

                entity.M_BillOfMaterialDetail = BOMMateriallist;
            }

            return entity;
        }
        public long Insert(M_BillOfMaterialMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_BillOfMaterialMaster item = new M_BillOfMaterialMaster();
                
                item.CreatedBy = entity.CreatedBy;
                item.CreatedDate =DateTime.Now;
                item.CustomerId = entity.CustomerId;
                item.CustomerAddressId = entity.CustomerAddressId;
                item.EnquiryId = entity.EnquiryId;
                item.Poid = entity.Poid;
                item.IsActive = entity.IsActive;
                item.IsPRDone = false;
                item.RevisionNo = entity.RevisionNo;
                item.IsReadymate = entity.IsReadymate;
                if (entity.IsReadymate == 0)
                {
                    item.Prooce_Cycle_id = entity.Prooce_Cycle_id;
                }
                
                item.SizeId = entity.SizeId;
                item.StyleId = entity.StyleId;
                item.Company_ID = entity.Company_ID;
                item.BranchId = entity.BranchId;
                if (entity.IsReadymate == 0)
                {
                    item.BOM_No = "RM-" + Convert.ToString(getnextbomno());
                }
                else
                {
                    item.BOM_No = "RF-" + Convert.ToString(getnextbomno());
                }
                item.BOM_ID = getnextbomID();
                db.M_BillOfMaterialMaster.Add(item);
                db.SaveChanges();
                id = item.BOM_ID;
                
                if(entity.BOM_ProcessCycle!=null)
                {
                    foreach( var data in entity.BOM_ProcessCycle)
                    {
                        M_BOM_ProcessCycle PCdata = new M_BOM_ProcessCycle();
                        PCdata.BOM_ID = id;
                        PCdata.ProcessId = data.ProcessId;
                        PCdata.Processsequence = data.Processsequence;
                        PCdata.ItemOutId = data.ItemOutId;
                        PCdata.Item_Attribute_ID = data.Item_Attribute_ID;
                        PCdata.PerQty = data.PerQty;
                        PCdata.RequiredQty = data.RequiredQty;
                        PCdata.IsActive =true;
                        PCdata.CreatedDate = DateTime.Now;
                        PCdata.CreatedBy = data.CreatedBy;
                        PCdata.Company_ID = data.Company_ID;
                        PCdata.BranchId = data.BranchId;
                        db.M_BOM_ProcessCycle.Add(PCdata);
                        db.SaveChanges();
                    }
                }

                if (entity.styleimglist != null && entity.styleimglist.Count>0)
                {
                    foreach (var img in entity.styleimglist)
                    {
                        if (img != "")
                        {
                            M_BOMStyleImage imgentity = new M_BOMStyleImage();
                            imgentity.BOM_ID = id;
                            imgentity.BOMimg = img;
                            imgentity.Createdate = DateTime.Now;
                            imgentity.IsActive = true;
                            imgentity.Createdby = entity.CreatedBy;
                            imgentity.Company_ID = entity.Company_ID;
                            imgentity.BranchId = entity.BranchId;
                            db.M_BOMStyleImage.Add(imgentity);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return id;
        }
        public bool Update(M_BillOfMaterialMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == entity.BOM_ID).FirstOrDefault();
                record.IsReadymate = entity.IsReadymate;
                record.BOM_No = entity.BOM_No;
                record.CustomerId = entity.CustomerId;
                record.CustomerAddressId = entity.CustomerAddressId;
                if (entity.IsReadymate == 0)
                {
                    record.Prooce_Cycle_id = entity.Prooce_Cycle_id;
                }
                record.IsActive = entity.IsActive;
                record.RevisionNo = entity.RevisionNo;
                record.StyleId = entity.StyleId;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate = DateTime.Now;
                record.RevisionNo = 1;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                    if (entity.BOM_ProcessCycle != null)
                    {
                        if (entity.BOM_ProcessCycle.Count > 0)
                        {
                            foreach (var data in entity.BOM_ProcessCycle)
                            {
                                var recordPC = db.M_BOM_ProcessCycle.Where(x => x.ID == data.ID).FirstOrDefault();
                                recordPC.ProcessId = data.ProcessId;
                                recordPC.Processsequence = data.Processsequence;
                                recordPC.ItemOutId = data.ItemOutId;
                                recordPC.Item_Attribute_ID = data.Item_Attribute_ID;
                                recordPC.UpdatedDate = DateTime.Now;
                                recordPC.UpdatedBy = data.UpdatedBy;
                                db.SaveChanges();

                            }
                        }
                    }

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool UpdatePRDone(M_BillOfMaterialMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == entity.BOM_ID).FirstOrDefault();
                record.IsPRDone = entity.IsPRDone;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == id).FirstOrDefault();
                contact.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
