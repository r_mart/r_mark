//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_StyleMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public M_StyleMaster()
        {
            this.CRM_CustomerPOStyleDetail = new HashSet<CRM_CustomerPOStyleDetail>();
            this.M_BillOfMaterialMaster = new HashSet<M_BillOfMaterialMaster>();
            this.M_ItemSizeMaster = new HashSet<M_ItemSizeMaster>();
            this.M_SchemeMaster = new HashSet<M_SchemeMaster>();
            this.MFG_ALLOCATION = new HashSet<MFG_ALLOCATION>();
            this.MFG_MaterialIssueMaster = new HashSet<MFG_MaterialIssueMaster>();
            this.MFG_PRODUCTION_DAILY_PLAN_Entry = new HashSet<MFG_PRODUCTION_DAILY_PLAN_Entry>();
            this.MFG_PRODUCTION_DAILY_PLAN = new HashSet<MFG_PRODUCTION_DAILY_PLAN>();
            this.MFG_PRODUCTION_PLAN_DETAILS = new HashSet<MFG_PRODUCTION_PLAN_DETAILS>();
            this.MFG_PRODUCTION_PLAN = new HashSet<MFG_PRODUCTION_PLAN>();
            this.MFG_PRODUCTION_WEEKLY_PLAN_DETAILS = new HashSet<MFG_PRODUCTION_WEEKLY_PLAN_DETAILS>();
            this.MFG_WORK_ORDER = new HashSet<MFG_WORK_ORDER>();
            this.SRM_PurchaseRequestMaster = new HashSet<SRM_PurchaseRequestMaster>();
        }
    
        public long Id { get; set; }
        public string StyleNo { get; set; }
        public string Stylename { get; set; }
        public string StyleShortname { get; set; }
        public string Styledescription { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CRM_CustomerPOStyleDetail> CRM_CustomerPOStyleDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_BillOfMaterialMaster> M_BillOfMaterialMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_ItemSizeMaster> M_ItemSizeMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_SchemeMaster> M_SchemeMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_ALLOCATION> MFG_ALLOCATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_MaterialIssueMaster> MFG_MaterialIssueMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PRODUCTION_DAILY_PLAN_Entry> MFG_PRODUCTION_DAILY_PLAN_Entry { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PRODUCTION_DAILY_PLAN> MFG_PRODUCTION_DAILY_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PRODUCTION_PLAN_DETAILS> MFG_PRODUCTION_PLAN_DETAILS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PRODUCTION_PLAN> MFG_PRODUCTION_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PRODUCTION_WEEKLY_PLAN_DETAILS> MFG_PRODUCTION_WEEKLY_PLAN_DETAILS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_WORK_ORDER> MFG_WORK_ORDER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRM_PurchaseRequestMaster> SRM_PurchaseRequestMaster { get; set; }
    }
}
