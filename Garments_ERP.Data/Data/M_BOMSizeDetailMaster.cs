//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_BOMSizeDetailMaster
    {
        public long Id { get; set; }
        public Nullable<long> BOMD_Id { get; set; }
        public Nullable<long> SizeId { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> Segmenttypeid { get; set; }
        public Nullable<long> Styleid { get; set; }
        public Nullable<long> Itemid { get; set; }
        public string Styleno { get; set; }
        public string Styledesc { get; set; }
        public string Brand { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<decimal> Totalqty { get; set; }
        public Nullable<decimal> SellRate { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
    }
}
