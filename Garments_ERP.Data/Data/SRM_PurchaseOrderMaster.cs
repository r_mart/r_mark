//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SRM_PurchaseOrderMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SRM_PurchaseOrderMaster()
        {
            this.MFG_PO_DeliveryDetail = new HashSet<MFG_PO_DeliveryDetail>();
            this.SRM_GRNInward = new HashSet<SRM_GRNInward>();
            this.SRM_PurchaseOrderItemDetail = new HashSet<SRM_PurchaseOrderItemDetail>();
        }
    
        public long Id { get; set; }
        public string PO_NO { get; set; }
        public Nullable<System.DateTime> PO_Date { get; set; }
        public Nullable<long> QuotationId { get; set; }
        public Nullable<System.DateTime> QuotationDate { get; set; }
        public Nullable<long> SupplierId { get; set; }
        public Nullable<long> ContactPersonId { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Craetedby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public string POType { get; set; }
        public Nullable<decimal> ItemTotal { get; set; }
        public Nullable<decimal> TaxTotal { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public Nullable<System.DateTime> POValidityFrom { get; set; }
        public Nullable<System.DateTime> POValidityTo { get; set; }
        public Nullable<long> DeliveryLoaction { get; set; }
        public Nullable<int> PaymentTerm { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public string Narration { get; set; }
        public Nullable<decimal> totalcost { get; set; }
        public Nullable<int> IsReadymade { get; set; }
    
        public virtual M_PaymentTermMaster M_PaymentTermMaster { get; set; }
        public virtual M_WAREHOUSE M_WAREHOUSE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PO_DeliveryDetail> MFG_PO_DeliveryDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRM_GRNInward> SRM_GRNInward { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRM_PurchaseOrderItemDetail> SRM_PurchaseOrderItemDetail { get; set; }
        public virtual SRM_QuotationMaster SRM_QuotationMaster { get; set; }
    }
}
