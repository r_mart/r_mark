//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SRM_PRItemDetail
    {
        public long Id { get; set; }
        public Nullable<long> PR_ID { get; set; }
        public Nullable<int> ItemSubCategoryId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public string ItemDesc { get; set; }
        public Nullable<System.DateTime> RequiredDate { get; set; }
        public Nullable<int> UnitId { get; set; }
        public string Comment { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Isreadymade { get; set; }
        public string HSNCode { get; set; }
    
        public virtual M_ItemMaster M_ItemMaster { get; set; }
        public virtual M_ItemSubCategoryMaster M_ItemSubCategoryMaster { get; set; }
        public virtual M_UnitMaster M_UnitMaster { get; set; }
        public virtual SRM_PurchaseRequest SRM_PurchaseRequest { get; set; }
    }
}
